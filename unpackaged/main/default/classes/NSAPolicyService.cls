public inherited sharing class NSAPolicyService {
    
    private static Set<String> contactSocietyFieldAPINames;

    /**
     * Gets polices by account ids
     * 
     * @param	accountIds	account ids to get policies for
     * @return	Policies records related to passed account ids
     * */
    public static List<NSA_Policy__c> getPoliciesByAccIds(Set<Id> accountIds) {
        return [SELECT ID,Society_Account__c,Contact_Society_Field_API_Name__c,Advanced_Assignment__c,Territory_Group_Name__c 
                  FROM NSA_Policy__c 
                 WHERE Society_Account__c IN :accountIds 
                   AND Is_Active__c = true
               	   AND Policy_Status__c = 'Approved'];
    }
    
    /**
     * Gets polices by territory group names
     * 
     * @param	territoryGroupNames	territory group Names to get policies for
     * @return	Policies records related to passed territory group names
     * */
    public static List<NSA_Policy__c> getPoliciesByTerritoryGroupNames(Set<String> territoryGroupNames) {
        return [SELECT ID,Society_Account__c,Contact_Society_Field_API_Name__c,
                	   Advanced_Assignment__c,Territory_Group_Name__c,Library_Id__c
                  FROM NSA_Policy__c 
                 WHERE Territory_Group_Name__c IN :territoryGroupNames 
                   AND Is_Active__c = true
                   AND Policy_Status__c = 'Approved'];
    }
    
    /**
     * Groups policy records by account ids
     * 
     * @param	policies	policies to group
     * @return	map society account Id to list policies
     * */
    public static Map<Id, List<NSA_Policy__c>> groupPoliciesByAccountId(List<NSA_Policy__c> policies) {
        Map<Id, List<NSA_Policy__c>> mapAccIdToPolicies = new Map<ID, List<NSA_policy__c>>();
        if(policies != null && !policies.isEmpty()) {
            for(NSA_Policy__c policy: policies) {
                if(String.isNotBlank(policy.Society_Account__c)) {
                    List<NSA_Policy__c> policiesList = mapAccIdToPolicies.get(policy.Society_Account__c);
                    if(policiesList == null) {
                        policiesList = new List<NSA_Policy__c>();
                    }
                    policiesList.add(policy);
                    mapAccIdToPolicies.put(policy.Society_Account__c, policiesList);
                }
            }
        }
        return mapAccIdToPolicies;
    }
    
    /**
     * Groups policy records by territory group Name
     * 
     * @param	policies	policies to group
     * @return	map territory group Name to list policies
     * */
    public static Map<String, List<NSA_Policy__c>> groupPoliciesByTerritoryGroupName(List<NSA_Policy__c> policies) {
        Map<String, List<NSA_Policy__c>> result = new Map<String, List<NSA_policy__c>>();
        if(policies != null && !policies.isEmpty()) {
            for(NSA_Policy__c policy: policies) {
                if(String.isNotBlank(policy.Territory_Group_Name__c)) {
                    List<NSA_Policy__c> policiesList = result.get(policy.Territory_Group_Name__c);
                    if(policiesList == null) {
                        policiesList = new List<NSA_Policy__c>();
                    }
                    policiesList.add(policy);
                    result.put(policy.Territory_Group_Name__c, policiesList);
                }
            }
        }
        return result;
    }
    
    /**
     * Change contact checkboxes based on passed policies
     * 
     * @param	contact		record to apply changes
     * @param	policies	policies to get field names from
     * @param	checkboxVal	value of chekboxes
     * @return	true if at least one field was changed
     * */
    public static Boolean changeContactCFACheckboxes(Contact contact, List<NSA_Policy__c> policies, Boolean checkboxVal) {
        Boolean result = false;
        if(contact != null && policies != null && !policies.isEmpty() && checkboxVal != null) {
            for(NSA_Policy__c policy: policies) {
                String fName = policy.Contact_Society_Field_API_Name__c;
                if(contact.isSet(fName) && contact.get(fName)instanceof Boolean && contact.get(fName) != checkboxVal) {
                    contact.put(fName, checkboxVal);
                    result = true;
                }
            }
        }
        return result;
    }
    
    /**
     * Returns contact cfa society field api names form NSA_Policy__c records
     * 
     * @return	contact society field api names
     * */
    public static Set<String> getContactSocietyFieldAPINamesFromPolicies() {
        if(contactSocietyFieldAPINames == null) {
            contactSocietyFieldAPINames = 
                SObjectService.getFieldValues(
                    [SELECT Contact_Society_Field_API_Name__c 
                       FROM NSA_Policy__c 
                      WHERE Is_Active__c = true
                    	AND Policy_Status__c = 'Approved'], 
                    'Contact_Society_Field_API_Name__c'
                );
        }
        return new Set<String>(contactSocietyFieldAPINames);
    }
}
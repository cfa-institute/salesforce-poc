/* 
        # Created By: Dhana Prasad
        # Created Date: 12/30/2019
        # Description: Test Class for CFA_CaseFileHandler class
        ---------------------------------------------------------------------------------------------------------------
        # Modification inforamtion
        - Modified By                 Modified Date               Description                     Associated Task (If)
        #
        ---------------------------------------------------------------------------------------------------------------
    */
@isTest
public class CFA_CaseFileHandler_Test {
    /* 
        # Created By: Dhana Prasad
        # Created Date: 12/30/2019
        # Description: Method to test getAll_ADA_CaseDocuments method of CFA_CaseFileHandler class
        ---------------------------------------------------------------------------------------------------------------
        # Modification inforamtion
        - Modified By                 Modified Date               Description                     Associated Task (If)
        #
        ---------------------------------------------------------------------------------------------------------------
    */
    @isTest
    static void getAll_ADA_CaseDocumentsTest()
    {
        CaseTriggerHandler.TriggerDisabled = true;
        SocietyOps_Backoffice__c societyOps= new SocietyOps_Backoffice__c();
        societyOps.Name= 'trigger-details';
        societyOps.No_reply_email__c = 'noreply@cfainstitute.org';
        societyOps.CaseComment_TemplateName__c = 'SocietyOps-New Comment Notification';
        societyOps.CaseReassignment_TemplateName__c = 'SocietyOps-Case Reassignment';
        insert societyOps;
        List<Account> accList =  CFA_TestDataFactory.createAccountRecords('TestAccount', null , 1 ,true);
        List<Contact> conList = CFA_TestDataFactory.createContactRecords('CFA Contact', 'TestContact', 1, false);
        conList[0].AccountId = accList[0].Id;
        insert conList;
        List<Case> caseList = CFA_TestDataFactory.createCaseRecords(Label.CFA_ADALabel,1,false);
        caseList[0].ContactId = conList[0].Id;
        insert caseList;
        Id testRecordType = caseList[0].RecordTypeId;
         String recordName = CFA_RecordTypeUtility.getRecordTypeName(Case.getSObjectType(), testRecordType, CFA_DescribeSchemaUtility.getSObjectAPIName('Case'));
        ContentVersion contentVersion = new ContentVersion(
            Title = 'Penguins',
            PathOnClient = 'Penguins.jpg',
            VersionData = Blob.valueOf('Test Content'),
            IsMajorVersion = true
        );
        insert contentVersion;    
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
        
        //create ContentDocumentLink  record 
        ContentDocumentLink cdl = New ContentDocumentLink();
        cdl.LinkedEntityId = caseList[0].id;
        cdl.ContentDocumentId = documents[0].Id;
        cdl.shareType = 'V';
        insert cdl;
        Test.startTest();
        List<CFA_CaseFileHandler.ADA_Documents> adaList = CFA_CaseFileHandler.getAll_ADA_CaseDocuments(caseList[0].id);
        Test.stopTest();
        System.assertEquals(adaList[0].fileId, documents[0].id);
        
    }
    
}
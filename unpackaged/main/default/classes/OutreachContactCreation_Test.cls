/**************************************************************************************************
* Apex Class Name   : OutreachContactCreation_Test
* Purpose           : Test Class for OutreachContact Creation Class
* Version           : 1.0 Initial Version
* Organization      : CFA Institute
* Created Date      : 05-June-2019

***************************************************************************************************/

@isTest
public class OutreachContactCreation_Test{
    static testmethod void addPricebookEntries() {
         
        Account newAccount = CFA_TestDataFactory.createAccount('Test Account', 'University', false);
        newAccount.type = Label.MDM_Default_Account; 
        insert newAccount;

		Contact newOutreachContact = CFA_TestDataFactory.createContact('Outreach Contact', newAccount.Id, 'Outreach Contact', false);
		newOutreachContact.Contact_Data_Source__c = 'Conference';
		newOutreachContact.Phone = '55555555';
		insert newOutreachContact;

		Id OutreachContactRecordType = newOutreachContact.RecordTypeId;

        OutreachContactCreation outReach = new OutreachContactCreation(new ApexPages.StandardController(newOutreachContact));
        outReach.redirectpage();
        outReach.save();
        newOutreachContact.RecordTypeId= null;
        outReach = new OutreachContactCreation(new ApexPages.StandardController(newOutreachContact));
        outReach.redirectpage();
        outReach.save();
        outReach.con = new contact(
			lastname='Test',
			RecordTypeId = OutreachContactRecordType,
			phone = '1234556',
			Contact_Data_Source__c  = 'Conference'
		);
        outReach.aff= new Affiliation__c();
        outReach.save();
        outReach = new OutreachContactCreation(new ApexPages.StandardController(new Contact()));
        outReach.redirectpage();
        outreach.save();
        outReach.aff.recordtypeid = null;
        outReach.aff.Role__c = '';
        outReach.aff.Start_Date__c = null;
        outReach.aff.Active_Affiliation__c = true;
        outReach.aff.Contact__c = null;
        outreach.save();
        
    }
}
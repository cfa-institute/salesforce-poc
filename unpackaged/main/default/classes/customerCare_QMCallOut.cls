/**************************************************************************************************
* Apex Class Name   : customerCare_QMCallOut
* Version           : 1.0 Initial Version
* Organization      : Cognizant
* Created Date      : 11-Dec-2017   
***************************************************************************************************/

public without sharing class customerCare_QMCallOut{ 
    public String currentRecordId {get;set;}
    public String VendorID{get; set;}
    public String PartnerID{get; set;}
    public String EmailID{get;set;} //change by AD for netsuite offline order 5/29/2019
    public String VitalSourceLink {get;set;}
    public String profileAppLink{get; set;}
    public String internalSupportLink{get; set;}
    public String NetSuiteLink{get; set;}
    public String PrivacyAppLink{get;set;}
    public String BasnoAppLink{get;set;}
    public String MembershipAppLink{get;set;}
    public String CFAProgramTile{get;set;} 
    public String RADAdjustments{get;set;} // Change Added by Geetha for Religious alternate date US.
    public String MembershipApplicationReview{get;set;} //change added by AD 11/1 for membership application custom links feature 20750.
    public String NSOfflineOrderlink{get;set;} //Netsuite Offiline Orders feature 34155 change added by AD 5/28
    public Boolean isPaused {get; set;}
    private String userName,playURL,pauseURL,domain;
    public Boolean netsuiteContactFlag{get; set;}
    public Boolean netsuiteContactPartnerFlag{get; set;}
    private string applicationjson='application/json';
    public String Pricing1{get;set;}
    public String Pricing2{get;set;}
    public String Pricing{get;set;}
    public String ChangeTestArea{get;set;}

    
    public customerCare_QMCallOut(ApexPages.StandardController controller) {
        isPaused = true;
        netsuiteContactFlag = netsuiteContactPartnerFlag = false;
        List<Case> caseList = new List<Case>();
        List<User> usr = [SELECT Cisco_User_Name__c,Id FROM User WHERE Id = :UserInfo.getUserId()];
        userName =usr[0].Cisco_User_Name__c;

        List<CustomerCareCiscoPlayPause__c> lstcc = CustomerCareCiscoPlayPause__c.getAll().values();
        if(!lstcc.isEmpty()){
            pauseURL = lstcc[0].Cisco_Pause__c;
            playURL = lstcc[0].Cisco_Play__c;
            domain = lstcc[0].Domain__c;
        }
        currentRecordId  = controller.getId();
        if(!String.isBlank(currentRecordId) && currentRecordId != NULL)
                caseList = [Select Contact.CFAMN__PersonID__c,ID,Contact.Partner_Id__c,ContactId,Contact.Email from Case where id =: currentRecordId];
          
        if(!caseList.isEmpty()){
            profileAppLink = System.Label.CustomerCareProfileAppLink;
            internalSupportLink = System.Label.CustomerCareInternalAppLink;
            PrivacyAppLink = System.Label.CustomerCare_PrivacyAppLink;
            MembershipAppLink = System.Label.MembershipAppLink;
            RADAdjustments = System.Label.RADAdjustments;
            ChangeTestArea = System.Label.ChangeTestArea;
            CFAProgramTile = DynamicEnvVariableService.genericdatafetcher('CFA_Program_Link');
            MembershipApplicationReview = System.Label.Membership_Application_Review; //change added by AD 11/1/2018
			Pricing1 = System.Label.PricingLink1;       
			Pricing2 = System.Label.PricingLink2;       
            VendorID = caseList[0].Contact.CFAMN__PersonID__c;
            PartnerID = caseList[0].Contact.Partner_Id__c;    
            EmailID = caseList[0].Contact.Email; //change by AD for netsuite offline order 5/29/2019
            
            if(!String.isBlank(VendorID)){
                profileAppLink = profileAppLink + VendorID;
                internalSupportLink = internalSupportLink + VendorID;
                PrivacyAppLink = PrivacyAppLink + VendorID;
                MembershipAppLink = MembershipAppLink + VendorID;
                CFAProgramTile = CFAProgramTile + VendorID;
                RADAdjustments = RADAdjustments + PartnerID; // Added by Geetha as per US 28867
                ChangeTestArea = ChangeTestArea + PartnerID;
                Pricing = Pricing1+PartnerID+Pricing2;
                MembershipApplicationReview = MembershipApplicationReview + VendorID; //change added by AD 11/1/2018
                
            }
            if(!String.isBlank(caseList[0].ContactId) && !String.isBlank(PartnerID)){
                netsuiteContactFlag = True;
                netsuiteContactPartnerFlag = True;
                NetSuiteLink=System.Label.NetSuiteLink+PartnerID;
                NSOfflineOrderlink=System.Label.NSOfflineOrderlink+EmailID;//change by AD for netsuite offline order 5/29/2019
                
            }else if(!String.isBlank(caseList[0].ContactId) && String.isBlank(PartnerID)){
                netsuiteContactFlag = True;
                netsuiteContactPartnerFlag = False;
            }else if(String.isBlank(caseList[0].ContactId) && String.isBlank(PartnerID)){
                netsuiteContactFlag = False;
                netsuiteContactPartnerFlag = False;
            }

             
        }
      
        VitalSourceLink =System.Label.VitalSourceLink;
        /* BasnoAppLink */
        BasnoAppLink = System.Label.CustomerCareBasnoAppLink;
  
    }
     
    public void cc_PauseRecording(){

        try{
            JSONGenerator gen = JSON.createGenerator(true);
            if(!Test.isRunningTest()){
                gen.writeStartObject();

                if(!String.isBlank(domain))
                    gen.writeStringField('userdomain', domain);
                if(!String.isBlank(userName))
                    gen.writeStringField('username', userName);
            
                gen.writeEndObject();
            }
            
            Http http = new Http();
            HttpRequest req = new HttpRequest();
            req.setEndpoint(pauseURL);
            req.setHeader('Content-Type', applicationjson);
            req.setHeader('Accept', applicationjson);
            req.setMethod('POST');
            
            req.setBody(gen.getAsString());
            HttpResponse res = http.send(req);
            
            if(res.getStatusCode() == 200)
                    isPaused = false;
            else
                    isPaused = true;
               
            
        }catch(Exception e){
                isPaused = true;
        }
        
    }
    
    public void cc_ResumeRecording(){
        try{
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            gen.writeStringField('userdomain', domain);
            if(!String.isBlank(userName))
                gen.writeStringField('username', userName);
            gen.writeEndObject();
            Http http = new Http();
            HttpRequest req = new HttpRequest();
            req.setEndpoint(playURL);
            req.setHeader('Content-Type', applicationjson);
            req.setHeader('Accept', applicationjson);
            req.setMethod('POST');
            req.setBody(gen.getAsString());
            HttpResponse res = http.send(req);
            if(res.getStatusCode() == 200)
                isPaused = true;
            else
                isPaused  = false;
            }catch(Exception e){
                    isPaused  = false;
            }
        
    }
}
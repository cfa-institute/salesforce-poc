public inherited sharing class TerritoryService {
    
    /**
     * Queries territories selected by name
     * 
     * @param	territoryNames territory names to select territories
     * @return	teritories selected by name
     */
    public static List<Territory2> getTerritoriesByNames(Set<String> territoryNames) {
        return [SELECT ID,Name FROM Territory2 WHERE Name IN :territoryNames];
    }
    
    /**
     * Queries territories by Id
     * 
     * @param	territoriesIds territory Ids to select territories
     * @return	teritories selected by Id
     */
    public static List<Territory2> getTerritoriesByIds(Set<Id> territoriesIds) {
        return [SELECT ID,Name FROM Territory2 WHERE ID IN :territoriesIds];
    }
    
    /**
     * Groups Territory Name to Id
     * 
     * @param	territories	territories to group	
     * @return	teritory name to territory Id Map
     */
    public static Map<String, Id> groupTerritoryNameToId(List<Territory2> territories) {
        Map<String, Id> result = new Map<String, Id>();
        if(territories != null && !territories.isEmpty()) {
            for(Territory2 territory: territories) {
                result.put(territory.Name, territory.Id);
            }    
        }
        return result;
    }
    
    /**
     * Returns territory groups by related ids
     * 
     * @param	relatedIds	related id field values
     * @return	territory groups by related ids
     */
    public static List<Group> getTerritoryGroupsByRelatedIds(Set<Id> relatedIds) {
        return [SELECT ID,NAme,Type,relatedId 
                  FROM Group 
                 WHERE Type = 'Territory' AND relatedId IN :relatedIds];
    }
    
    /**
     * Groups related Id To Territory Id
     * 
     * @param	groupList	LIst of Group records. Should have relatedId and Id
     * @return	map relatedID to group id
     */
    public static Map<Id,Id> groupRelatedIdToGroupId(List<Group> groupList) {
        Map<Id,Id> result = new Map<Id,Id>();
        if(groupList != null && !groupList.isEmpty()) {
            for(Group gr: groupList) {
                result.put(gr.relatedId, gr.Id);
            }
        }
        return result;
    }
    
    /**
     * Share campaign with territories
     * 
     * @param	mapCampaignIdToTerritoryIds	Map Campaign Id to territory Ids to share campaign with
     * @param	campaignAccessLevel			campaign access level for campaign share record
     */
    public static void shareCampaignWithTerritories(Map<Id, Set<Id>> mapCampaignIdToTerritoryIds, String campaignAccessLevel) {
        Set<Id> territoryIds = new Set<Id>();
        for (String recordId : mapCampaignIdToTerritoryIds.keySet()) {
            territoryIds.addAll(mapCampaignIdToTerritoryIds.get(recordId));
        }
        Map<Id,Id> territoryIdToGroupId = groupRelatedIdToGroupId(getTerritoryGroupsByRelatedIds(territoryIds));
        List<CampaignShare> sharingRecordsToCreate = new List<CampaignShare>();
        for (String recordId : mapCampaignIdToTerritoryIds.keySet()) {
            Set<Id> tIds = mapCampaignIdToTerritoryIds.get(recordId);
            for (Id territoryId : tIds) {
                if (territoryIdToGroupId.containsKey(territoryId)) {
                    sharingRecordsToCreate.add(new CampaignShare(campaignId = recordId, CampaignAccessLevel = campaignAccessLevel, UserOrGroupId = territoryIdToGroupId.get(territoryId)));
                }
            }
        }
        if (!sharingRecordsToCreate.isEmpty()) {
            insert sharingRecordsToCreate;
        }
    }

    /**
     * Share Email Send with territories
     *
     * @param	mapEmailSendIdToTerritoryIds	Map Email Send Id to territory Ids
     * @param	accessLevel			            Access level for record sharing
     */
    public static void shareEmailSendWithTerritories(Map<Id, Set<Id>> mapEmailSendIdToTerritoryIds, String accessLevel) {
        Set<Id> territoryIds = new Set<Id>();
        for (String recordId : mapEmailSendIdToTerritoryIds.keySet()) {
            territoryIds.addAll(mapEmailSendIdToTerritoryIds.get(recordId));
        }
        Map<Id,Id> territoryIdToGroupId = groupRelatedIdToGroupId(getTerritoryGroupsByRelatedIds(territoryIds));
        List<et4ae5__SendDefinition__Share> sharingRecordsToCreate = new List<et4ae5__SendDefinition__Share>();
        for (String recordId : mapEmailSendIdToTerritoryIds.keySet()) {
            Set<Id> tIds = mapEmailSendIdToTerritoryIds.get(recordId);
            for (Id territoryId : tIds) {
                if (territoryIdToGroupId.containsKey(territoryId)) {
                    sharingRecordsToCreate.add(
                            new et4ae5__SendDefinition__Share(
                                    ParentId = recordId
                                    , AccessLevel = accessLevel
                                    , UserOrGroupId = territoryIdToGroupId.get(territoryId)
                            )
                    );
                }
            }
        }
        if (!sharingRecordsToCreate.isEmpty()) {
            insert sharingRecordsToCreate;
        }
    }

}
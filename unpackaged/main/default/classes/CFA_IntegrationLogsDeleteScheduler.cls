/*********************************************************************
 * Description - Batch Scheduler class for CFA_IntegrationLogsDeleteBatch
 * Author - Yaswanth
 *********************************************************************/ 
 
 global class CFA_IntegrationLogsDeleteScheduler implements Schedulable {
  global void execute(SchedulableContext SC) {
        CFA_IntegrationLogsDeleteBatch ob = new CFA_IntegrationLogsDeleteBatch(); 
        database.executebatch(ob);
    }
}
@isTest
private class RunPolicyControllerTest {
    
    @isTest
    private static void whenCallRunPolicyThenRunBatch() {
        Account accCfaInstitute = CFA_TestDataFactory.createAccount('CFA Institute', 'Society', true);
        NSA_Policy__c policy = CFA_TestDataFactory.createNsaPolicy(accCfaInstitute.Id, 'CFA_China__c', null, null, null, true);
        Test.startTest();
        RunPolicyController.runPolicy(policy.Id);
        Test.stopTest();
        System.assertEquals(0, [SELECT count() FROM CFA_Integration_Log__c]);
    }

}
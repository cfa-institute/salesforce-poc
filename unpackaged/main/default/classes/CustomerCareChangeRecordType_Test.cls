/**************************************************************************************************
* Apex Class Name   : CustomerCareChangeRecordType_Test
* Purpose           : This  Test class is used for case record type.   
* Version           : 1.0 Initial Version
* Organization      : Cognizant
* Created Date      : 19-Feb-2018  
***************************************************************************************************/

@isTest
public class CustomerCareChangeRecordType_Test{

    static testmethod void cc_ChangeRT(){
        boolean isSB =[SELECT IsSandbox FROM Organization LIMIT 1].IsSandbox;
        User usr = CustomerCare_CommonTestData.testDataUser();
         SocietyOps_Backoffice__c obj = new SocietyOps_Backoffice__c(name='trigger-details',CaseReassignment_TemplateName__c='SocietyOps-Case Reassignment',CaseComment_TemplateName__c='SocietyOps-New Comment Notification');
        if(isSB==true)
        {
            obj.No_reply_email__c='researchchallenge@cfainstitute.org';
         }
        else
        {
            obj.No_reply_email__c='noreply.societyoperations@cfainstitute.org';
         }
        insert obj;
        system.runAs(usr){
            List<String> Testcaselist = new List<string>();
            Case Testcase = CustomerCare_CommonTestData.TestDataEmailCase();
            Testcase.OwnerId=usr.id;
            system.assert(Testcase.OwnerId==usr.id);
            Update Testcase;
            Testcaselist.add(Testcase.id);
            //below code for Fo
            CustomerCareChangeRecordType.validateUser(Testcaselist);
            CustomerCareChangeRecordType.fetchRecordType(Testcaselist);
            CustomerCareChangeRecordType.switchRecordType(Testcaselist);
            //below code to check for bo
            CustomerCareChangeRecordType.validateUser(Testcaselist);
            CustomerCareChangeRecordType.fetchRecordType(Testcaselist);
            CustomerCareChangeRecordType.switchRecordType(Testcaselist);
            Testcaselist.clear();    
            //below code for cfa recordtype
            Id CCRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('CFA Institute Record Type').getRecordTypeId();
            Case emailCase = new Case(Status = 'New', Origin = 'Email',RecordTypeId=CCRecordType,Priority='Medium', Subject = 'TestEmailTOCaseVerificationcfa', Description='Testing Email To case, CFA Institute Membershipcfa 47856985256314');
            insert emailCase;
            Testcaselist.add(emailCase.id);
            system.assert(!Testcaselist.isEmpty());
            CustomerCareChangeRecordType.validateUser(Testcaselist);
            CustomerCareChangeRecordType.fetchRecordType(Testcaselist);
            CustomerCareChangeRecordType.switchRecordType(Testcaselist);
            CustomerCareChangeRecordType.message='testing';
            CustomerCareChangeRecordType.errorMsg='test';
            
        }
    }
    
}
public without sharing class UpdateCaseOwnerToPersonalQueue {
    @InvocableMethod(label='Update Owner To Personal Queue' description='Updates Owner of the Case to the Personal Queue according to CustomerCare Private Queue Assignment custom setting')
    public static void updateOwner(List<Id> caseIds){
        Map<String, String> caseOwnerToQueueId = getCaseOwnerToQueueId();
        if(!caseOwnerToQueueId.isEmpty()) {
            List<Case> casesToUpdate = new List<Case>();
            List<Case> cases = [ SELECT Owner.Alias FROM Case WHERE Id IN :caseIds ];
            
            for(Case c : cases) {
                if(caseOwnerToQueueId.get(c.Owner.Alias) != null) {
                    c.OwnerId = caseOwnerToQueueId.get(c.Owner.Alias);
                    casesToUpdate.add(c);
                }
            }

            if(!casesToUpdate.isEmpty()) {
                update casesToUpdate;
            }
        }
    }

    private static Map<String, String> getCaseOwnerToQueueId() {
        CustomerCare_Private_Queue_Assignment__c settings = CustomerCare_Private_Queue_Assignment__c.getInstance();
        Map<String, String> result = new Map<String, String>();
        Map<String, Schema.SObjectField> fields = Schema.SObjectType.CustomerCare_Private_Queue_Assignment__c.fields.getMap();

        for(String fieldName : fields.keySet()) {
            if(fieldName.containsIgnoreCase('case_owner_')) {
                String fieldIndex = fieldName.toLowerCase().substringBetween('case_owner_', '__c');
                String queueFieldName = 'queue_id_' + fieldIndex + '__c';

                if(settings.get(fieldName) != null && fields.containsKey(queueFieldName)) {
                    result.put((String) settings.get(fieldName), (String) settings.get(queueFieldName));
                }
            }
        }
        return result;
    }
}
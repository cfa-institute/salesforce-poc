/* 
    # Created By: Dhana Prasad
    # Created Date: 05/14/2020
    # Description: To reprocess Cases which are DA Request record type and not in DA switch time frame

    ---------------------------------------------------------------------------------------------------------------
    # Modification inforamtion
    - Modified By                 Modified Date               Description                        Associated Task (If)

    ---------------------------------------------------------------------------------------------------------------
*/

global class CFA_DARequestReprocessingBatch implements Database.Batchable<sObject>,Schedulable {
    
    global Database.QueryLocator start(Database.BatchableContext context) {
        
     Id DAResourceRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('DA Request').getRecordTypeId(); 
     System.debug('recordtypeid: '+ DAResourceRecordTypeId );
        String intStatus = 'Queued';
        String query = 'SELECT Id, CAT_Reprocessing__c FROM CASE WHERE RecordTypeID =\''+ DAResourceRecordTypeId+'\' AND CAT_Reprocessing__c = false AND Integration_Status__c =\''+ intStatus+'\'';
        system.debug('query'+query);
        System.debug('results: ' + Database.getQueryLocator(query));
        return Database.getQueryLocator(query);
    }   
    
    global void execute(Database.BatchableContext context, List<Case> sObjectList) {
      //  system.debug('sObjectList'+sObjectList);
        DA_Event_Swith__mdt timeFrameSwitch;
        timeFrameSwitch = [SELECT Id,DeveloperName,timeFrameActive__c,MasterLabel,NamespacePrefix,Label,QualifiedApiName from  DA_Event_Swith__mdt LIMIT 1];
        if(timeFrameSwitch.timeFrameActive__c ==  true){
             for(Case processCase: sObjectList){
                processCase.CAT_Reprocessing__c= true;
            }
            
        }
         
        update sObjectList;
    }

    global void finish(Database.BatchableContext context) {
       
    }
    global void execute(SchedulableContext SC) {
        Database.executebatch(new CFA_DARequestReprocessingBatch());
    }
    


}
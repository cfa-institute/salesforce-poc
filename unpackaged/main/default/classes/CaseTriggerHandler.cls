public class CaseTriggerHandler implements ITriggerHandler
{
    // Allows unit tests (or other code) to disable this trigger for the transaction
    public static Boolean TriggerDisabled = false;
    /*
Checks to see if the trigger has been disabled either by custom setting or by running code
*/
    public Boolean IsDisabled()
    {
        if (Test.isRunningTest()) {
            return TriggerDisabled;
        } else {
            return DynamicTriggerStatus.getTriggerStatus('CaseTrigger') || triggerDisabled;
        }
    }

    public void BeforeInsert(List<SObject> newItems) {}
    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {}
    public void BeforeDelete(Map<Id, SObject> oldItems) {}
    public void AfterInsert(Map<Id, SObject> newItems) {
        List<Case> caseList = new List<Case>();
        Id recordTypeIdForDARequest = CFA_RecordTypeUtility.getRecordTypeId(Case.getSObjectType(), Label.CFA_ADALabel, CFA_DescribeSchemaUtility.getSObjectAPIName('Case'));
    Id recordTypeIdForCBTDARequest = CFA_RecordTypeUtility.getRecordTypeId(Case.getSObjectType(), 'CBT DA Request' , CFA_DescribeSchemaUtility.getSObjectAPIName('Case'));
         List<Case> contentDocumentCreationCasesList  = new List<Case>();
        for(Case cs:(List<Case>)newItems.Values()){
            if((cs.RecordType.Name =='DA Request' || cs.RecordType.Name == 'CBT DA Request') && (cs.status == 'Review Completed' ||cs.status == 'Withdrawn')){
                caseList.add(cs);
            }
        }
        CaseTriggerHelper.createADAEvent(caseList);
        for(Case cs:(List<Case>)newItems.Values()){
            System.debug('Step 1 Checkin');
            if((cs.RecordTypeId == recordTypeIdForDARequest || cs.RecordTypeId == recordTypeIdForCBTDARequest)){
                contentDocumentCreationCasesList.add(cs);
            }
        }
        if(contentDocumentCreationCasesList.size()!=0)
        CaseTriggerHelper.createContentDocumentLinks(contentDocumentCreationCasesList);
    }
    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        Set<Id> caseIds = new Set<Id>();
        for(Case cs:(List<Case>)newItems.Values()){
            caseIds.add(cs.Id);
        }
        Schema.SObjectType objectType = Case.getSObjectType(); //Note that you can actually call getSObjectType on the type name without instantiating a variable
        List<String> caseFields = new List<string>(objectType.getDescribe().fields.getMap().keySet());
        string query = 'select '+ String.join(caseFields, ',') +',contact.Partner_Id__c,RecordType.Name'+' from Case';
        query += ' WHERE Id IN:caseIds ';
        List<Case> caseList = new List<Case>();
        for(Case cs:Database.query(query)){
            Case oldc = (Case)oldItems.get(cs.Id);
            System.debug('Casetriiger'+cs);
            if((cs.RecordType.Name =='DA Request' || cs.RecordType.Name == 'CBT DA Request') && ((cs.status == 'Review Completed' && oldc.status != 'Review Completed' ) || (cs.status == 'Withdrawn' && oldc.status != 'Withdrawn') || (cs.CAT_Reprocessing__c == true && oldc.CAT_Reprocessing__c != true))){ 
                caseList.add(cs);
            }
        }
        if(!caseList.isEmpty() && RecursiveTriggerHandler.isFirstTime){
            RecursiveTriggerHandler.isFirstTime = false;
            CaseTriggerHelper.createADAEvent(caseList);
        }

    }
    public void AfterDelete(Map<Id, SObject> oldItems) {}
    public void AfterUndelete(Map<Id, SObject> oldItems) {}
}
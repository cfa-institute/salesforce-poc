@isTest
private class MDM_newAffiliationButtonController_Test {

    @testSetup static void setup() {
        ContactTriggerHandler.triggerDisabled = true;

        Account newAccount = CFA_TestDataFactory.createAccount('CFA Society New York', null, true);
        
        MDM_RollupSummary__c setting = new MDM_RollupSummary__c(
			Name='Test cStetting',
			Filter_Field__c = 'CFAMN__CFAMemberCodeDescription__c',
			RollUpSummaryFields__c = 'Affiliate Member / No Society',
			Rollup_to_Field__c ='CFA_Member_Code_Description_Count__c'
		);
        insert setting;

		Contact newCFAContact = CFA_TestDataFactory.createContact('CFA Contact', newAccount.Id, 'CFA Contact', false);
		newCFAContact.Contact_Data_Source__c ='Business Card';
		newCFAContact.Phone = '+1 555 555-55-55';
		newCFAContact.GDPR_Consent__c=true;
		insert newCFAContact;
    }

    private static testMethod void testAccountVF(){
        list<Account> lstAcc = [Select Id, Name From Account limit 1];
        ApexPages.currentPage().getParameters().put('retURL','/'+lstAcc[0].id);
        ApexPages.StandardSetController controller = new ApexPages.StandardSetController(lstAcc);
        MDM_newAffiliationButtonController objSSC = new MDM_newAffiliationButtonController(controller); 
        
        MDM_newAffiliationButtonController objAB = new MDM_newAffiliationButtonController(); 
        PageReference pgref = objAB.newAffiliation();
        system.assert(String.isNotBlank(String.valueOf(pgref)));
    }
    
     private static testMethod void testContactVF(){
        list<Contact> lstCon = [Select Id, Name From Contact limit 1];
        ApexPages.currentPage().getParameters().put('retURL','/'+lstCon [0].id);
        
        MDM_newAffiliationButtonController objAB = new MDM_newAffiliationButtonController(); 
        PageReference pgref = objAB.newAffiliation();
        system.assert(String.isNotBlank(String.valueOf(pgref)));
    }
}
/*****************************************************************
Name: ContactTriggerHelperTest
Copyright © 2021 ITC
============================================================
Purpose: Test for for ContactTriggerHelper
============================================================
History
-------
VERSION  AUTHOR          DATE         DETAIL    Description
1.0      Alona Zubenko   04.02.2021   Created   Logic for Assigning Contacts To Societies By Address
*****************************************************************/

@IsTest
private class ContactTriggerHelperTest {
    private static String COUNTRY_ISO_CODE_CHINA = 'CHN';
    private static String COUNTRY_ISO_CODE_GERMANY = 'DEU';
    private static String COUNTRY_ISO_CODE_UKRAINE = 'UKR';
    private static String COUNTRY_ISO_CODE_BELGIUM = 'BEL';
    private static String STATE_CODE_1 = 'CA-AB';
    private static String STATE_CODE_2 = 'CA-BC';
    private static String PROFILE_INTEGRATION_USER = 'Integration profile';
    private static String PERMISSION_SET_EDIT_ACCOUNT_NAME = 'Edit_Account_Name';
    private static Integer NUMBER_OF_CONTACTS = 10;
    
    /**
        Geographic Rules for testing:

        Country - State - Low Zip Code - High Zip Code

        China - STATE_CODE_1 - 0000 - 0099
        China - STATE_CODE_2 - 10000 - 12000

        Germany - STATE_CODE_2 - 13000 - 13099
        Germany - null - 14100 - 15000

        Ukraine - STATE_CODE_1 - 15100 - 15199
        Ukraine - STATE_CODE_2 - null - null

        Belgium - STATE_CODE_1 - 70000 - 70099
        Belgium - null - null - null

     */
    @TestSetup
    private static void init() {
        Account accCfaInstitute = CFA_TestDataFactory.createAccount('CFA Institute', 'Society', true);

        Map<String, Account> accountByNameMap = new Map<String, Account>{
                'CFA China' => CFA_TestDataFactory.createAccount('CFA China', 'Society', false)
                , 'CFA Germany' => CFA_TestDataFactory.createAccount('CFA Germany', 'Society', false)
                , 'CFA Ukraine' => CFA_TestDataFactory.createAccount('CFA Ukraine', 'Society', false)
                , 'CFA Belgium' => CFA_TestDataFactory.createAccount('CFA Belgium', 'Society', false)
        };
        insert accountByNameMap.values();

        Map<String, NSA_Policy__c> policyByNameMap = new Map<String, NSA_Policy__c>{
                'NSA China' => CFA_TestDataFactory.createNsaPolicy(accountByNameMap.get('CFA China').Id, 'CFA_China__c', null, null, null, false)
                , 'NSA Germany' => CFA_TestDataFactory.createNsaPolicy(accountByNameMap.get('CFA Germany').Id, 'CFA_Society_Germany__c', null, null, null, false)
                , 'NSA Ukraine' => CFA_TestDataFactory.createNsaPolicy(accountByNameMap.get('CFA Ukraine').Id, 'CFA_Society_Ukraine__c', null, null, null, false)
                , 'NSA Belgium' => CFA_TestDataFactory.createNsaPolicy(accountByNameMap.get('CFA Belgium').Id, 'CFA_Society_Belgium__c', null, null, null, false)
        };
        insert policyByNameMap.values();

        List<NSA_Geographic_Rule__c> geographicRules = new List<NSA_Geographic_Rule__c>();

        //China
        Integer i = 0;
        while (i < 100) {
            String highZipCode = (i < 10) ? '000' + 1 : '00' + i;
            geographicRules.add(CFA_TestDataFactory.createNsaGeographicRule(policyByNameMap.get('NSA China').Id, COUNTRY_ISO_CODE_CHINA, STATE_CODE_1, '0000', highZipCode, false));
            i++;
        }
        geographicRules.add(CFA_TestDataFactory.createNsaGeographicRule(policyByNameMap.get('NSA China').Id, COUNTRY_ISO_CODE_CHINA, STATE_CODE_2, '10000', '12000', false));

        //Germany
        i = 0;
        while (i < 100) {
            String highZipCode = (i < 10) ? '1400' + 1 : '140' + i;
            geographicRules.add(CFA_TestDataFactory.createNsaGeographicRule(policyByNameMap.get('NSA Germany').Id, COUNTRY_ISO_CODE_GERMANY, STATE_CODE_1, '13000', highZipCode, false));
            i++;
        }
        geographicRules.add(CFA_TestDataFactory.createNsaGeographicRule(policyByNameMap.get('NSA Germany').Id, COUNTRY_ISO_CODE_GERMANY, null, '14100', '15000', false));

        //Ukraine
        i = 0;
        while (i < 100) {
            String highZipCode = (i < 10) ? '1510' + 1 : '151' + i;
            geographicRules.add(CFA_TestDataFactory.createNsaGeographicRule(policyByNameMap.get('NSA Ukraine').Id, COUNTRY_ISO_CODE_UKRAINE, STATE_CODE_1, '15100', '16000', false));
            i++;
        }
        geographicRules.add(CFA_TestDataFactory.createNsaGeographicRule(policyByNameMap.get('NSA Ukraine').Id, COUNTRY_ISO_CODE_UKRAINE, STATE_CODE_2, null, null, false));

        //Belgium
        i = 0;
        while (i < 100) {
            String highZipCode = (i < 10) ? '7000' + 1 : '700' + i;
            geographicRules.add(CFA_TestDataFactory.createNsaGeographicRule(policyByNameMap.get('NSA Belgium').Id, COUNTRY_ISO_CODE_BELGIUM, STATE_CODE_1, '70000', null, false));
            i++;
        }
        geographicRules.add(CFA_TestDataFactory.createNsaGeographicRule(policyByNameMap.get('NSA Belgium').Id, COUNTRY_ISO_CODE_BELGIUM, null, null, null, false));

        insert geographicRules;
    }

    @IsTest
    static void assignContactsToSocietiesByAddressOnInsertTest() {
        System.runAs(CFA_TestDataFactory.getUserWithPermission(PROFILE_INTEGRATION_USER,PERMISSION_SET_EDIT_ACCOUNT_NAME)) {
            List<Contact> contacts = CFA_TestDataFactory.createContactRecords('CFA Contact', 'TestSet', NUMBER_OF_CONTACTS * 4, false);
            Integer i = 1;
            for (Contact con : contacts) {
                if (i <= NUMBER_OF_CONTACTS) {
                    con.Country_ISO_Code__c = COUNTRY_ISO_CODE_CHINA;
                    con.State_Province_ISO_Code__c  = STATE_CODE_1;
                    con.MailingPostalCode = '0000';
                } else if (i > NUMBER_OF_CONTACTS && i <= NUMBER_OF_CONTACTS * 2) {
                    con.Country_ISO_Code__c = COUNTRY_ISO_CODE_BELGIUM;
                    con.State_Province_ISO_Code__c  = STATE_CODE_1;
                    con.MailingPostalCode = '70001';
                } else if (i > NUMBER_OF_CONTACTS * 2 && i <= NUMBER_OF_CONTACTS * 3) {
                    con.Country_ISO_Code__c = COUNTRY_ISO_CODE_GERMANY;
                    con.State_Province_ISO_Code__c  = STATE_CODE_1;
                    con.MailingPostalCode = '13001';
                } else if (i > NUMBER_OF_CONTACTS * 3 && i <= NUMBER_OF_CONTACTS * 4) {
                    con.Country_ISO_Code__c = COUNTRY_ISO_CODE_UKRAINE;
                    con.State_Province_ISO_Code__c  = STATE_CODE_1;
                    con.MailingPostalCode = '15101';
                }
                i++;
            }

            Test.startTest();
            insert contacts;
            Test.stopTest();
        }

        Integer numberOfCfaChinaAccount = [SELECT COUNT() FROM Contact WHERE Account.Name = 'CFA China'];
        Integer numberOfCfaGermanyAccount = [SELECT COUNT() FROM Contact WHERE Account.Name = 'CFA Germany'];
        Integer numberOfCfaUkraineAccount = [SELECT COUNT() FROM Contact WHERE Account.Name = 'CFA Ukraine'];
        Integer numberOfCfaBelgiumAccount = [SELECT COUNT() FROM Contact WHERE Account.Name = 'CFA Belgium'];

        System.assertEquals(NUMBER_OF_CONTACTS, numberOfCfaChinaAccount);
        System.assertEquals(NUMBER_OF_CONTACTS, numberOfCfaGermanyAccount);
        System.assertEquals(NUMBER_OF_CONTACTS, numberOfCfaUkraineAccount);
        System.assertEquals(NUMBER_OF_CONTACTS, numberOfCfaBelgiumAccount);
    }

    @IsTest
    static void assignContactsToSocietiesByAddressOnUpdateTest() {
        System.runAs(CFA_TestDataFactory.getUserWithPermission(PROFILE_INTEGRATION_USER,PERMISSION_SET_EDIT_ACCOUNT_NAME)) {
            List<Contact> contacts = CFA_TestDataFactory.createContactRecords('CFA Contact', 'Test', NUMBER_OF_CONTACTS, false);
            insert contacts;

            for (Contact con : contacts) {
                con.Country_ISO_Code__c = COUNTRY_ISO_CODE_BELGIUM;
                con.State_Province_ISO_Code__c  = STATE_CODE_1;
                con.MailingPostalCode = '70001';
            }

            Test.startTest();
            update contacts;
            Test.stopTest();
        }

        Integer numberOfCfaBelgiumAccount = [SELECT COUNT() FROM Contact WHERE Account.Name = 'CFA Belgium'];
        System.assertEquals(NUMBER_OF_CONTACTS, numberOfCfaBelgiumAccount);
    }

    private static testMethod void doContactCventCallTest() {
		Account newAccount = CFA_TestDataFactory.createAccount('CFA Society New York', null, false);
        newAccount.cvent_user__c = true;
        insert newAccount;

		Contact newContact = CFA_TestDataFactory.createContact('TestContact', newAccount.Id, 'CFA Contact', false);
		newContact.Partner_Id__c = cfa_ApexUtilities.generateRandomString(5);
		insert newContact;


        Test.startTest();

        contact testContact = [Select Id, LastName, OwnerID From Contact where ID = : newContact.Id ];
        testContact.FirstName = 'Garmella';

        System.runAs(CFA_TestDataFactory.getUserWithPermission('System Administrator', 'Edit_Account_Name')){
			update testContact;
        }

        Map<Id, Contact> contactMap = new Map<Id, Contact>();
        contactMap.put(testContact.Id, testContact);
        ContactTriggerHelper.contactCventCall(new List<Contact> {newContact}, contactMap);

        Test.stopTest();

        system.assertEquals(testContact.Id, newContact.Id);

    }
    private static testMethod void donegativeTest() {

        Test.startTest();
        ContactTriggerHelper.contactCventCall(null, null);        
        Test.stopTest();
    }
    
    @isTest
    private static void whenLocalContactIsCreatedThenCheckOnlyAccCFASocietyField() {
        Account acc = [SELECT ID FROM Account WHERE Name = 'CFA China' LIMIT 1];
        String contactName = 'Test Local Contact';
        Contact contact = CFA_TestDataFactory.createContact(contactName,acc.Id,'Local Member', false);
        contact.CFA_China__c = false;
        contact.CFA_Society_Germany__c = true;
        Test.startTest();
        insert contact;
        Test.stopTest();
        Contact result = [SELECT CFA_China__c,CFA_Society_Germany__c FROM Contact WHERE ID =:contact.Id];
        System.assert(result.CFA_China__c);
        System.assert(!result.CFA_Society_Germany__c);
    }

    @isTest
    private static void whenContactIsCreatedThenCopyContactFields() {
        List<Contact> contacts = CFA_TestDataFactory.createContactRecords('CFA Contact', 'ContactRecord', 200, false);
        updateContactFields(contacts);
        
        Test.startTest();
        insert contacts;
        Test.stopTest();
        List<Contact> result = 
            [SELECT Id,CFAMN__PersonID__c , Person_Id__c,DonorApi__Suffix__c , Suffix,CFAMN__Gender__c , 
                    Gender__c,CFAMN__Employer__c , Employer_Name__c,CFAMN__CFAClassCodeList__c , 
                    CFA_Class_Code__c, CFAMN__CFACandidateCode__c , CFA_Candidate_Code__c,
                    CFAMN__CFAPREP__c , CFA_Program_Prep_Allow_Contact__c, CFAMN__CFAMemberCode__c , 
            		CFA_Member_Code__c, CFAMN__CharterAwardDate__c , CFA_Charter_Award_Date__c,
                    CFAMN__NMAIL__c , HasOptedOutOfMail__c,CFAMN__NEML__c , HasOptedOutOfEmail,
                    CFAMN__CLASOC__c , Investment_Foundations_Candidate_SOC__c,CFAMN__ClaritasCertificateAwardDate__c , 
                    Investment_Foundations_Certificate_Award__c, CFAMN__ClaritasCandidateCode__c , 
                    Investment_Foundations_Candidate_Code__c,CFAMN__CIPMMemberCode__c , CIPM_Member_Code__c,
                    CFAMN__CIPMCandidateCode__c , CIPM_Candidate_Code__c, CFAMN__CIPMCertificateDate__c, 
             		CIPM_Certificate_Award_Date__c, CFA__c , Is_CFA_Charter_Holder__c,CIPM__c , 
                    Is_CIPM_Certificate_Holder__c
               FROM Contact WHERE ID IN :contacts];
        for(Contact contact: result) {
            System.assertNotEquals(null, contact.CFAMN__PersonID__c);
            System.assertNotEquals(null, contact.Person_Id__c);
            System.assertNotEquals(null, contact.DonorApi__Suffix__c);
            System.assertNotEquals(null, contact.Suffix);
            System.assertNotEquals(null, contact.CFAMN__Gender__c);
            System.assertNotEquals(null, contact.Gender__c);
            System.assertNotEquals(null, contact.CFAMN__Employer__c);
            System.assertNotEquals(null, contact.Employer_Name__c);
            System.assertNotEquals(null, contact.CFAMN__CFAClassCodeList__c);
            System.assertNotEquals(null, contact.CFA_Class_Code__c);
            System.assertNotEquals(null, contact.CFAMN__CFACandidateCode__c);
            System.assertNotEquals(null, contact.CFA_Candidate_Code__c);
            System.assertNotEquals(null, contact.CFAMN__CFAPREP__c);
            System.assertNotEquals(null, contact.CFA_Program_Prep_Allow_Contact__c);
            System.assertNotEquals(null, contact.CFAMN__CFAMemberCode__c);
            System.assertNotEquals(null, contact.CFA_Member_Code__c);
            System.assertNotEquals(null, contact.CFAMN__CharterAwardDate__c);
            System.assertNotEquals(null, contact.CFA_Charter_Award_Date__c.dateGMT());
            System.assertNotEquals(null, contact.CFAMN__NMAIL__c);
            System.assertNotEquals(null, contact.HasOptedOutOfMail__c);
            System.assertNotEquals(null, contact.CFAMN__NEML__c); 
            System.assertNotEquals(null, contact.HasOptedOutOfEmail);
            System.assertNotEquals(null, contact.CFAMN__CLASOC__c);
            System.assertNotEquals(null, contact.Investment_Foundations_Candidate_SOC__c);
            System.assertNotEquals(null, contact.CFAMN__ClaritasCertificateAwardDate__c);
            System.assertNotEquals(null, contact.Investment_Foundations_Certificate_Award__c.dateGMT());
            System.assertNotEquals(null, contact.CFAMN__ClaritasCandidateCode__c);
            System.assertNotEquals(null, contact.Investment_Foundations_Candidate_Code__c);
            System.assertNotEquals(null, contact.CFAMN__CIPMMemberCode__c);
            System.assertNotEquals(null, contact.CIPM_Member_Code__c);
            System.assertNotEquals(null, contact.CFAMN__CIPMCandidateCode__c);
            System.assertNotEquals(null, contact.CIPM_Candidate_Code__c);
            System.assertNotEquals(null, contact.CFAMN__CIPMCertificateDate__c);
            System.assertNotEquals(null, contact.CIPM_Certificate_Award_Date__c.dateGMT());
            System.assertNotEquals(null, contact.CFA__c);
            System.assertNotEquals(null, contact.Is_CFA_Charter_Holder__c);
            System.assertNotEquals(null, contact.CIPM__c);
            System.assertNotEquals(null, contact.Is_CIPM_Certificate_Holder__c);

            System.assertEquals(contact.CFAMN__PersonID__c, contact.Person_Id__c);
            System.assertEquals(contact.DonorApi__Suffix__c, contact.Suffix);
            System.assertEquals(contact.CFAMN__Gender__c, contact.Gender__c);
            System.assertEquals(contact.CFAMN__Employer__c, contact.Employer_Name__c);
            System.assertEquals(contact.CFAMN__CFAClassCodeList__c, contact.CFA_Class_Code__c);
            System.assertEquals(contact.CFAMN__CFACandidateCode__c, contact.CFA_Candidate_Code__c);
            System.assertEquals(contact.CFAMN__CFAPREP__c, contact.CFA_Program_Prep_Allow_Contact__c);
            System.assertEquals(contact.CFAMN__CFAMemberCode__c, contact.CFA_Member_Code__c);
            System.assertEquals(contact.CFAMN__CharterAwardDate__c, contact.CFA_Charter_Award_Date__c.dateGMT());
            System.assertEquals(contact.CFAMN__NMAIL__c, contact.HasOptedOutOfMail__c);
            System.assertEquals(contact.CFAMN__NEML__c, contact.HasOptedOutOfEmail);
            System.assertEquals(contact.CFAMN__CLASOC__c, contact.Investment_Foundations_Candidate_SOC__c);
            System.assertEquals(contact.CFAMN__ClaritasCertificateAwardDate__c, contact.Investment_Foundations_Certificate_Award__c.dateGMT());
            System.assertEquals(contact.CFAMN__ClaritasCandidateCode__c, contact.Investment_Foundations_Candidate_Code__c);
            System.assertEquals(contact.CFAMN__CIPMMemberCode__c, contact.CIPM_Member_Code__c);
            System.assertEquals(contact.CFAMN__CIPMCandidateCode__c, contact.CIPM_Candidate_Code__c);
            System.assertEquals(contact.CFAMN__CIPMCertificateDate__c, contact.CIPM_Certificate_Award_Date__c.dateGMT());
            System.assertEquals(contact.CFA__c, contact.Is_CFA_Charter_Holder__c);
            System.assertEquals(contact.CIPM__c, contact.Is_CIPM_Certificate_Holder__c);
        }
    }

    @isTest
    private static void whenContactIsUpdatedThenCopyContactFields() {
        List<Contact> contacts = CFA_TestDataFactory.createContactRecords('CFA Contact', 'ContactRecord', 200, true);
        Integer personId = 10000;
        updateContactFields(contacts);
        
        System.runAs(CFA_TestDataFactory.getUserWithPermission(PROFILE_INTEGRATION_USER,PERMISSION_SET_EDIT_ACCOUNT_NAME)) {
            Test.startTest();
            update contacts;
            Test.stopTest();
            List<Contact> result = 
                [SELECT Id,CFAMN__PersonID__c , Person_Id__c,DonorApi__Suffix__c , Suffix,CFAMN__Gender__c , 
                 Gender__c,CFAMN__Employer__c , Employer_Name__c,CFAMN__CFAClassCodeList__c , 
                 CFA_Class_Code__c, CFAMN__CFACandidateCode__c , CFA_Candidate_Code__c,
                 CFAMN__CFAPREP__c , CFA_Program_Prep_Allow_Contact__c, CFAMN__CFAMemberCode__c , 
                 CFA_Member_Code__c, CFAMN__CharterAwardDate__c , CFA_Charter_Award_Date__c,
                 CFAMN__NMAIL__c , HasOptedOutOfMail__c,CFAMN__NEML__c , HasOptedOutOfEmail,
                 CFAMN__CLASOC__c , Investment_Foundations_Candidate_SOC__c,CFAMN__ClaritasCertificateAwardDate__c , 
                 Investment_Foundations_Certificate_Award__c, CFAMN__ClaritasCandidateCode__c , 
                 Investment_Foundations_Candidate_Code__c,CFAMN__CIPMMemberCode__c , CIPM_Member_Code__c,
                 CFAMN__CIPMCandidateCode__c , CIPM_Candidate_Code__c, CFAMN__CIPMCertificateDate__c, 
                 CIPM_Certificate_Award_Date__c, CFA__c , Is_CFA_Charter_Holder__c,CIPM__c , 
                 Is_CIPM_Certificate_Holder__c
                 FROM Contact WHERE ID IN :contacts];
            for(Contact contact: result) {
                System.assertNotEquals(null, contact.CFAMN__PersonID__c);
                System.assertNotEquals(null, contact.Person_Id__c);
                System.assertNotEquals(null, contact.DonorApi__Suffix__c);
                System.assertNotEquals(null, contact.Suffix);
                System.assertNotEquals(null, contact.CFAMN__Gender__c);
                System.assertNotEquals(null, contact.Gender__c);
                System.assertNotEquals(null, contact.CFAMN__Employer__c);
                System.assertNotEquals(null, contact.Employer_Name__c);
                System.assertNotEquals(null, contact.CFAMN__CFAClassCodeList__c);
                System.assertNotEquals(null, contact.CFA_Class_Code__c);
                System.assertNotEquals(null, contact.CFAMN__CFACandidateCode__c);
                System.assertNotEquals(null, contact.CFA_Candidate_Code__c);
                System.assertNotEquals(null, contact.CFAMN__CFAPREP__c);
                System.assertNotEquals(null, contact.CFA_Program_Prep_Allow_Contact__c);
                System.assertNotEquals(null, contact.CFAMN__CFAMemberCode__c);
                System.assertNotEquals(null, contact.CFA_Member_Code__c);
                System.assertNotEquals(null, contact.CFAMN__CharterAwardDate__c);
                System.assertNotEquals(null, contact.CFA_Charter_Award_Date__c.dateGMT());
                System.assertNotEquals(null, contact.CFAMN__NMAIL__c);
                System.assertNotEquals(null, contact.HasOptedOutOfMail__c);
                System.assertNotEquals(null, contact.CFAMN__NEML__c); 
                System.assertNotEquals(null, contact.HasOptedOutOfEmail);
                System.assertNotEquals(null, contact.CFAMN__CLASOC__c);
                System.assertNotEquals(null, contact.Investment_Foundations_Candidate_SOC__c);
                System.assertNotEquals(null, contact.CFAMN__ClaritasCertificateAwardDate__c);
                System.assertNotEquals(null, contact.Investment_Foundations_Certificate_Award__c.dateGMT());
                System.assertNotEquals(null, contact.CFAMN__ClaritasCandidateCode__c);
                System.assertNotEquals(null, contact.Investment_Foundations_Candidate_Code__c);
                System.assertNotEquals(null, contact.CFAMN__CIPMMemberCode__c);
                System.assertNotEquals(null, contact.CIPM_Member_Code__c);
                System.assertNotEquals(null, contact.CFAMN__CIPMCandidateCode__c);
                System.assertNotEquals(null, contact.CIPM_Candidate_Code__c);
                System.assertNotEquals(null, contact.CFAMN__CIPMCertificateDate__c);
                System.assertNotEquals(null, contact.CIPM_Certificate_Award_Date__c.dateGMT());
                System.assertNotEquals(null, contact.CFA__c);
                System.assertNotEquals(null, contact.Is_CFA_Charter_Holder__c);
                System.assertNotEquals(null, contact.CIPM__c);
                System.assertNotEquals(null, contact.Is_CIPM_Certificate_Holder__c);

                System.assertEquals(contact.CFAMN__PersonID__c, contact.Person_Id__c);
                System.assertEquals(contact.DonorApi__Suffix__c, contact.Suffix);
                System.assertEquals(contact.CFAMN__Gender__c, contact.Gender__c);
                System.assertEquals(contact.CFAMN__Employer__c, contact.Employer_Name__c);
                System.assertEquals(contact.CFAMN__CFAClassCodeList__c, contact.CFA_Class_Code__c);
                System.assertEquals(contact.CFAMN__CFACandidateCode__c, contact.CFA_Candidate_Code__c);
                System.assertEquals(contact.CFAMN__CFAPREP__c, contact.CFA_Program_Prep_Allow_Contact__c);
                System.assertEquals(contact.CFAMN__CFAMemberCode__c, contact.CFA_Member_Code__c);
                System.assertEquals(contact.CFAMN__CharterAwardDate__c, contact.CFA_Charter_Award_Date__c.dateGMT());
                System.assertEquals(contact.CFAMN__NMAIL__c, contact.HasOptedOutOfMail__c);
                System.assertEquals(contact.CFAMN__NEML__c, contact.HasOptedOutOfEmail);
                System.assertEquals(contact.CFAMN__CLASOC__c, contact.Investment_Foundations_Candidate_SOC__c);
                System.assertEquals(contact.CFAMN__ClaritasCertificateAwardDate__c, contact.Investment_Foundations_Certificate_Award__c.dateGMT());
                System.assertEquals(contact.CFAMN__ClaritasCandidateCode__c, contact.Investment_Foundations_Candidate_Code__c);
                System.assertEquals(contact.CFAMN__CIPMMemberCode__c, contact.CIPM_Member_Code__c);
                System.assertEquals(contact.CFAMN__CIPMCandidateCode__c, contact.CIPM_Candidate_Code__c);
                System.assertEquals(contact.CFAMN__CIPMCertificateDate__c, contact.CIPM_Certificate_Award_Date__c.dateGMT());
                System.assertEquals(contact.CFA__c, contact.Is_CFA_Charter_Holder__c);
                System.assertEquals(contact.CIPM__c, contact.Is_CIPM_Certificate_Holder__c);
            }
        }
    }
    
    @isTest
    private static void whenContactIsCreatedAndNewContactFieldIsDisabledThenCopyOldContactFields() {
        List<Contact> contacts = CFA_TestDataFactory.createContactRecords('CFA Contact', 'ContactRecord', 200, false);
        updateOldContactFields(contacts);
        
        Test.startTest();
        ContactTriggerHandler.disabledNewContactFieldsLogic = true;
        insert contacts;
        Test.stopTest();
        List<Contact> result = 
            [SELECT Id,CFAMN__PersonID__c , Person_Id__c,DonorApi__Suffix__c , Suffix,CFAMN__Gender__c , 
                    Gender__c,CFAMN__Employer__c , Employer_Name__c,CFAMN__CFAClassCodeList__c , 
                    CFA_Class_Code__c, CFAMN__CFACandidateCode__c , CFA_Candidate_Code__c,
                    CFAMN__CFAPREP__c , CFA_Program_Prep_Allow_Contact__c, CFAMN__CFAMemberCode__c , 
            		CFA_Member_Code__c, CFAMN__CharterAwardDate__c , CFA_Charter_Award_Date__c,
                    CFAMN__NMAIL__c , HasOptedOutOfMail__c,CFAMN__NEML__c , HasOptedOutOfEmail,
                    CFAMN__CLASOC__c , Investment_Foundations_Candidate_SOC__c,CFAMN__ClaritasCertificateAwardDate__c , 
                    Investment_Foundations_Certificate_Award__c, CFAMN__ClaritasCandidateCode__c , 
                    Investment_Foundations_Candidate_Code__c,CFAMN__CIPMMemberCode__c , CIPM_Member_Code__c,
                    CFAMN__CIPMCandidateCode__c , CIPM_Candidate_Code__c, CFAMN__CIPMCertificateDate__c, 
             		CIPM_Certificate_Award_Date__c, CFA__c , Is_CFA_Charter_Holder__c,CIPM__c , 
                    Is_CIPM_Certificate_Holder__c
               FROM Contact WHERE ID IN :contacts];
        for(Contact contact: result) {
            System.assertNotEquals(null, contact.CFAMN__PersonID__c);
            System.assertNotEquals(null, contact.Person_Id__c);
            System.assertNotEquals(null, contact.DonorApi__Suffix__c);
            System.assertNotEquals(null, contact.Suffix);
            System.assertNotEquals(null, contact.CFAMN__Gender__c);
            System.assertNotEquals(null, contact.Gender__c);
            System.assertNotEquals(null, contact.CFAMN__Employer__c);
            System.assertNotEquals(null, contact.Employer_Name__c);
            System.assertNotEquals(null, contact.CFAMN__CFAClassCodeList__c);
            System.assertNotEquals(null, contact.CFA_Class_Code__c);
            System.assertNotEquals(null, contact.CFAMN__CFACandidateCode__c);
            System.assertNotEquals(null, contact.CFA_Candidate_Code__c);
            System.assertNotEquals(null, contact.CFAMN__CFAPREP__c);
            System.assertNotEquals(null, contact.CFA_Program_Prep_Allow_Contact__c);
            System.assertNotEquals(null, contact.CFAMN__CFAMemberCode__c);
            System.assertNotEquals(null, contact.CFA_Member_Code__c);
            System.assertNotEquals(null, contact.CFAMN__CharterAwardDate__c);
            System.assertNotEquals(null, contact.CFA_Charter_Award_Date__c.dateGMT());
            System.assertNotEquals(null, contact.CFAMN__NMAIL__c);
            System.assertNotEquals(null, contact.HasOptedOutOfMail__c);
            System.assertNotEquals(null, contact.CFAMN__NEML__c); 
            System.assertNotEquals(null, contact.HasOptedOutOfEmail);
            System.assertNotEquals(null, contact.CFAMN__CLASOC__c);
            System.assertNotEquals(null, contact.Investment_Foundations_Candidate_SOC__c);
            System.assertNotEquals(null, contact.CFAMN__ClaritasCertificateAwardDate__c);
            System.assertNotEquals(null, contact.Investment_Foundations_Certificate_Award__c.dateGMT());
            System.assertNotEquals(null, contact.CFAMN__ClaritasCandidateCode__c);
            System.assertNotEquals(null, contact.Investment_Foundations_Candidate_Code__c);
            System.assertNotEquals(null, contact.CFAMN__CIPMMemberCode__c);
            System.assertNotEquals(null, contact.CIPM_Member_Code__c);
            System.assertNotEquals(null, contact.CFAMN__CIPMCandidateCode__c);
            System.assertNotEquals(null, contact.CIPM_Candidate_Code__c);
            System.assertNotEquals(null, contact.CFAMN__CIPMCertificateDate__c);
            System.assertNotEquals(null, contact.CIPM_Certificate_Award_Date__c.dateGMT());
            System.assertNotEquals(null, contact.CFA__c);
            System.assertNotEquals(null, contact.Is_CFA_Charter_Holder__c);
            System.assertNotEquals(null, contact.CIPM__c);
            System.assertNotEquals(null, contact.Is_CIPM_Certificate_Holder__c);

            System.assertEquals(contact.CFAMN__PersonID__c, contact.Person_Id__c);
            System.assertEquals(contact.DonorApi__Suffix__c, contact.Suffix);
            System.assertEquals(contact.CFAMN__Gender__c, contact.Gender__c);
            System.assertEquals(contact.CFAMN__Employer__c, contact.Employer_Name__c);
            System.assertEquals(contact.CFAMN__CFAClassCodeList__c, contact.CFA_Class_Code__c);
            System.assertEquals(contact.CFAMN__CFACandidateCode__c, contact.CFA_Candidate_Code__c);
            System.assertEquals(contact.CFAMN__CFAPREP__c, contact.CFA_Program_Prep_Allow_Contact__c);
            System.assertEquals(contact.CFAMN__CFAMemberCode__c, contact.CFA_Member_Code__c);
            System.assertEquals(contact.CFAMN__CharterAwardDate__c, contact.CFA_Charter_Award_Date__c.dateGMT());
            System.assertEquals(contact.CFAMN__NMAIL__c, contact.HasOptedOutOfMail__c);
            System.assertEquals(contact.CFAMN__NEML__c, contact.HasOptedOutOfEmail);
            System.assertEquals(contact.CFAMN__CLASOC__c, contact.Investment_Foundations_Candidate_SOC__c);
            System.assertEquals(contact.CFAMN__ClaritasCertificateAwardDate__c, contact.Investment_Foundations_Certificate_Award__c.dateGMT());
            System.assertEquals(contact.CFAMN__ClaritasCandidateCode__c, contact.Investment_Foundations_Candidate_Code__c);
            System.assertEquals(contact.CFAMN__CIPMMemberCode__c, contact.CIPM_Member_Code__c);
            System.assertEquals(contact.CFAMN__CIPMCandidateCode__c, contact.CIPM_Candidate_Code__c);
            System.assertEquals(contact.CFAMN__CIPMCertificateDate__c, contact.CIPM_Certificate_Award_Date__c.dateGMT());
            System.assertEquals(contact.CFA__c, contact.Is_CFA_Charter_Holder__c);
            System.assertEquals(contact.CIPM__c, contact.Is_CIPM_Certificate_Holder__c);
        }
    }
    
    @isTest
    private static void whenContactIsUpdatedAndNewContactFieldsIsDisabledThenCopyContactFields() {
        List<Contact> contacts = CFA_TestDataFactory.createContactRecords('CFA Contact', 'ContactRecord', 200, true);
        Integer personId = 10000;
        updateOldContactFields(contacts);
        
        System.runAs(CFA_TestDataFactory.getUserWithPermission(PROFILE_INTEGRATION_USER,PERMISSION_SET_EDIT_ACCOUNT_NAME)) {
            Test.startTest();
            ContactTriggerHandler.disabledNewContactFieldsLogic = true;
            update contacts;
            Test.stopTest();
            List<Contact> result = 
                [SELECT Id,CFAMN__PersonID__c , Person_Id__c,DonorApi__Suffix__c , Suffix,CFAMN__Gender__c , 
                 Gender__c,CFAMN__Employer__c , Employer_Name__c,CFAMN__CFAClassCodeList__c , 
                 CFA_Class_Code__c, CFAMN__CFACandidateCode__c , CFA_Candidate_Code__c,
                 CFAMN__CFAPREP__c , CFA_Program_Prep_Allow_Contact__c, CFAMN__CFAMemberCode__c , 
                 CFA_Member_Code__c, CFAMN__CharterAwardDate__c , CFA_Charter_Award_Date__c,
                 CFAMN__NMAIL__c , HasOptedOutOfMail__c,CFAMN__NEML__c , HasOptedOutOfEmail,
                 CFAMN__CLASOC__c , Investment_Foundations_Candidate_SOC__c,CFAMN__ClaritasCertificateAwardDate__c , 
                 Investment_Foundations_Certificate_Award__c, CFAMN__ClaritasCandidateCode__c , 
                 Investment_Foundations_Candidate_Code__c,CFAMN__CIPMMemberCode__c , CIPM_Member_Code__c,
                 CFAMN__CIPMCandidateCode__c , CIPM_Candidate_Code__c, CFAMN__CIPMCertificateDate__c, 
                 CIPM_Certificate_Award_Date__c, CFA__c , Is_CFA_Charter_Holder__c,CIPM__c , 
                 Is_CIPM_Certificate_Holder__c
                 FROM Contact WHERE ID IN :contacts];
            for(Contact contact: result) {
                System.assertEquals(contact.CFAMN__PersonID__c, contact.Person_Id__c);
                System.assertEquals(contact.DonorApi__Suffix__c, contact.Suffix);
                System.assertEquals(contact.CFAMN__Gender__c, contact.Gender__c);
                System.assertEquals(contact.CFAMN__Employer__c, contact.Employer_Name__c);
                System.assertEquals(contact.CFAMN__CFAClassCodeList__c, contact.CFA_Class_Code__c);
                System.assertEquals(contact.CFAMN__CFACandidateCode__c, contact.CFA_Candidate_Code__c);
                System.assertEquals(contact.CFAMN__CFAPREP__c, contact.CFA_Program_Prep_Allow_Contact__c);
                System.assertEquals(contact.CFAMN__CFAMemberCode__c, contact.CFA_Member_Code__c);
                System.assertEquals(contact.CFAMN__CharterAwardDate__c, contact.CFA_Charter_Award_Date__c.dateGMT());
                System.assertEquals(contact.CFAMN__NMAIL__c, contact.HasOptedOutOfMail__c);
                System.assertEquals(contact.CFAMN__NEML__c, contact.HasOptedOutOfEmail);
                System.assertEquals(contact.CFAMN__CLASOC__c, contact.Investment_Foundations_Candidate_SOC__c);
                System.assertEquals(contact.CFAMN__ClaritasCertificateAwardDate__c, contact.Investment_Foundations_Certificate_Award__c.dateGMT());
                System.assertEquals(contact.CFAMN__ClaritasCandidateCode__c, contact.Investment_Foundations_Candidate_Code__c);
                System.assertEquals(contact.CFAMN__CIPMMemberCode__c, contact.CIPM_Member_Code__c);
                System.assertEquals(contact.CFAMN__CIPMCandidateCode__c, contact.CIPM_Candidate_Code__c);
                System.assertEquals(contact.CFAMN__CIPMCertificateDate__c, contact.CIPM_Certificate_Award_Date__c.dateGMT());
                System.assertEquals(contact.CFA__c, contact.Is_CFA_Charter_Holder__c);
                System.assertEquals(contact.CIPM__c, contact.Is_CIPM_Certificate_Holder__c);
            }
        }
    }
    
    @isTest
    private static void whenCFAContactIsCreatedThenSetAccountToCFAAccount() {
        ContactTriggerHandler.disableNSALogic = true;
        ContactTriggerHandler.disabledNewContactFieldsLogic = false;
        Test.startTest();
        List<Contact> contacts = CFA_TestDataFactory.createContactRecords('CFA Contact', 'CFAContactRecord', 200, true);
        Test.stopTest();
        Account cfaInstituteAccount = [SELECT Id FROM Account WHERE Name = 'CFA Institute' LIMIT 1];
        List<Contact> result = [SELECT ID, AccountID FROM Contact WHERE ID IN: contacts];
        for(Contact c: result) {
            System.assertEquals(cfaInstituteAccount.Id, c.AccountId);
        }
    }
    
    @isTest
    private static void whenNotCFAContactIsCreatedThenAccountIdIsNull() {
        Test.startTest();
        List<Contact> contacts = CFA_TestDataFactory.createContactRecords('Local Member', 'LMContactRecord', 200, true);
        Test.stopTest();
        List<Contact> result = [SELECT ID, AccountID FROM Contact WHERE ID IN: contacts];
        for(Contact c: result) {
            System.assertEquals(null, c.AccountId);
        }
    }
    
    @isTest
    private static void handleUndeleteCase() {
        List<Account> accs = [SELECT ID, Cvent_User__c FROM Account WHERE Name = 'CFA China' OR NAme = 'CFA Germany'];
        for(Account acc: accs) {
            acc.Cvent_User__c = true;
        }
        update accs;
        List<Contact> contacts = CFA_TestDataFactory.createContactRecords('Local Member', 'LMContactRecord', 200, false);
        for(Contact contact: contacts) {
            contact.CFA_China__c = true;
            contact.CFA_Society_Germany__c = true;
        }
        insert contacts;
        delete contacts;
        Test.startTest();
        undelete contacts;
        Test.stopTest();
    }
    
    private static void updateOldContactFields(List<Contact> contacts) {
        Integer personId = 10000;
        for(Contact contact: contacts) {
            contact.CFAMN__PersonID__c = String.valueof(personId);
            personId++;
            contact.DonorApi__Suffix__c = 'test suffix';
            contact.CFAMN__Gender__c = 'Male';
            contact.CFAMN__Employer__c = 'Test Name';
            contact.CFAMN__CFAClassCodeList__c = '345';
            contact.CFAMN__CFACandidateCode__c = '1P';
            contact.CFAMN__CFAPREP__c = true;
            contact.CFAMN__CFAMemberCode__c = 'AOA';
            contact.CFAMN__CharterAwardDate__c = Date.today();
            contact.CFAMN__NMAIL__c = true;
            contact.CFAMN__NEML__c = true;
            contact.CFAMN__CLASOC__c = true;
            contact.CFAMN__ClaritasCertificateAwardDate__c = Date.today().addDays(1);
            contact.CFAMN__ClaritasCandidateCode__c = '1PX';
            contact.CFAMN__CIPMMemberCode__c = 'MB';
            contact.CFAMN__CIPMCandidateCode__c = '1C';
            contact.CFAMN__CIPMCertificateDate__c = Date.today().addDays(2);            
            contact.CFA__c = true;
            contact.CIPM__c = true;
        }
    }

    private static void updateContactFields(List<Contact> contacts) {
        Integer personId = 10000;
        for(Contact contact: contacts) {
            contact.Person_Id__c = String.valueof(personId);
            personId++;
            contact.Suffix = 'test suffix';
            contact.Gender__c = 'Male';
            contact.Employer_Name__c = 'Test Name';
            contact.CFA_Class_Code__c = '345';
            contact.CFA_Candidate_Code__c = '1P';
            contact.CFA_Program_Prep_Allow_Contact__c = true;
            contact.CFA_Member_Code__c = 'AOA';
            contact.CFA_Charter_Award_Date__c = DateTime.now();
            contact.HasOptedOutOfMail__c = true;
            contact.HasOptedOutOfEmail = true;
            contact.Investment_Foundations_Candidate_SOC__c = true;
            contact.Investment_Foundations_Certificate_Award__c = dateTime.now().addDays(1);
            contact.Investment_Foundations_Candidate_Code__c = '1PX';
            contact.CIPM_Member_Code__c = 'MB';
            contact.CIPM_Candidate_Code__c = '1C';
            contact.CIPM_Certificate_Award_Date__c = DateTime.now().addDays(2);            
            contact.Is_CFA_Charter_Holder__c = true;
            contact.Is_CIPM_Certificate_Holder__c = true;
        }
    }
}
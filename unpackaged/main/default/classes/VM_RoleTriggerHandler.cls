public class VM_RoleTriggerHandler
{
    public void onBeforeInsert(List<Role__c> roleLst)
    {
    
        Integer c=0;
        List<Id> engLst=new List<Id>();
        for(Role__c r:roleLst)
        {
            engLst.add(r.Engagement_name__c);        
        }
        Map<Id,Engagement__c> engMap=new Map<Id,Engagement__c>([select id,name from Engagement__c where id IN:engLst]);
        for(Role__c r:roleLst)
        {
        if(r.getCloneSourceId()==null) 
             r.name=engMap.get(r.Engagement_name__c).name+' '+r.name; 
        if(r.getCloneSourceId()<>null){
            r.name=engMap.get(r.Engagement_name__c).name+' '+r.name; 
            String id1=String.valueOf(r.getCloneSourceId()).subString(0,15);
            
            System.debug('id#############:'+id1);
            
            Role__c r1=[Select Confidential__c,Engagement_name__c,
            Number_of_positions__c,Name,Position_Title__c,Recruiting_End_Date__c,Recruiting_Start_Date__c,Role__c,
                        Role_Type__c,Start_date__c, VM_Conflict1__c,VM_Conflict_2__c,VM_Conflict_3__c,VM_Conflict_4__c,
                        VM_Conflict_5__c,
Time_commitment__c,Volunteer_Certifications__c,Volunteer_Compet__c,Volunteer_Experience__c,Volunteer_Impact__c,
                        Volunteer_Roles_and_Responsibilities__c,Volunteer_Work_location__c from Role__c where Id =:id1];
            if(r.Confidential__c<>r1.Confidential__c||r.Name<>r1.Name||r.Engagement_name__c<>r1.Engagement_name__c||r.Number_of_positions__c<>r1.Number_of_positions__c||r.Position_Title__c<>r1.Position_Title__c||r.VM_Conflict1__c<>r1.VM_Conflict1__c||r.VM_Conflict_2__c<>r1.VM_Conflict_2__c||r.VM_Conflict_3__c<>r1.VM_Conflict_3__c||r.VM_Conflict_4__c<>r1.VM_Conflict_4__c||r.VM_Conflict_5__c<>r1.VM_Conflict_5__c||r.Recruiting_End_Date__c<>r1.Recruiting_End_Date__c||r.Recruiting_Start_Date__c<>r1.Recruiting_Start_Date__c||r.Role__c<>r1.Role__c||r.Role_Type__c<>r1.Role_Type__c||r.Start_date__c<>r1.Start_date__c||r.Time_commitment__c<>r1.Time_commitment__c||r.Volunteer_Certifications__c<>r1.Volunteer_Certifications__c||r.Volunteer_Compet__c<>r1.Volunteer_Compet__c||r.Volunteer_Experience__c<>r1.Volunteer_Experience__c||r.Volunteer_Impact__c<>r1.Volunteer_Impact__c||r.Volunteer_Roles_and_Responsibilities__c<>r1.Volunteer_Roles_and_Responsibilities__c||r.Volunteer_Work_location__c<>r1.Volunteer_Work_location__c){
                c=1;
                
            }
            if(c==0){
                   r.addError('Please make atleast one change!');                
            }
        }            
    }
            
        
   
        }
    
 
     public void OnBeforeUpdate(List<Role__c> oldRoleLst,List<Role__c> newRoleLst,Map<Id,Role__c> newRoleMap,Map<Id,Role__c> oldRoleMap)
    {
        List<PermissionSetAssignment> permAss = [SELECT AssigneeId,Id,PermissionSetId,PermissionSet.name FROM PermissionSetAssignment where PermissionSet.name='Relationship_Manager'];
        Set<ID> rmuser = new Set<ID>();
        if(!permAss.isEmpty()){
            for(PermissionSetAssignment psaa : permAss){
                  rmuser.add(psaa.AssigneeId);  
            }
        }
        Set<ID> roleSet = new Set<ID>();
        Map<Role__c,String[]> roleErrors = new Map<Role__c,String[]>();
        /*Set<Id> engIdSet=new Set<Id>();
        for(Role__c r:newRoleLst)
        {
            engIdSet.add(r.Engagement_name__c);
        }
        List<Engagement_Volunteer__c> engVolLst=new List<Engagement_Volunteer__c>();
        Map<Id,List<Engagement_Volunteer__c>> engagementMap=new Map<Id,List<Engagement_Volunteer__c>>();
        
        */
       /*Set<Id> roleIdSet=new Set<Id>();
       for(Role__c r:newRoleLst)
       {
           roleIdSet.add(r.Id);
       } 
       List<Engagement_Volunteer__c> engvolLst=new List<Engagement_Volunteer__c>();
       Map<Id,List<Engagement_Volunteer__c>> engVolMap=new Map<Id,List<Engagement_Volunteer__c>>();                     
       engvolLst=[select Id,Contact__c,Engagement__c,Role_del__c,Status__c from Engagement_Volunteer__c where Status__c='On-boarded'];
       for(Engagement_Volunteer__c engvol:engvolLst)
       {
              
       
       }*/
       Set<Id> roleIdSet=new Set<Id>();
       for(Role__c r:newRoleLst)
       {
           roleIdSet.add(r.Id);
       } 
       Map<Id,Role__c> roleMap=new Map<Id,Role__c>([select id,(select Id,Contact__c,Engagement__c,Role_del__c,Status__c from Engagement_Volunteers1_del__r where Status__c='On-boarded') from Role__c where ID IN:roleIdSet]);
       
       for(Role__c r:newRoleLst)
       {
           if((r.Number_of_Positions__c!=null)&&(oldRoleMap.get(r.id).Number_of_Positions__c!=null))
           {
               if(r.Number_of_Positions__c!=oldRoleMap.get(r.id).Number_of_Positions__c)
               {
                   if(r.Number_of_Positions__c<oldRoleMap.get(r.id).Number_of_Positions__c)
                   {
                       r.addError('Please set incrememtal values for Number of Positions');
                   }
                   else
                   {
                       
                       Role__c rol=roleMap.get(r.id);
                       List<Engagement_Volunteer__c> engVolLst=rol.Engagement_Volunteers1_del__r;
                       if(engVolLst.size()>=1)
                       {
                          Integer count=engVolLst.size();
                          r.Available_Positions__c=r.Number_of_Positions__c-count;
                       }
                       if(engVolLst.size()==0)
                       {
                          Integer count=engVolLst.size();
                          r.Available_Positions__c=r.Number_of_Positions__c-0;
                       }
                       
                   }
               }
           }
           if(rmuser.contains(userInfo.getUserID()) && r.Application_Submitted_Count__c >= 1){
                //roleSet.add(r.Id);
                //r.Name.addError('Role Cannot be Edited in case of submitted application'); 
               // r.addError('Role Cannot be Edited in case of submitted application');         
           }
                
       }
           
       
    }
    public void OnAfterUpdate(List<Role__c> roleLst)
    {
           
       
    }
    
    public void OnBeforeDelete(List<Role__c> oldRoleLst,Map<Id,Role__c> oldRoleMap)
    {
        for (Role__c b:oldRoleLst) {
            System.debug('bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb');
            if(b.No_of_Applied_Application__c>0) 
            {
                System.debug('aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa');
                b.addError('Delete Operation can not be performed as an applied Application exists for this Role.');
            }
           }
        
    }

}
public class VM_VolunteerProfile {
 @AuraEnabled
    public static void getcontact(contact contact){
        system.debug('***contact***'+contact);
         update contact;            
    }
    @AuraEnabled
    public static List<String> getContactCompetencies(){
        
        List<String> options = new List<String>();
        Schema.DescribeFieldResult fieldResult = Contact.MDM_Competencies__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry f: ple) {
            options.add(f.getLabel());
        }
        
        return options; 
    }
    @AuraEnabled
    public static List<String> getAvailabilityStatus(){
        List<String> options1 = new List<String>();
        Schema.DescribeFieldResult fieldResult = Contact.MDM_Availability_Status__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry f: ple) {
            options1.add(f.getLabel());
        }
        return options1; 
    }
    
    @AuraEnabled
    public static Contact getdetails(){
        String userId=UserInfo.getUserId();
        User u=[SELECT Id,contactid FROM User WHERE Id =:userId limit 1];        
        Contact contact = [Select ID,Name,Email,CFAMN__Gender__c,MDM_Competencies__c,MDM_Activities_of_Interest__c,Birthdate,CFAMN__Employer__c,MDM_Preferred_Email_for_Volunteering__c,
                           Phone,MDM_Availability_Status__c,
                           MDM_Emergency_Contact__c,MailingAddress,Contact_Status__c from Contact where ID =:u.contactid]; 
        return contact; 
    }
}
@isTest
public class  VM_EngagementVolunteerTrigger_Test {
    
     /*
     public static testMethod void validateOnBeforeUpdate(){
        Engagement_Volunteer__c engg =new Engagement_Volunteer__c();
     }*/
     
     public static testMethod void PrepareEngagementData(){
        Account VM_Account = new Account();
        VM_Account.Name ='Test_VMAcc1';
        VM_Account.Phone = '222568974';
        insert VM_Account;
         
        Contact VM_Contact = new Contact();
        VM_Contact.RecordTypeID = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Local Member').getRecordTypeId();
        VM_Contact.LastName = 'Test_LVM1';
        VM_Contact.FirstName = 'Test_FVM1';
        VM_Contact.AccountId = VM_Account.ID;
        VM_Contact.Email = 'TestVM_test01@test.com';
        VM_Contact.Phone = '0123456789';
        insert VM_Contact;
        
        Engagement__c VM_Engage=new Engagement__c();
         User RMOwner=VM_TestDataFactory.createUser('RMTest5236@gmail.com','RMTest','Relationship Manager Staff','');
        User MDUSer=VM_TestDataFactory.createUser('MDTest5236@gmail.com','MDTest','Outreach Management User','Managing Director');
        User ReportsToUSer=VM_TestDataFactory.createUser('Anne5236@gmail.com','Huff','Outreach Management User','Relationship Manager');
        
        VM_Engage=VM_TestDataFactory.createEngagement('t FY20',RMOwner,MDUSer,ReportsToUSer);
        /*VM_Engage.name='Testt FY20';
        VM_Engage.Engagement_Type__c='Committee';
        VM_Engage.Start_Date__c=Date.today();
        VM_Engage.Expiration_Date__c=date.today().adddays(20);
        VM_Engage.Business_Objective__c='Test_VM';
        //VM_Engage.ByPassEngagementApproval__c=true;
        VM_Engage.Status__c='New';
        VM_Engage.Engagement_External_Id__c='123456';
        VM_Engage.Aligns_to_which_Strategic_Objective__c='Delivery member value';
        VM_Engage.Business_Owner_Cost_Center__c='Test';
        VM_Engage.Estimated_Cost__c=12.345;
        VM_Engage.Approved__c=true;*/
        insert VM_Engage;
        
        Role__c VM_Role=new Role__c();
        VM_Role.Engagement_name__c=VM_Engage.id;
        VM_Role.Position_Title__c='Test_VMRole';
        VM_Role.Role_Type__c='CIPM Committee Member';
        VM_Role.Conflict_filter__c='Writers';
        VM_Role.Number_of_Positions__c=123;
        VM_Role.Start_date__c=Date.today();
        VM_Role.Recruiting_Start_Date__c=Date.today();
        VM_Role.Recruiting_End_Date__c=Date.today().addDays(10);
        VM_Role.PC_Check_required__c=true;
        VM_Role.Confidential__c=true;
        //VM_Role.ByPassApproval__c=true;
        VM_Role.Volunteer_Impact__c='Test';
        VM_Role.Volunteer_Work_location__c='Test';
        VM_Role.Volunteer_Roles_and_Responsibilities__c='Test';
        VM_Role.Volunteer_Experience__c='Test';
        VM_Role.Volunteer_Compet__c='Chief Financial Officer (CFO)';
        VM_Role.Volunteer_Certifications__c='CFA Charterholder';
        VM_Role.Conflicts_Specific_To_The_Role__c='Employment and/or affiliation with prep provider prohibited';
        VM_Role.VMApprovalStatus__c=true;
        insert VM_Role;
        
       Job_Application__c VM_Job = new Job_Application__c();
       VM_Job.RecordTypeID = Schema.SObjectType.Job_Application__c.getRecordTypeInfosByName().get('CIPMCommittee').getRecordTypeId();
       VM_Job.Position__c=VM_Role.id;
       VM_Job.Contact__c=VM_Contact.id;
       VM_Job.Status__c='Applied';
       VM_Job.Alternate_E_mail_Address__c='TestVM_test@test.com';
       VM_Job.Professional_Affiliations__c='Test';
       VM_Job.Volunteered_before__c='Yes';
       VM_Job.If_YES_list_experiences__c='Yes';
       VM_Job.Disciplinary_Action_Imposed__c='Yes';
       VM_Job.Disciplinary_action_Explain__c='Test';
       VM_Job.What_interests_you_about_this_role__c='Test';
       VM_Job.How_can_you_contribute_to_this_role__c='Test';
       VM_Job.Why_are_you_best_for_this_role__c='Test';
       VM_Job.Have_you_presented_on_these_topics__c='Test';
       VM_Job.Do_you_have_employer_support__c='Yes';
       
       VM_Job.Select_all_competencies__c='Central Banking 10 plus years';
       VM_Job.Degree1__c='Test';
       VM_Job.Degree2__c='Test';
       VM_Job.Degree3__c='Test';
       VM_Job.Reference_Name1__c='Test';
       VM_Job.Reference_Name2__c='Test';
       VM_Job.Reference_Name3__c='Test';
       VM_Job.Email1__c='TestVM56@test.com';
       VM_Job.Email2__c='TestVM57@test.com';
       VM_Job.Email3__c='TestVM58@test.com';
       VM_Job.Phone1__c='1234567897';
       VM_Job.Phone2__c='1234567897';
       VM_Job.Phone3__c='1234567897';
       VM_Job.Address1__c='Test';
       VM_Job.Address2__c='Test';
       VM_Job.Address3__c='Test';
       VM_Job.Personal_gain_or_benefit__c='Yes';
       VM_Job.Inappropriate_influence__c='Yes';
       VM_Job.Relationship_to_Prep_Provider__c='Yes';
       VM_Job.Relationship_with_Testing_Organization__c='Yes';
       VM_Job.Relationship_with_Candidate__c='Yes';
       VM_Job.I_have_read_the_disclosed_conflicts__c=true;
       VM_Job.I_Agree_to_the_Conflict_Policy__c=true;
       VM_Job.Other_potential_conflicts__c='Test';
       insert VM_Job;
       
       
       
       Engagement_Volunteer__c engVol=new Engagement_Volunteer__c();
       engVol.Engagement__c=VM_Engage.id;
       engVol.Role_del__c=VM_Role.id;
       engVol.Contact__c=VM_Contact.id;
       engVol.Status__c='On-boarded';
       insert engVol;
       
       /*Engagement_Volunteer__Share eshare = new Engagement_Volunteer__Share();
       eshare.parentid=engVol.id;
       eshare.UserOrGroupId = UserInfo.getUserID();
       eshare.AccessLevel = 'Read';
       insert eshare;*/
       
       
       Test.startTest();
       List<Engagement_Volunteer__c> engageList = [Select ID,Engagement__c,Role_del__c,Contact__c,Status__c from Engagement_Volunteer__c where ID =:engVol.ID];
       if(!engageList.isEmpty())
           update engageList[0];
       Test.stopTest();  
   }
   
    public static testMethod void PrepareOffboardingData(){
        Account VM_Account = new Account();
        VM_Account.Name ='Test_VMAcc1';
        VM_Account.Phone = '222568974';
        insert VM_Account;
         
        Contact VM_Contact = new Contact();
        VM_Contact.RecordTypeID = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Local Member').getRecordTypeId();
        VM_Contact.LastName = 'Test_LVM1';
        VM_Contact.FirstName = 'Test_FVM1';
        VM_Contact.AccountId = VM_Account.ID;
        VM_Contact.Email = 'TestVM_test01@test.com';
        VM_Contact.Phone = '0123456789';
        insert VM_Contact;
        
        Engagement__c VM_Engage=new Engagement__c();
         User RMOwner=VM_TestDataFactory.createUser('RMTest5412@gmail.com','RMTest','Relationship Manager Staff','');
        User MDUSer=VM_TestDataFactory.createUser('MDTest5412@gmail.com','MDTest','Outreach Management User','Managing Director');
        User ReportsToUSer=VM_TestDataFactory.createUser('Anne5412@gmail.com','Huff','Outreach Management User','Relationship Manager');
        
        VM_Engage=VM_TestDataFactory.createEngagement('t FY21',RMOwner,MDUSer,ReportsToUSer);
        /*VM_Engage.name='Testt FY20';
        VM_Engage.Engagement_Type__c='Committee';
        VM_Engage.Start_Date__c=Date.today();
        VM_Engage.Expiration_Date__c=date.today().adddays(20);
        VM_Engage.Business_Objective__c='Test_VM';
        //VM_Engage.ByPassEngagementApproval__c=true;
        VM_Engage.Status__c='New';
        VM_Engage.Engagement_External_Id__c='123456';
        VM_Engage.Aligns_to_which_Strategic_Objective__c='Delivery member value';
        VM_Engage.Business_Owner_Cost_Center__c='Test';
        VM_Engage.Estimated_Cost__c=12.345;
        VM_Engage.Approved__c=true;*/
        insert VM_Engage;
        
        Role__c VM_Role=new Role__c();
        VM_Role.Engagement_name__c=VM_Engage.id;
        VM_Role.Position_Title__c='Test_VMRole';
        VM_Role.Role_Type__c='CIPM Committee Member';
        VM_Role.Conflict_filter__c='Writers';
        VM_Role.Number_of_Positions__c=123;
        VM_Role.Start_date__c=Date.today();
        VM_Role.Recruiting_Start_Date__c=Date.today();
        VM_Role.Recruiting_End_Date__c=Date.today().addDays(10);
        VM_Role.PC_Check_required__c=true;
        VM_Role.Confidential__c=true;
        //VM_Role.ByPassApproval__c=true;
        VM_Role.Volunteer_Impact__c='Test';
        VM_Role.Volunteer_Work_location__c='Test';
        VM_Role.Volunteer_Roles_and_Responsibilities__c='Test';
        VM_Role.Volunteer_Experience__c='Test';
        VM_Role.Volunteer_Compet__c='Chief Financial Officer (CFO)';
        VM_Role.Volunteer_Certifications__c='CFA Charterholder';
        VM_Role.Conflicts_Specific_To_The_Role__c='Employment and/or affiliation with prep provider prohibited';
        VM_Role.VMApprovalStatus__c=true;
        insert VM_Role;
        
       Job_Application__c VM_Job = new Job_Application__c();
       VM_Job.RecordTypeID = Schema.SObjectType.Job_Application__c.getRecordTypeInfosByName().get('CIPMCommittee').getRecordTypeId();
       VM_Job.Position__c=VM_Role.id;
       VM_Job.Contact__c=VM_Contact.id;
       VM_Job.Status__c='Applied';
       VM_Job.Alternate_E_mail_Address__c='TestVM_test@test.com';
       VM_Job.Professional_Affiliations__c='Test';
       VM_Job.Volunteered_before__c='Yes';
       VM_Job.If_YES_list_experiences__c='Yes';
       VM_Job.Disciplinary_Action_Imposed__c='Yes';
       VM_Job.Disciplinary_action_Explain__c='Test';
       VM_Job.What_interests_you_about_this_role__c='Test';
       VM_Job.How_can_you_contribute_to_this_role__c='Test';
       VM_Job.Why_are_you_best_for_this_role__c='Test';
       VM_Job.Have_you_presented_on_these_topics__c='Test';
       VM_Job.Do_you_have_employer_support__c='Yes';
       
       VM_Job.Select_all_competencies__c='Central Banking 10 plus years';
       VM_Job.Degree1__c='Test';
       VM_Job.Degree2__c='Test';
       VM_Job.Degree3__c='Test';
       VM_Job.Reference_Name1__c='Test';
       VM_Job.Reference_Name2__c='Test';
       VM_Job.Reference_Name3__c='Test';
       VM_Job.Email1__c='TestVM56@test.com';
       VM_Job.Email2__c='TestVM57@test.com';
       VM_Job.Email3__c='TestVM58@test.com';
       VM_Job.Phone1__c='1234567897';
       VM_Job.Phone2__c='1234567897';
       VM_Job.Phone3__c='1234567897';
       VM_Job.Address1__c='Test';
       VM_Job.Address2__c='Test';
       VM_Job.Address3__c='Test';
       VM_Job.Personal_gain_or_benefit__c='Yes';
       VM_Job.Inappropriate_influence__c='Yes';
       VM_Job.Relationship_to_Prep_Provider__c='Yes';
       VM_Job.Relationship_with_Testing_Organization__c='Yes';
       VM_Job.Relationship_with_Candidate__c='Yes';
       VM_Job.I_have_read_the_disclosed_conflicts__c=true;
       VM_Job.I_Agree_to_the_Conflict_Policy__c=true;
       VM_Job.Other_potential_conflicts__c='Test';
       insert VM_Job;
       
       Assessment__c VM_Assmnt = new Assessment__c();
       
       VM_Assmnt.recordtypeid= Schema.SObjectType.Assessment__c.getRecordTypeInfosByName().get('Engagement Assessment').getRecordTypeId();
       VM_Assmnt.Engagement__c=VM_Engage.id;
       VM_Assmnt.Date__c=date.parse('11/05/2019');
       VM_Assmnt.Did_we_meet_Business_Objective_del__c='Yes';
       insert VM_Assmnt;
       
       Engagement_Volunteer__c engVol=new Engagement_Volunteer__c();
       engVol.Engagement__c=VM_Engage.id;
       engVol.Role_del__c=VM_Role.id;
       engVol.Contact__c=VM_Contact.id;
       engVol.Status__c='On-boarded';
       //engVol.Reason_for_Off_boarding__c = 'Engagement Closed';
       engVol.Reason_Explanation__c = 'Test_VM';
       insert engVol;
       
       Recognition__c rcVM = new Recognition__c();
       rcVM.Engagement__c = VM_Engage.id;
       rcVM.Engagement_Volunteer__c = engVol.Id;
       rcVM.Recognition_Type__c = 'Thank you Note';
       rcVM.Contact__c = VM_Contact.ID;
       insert rcVM;
       
       Test.startTest();
       List<Engagement_Volunteer__c> engageList = [Select ID,Engagement__c,Role_del__c,Contact__c,Status__c from Engagement_Volunteer__c where ID =:engVol.ID];
       if(!engageList.isEmpty())
           update engageList[0];
       Test.stopTest();  
   }   

}
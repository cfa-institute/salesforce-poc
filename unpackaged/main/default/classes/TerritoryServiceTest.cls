@isTest
private class TerritoryServiceTest {
    
    private static Integer RECORD_NUMBER = 200;
    
    @TestSetup
    private static void testSetup() {
        List<Campaign> campaigns = new List<Campaign>();
        List<et4ae5__SendDefinition__c> emailSends = new List<et4ae5__SendDefinition__c>();

        for(Integer i = 0; i < RECORD_NUMBER ; i ++) {
            campaigns.add(CFA_TestDataFactory.createCampaign('Campaign' + i, false));
            emailSends.add((et4ae5__SendDefinition__c) CFA_TestDataFactory.create(new et4ae5__SendDefinition__c().getSObjectType(),null,null,false ));
        }
        insert campaigns;
        insert emailSends;
    }
    
    @isTest
    private static void whenGetTerritoriesByNamesThenReturnterritories() {
        Test.startTest();
        List<Territory2> result = TerritoryService.getTerritoriesByNames(new Set<String>{'CFA China', 'CFA Oman'});
        Test.stopTest();
        System.assertEquals(2, result.size());
    }
    
    @isTest
    private static void whenGetTerritoriesByIdsThenReturnterritories() {
        List<Territory2> territories = [SELECT Id FROM Territory2 LIMIT 2];
        Test.startTest();
        List<Territory2> result = TerritoryService.getTerritoriesByIds(new Set<Id>{territories[0].Id, territories[1].Id});
        Test.stopTest();
        System.assertEquals(territories.size(), result.size());
    }
    
    @isTest
    private static void whenGroupTerritoryNameToIdThenReturnMap() {
        List<Territory2> territories = new List<Territory2>();
        territories.add(new Territory2(Name = 'Territory1'));
        territories.add(new Territory2(Name = 'Territory2'));
        Test.startTest();
        Map<String, Id> result = TerritoryService.groupTerritoryNameToId(territories);
        Test.stopTest();
        System.assertEquals(2, result.size());
        System.assert(result.containsKey(territories[0].Name));
        System.assert(result.containsKey(territories[1].Name));
    }
    
    @isTest
    private static void whenShareCampaignWithTerritoriesThenCampaignShareShouldBeCreated() {
        List<Campaign> campaigns = [SELECT ID,Name FROM Campaign];
        Territory2 territory = [SELECT ID FROM Territory2 LIMIT 1];
        Group gr = [SELECT ID,Name,Type,relatedId FROM Group WHERE Type = 'Territory' AND relatedId = :territory.ID];
        
        Map<Id, Set<Id>> input = new Map<Id, Set<Id>>();
        for(Campaign campaign: campaigns) {
        	input.put(campaign.Id, new Set<Id>{territory.Id});
        }
        Test.startTest();
        TerritoryService.shareCampaignWithTerritories(input, 'Read');
        Test.stopTest();
        System.assertEquals(campaigns.size(), [SELECT count() FROM CampaignShare WHERE CampaignId IN :campaigns AND UserOrGroupID = :gr.Id]);
    }

    @isTest
    private static void whenShareEmailSendWithTerritoriesThenCampaignShareShouldBeCreated() {
        List<et4ae5__SendDefinition__c> emailSends = [SELECT Id,Name FROM et4ae5__SendDefinition__c];
        Territory2 territory = [SELECT Id FROM Territory2 LIMIT 1];
        Group gr = [SELECT Id,Name,Type,RelatedId FROM Group WHERE Type = 'Territory' AND RelatedId = :territory.Id];

        Map<Id, Set<Id>> input = new Map<Id, Set<Id>>();
        for(et4ae5__SendDefinition__c emailSend: emailSends) {
        	input.put(emailSend.Id, new Set<Id>{territory.Id});
        }

        Test.startTest();
        TerritoryService.shareEmailSendWithTerritories(input, 'Edit');
        Test.stopTest();

        System.assertEquals(emailSends.size(), [SELECT COUNT() FROM et4ae5__SendDefinition__Share WHERE UserOrGroupId = :gr.Id]);
    }

}
/*****************************************************************
Name: NSAAssignmentBatchTest
Copyright © 2021 ITC
============================================================
Purpose:
============================================================
History
-------
VERSION  AUTHOR          DATE         DETAIL    Description
1.0      Alona Zubenko   12.02.2021   Created   CSR:
*****************************************************************/
@IsTest
private with sharing class NSAAssignmentBatchTest {
    private static String COUNTRY_ISO_CODE_CHINA = 'CHN';
    private static String COUNTRY_ISO_CODE_GERMANY = 'DEU';
    private static String COUNTRY_ISO_CODE_UKRAINE = 'UKR';
    private static String COUNTRY_ISO_CODE_BELGIUM = 'BEL';
    private static String STATE_CODE_1 = 'CA-AB';
    private static String STATE_CODE_2 = 'CA-BC';
    private static String PROFILE_INTEGRATION_USER = 'Integration profile';
    private static String PERMISSION_SET_EDIT_ACCOUNT_NAME = 'Edit_Account_Name';
    private static Integer NUMBER_OF_CONTACTS = 50;

    @TestSetup
    private static void init() {
        ContactTriggerHandler.triggerDisabled = true;
        MembershipApplicationTriggerHandler.triggerDisabled = true;

        Account accCfaInstitute = CFA_TestDataFactory.createAccount('CFA Institute', 'Society', true);

        Map<String, Account> accountByNameMap = new Map<String, Account>{
                'CFA China' => CFA_TestDataFactory.createAccount('CFA China', 'Society', false)
                , 'CFA Germany' => CFA_TestDataFactory.createAccount('CFA Germany', 'Society', false)
                , 'CFA Ukraine' => CFA_TestDataFactory.createAccount('CFA Ukraine', 'Society', false)
                , 'CFA Belgium' => CFA_TestDataFactory.createAccount('CFA Belgium', 'Society', false)
        };
        insert accountByNameMap.values();
        
        Map<String, NSA_Policy__c> policyByNameMap = new Map<String, NSA_Policy__c>{
                 'NSA China' => CFA_TestDataFactory.createNsaPolicy(accountByNameMap.get('CFA China').Id, 'CFA_China__c', null, null, null, false)
                , 'NSA Germany' => CFA_TestDataFactory.createNsaPolicy(accountByNameMap.get('CFA Germany').Id, 'CFA_Society_Germany__c', null, null, null, false)
                , 'NSA Ukraine' => CFA_TestDataFactory.createNsaPolicy(accountByNameMap.get('CFA Ukraine').Id, 'CFA_Society_Ukraine__c', null, null, null, false)
                , 'NSA Belgium' => CFA_TestDataFactory.createNsaPolicy(accountByNameMap.get('CFA Belgium').Id, 'CFA_Society_Belgium__c', null, null, null, false)
        };
        insert policyByNameMap.values();
        
        List<NSA_Geographic_Rule__c> geographicRules = new List<NSA_Geographic_Rule__c>();


        //China
        Integer i = 0;
        while (i < 10) {
            String highZipCode = (i < 10) ? '000' + 1 : '00' + i;
            geographicRules.add(CFA_TestDataFactory.createNsaGeographicRule(policyByNameMap.get('NSA China').Id, COUNTRY_ISO_CODE_CHINA, STATE_CODE_1, '0000', highZipCode, false));
            i++;
        }
        geographicRules.add(CFA_TestDataFactory.createNsaGeographicRule(policyByNameMap.get('NSA China').Id, COUNTRY_ISO_CODE_CHINA, STATE_CODE_2, '10000', '12000', false));
		
        //Toronto
        i = 0;
        while (i < 10) {
            String highZipCode = (i < 10) ? '1400' + 1 : '140' + i;
            geographicRules.add(CFA_TestDataFactory.createNsaGeographicRule(policyByNameMap.get('NSA Germany').Id, COUNTRY_ISO_CODE_GERMANY, STATE_CODE_1, '13000', highZipCode, false));
            i++;
        }
        geographicRules.add(CFA_TestDataFactory.createNsaGeographicRule(policyByNameMap.get('NSA Germany').Id, COUNTRY_ISO_CODE_GERMANY, null, '14100', '15000', false));

        //Ukraine
        i = 0;
        while (i < 10) {
            String highZipCode = (i < 10) ? '1510' + 1 : '151' + i;
            geographicRules.add(CFA_TestDataFactory.createNsaGeographicRule(policyByNameMap.get('NSA Ukraine').Id, COUNTRY_ISO_CODE_UKRAINE, STATE_CODE_1, '15100', '16000', false));
            i++;
        }
        geographicRules.add(CFA_TestDataFactory.createNsaGeographicRule(policyByNameMap.get('NSA Ukraine').Id, COUNTRY_ISO_CODE_UKRAINE, STATE_CODE_2, null, null, false));

        //Atlanta
        i = 0;
        while (i < 10) {
            String highZipCode = (i < 10) ? '7000' + 1 : '700' + i;
            geographicRules.add(CFA_TestDataFactory.createNsaGeographicRule(policyByNameMap.get('NSA Belgium').Id, COUNTRY_ISO_CODE_BELGIUM, STATE_CODE_1, '70000', null, false));
            i++;
        }
        geographicRules.add(CFA_TestDataFactory.createNsaGeographicRule(policyByNameMap.get('NSA Belgium').Id, COUNTRY_ISO_CODE_BELGIUM, null, null, null, false));

        insert geographicRules;

        List<Contact> contacts = new List<Contact>();

        List<Contact> chinaContacts = createContacts(COUNTRY_ISO_CODE_CHINA, STATE_CODE_1, '0000', NUMBER_OF_CONTACTS); // address and membership
        List<Contact> germanyContacts = createContacts(COUNTRY_ISO_CODE_GERMANY, STATE_CODE_1, '13001', NUMBER_OF_CONTACTS);// just address
        List<Contact> belgiumContacts = createContacts(null, null, null, NUMBER_OF_CONTACTS);// just membership
        List<Contact> ukraineContacts = createContacts(null, null, null, NUMBER_OF_CONTACTS);// not address and not membership

        contacts.addAll(chinaContacts);
        contacts.addAll(germanyContacts);
        contacts.addAll(belgiumContacts);
        contacts.addAll(ukraineContacts);

        insert contacts;

        List<Membership_Application__c> membershipApplicationChina = CFA_TestDataFactory.createMembershipApplications(accountByNameMap.get('CFA China').Id, chinaContacts, true);
        List<Membership_Application__c> membershipApplicationGermany = CFA_TestDataFactory.createMembershipApplications(accountByNameMap.get('CFA Belgium').Id, germanyContacts, true);
    }

    @IsTest
    private static void runBatch1WithMembershipApplicationPolicyHandlerTest(){
        System.runAs(CFA_TestDataFactory.getUserWithPermission(PROFILE_INTEGRATION_USER,PERMISSION_SET_EDIT_ACCOUNT_NAME)) {
            List<NSA_Policy__c> policies = getAllPolicies();
            List<INSAPolicyHandler> handlers = new List<INSAPolicyHandler>{new MembershipApplicationPolicyHandler()};
                NSAAssignmentBatch batch = new NSAAssignmentBatch(handlers, policies,'',true);
            
            Test.startTest();
            Id batchProcessId = Database.executeBatch(batch);
            Test.stopTest();
        }

        //chinaContacts - address and membership
        //germanyContacts - just address
        //belgiumContacts - just membership
        //ukraineContacts - not address and not membership

        Integer numberOfCfaChinaAccount = [SELECT COUNT() FROM Contact WHERE CFA_China__c = TRUE];
        Integer numberOfCfaGermanyAccount = [SELECT COUNT() FROM Contact WHERE CFA_Society_Germany__c = TRUE];
        Integer numberOfCfaUkraineAccount = [SELECT COUNT() FROM Contact WHERE CFA_Society_Ukraine__c = TRUE];
        Integer numberOfCfaBelgiumAccount = [SELECT COUNT() FROM Contact WHERE CFA_Society_Belgium__c = TRUE];

        System.assertEquals(NUMBER_OF_CONTACTS, numberOfCfaChinaAccount);
        System.assertEquals(NUMBER_OF_CONTACTS, numberOfCfaBelgiumAccount);
        System.assertEquals(0, numberOfCfaGermanyAccount);
        System.assertEquals(0, numberOfCfaUkraineAccount);
    }

    @IsTest
    private static void runBatch1WithMembershipApplicationPolicyHandlerAndAddressPolicyHandlerTest(){
        System.runAs(CFA_TestDataFactory.getUserWithPermission(PROFILE_INTEGRATION_USER,PERMISSION_SET_EDIT_ACCOUNT_NAME)) {
            List<NSA_Policy__c> policies = getAllPolicies();
            List<INSAPolicyHandler> handlers = new List<INSAPolicyHandler>{
                    new MembershipApplicationPolicyHandler(), new AddressPolicyHandler()
            };
            NSAAssignmentBatch batch = new NSAAssignmentBatch(handlers, policies, '', true);

            Test.startTest();
            Id batchProcessId = Database.executeBatch(batch);
            Test.stopTest();
        }

        //chinaContacts - address and membership
        //germanyContacts - just address
        //belgiumContacts - just membership
        //ukraineContacts - not address and not membership

        //MembershipApplicationPolicyHandler
        Integer numberOfCfaChinaContact = [SELECT COUNT() FROM Contact WHERE CFA_China__c = TRUE];
        Integer numberOfCfaGermanyContact = [SELECT COUNT() FROM Contact WHERE CFA_Society_Germany__c = TRUE];
        Integer numberOfCfaUkraineContact = [SELECT COUNT() FROM Contact WHERE CFA_Society_Ukraine__c = TRUE];
        Integer numberOfCfaBelgiumContact = [SELECT COUNT() FROM Contact WHERE CFA_Society_Belgium__c = TRUE];

        System.assertEquals(NUMBER_OF_CONTACTS, numberOfCfaChinaContact);
        System.assertEquals(NUMBER_OF_CONTACTS, numberOfCfaGermanyContact);
        System.assertEquals(NUMBER_OF_CONTACTS, numberOfCfaBelgiumContact);
        System.assertEquals(0, numberOfCfaUkraineContact);

        //AddressPolicyHandler
        Integer numberOfCfaChinaAccount = [SELECT COUNT() FROM Contact WHERE Account.Name = 'CFA China'];
        Integer numberOfCfaGermanyAccount = [SELECT COUNT() FROM Contact WHERE Account.Name = 'CFA Germany'];
        Integer numberOfCfaUkraineAccount = [SELECT COUNT() FROM Contact WHERE Account.Name = 'CFA Ukraine'];
        Integer numberOfCfaBelgiumAccount = [SELECT COUNT() FROM Contact WHERE Account.Name = 'CFA Belgium'];

        System.assertEquals(NUMBER_OF_CONTACTS, numberOfCfaChinaAccount);
        System.assertEquals(NUMBER_OF_CONTACTS, numberOfCfaGermanyAccount);
        System.assertEquals(0, numberOfCfaUkraineAccount);
        System.assertEquals(0, numberOfCfaBelgiumAccount);
    }

    @IsTest
    private static void runBatch1WithAddressPolicyHandlerTest(){
        System.runAs(CFA_TestDataFactory.getUserWithPermission(PROFILE_INTEGRATION_USER,PERMISSION_SET_EDIT_ACCOUNT_NAME)) {
            List<NSA_Policy__c> policies = getAllPolicies();
            List<INSAPolicyHandler> handlers = new List<INSAPolicyHandler>{
                    new AddressPolicyHandler()
            };
            NSAAssignmentBatch batch = new NSAAssignmentBatch(handlers, policies, '', true);

            Test.startTest();
            Id batchProcessId = Database.executeBatch(batch);
            Test.stopTest();
        }

        //chinaContacts - address and membership
        //germanyContacts - just address
        //belgiumContacts - just membership
        //ukraineContacts - not address and not membership

        //MembershipApplicationPolicyHandler
        Integer numberOfCfaChinaContact = [SELECT COUNT() FROM Contact WHERE CFA_China__c = TRUE];
        Integer numberOfCfaGermanyContact = [SELECT COUNT() FROM Contact WHERE CFA_Society_Germany__c = TRUE];
        Integer numberOfCfaUkraineContact = [SELECT COUNT() FROM Contact WHERE CFA_Society_Ukraine__c = TRUE];
        Integer numberOfCfaBelgiumContact = [SELECT COUNT() FROM Contact WHERE CFA_Society_Belgium__c = TRUE];

        System.assertEquals(NUMBER_OF_CONTACTS, numberOfCfaChinaContact);
        System.assertEquals(NUMBER_OF_CONTACTS, numberOfCfaGermanyContact);
        System.assertEquals(0, numberOfCfaBelgiumContact);
        System.assertEquals(0, numberOfCfaUkraineContact);

        //AddressPolicyHandler
        Integer numberOfCfaChinaAccount = [SELECT COUNT() FROM Contact WHERE Account.Name = 'CFA China'];
        Integer numberOfCfaGermanyAccount = [SELECT COUNT() FROM Contact WHERE Account.Name = 'CFA Germany'];
        Integer numberOfCfaUkraineAccount = [SELECT COUNT() FROM Contact WHERE Account.Name = 'CFA Ukraine'];
        Integer numberOfCfaBelgiumAccount = [SELECT COUNT() FROM Contact WHERE Account.Name = 'CFA Belgium'];

        System.assertEquals(NUMBER_OF_CONTACTS, numberOfCfaChinaAccount);
        System.assertEquals(NUMBER_OF_CONTACTS, numberOfCfaGermanyAccount);
        System.assertEquals(0, numberOfCfaUkraineAccount);
        System.assertEquals(0, numberOfCfaBelgiumAccount);
    }

    @IsTest
    private static void runBatch2WithMembershipApplicationPolicyHandlerTest(){
        System.runAs(CFA_TestDataFactory.getUserWithPermission(PROFILE_INTEGRATION_USER,PERMISSION_SET_EDIT_ACCOUNT_NAME)) {
            List<NSA_Policy__c> policies = getAllPolicies();
            List<INSAPolicyHandler> handlers = new List<INSAPolicyHandler>{new MembershipApplicationPolicyHandler()};
                NSAAssignmentBatch batch = new NSAAssignmentBatch(handlers,'',true);

            Test.startTest();
            Id batchProcessId = Database.executeBatch(batch);
            Test.stopTest();
        }
        //chinaContacts - address and membership
        //germanyContacts - just address
        //belgiumContacts - just membership
        //ukraineContacts - not address and not membership

        Integer numberOfCfaChinaAccount = [SELECT COUNT() FROM Contact WHERE CFA_China__c = TRUE];
        Integer numberOfCfaGermanyAccount = [SELECT COUNT() FROM Contact WHERE CFA_Society_Germany__c = TRUE];
        Integer numberOfCfaUkraineAccount = [SELECT COUNT() FROM Contact WHERE CFA_Society_Ukraine__c = TRUE];
        Integer numberOfCfaBelgiumAccount = [SELECT COUNT() FROM Contact WHERE CFA_Society_Belgium__c = TRUE];

        System.assertEquals(NUMBER_OF_CONTACTS, numberOfCfaChinaAccount);
        System.assertEquals(NUMBER_OF_CONTACTS, numberOfCfaBelgiumAccount);
        System.assertEquals(0, numberOfCfaGermanyAccount);
        System.assertEquals(0, numberOfCfaUkraineAccount);
    }

    @IsTest
    private static void runBatch2WithMembershipApplicationPolicyHandlerAndAddressPolicyHandlerTest(){
        System.runAs(CFA_TestDataFactory.getUserWithPermission(PROFILE_INTEGRATION_USER,PERMISSION_SET_EDIT_ACCOUNT_NAME)) {
            List<INSAPolicyHandler> handlers = new List<INSAPolicyHandler>{
                    new MembershipApplicationPolicyHandler(), new AddressPolicyHandler()
            };
            NSAAssignmentBatch batch = new NSAAssignmentBatch(handlers, '', true);

            Test.startTest();
            Id batchProcessId = Database.executeBatch(batch);
            Test.stopTest();
        }


        //chinaContacts - address and membership
        //germanyContacts - just address
        //belgiumContacts - just membership
        //ukraineContacts - not address and not membership

        //MembershipApplicationPolicyHandler
        Integer numberOfCfaChinaContact = [SELECT COUNT() FROM Contact WHERE CFA_China__c = TRUE];
        Integer numberOfCfaGermanyContact = [SELECT COUNT() FROM Contact WHERE CFA_Society_Germany__c = TRUE];
        Integer numberOfCfaUkraineContact = [SELECT COUNT() FROM Contact WHERE CFA_Society_Ukraine__c = TRUE];
        Integer numberOfCfaBelgiumContact = [SELECT COUNT() FROM Contact WHERE CFA_Society_Belgium__c = TRUE];

        System.assertEquals(NUMBER_OF_CONTACTS, numberOfCfaChinaContact);
        System.assertEquals(NUMBER_OF_CONTACTS, numberOfCfaGermanyContact);
        System.assertEquals(NUMBER_OF_CONTACTS, numberOfCfaBelgiumContact);
        System.assertEquals(0, numberOfCfaUkraineContact);

        //AddressPolicyHandler
        Integer numberOfCfaChinaAccount = [SELECT COUNT() FROM Contact WHERE Account.Name = 'CFA China'];
        Integer numberOfCfaGermanyAccount = [SELECT COUNT() FROM Contact WHERE Account.Name = 'CFA Germany'];
        Integer numberOfCfaUkraineAccount = [SELECT COUNT() FROM Contact WHERE Account.Name = 'CFA Ukraine'];
        Integer numberOfCfaBelgiumAccount = [SELECT COUNT() FROM Contact WHERE Account.Name = 'CFA Belgium'];

        System.assertEquals(NUMBER_OF_CONTACTS, numberOfCfaChinaAccount);
        System.assertEquals(NUMBER_OF_CONTACTS, numberOfCfaGermanyAccount);
        System.assertEquals(0, numberOfCfaBelgiumAccount);
        System.assertEquals(0, numberOfCfaUkraineAccount);
    }

    @IsTest
    private static void runBatch2WithAddressPolicyHandlerTest(){
        System.runAs(CFA_TestDataFactory.getUserWithPermission(PROFILE_INTEGRATION_USER,PERMISSION_SET_EDIT_ACCOUNT_NAME)) {
            List<INSAPolicyHandler> handlers = new List<INSAPolicyHandler>{
                    new AddressPolicyHandler()
            };
            NSAAssignmentBatch batch = new NSAAssignmentBatch(handlers, '', true);

            Test.startTest();
            Id batchProcessId = Database.executeBatch(batch);
            Test.stopTest();
        }


        //chinaContacts - address and membership
        //germanyContacts - just address
        //belgiumContacts - just membership
        //ukraineContacts - not address and not membership

        //MembershipApplicationPolicyHandler
        Integer numberOfCfaChinaContact = [SELECT COUNT() FROM Contact WHERE CFA_China__c = TRUE];
        Integer numberOfCfaGermanyContact = [SELECT COUNT() FROM Contact WHERE CFA_Society_Germany__c = TRUE];
        Integer numberOfCfaUkraineContact = [SELECT COUNT() FROM Contact WHERE CFA_Society_Ukraine__c = TRUE];
        Integer numberOfCfaBelgiumContact = [SELECT COUNT() FROM Contact WHERE CFA_Society_Belgium__c = TRUE];

        System.assertEquals(NUMBER_OF_CONTACTS, numberOfCfaChinaContact);
        System.assertEquals(NUMBER_OF_CONTACTS, numberOfCfaGermanyContact);
        System.assertEquals(0, numberOfCfaBelgiumContact);
        System.assertEquals(0, numberOfCfaUkraineContact);

        //AddressPolicyHandler
        Integer numberOfCfaChinaAccount = [SELECT COUNT() FROM Contact WHERE Account.Name = 'CFA China'];
        Integer numberOfCfaGermanyAccount = [SELECT COUNT() FROM Contact WHERE Account.Name = 'CFA Germany'];
        Integer numberOfCfaUkraineAccount = [SELECT COUNT() FROM Contact WHERE Account.Name = 'CFA Ukraine'];
        Integer numberOfCfaBelgiumAccount = [SELECT COUNT() FROM Contact WHERE Account.Name = 'CFA Belgium'];

        System.assertEquals(NUMBER_OF_CONTACTS, numberOfCfaChinaAccount);
        System.assertEquals(NUMBER_OF_CONTACTS, numberOfCfaGermanyAccount);
        System.assertEquals(0, numberOfCfaBelgiumAccount);
        System.assertEquals(0, numberOfCfaUkraineAccount);
    }

    @IsTest
    private static void runBatch3WithMembershipApplicationPolicyHandlerTest(){
        System.runAs(CFA_TestDataFactory.getUserWithPermission(PROFILE_INTEGRATION_USER,PERMISSION_SET_EDIT_ACCOUNT_NAME)) {
            List<NSA_Policy__c> policies = [
                    SELECT Id
                            , Advanced_Assignment__c
                            , Advanced_Assignment_Handler__c
                            , Society_Account__c
                            , Contact_Society_Field_API_Name__c
                            , (
                            SELECT Country_Name__c
                                    , State__c
                                    , Zip_Code_High__c
                                    , Zip_Code_Low__c
                                    , NSA_Policy__c
                            FROM NSA_Geograhpic_Rules__r
                    )
                    FROM NSA_Policy__c
                    WHERE Society_Account__r.Name = 'CFA China'
                	AND Is_Active__c = TRUE
                	AND Policy_Status__c = 'Approved'
            ];
            List<INSAPolicyHandler> handlers = new List<INSAPolicyHandler>{new MembershipApplicationPolicyHandler()};
            NSAAssignmentBatch batch = new NSAAssignmentBatch(handlers, policies[0]);

            Test.startTest();
            Id batchProcessId = Database.executeBatch(batch);
            Test.stopTest();
        }
        //chinaContacts - address and membership
        //germanyContacts - just address
        //belgiumContacts - just membership
        //ukraineContacts - not address and not membership

        Integer numberOfCfaChinaAccount = [SELECT COUNT() FROM Contact WHERE CFA_China__c = TRUE];
        Integer numberOfCfaGermanyAccount = [SELECT COUNT() FROM Contact WHERE CFA_Society_Germany__c = TRUE];
        Integer numberOfCfaUkraineAccount = [SELECT COUNT() FROM Contact WHERE CFA_Society_Ukraine__c = TRUE];
        Integer numberOfCfaBelgiumAccount = [SELECT COUNT() FROM Contact WHERE CFA_Society_Belgium__c = TRUE];

        System.assertEquals(NUMBER_OF_CONTACTS, numberOfCfaChinaAccount);
        System.assertEquals(0, numberOfCfaGermanyAccount);
        System.assertEquals(0, numberOfCfaBelgiumAccount);
        System.assertEquals(0, numberOfCfaUkraineAccount);
    }

    @IsTest
    private static void runBatch3WithMembershipApplicationPolicyHandlerAndAddressPolicyHandlerTest(){
        System.runAs(CFA_TestDataFactory.getUserWithPermission(PROFILE_INTEGRATION_USER,PERMISSION_SET_EDIT_ACCOUNT_NAME)) {
            List<NSA_Policy__c> policies = [
                    SELECT Id
                            , Advanced_Assignment__c
                            , Advanced_Assignment_Handler__c
                            , Society_Account__c
                            , Contact_Society_Field_API_Name__c
                            , (
                            SELECT Country_Name__c
                                    , State__c
                                    , Zip_Code_High__c
                                    , Zip_Code_Low__c
                                    , NSA_Policy__c
                            FROM NSA_Geograhpic_Rules__r
                    )
                    FROM NSA_Policy__c
                    WHERE Society_Account__r.Name = 'CFA China'
                	AND Is_Active__c = TRUE
                	AND Policy_Status__c = 'Approved'
            ];
            List<INSAPolicyHandler> handlers = new List<INSAPolicyHandler>{
                    new MembershipApplicationPolicyHandler(), new AddressPolicyHandler()
            };
            NSAAssignmentBatch batch = new NSAAssignmentBatch(handlers, policies[0]);

            Test.startTest();
            Id batchProcessId = Database.executeBatch(batch);
            Test.stopTest();
        }

        //chinaContacts - address and membership
        //germanyContacts - just address
        //belgiumContacts - just membership
        //ukraineContacts - not address and not membership

        //MembershipApplicationPolicyHandler
        Integer numberOfCfaChinaContact = [SELECT COUNT() FROM Contact WHERE CFA_China__c = TRUE];
        Integer numberOfCfaGermanyContact = [SELECT COUNT() FROM Contact WHERE CFA_Society_Germany__c = TRUE];
        Integer numberOfCfaUkraineContact = [SELECT COUNT() FROM Contact WHERE CFA_Society_Ukraine__c = TRUE];
        Integer numberOfCfaBelgiumContact = [SELECT COUNT() FROM Contact WHERE CFA_Society_Belgium__c = TRUE];

        System.assertEquals(NUMBER_OF_CONTACTS, numberOfCfaChinaContact);
        System.assertEquals(0, numberOfCfaGermanyContact);
        System.assertEquals(0, numberOfCfaBelgiumContact);
        System.assertEquals(0, numberOfCfaUkraineContact);

        //AddressPolicyHandler
        Integer numberOfCfaChinaAccount = [SELECT COUNT() FROM Contact WHERE Account.Name = 'CFA China'];
        Integer numberOfCfaGermanyAccount = [SELECT COUNT() FROM Contact WHERE Account.Name = 'CFA Germany'];
        Integer numberOfCfaUkraineAccount = [SELECT COUNT() FROM Contact WHERE Account.Name = 'CFA Ukraine'];
        Integer numberOfCfaBelgiumAccount = [SELECT COUNT() FROM Contact WHERE Account.Name = 'CFA Belgium'];

        System.assertEquals(NUMBER_OF_CONTACTS, numberOfCfaChinaAccount);
        System.assertEquals(0, numberOfCfaGermanyAccount);
        System.assertEquals(0, numberOfCfaUkraineAccount);
        System.assertEquals(0, numberOfCfaBelgiumAccount);
    }

    @IsTest
    private static void runBatch3WithAddressPolicyHandlerTest(){
        System.runAs(CFA_TestDataFactory.getUserWithPermission(PROFILE_INTEGRATION_USER,PERMISSION_SET_EDIT_ACCOUNT_NAME)) {
            List<NSA_Policy__c> policies = [
                    SELECT Id
                            , Advanced_Assignment__c
                            , Advanced_Assignment_Handler__c
                            , Society_Account__c
                            , Contact_Society_Field_API_Name__c
                            , (
                            SELECT Country_Name__c
                                    , State__c
                                    , Zip_Code_High__c
                                    , Zip_Code_Low__c
                                    , NSA_Policy__c
                            FROM NSA_Geograhpic_Rules__r
                    )
                    FROM NSA_Policy__c
                    WHERE Society_Account__r.Name = 'CFA China'
                	AND Is_Active__c = TRUE
                	AND Policy_Status__c = 'Approved'
            ];
            List<INSAPolicyHandler> handlers = new List<INSAPolicyHandler>{
                    new AddressPolicyHandler()
            };
            NSAAssignmentBatch batch = new NSAAssignmentBatch(handlers, policies[0]);

            Test.startTest();
            Id batchProcessId = Database.executeBatch(batch);
            Test.stopTest();
        }

        //chinaContacts - address and membership
        //germanyContacts - just address
        //belgiumContacts - just membership
        //ukraineContacts - not address and not membership

        //MembershipApplicationPolicyHandler
        Integer numberOfCfaChinaContact = [SELECT COUNT() FROM Contact WHERE CFA_China__c = TRUE];
        Integer numberOfCfaGermanyContact = [SELECT COUNT() FROM Contact WHERE CFA_Society_Germany__c = TRUE];
        Integer numberOfCfaUkraineContact = [SELECT COUNT() FROM Contact WHERE CFA_Society_Ukraine__c = TRUE];
        Integer numberOfCfaBelgiumContact = [SELECT COUNT() FROM Contact WHERE CFA_Society_Belgium__c = TRUE];

        System.assertEquals(NUMBER_OF_CONTACTS, numberOfCfaChinaContact);
        System.assertEquals(0, numberOfCfaGermanyContact);
        System.assertEquals(0, numberOfCfaBelgiumContact);
        System.assertEquals(0, numberOfCfaUkraineContact);

        //AddressPolicyHandler
        Integer numberOfCfaChinaAccount = [SELECT COUNT() FROM Contact WHERE Account.Name = 'CFA China'];
        Integer numberOfCfaGermanyAccount = [SELECT COUNT() FROM Contact WHERE Account.Name = 'CFA Germany'];
        Integer numberOfCfaUkraineAccount = [SELECT COUNT() FROM Contact WHERE Account.Name = 'CFA Ukraine'];
        Integer numberOfCfaBelgiumAccount = [SELECT COUNT() FROM Contact WHERE Account.Name = 'CFA Belgium'];

        System.assertEquals(NUMBER_OF_CONTACTS, numberOfCfaChinaAccount);
        System.assertEquals(0, numberOfCfaGermanyAccount);
        System.assertEquals(0, numberOfCfaUkraineAccount);
        System.assertEquals(0, numberOfCfaBelgiumAccount);
    }

    private static List<Contact> createContacts(String country, String state, String zipCode, Integer numberOfRecords) {
        List<Contact> contacts = new List<Contact>();
        for (Contact con : CFA_TestDataFactory.createContactRecords('CFA Contact', 'Test', numberOfRecords, false)) {
            con.Country_ISO_Code__c = country;
            con.State_Province_ISO_Code__c = state;
            con.MailingPostalCode = zipCode;
            contacts.add(con);
        }
        return contacts;
    }

    private static List<NSA_Policy__c> getAllPolicies() {
        return [
                SELECT Id
                        , Advanced_Assignment__c
                        , Advanced_Assignment_Handler__c
                        , Society_Account__c
                        , Contact_Society_Field_API_Name__c
                        , (
                        SELECT Country_Name__c
                                , State__c
                                , Zip_Code_High__c
                                , Zip_Code_Low__c
                                , NSA_Policy__c
                        FROM NSA_Geograhpic_Rules__r
                )
                FROM NSA_Policy__c
               WHERE Is_Active__c = TRUE
                AND Policy_Status__c = 'Approved'
        ];
    }

}
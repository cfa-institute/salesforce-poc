@isTest
private class SocietyMembershipServiceTest {
    
    private static String PROFILE_INTEGRATION_USER = 'Integration profile';
    private static String PERMISSION_SET_EDIT_ACCOUNT_NAME = 'Edit_Account_Name';
    @TestSetup
    private static void setup(){
        CFA_TestDataFactory.createAccount('CFA Institute', 'Society', true); 
        Account acc = new Account(Name = 'Test Account');
        insert acc;
        List<Contact> conList = CFA_TestDataFactory.createContactRecords('CFA Contact', 'TestContact', 1, false);
        conList[0].AccountId = acc.Id;
        insert conList;
    }
    
    @isTest
    private static void whenGetCFAandLocalContactsWithFieldsByIdsThenReturnCFAandLocalContacts() {
        List<Contact> conList = [SELECT Id FROM Contact];
        Test.startTest();
        List<Contact> result = SocietyMembershipService.getCFAandLocalContactsWithFieldsByIds(new Set<String>{'Name'}, new Set<String>{conList[0].Id});
        Test.stopTest();

        System.assertEquals(1, result.size());
    }
    
    @isTest
    private static void whenGetSocietyMembershipsByIdsThenReturnSocuetyMemberships() {
        List<Contact> conList = [SELECT Id, AccountId FROM Contact];
        Test.startTest();
        Membership__c membership = new Membership__c(Account_lookup__c = conList[0].AccountId,
                Contact_lookup__c = conList[0].Id, Transaction_Type__c = 'Join',
                Membership_Type__c = 'Society');
        System.runAs(CFA_TestDataFactory.getUserWithPermission(PROFILE_INTEGRATION_USER,PERMISSION_SET_EDIT_ACCOUNT_NAME)) {
        	insert membership;
        }
        List<Membership__c> result = SocietyMembershipService.getSocietyMembershipsByIds(new Set<String>{
                membership.Id
        });
        Test.stopTest();

        System.assertEquals(1, result.size());
    }
    
    @isTest
    private static void whenCreateCampaignMembersThenCampaignMembersShouldBeCreated() {
        List<Contact> conList = [SELECT Id FROM Contact];
        Test.startTest();
        Campaign campaign = new Campaign(name = 'Test Campaign',isActive = true);
        insert campaign;
        Map<Id, Set<Id>> input = new Map<Id, Set<Id>>();
        input.put(conList[0].Id, new Set<Id>{campaign.Id});
        SocietyMembershipService.createCampaignMembers(input);
        Test.stopTest();

        System.assertEquals(1, [SELECT count() FROM campaignMember WHERE ContactID = :conList[0].Id]);
    }
    
    @isTest
    private static void whenRemoveCampaignMembersThenCampaignMembersShouldBeRemoved() {
        List<Contact> conList = [SELECT Id FROM Contact];
        Test.startTest();
        Campaign campaign = new Campaign(name = 'Test Campaign',isActive = true);
        insert campaign;
        insert new CampaignMember(ContactId = conList[0].Id, CampaignId= campaign.Id);
        Map<Id, Set<Id>> input = new Map<Id, Set<Id>>();
        input.put(conList[0].Id, new Set<Id>{campaign.Id});
        SocietyMembershipService.removeCampaignMembers(input);
        Test.stopTest();

        System.assertEquals(0, [SELECT count() FROM campaignMember WHERE ContactID = :conList[0].Id]);
    }
}
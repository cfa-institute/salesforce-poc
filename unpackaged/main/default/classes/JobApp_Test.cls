@isTest
public class JobApp_Test { 
        
    public static testMethod void testRedirectionWithoutContactWithValidRecordType() {
       
        List<URLSetting__c> URLSettingLst = new List<URLSetting__c>();
        List<RoleURL__c> RoleSettingLst = new List<RoleURL__c>();
        List<JobApplicationRecordTypeSelection__c> JobApplicationSelection = new List<JobApplicationRecordTypeSelection__c>();
        URLSetting__c InternalURLSetting = new URLSetting__c(Name='InternalURLSetting',DevURL__c='test',TestURL__c='test',ProdURL__c='test',EnvironmentVariable__c='1');
        URLSetting__c ExternalURLSetting = new URLSetting__c(Name='ExternalURLSetting',DevURL__c='test',TestURL__c='test',ProdURL__c='test',EnvironmentVariable__c='1');
        RoleURL__c RoleFieldValue=new RoleURL__c(Name='RoleFieldValue',DevURL__c='test',TestURL__c='test',ProdURL__c='test');
        JobApplicationRecordTypeSelection__c jobSelect1=new JobApplicationRecordTypeSelection__c(Name='1',Engagement_Type__c='Committee',Role_Type__c='CIPM Committee Chair',JobApplication_RecordType__c='CIPMCommittee');                  
        JobApplicationRecordTypeSelection__c jobSelect2=new JobApplicationRecordTypeSelection__c(Name='10',Engagement_Type__c='Committee',Role_Type__c='CIPM Committee Member',JobApplication_RecordType__c='CIPMCommittee');                  
        URLSettingLst.add(InternalURLSetting);
        URLSettingLst.add(ExternalURLSetting);
        insert URLSettingLst;
        insert RoleFieldValue;
        JobApplicationSelection.add(jobSelect1);
        JobApplicationSelection.add(jobSelect2);
        insert JobApplicationSelection;
       
        Test.startTest();
        User RunningUser=VM_TestDataFactory.createUser('RunningUsertest@gmail.com','RunningTest','Relationship Manager Staff','');
        User RMOwner=VM_TestDataFactory.createUser('RMTest1234@gmail.com','RMTest','Relationship Manager Staff','');
        User MDUSer=VM_TestDataFactory.createUser('MDTest@gmail.com','MDTest','Outreach Management User','Managing Director');
        User ReportsToUSer=VM_TestDataFactory.createUser('Anne12er@gmail.com','Huff','Outreach Management User','Relationship Manager');
           
        system.runAs(RunningUser)
        {
        Engagement__c eng=VM_TestDataFactory.createEngagement('t FY20',RMOwner,MDUSer,ReportsToUSer);

        try
        {  
            eng.Engagement_Type__c='Committee';
            insert eng;
            
            system.debug('Successful Exe');
        }catch(DMLException ex)
        {
            system.debug('Npp eng'+ex.getDMLMessage(0));
        }
        Role__c role=VM_TestDataFactory.createRole(eng); 
        try
        {  
            role.Role_Type__c='CIPM Committee Member';
			//role.JARecordType__c ='abc';
            insert role;
            
            system.debug('Successful Exe role');

			Account acc=VM_TestDataFactory.createAccountRecord();
 
			Contact con=VM_TestDataFactory.createContactRecord(acc);

       
        
            Job_Application__c job=VM_TestDataFactory.createJobApplication(role,con); 
            system.debug('job' +job);
            PageReference pageRef = Page.JobApplicationPageRedirect;
            Test.setCurrentPage(pageRef);   
            ApexPages.StandardController sc = new ApexPages.StandardController(job);
            JobApp jobApplication = new JobApp(sc); 
            
            jobApplication.MyFWithCustomSettingNew();
        }catch(DMLException ex)
        {
            system.debug('Npp role '+ex.getDMLMessage(0));
        }
                 
        }
        Test.stopTest();
    }
    
    
     public static testMethod void testRedirectionWithoutContactWithoutValidRecordType() {
        
        List<URLSetting__c> URLSettingLst = new List<URLSetting__c>();
        List<RoleURL__c> RoleSettingLst = new List<RoleURL__c>();
        List<JobApplicationRecordTypeSelection__c> JobApplicationSelection = new List<JobApplicationRecordTypeSelection__c>();
        URLSetting__c InternalURLSetting = new URLSetting__c(Name='InternalURLSetting',DevURL__c='test',TestURL__c='test',ProdURL__c='test',EnvironmentVariable__c='2');
        URLSetting__c ExternalURLSetting = new URLSetting__c(Name='ExternalURLSetting',DevURL__c='test',TestURL__c='test',ProdURL__c='test',EnvironmentVariable__c='2');
        RoleURL__c RoleFieldValue=new RoleURL__c(Name='RoleFieldValue',DevURL__c='test',TestURL__c='test',ProdURL__c='test');
        JobApplicationRecordTypeSelection__c jobSelect1=new JobApplicationRecordTypeSelection__c(Name='1',Engagement_Type__c='Committee',Role_Type__c='CIPM Committee Chair',JobApplication_RecordType__c='CIPMCommittee');                  
        JobApplicationRecordTypeSelection__c jobSelect2=new JobApplicationRecordTypeSelection__c(Name='10',Engagement_Type__c='Committee',Role_Type__c='CIPM Committee Member',JobApplication_RecordType__c='CIPMCommittee');                  
        URLSettingLst.add(InternalURLSetting);
        URLSettingLst.add(ExternalURLSetting);
        insert URLSettingLst;
        insert RoleFieldValue;
        JobApplicationSelection.add(jobSelect1);
        JobApplicationSelection.add(jobSelect2);
        insert JobApplicationSelection;
       
        Test.startTest();
        User RunningUser=VM_TestDataFactory.createUser('RunningUsertest@gmail.com','RunningTest','System Administrator','');
        User RMOwner=VM_TestDataFactory.createUser('RMTest@gmail.com1234','RMTest','Relationship Manager Staff','');
        User MDUSer=VM_TestDataFactory.createUser('MDTest@gmail.com','MDTest','Outreach Management User','Managing Director');
        User ReportsToUSer=VM_TestDataFactory.createUser('Anneexp@gmail.com','Huff','Outreach Management User','Relationship Manager');
               
        system.runAs(RunningUser)
        {
        Engagement__c eng=VM_TestDataFactory.createEngagement('t FY20',RMOwner,MDUSer,ReportsToUSer);

        try
        {  
            eng.Engagement_Type__c='Committee';
            insert eng;
            
            system.debug('Successful Exe');
        }catch(DMLException ex)
        {
            system.debug('Npp eng'+ex.getDMLMessage(0));
        }
        Role__c role=VM_TestDataFactory.createRole(eng); 
        try
        {  
            role.Role_Type__c='DRC Committee Member';
			role.JARecordType__c ='abc';
            insert role;
            
            system.debug('Successful Exe role');
        }catch(DMLException ex)
        {
            system.debug('Npp role '+ex.getDMLMessage(0));
        }
        Account acc=VM_TestDataFactory.createAccountRecord();
 
        Contact con=VM_TestDataFactory.createContactRecord(acc);

       
        system.debug('roleid' +role.id);
            Job_Application__c job=VM_TestDataFactory.createJobApplication(role,con); 
            PageReference pageRef = Page.JobApplicationPageRedirect;
            Test.setCurrentPage(pageRef);   
            ApexPages.StandardController sc = new ApexPages.StandardController(job);
            JobApp jobApplication = new JobApp(sc); 
            
            jobApplication.MyFWithCustomSettingNew();
                 
        }
        Test.stopTest();
    }

 
    public static testMethod void testRedirection() {
        
        List<URLSetting__c> URLSettingLst = new List<URLSetting__c>();
        List<RoleURL__c> RoleSettingLst = new List<RoleURL__c>();
        List<JobApplicationRecordTypeSelection__c> JobApplicationSelection = new List<JobApplicationRecordTypeSelection__c>();
        URLSetting__c InternalURLSetting = new URLSetting__c(Name='InternalURLSettings',DevURL__c='test',TestURL__c='test',ProdURL__c='test',EnvironmentVariable__c='3');
        URLSetting__c ExternalURLSetting = new URLSetting__c(Name='ExternalURLSettings',DevURL__c='test',TestURL__c='test',ProdURL__c='test',EnvironmentVariable__c='3');
        RoleURL__c RoleFieldValue=new RoleURL__c(Name='RoleFieldValue',DevURL__c='test',TestURL__c='test',ProdURL__c='test');
        JobApplicationRecordTypeSelection__c jobSelect1=new JobApplicationRecordTypeSelection__c(Name='1',Engagement_Type__c='Committee',Role_Type__c='CIPM Committee Chair',JobApplication_RecordType__c='CIPMCommittee');                  
        JobApplicationRecordTypeSelection__c jobSelect2=new JobApplicationRecordTypeSelection__c(Name='10',Engagement_Type__c='Committee',Role_Type__c='CIPM Committee Member',JobApplication_RecordType__c='CIPMCommittee');                  
        URLSettingLst.add(InternalURLSetting);
        URLSettingLst.add(ExternalURLSetting);
        insert URLSettingLst;
        insert RoleFieldValue;
        JobApplicationSelection.add(jobSelect1);
        JobApplicationSelection.add(jobSelect2);
        insert JobApplicationSelection;
       
        Test.startTest();
        User RunningUser=VM_TestDataFactory.createUser('RunningUsertest@gmail.com','RunningTest','Relationship Manager Staff','');
        User RMOwner=VM_TestDataFactory.createUser('RMTest@gmail.com1234','RMTest','Relationship Manager Staff','');
        User MDUSer=VM_TestDataFactory.createUser('MDTest@gmail.com','MDTest','Outreach Management User','Managing Director');
        User ReportsToUSer=VM_TestDataFactory.createUser('Anne12er2@gmail.com','Huff','Outreach Management User','Relationship Manager');

        system.runAs(RunningUser)
        {
        Engagement__c eng=VM_TestDataFactory.createEngagement('t FY20',RMOwner,MDUSer,ReportsToUSer);

        try
        {  
            eng.Engagement_Type__c='Committee';
            insert eng;
            
            system.debug('Successful Exe');
        }catch(DMLException ex)
        {
            system.debug('Npp eng'+ex.getDMLMessage(0));
        }
        Role__c role=VM_TestDataFactory.createRole(eng); 
        try
        {  
            role.Role_Type__c='CIPM Committee Member';
			//role.JARecordType__c ='abc';
            insert role;
            
            system.debug('Successful Exe role');

        Account acc=VM_TestDataFactory.createAccountRecord();
 
        Contact con=VM_TestDataFactory.createContactRecord(acc); 

       
        
            Job_Application__c job=VM_TestDataFactory.createJobApplicationDefault(role,con); 
            PageReference pageRef = Page.JobApplicationPageRedirect;
            Test.setCurrentPage(pageRef);   
            ApexPages.StandardController sc = new ApexPages.StandardController(job);
            JobApp jobApplication = new JobApp(sc); 
        }catch(DMLException ex)
        {
            system.debug('Npp role '+ex.getDMLMessage(0));
        }
                 
        }
        Test.stopTest();
    }

}
@isTest
public class VM_RoleTriggerHandler_Test_aux{

    
    @isTest
    public static void test0(){
        
        User RMOwner=VM_TestDataFactory.createUser('RMTest5@gmail.com','RMTest','Relationship Manager Staff','');
        User MDUSer=VM_TestDataFactory.createUser('MDTest1@gmail.com','MDTest','Outreach Management User','Managing Director');
        User ReportsToUSer=VM_TestDataFactory.createUser('Anne1@gmail.com','Huff','Outreach Management User','Relationship Manager');
        String ename ='Test Data FY14';
        Engagement__c eng  = VM_TestDataFactory.createEngagement(Ename,RMOwner,MDUSer,ReportsToUSer) ;
        
        try{  
            insert eng;
            system.debug('Engagement inserted');
        }catch(DMLException ex)
        {
            System.debug(ex) ;
        }
        
        Role__c test_role= VM_TestDataFactory.createRole(eng);

        test_role.Position_Title__c='test class FY17';
        
        
        try{
             insert test_role;  
           
            system.debug('role inserted');
        }catch(DMLException ex)
        {
            System.debug(ex) ;
        }
        Role__c test_role2= test_role.clone(false,false,false,false);
        test_role2.Position_Title__c='Hello World FY18';
      
        try{
            insert test_role2;
        }catch(Exception e){
            
	//	Boolean expectedExceptionThrown =  (e.getMessage().contains('Please make atleast one change!')) ? true : false;

      //  System.AssertEquals(expectedExceptionThrown, true); 
        }
             
    }
    
    @isTest
    public static void test1(){
        
         VM_RoleTriggerHandler roleHandler = new VM_RoleTriggerHandler(); 
    test.startTest();  
//    
    test.stopTest();
            
    }

    @isTest
    public static void test2(){
        
        test.startTest();
        
        List<Role__c> roleLst = new List<Role__c>();
 		Map<Id, Role__c> roleMap=new Map<Id, Role__c>();
     
        // role insertion
        User RMOwner=VM_TestDataFactory.createUser('RMTest5@gmail.com','RMTest','Relationship Manager Staff','');
        User MDUSer=VM_TestDataFactory.createUser('MDTest1@gmail.com','MDTest','Outreach Management User','Managing Director');
        User ReportsToUSer=VM_TestDataFactory.createUser('Anne1@gmail.com','Huff','Outreach Management User','Relationship Manager');
        String ename ='Test Data FY15';
        Engagement__c eng  = VM_TestDataFactory.createEngagement(Ename,RMOwner,MDUSer,ReportsToUSer) ;
        
        try{  
            insert eng;
            system.debug('Engagement inserted');
        }catch(DMLException ex)
        {
            System.debug(ex) ;
        }
        
        Role__c test_role11= VM_TestDataFactory.createRole(eng);

        test_role11.Position_Title__c='test class FY18';
        
        
        try{
             insert test_role11;  
           
            system.debug('role inserted');
        }catch(DMLException ex)
        {
            System.debug(ex) ;
        }
        
        roleLst.add(test_role11);
        roleMap.put(test_role11.Id,test_role11);
        
        // role insertion end
        VM_RoleTriggerHandler roleHandler = new VM_RoleTriggerHandler(); 
      roleHandler.OnAfterUpdate(roleLst);  	

        
        Account acc1 = VM_TestDataFactory.createAccountRecord() ;
       
        Contact con = VM_TestDataFactory.createContactRecord(acc1) ;
       
     

        
         
        Job_Application__c job_app = VM_TestDataFactory.createJobApplication(test_role11,con);
       
        job_app.status__c='Applied';
        
        try
        {    
            insert job_app;           
        }
        
        catch(DMLException ex){
            System.debug(ex) ;
        }
        
      
        
        
       /*    
       
        Role__c rl=roleLst.get(0);
        
        Job_Application__c job_app = VM_TestDataFactory.createJobApplication(rl,con);
        job_app.Submitted__c =true;
        job_app.Status__c='Applied';
        try
        {
            insert job_app;
            system.debug('Job application inserted');
            
        }catch(DMLException ex){
            System.debug(ex) ;
        }
*/
        
        try{
			roleHandler.OnBeforeDelete(roleLst,roleMap);
        }catch(Exception e){
            System.debug('ROle 333333'+e);
       
		Boolean expectedExceptionThrown =  (e.getMessage().contains('Delete Operation can not be performed as an applied Application exists for this Role.')) ? true : false;
        System.AssertEquals(expectedExceptionThrown, true); 
        }

        

    test.stopTest();
            
    }
    
}
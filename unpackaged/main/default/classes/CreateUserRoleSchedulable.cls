global class CreateUserRoleSchedulable implements Schedulable{
    global void execute(SchedulableContext sc) {
        Database.executeBatch(new CreateUserRoleBatch());
       // CreateUserRoleSchedulable.scheduleHourly();
        //CreateUserRoleSchedulable cr = new CreateUserRoleSchedulable();
        //cr.execute(null);
        
    }
    
    public static String scheduleHourly(){
        return System.schedule('Create UserRole Hourly', '0 0 * * * ?', new CreateUserRoleSchedulable());  
    }
    
}
public interface INSAPolicyHandler {
    void run(List<NSA_Policy__c> policies, List<Contact> contacts);
}
public with sharing class TaskTriggerHandler implements ITriggerHandler {
    
	// Allows unit tests (or other code) to disable this trigger for the transaction
    public static Boolean triggerDisabled = false;

    //Checks to see if the trigger has been disabled either by custom metadata or by running code
    public Boolean isDisabled() {
        if (Test.isRunningTest()) {
            return triggerDisabled;
        } else {
            return DynamicTriggerStatus.getTriggerStatus('TaskTrigger') || triggerDisabled;
        }
    }

    public void beforeInsert(List<SObject> newItems) {
        TaskTriggerHelper.checkAssginedTo((List<Task>)newItems, null);
    }

    public void beforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
       TaskTriggerHelper.checkAssginedTo((List<Task>)newItems.values(), (Map<Id,Task>) oldItems);
    }

    public void beforeDelete(Map<Id, SObject> oldItems) {}

    public void afterInsert(Map<Id, SObject> newItems) {
        TaskTriggerHelper.handleVMTaskAssignmenttoVolunteer(newItems.values());
    }

    public void afterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        TaskTriggerHelper.handleCompletedTasks(newItems, oldItems);
        TaskTriggerHelper.handleVMTaskAssignmenttoVolunteer(newItems.values());
    }

    public void afterDelete(Map<Id, SObject> oldItems) {}

    public void afterUndelete(Map<Id, SObject> oldItems) {}
}
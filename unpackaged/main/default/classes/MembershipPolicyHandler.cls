public without sharing class MembershipPolicyHandler implements INSAPolicyHandler {
    
    /**
     * change CFA society checkboxes based on membership type
     * 
     * @param	policies	policies to applu to contacts
     * @param	contacts	Contact to count CFA society checkboxes based on related
     * 						memberships
     * 
     * */
    public void run(List<NSA_Policy__c> policies, List<Contact> contacts) {
        if(contacts != null && !contacts.isEmpty()) {
            Map<Id,Contact> mapIdToContact = new Map<Id,Contact>(contacts);
            List<Membership__c> memberships = 
                [SELECT ID,Account_lookup__c,Transaction_Type__c,Contact_lookup__c,Transaction_Date__c  
                   FROM Membership__c 
                  WHERE Contact_lookup__c IN :contacts
                    AND (Membership_Type__c = 'Society' OR RecordType.DeveloperName = 'Local') ];
            Map<Id, List<NSA_Policy__c>> mapAccIdToPolicies = NSAPolicyService.groupPoliciesByAccountId(policies);
            List<Contact> contactsToUpdate = new List<Contact>();
            for(Membership__c membership: memberships) {
                if(mapAccIdToPolicies.keySet().contains(membership.Account_lookup__c)) {
                    Contact contact = mapIdToContact.get(membership.Contact_lookup__c);
                    List<NSA_Policy__c> policiesList = mapAccIdToPolicies.get(membership.Account_lookup__c);
                    if(
                        (membership.Transaction_Type__c == 'Join' || membership.Transaction_Type__c == 'Renew')
                        || (
                            (membership.Transaction_Type__c == 'Lapse' || membership.Transaction_Type__c == 'Cancel')
                            && membership.Transaction_Date__c != null && membership.Transaction_Date__c.addYears(2) >= DateTime.now()
                        )
                      ) {
                        NSAPolicyService.changeContactCFACheckboxes(contact, policiesList, true);
                    }
                }
            }
        }
    }
}
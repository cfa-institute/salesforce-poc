/**************************************************************************************************
* Apex Class Name   : CustomerHistoryWrapper
* Purpose           : This is the wrapper class which is used for mapping Individual key of CustomerHistoryWrapper Json Response into seperate field
* Version           : 1.0 Initial Version
* Organization      : CFA
***************************************************************************************************/
@isTest
public class CustomerHistoryWrapper_Test {
    
    public static testMethod void CustomerHistoryWrapper_Test1(){
        
        CustomerHistoryWrapper CHWrapper = new CustomerHistoryWrapper();
        CHWrapper.PersonId = 'TestPr1';
        CHWrapper.GivenName = 'TestES1';
        CHWrapper.Surname = 'TestLv1';
        CHWrapper.PassportNumber = 'TestCy1';
        CHWrapper.DateOfBirth = Date.today();
        CHWrapper.PassportExpirationDate = Date.today();
        CHWrapper.ValidFrom = Date.today();
        CHWrapper.ModifiedBy = 'TestRS1';
        CHWrapper.PassportExpirationDateFormatted = 'TestCy1';
        CHWrapper.DateOfBirthFormatted ='TestCy1';
        CHWrapper.ValidFromFormatted ='TestCy1';
        
    }
    
}
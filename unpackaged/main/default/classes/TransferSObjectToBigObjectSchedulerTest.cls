@isTest
private class TransferSObjectToBigObjectSchedulerTest {
    
    @isTest
    static void testScheduler() {
        String cronExp = '0 0 0 3 9 ? 2032';
        Test.startTest();
        TransferSObjectToBigObjectScheduler sch = new TransferSObjectToBigObjectScheduler();
        List<Big_object_data_archival_settings__mdt> testSettings = new List<Big_object_data_archival_settings__mdt>();
        Big_object_data_archival_settings__mdt setting = new Big_object_data_archival_settings__mdt();
        setting.SourceSObjectApiName__c = 'Account';
        setting.TargetBigObjectApiName__c = 'IndividualEmailResult__b';
        setting.SobjectQueryCriteria__c = '';
        setting.SobjectFieldsApiNames__c = 'Name';
        setting.BigObjectFieldsApiNames__c = 'Name__c';
        setting.DeleteSObjectRecords__c = false;
		testSettings.add(setting);
        sch.testSettings = testSettings;
        String jobId = System.schedule('TransferSObjectToBigObjectScheduler Test Class', cronExp, sch);
        Test.stopTest();
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
        System.assertEquals(0, ct.TimesTriggered);
        System.assertEquals('2032-09-03 00:00:00', String.valueOf(ct.NextFireTime));    
        
    }
}
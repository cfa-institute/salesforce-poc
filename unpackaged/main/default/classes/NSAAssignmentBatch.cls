/*****************************************************************
Name: NSAAssignmentBatch
Copyright © 2021 ITC
============================================================
Purpose: NSA Assignment Batch
============================================================
History
-------
VERSION  AUTHOR          DATE         DETAIL    Description
1.0      Alona Zubenko   12.02.2021   Created   NSA Assignment Batch for execution policies for Contacts
*****************************************************************/

public without sharing class NSAAssignmentBatch implements Database.Batchable<SObject>, Database.Stateful {
    private final List<String> societyFields;
    private final String queryForContacts;
    private final List<INSAPolicyHandler> handlers;
    private final List<NSA_Policy__c> policies;
    private final Boolean doClean;

    /**
     * @description Constructor
     *
     * @param handlers List<INSAPolicyHandler>
     * @param policies List<NSA_Policy__c>
     * @param whereCondition Conditions for Contacts
     * @param doClean Boolean
     */
    public NSAAssignmentBatch(List<INSAPolicyHandler> handlers, List<NSA_Policy__c> policies, String whereCondition, Boolean doClean) {
        this.handlers = handlers;
        this.policies = policies;
        this.doClean = doClean;
        this.societyFields = new List<String>(SObjectService.getFieldValues(policies, 'Contact_Society_Field_API_Name__c'));
        this.queryForContacts = getQueryForContacts() + ' ' + whereCondition;
    }

    /**
     * @description Constructor
     *
     * @param handlers List<INSAPolicyHandler>
     * @param whereCondition Conditions for Contacts
     * @param doClean Boolean
     */
    public NSAAssignmentBatch(List<INSAPolicyHandler> handlers, String whereCondition, Boolean doClean) {
        this.handlers = handlers;
        this.doClean = doClean;
        this.societyFields = new List<String>(NSAPolicyService.getContactSocietyFieldAPINamesFromPolicies());
        this.policies = getAllPolicies();
        this.queryForContacts = getQueryForContacts() + ' ' + whereCondition;
    }

    /**
     * @description Constructor
     *
     * @param handlers List<INSAPolicyHandler>
     * @param policy NSA_Policy__c
     */
    public NSAAssignmentBatch(List<INSAPolicyHandler> handlers, NSA_Policy__c policy) {
        this.doClean = false;
        this.handlers = handlers;
        this.policies = new List<NSA_Policy__c>{
                policy
        };
        societyFields = new List<String>(SObjectService.getFieldValues(this.policies, 'Contact_Society_Field_API_Name__c'));
        this.queryForContacts = getQueryForContacts();
    }

    /**
     * @description Start
     *
     * @param context Database.BatchableContext
     *
     * @return Database.QueryLocator
     */
    public Database.QueryLocator start(Database.BatchableContext context) {
        return Database.getQueryLocator(queryForContacts);
    }

    /**
     * @description Execute
     *
     * @param context Database.BatchableContext
     * @param contacts List<Contact>
     */
    public void execute(Database.BatchableContext context, List<Contact> contacts) {
        try {
            if(doClean) {
                clearExistedFlags(contacts);
            }
            NSAPolicyRunner policyRunner = new NSAPolicyRunner(policies);
            policyRunner.run(contacts,handlers,true);
        } catch (Exception ex) {
            insert CFA_IntegrationLogException.logError(ex, 'NSAAssignmentBatch', 'NSAAssignmentBatch', DateTime.now(), true, '', '', '', true, '');
        } 
    }

    /**
     * @description Finish
     *
     * @param param1 Database.BatchableContext
     */
    public void finish(Database.BatchableContext param1) {
    }

    // **************************** HELPERS *********************************//

    /**
     * @description Clear Existed Flags
     *
     * @param contacts List<Contact>
     */
    private void clearExistedFlags(List<Contact> contacts) {
        for(Contact con : contacts){
            for(String fieldName : societyFields){
                con.put(fieldName, false);
            }
        }
    }

    /**
     * @description Get Query for Contacts
     *
     * @return String
     */
    private String getQueryForContacts() {
        List<String> fieldNames = new List<String>{
                'Id', 'Country_ISO_Code__c', 'State_Province_ISO_Code__c', 'MailingPostalCode', 'Deceased_Date__c', 'RecordTypeId', 'AccountId'
        };
        fieldNames.addAll(societyFields);

        String query = ' SELECT ';
        query += String.join(fieldNames, ',');
        query += ' FROM Contact';

        return query;
    }

    /**
     * @description Get All Policies
     *
     * @return List<NSA_Policy__c>
     */
    private static List<NSA_Policy__c> getAllPolicies() {
        return [
                SELECT Id
                        , Advanced_Assignment__c
                        , Advanced_Assignment_Handler__c
                        , Society_Account__c
                        , Contact_Society_Field_API_Name__c
                        , (
                        SELECT Country_Name__c
                                , State__c
                                , Zip_Code_High__c
                                , Zip_Code_Low__c
                                , NSA_Policy__c
                        FROM NSA_Geograhpic_Rules__r
                )
                FROM NSA_Policy__c
                WHERE Is_Active__c = true
            	AND Policy_Status__c = 'Approved'
        ];
    }
}
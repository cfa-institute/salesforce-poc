@isTest
private class TransferSObjectToBigObjectBatchTest {
    
    @TestSetup
    static void makeData() {
        List<Account> testAccounts = new List<Account>();
        for(Integer i = 0; i < 200; i++) {
            testAccounts.add(new Account(Name = 'test' + i));
        }
        insert testAccounts;
    }

    @isTest
    static void testRunBatch() {
        Test.startTest();
        TransferSObjectToBigObjectBatch obj = new TransferSObjectToBigObjectBatch('Account', 'IndividualEmailResult__b', new Map<String, String>{'Name' => 'Name__c'}, '', true);
        DataBase.executeBatch(obj);
        Test.stopTest();
        System.assertEquals(0, [SELECT count() FROM Account], 'Source SObject records should be deleted');

    }
}
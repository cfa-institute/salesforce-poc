/*****************************************************************
Name: SocietyCampaignMemberServiceTest
Copyright © 2020 ITC
============================================================
Purpose: Unit test on SocietyCampaignMemberService class
============================================================
History
-------
VERSION  AUTHOR          DATE         DETAIL    Description
1.0      Alona Zubenko   24.11.2020   Created   cover logic for updating Contact on Create/Delete CampaignMember
*****************************************************************/
@IsTest
private with sharing class SocietyCampaignMemberServiceTest {

    @IsTest
    private static void getUpdatedRelatedContactsToEmptyTest(){
        Contact con = createContact();
        con.Society_Member__c = false;
        con.Campaign_Relations__c = 'Test';
        insert con;
        Campaign camp = createCampaign('Test');
        CampaignMember cm = CFA_TestDataFactory.createCampaignMember(camp.Id, con.Id, null, true);

        Test.startTest();
        List<Contact> conList = SocietyCampaignMemberService.getUpdatedRelatedContacts(new List<CampaignMember>{cm});
        Test.stopTest();

        System.assert(!conList.isEmpty());
        System.assertEquals(false, conList[0].Society_Member__c);
        System.assertEquals('', conList[0].Campaign_Relations__c);
    }

    @IsTest
    private static void getUpdatedRelatedContactsToFilledTest(){
        Contact con = createContact();
        con.Society_Member__c = false;
        con.Campaign_Relations__c = 'Test';
        insert con;
        Campaign camp = createCampaign('CFA Society Toronto Members');
        CampaignMember cm = CFA_TestDataFactory.createCampaignMember(camp.Id, con.Id, null, true);

        Test.startTest();
        List<Contact> conList = SocietyCampaignMemberService.getUpdatedRelatedContacts(new List<CampaignMember>{cm});
        Test.stopTest();

        System.assert(!conList.isEmpty());
        System.assertEquals(true, conList[0].Society_Member__c);
        System.assertEquals('CFA Society Toronto Members', conList[0].Campaign_Relations__c);
    }

    @IsTest
    private static void getCampaignMembersForChangeEventTest(){
        Contact con = createContact();
        con.Society_Member__c = false;
        con.Campaign_Relations__c = 'Test';
        insert con;
        Campaign camp = createCampaign('CFA Society Toronto Members');
        CampaignMember cm = CFA_TestDataFactory.createCampaignMember(camp.Id, con.Id, null, true);

        Test.startTest();
        List<CampaignMember> cmList = SocietyCampaignMemberService.getCampaignMembers(new Set<String>{cm.Id});
        Test.stopTest();

        System.assert(!cmList.isEmpty());
    }

    @IsTest
    private static void getCampaignMembersForPlatformEventTest(){
        Contact con = createContact();
        con.Society_Member__c = false;
        con.Campaign_Relations__c = 'Test';
        insert con;
        Campaign camp = createCampaign('CFA Society Toronto Members');
        CampaignMember cm = CFA_TestDataFactory.createCampaignMember(camp.Id, con.Id, null, true);
        CampaignMemberPlatformEvent__e event = new CampaignMemberPlatformEvent__e();
        event.CampaignId__c = cm.CampaignId;
        event.ContactId__c = cm.ContactId;
        event.ChangeType__c = 'DELETE';

        Test.startTest();
        List<CampaignMember> cmList = SocietyCampaignMemberService.getCampaignMembers(new List<CampaignMemberPlatformEvent__e>{event});
        Test.stopTest();

        System.assert(!cmList.isEmpty());
    }

    @IsTest
    private static void createPlatformEventTest(){
        Contact con = createContact();
        con.Society_Member__c = false;
        con.Campaign_Relations__c = 'Test';
        insert con;
        Campaign camp = createCampaign('CFA Society Toronto Members');
        CampaignMember cm = CFA_TestDataFactory.createCampaignMember(camp.Id, con.Id, null, true);

        Test.startTest();
        SocietyCampaignMemberService.createPlatformEvent(new Map<Id,CampaignMember>{cm.Id => cm});
        Test.stopTest();

        List<EventBusSubscriber> eventBusSubscribers = [
                SELECT Name
                FROM EventBusSubscriber
                WHERE Topic='CampaignMemberPlatformEvent__e' AND Type='ApexTrigger'
        ];
        System.assert(!eventBusSubscribers.isEmpty());
    }

    private static Campaign createCampaign(String campaignName) {

        Campaign camp = CFA_TestDataFactory.createCampaign(campaignName, false);
        camp.Type = 'Society';
        insert camp;
        return camp;
    }

    private static Contact createContact(){
        List<Account> accList = CFA_TestDataFactory.createAccountRecords('CFA Society Toronto', null, 1, true);
        Contact con = CFA_TestDataFactory.createContact('Test', accList[0].Id, 'CFA Contact', false);
        return con;
    }
}
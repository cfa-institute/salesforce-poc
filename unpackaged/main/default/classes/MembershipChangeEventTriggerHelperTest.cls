/*****************************************************************
Name: CampaignMemberChangeEventTriggerHTest
Copyright © 2020 ITC
============================================================
Purpose: Unit test on CampaignMemberChangeEventTriggerHandler class
============================================================
History
-------
VERSION  AUTHOR          DATE         DETAIL    Description
1.0      Vadym Merkotan               Create    Cover logic for Applying Member Society Campaigns Changes
2.0      Alona Zubenko   10.12.2020   Update    Cover logic for Updating Badge Visibility Dates
*****************************************************************/
@isTest
private class MembershipChangeEventTriggerHelperTest {
    private static final Integer CONTACT_COUNT = 10;
    private static final String RECORD_TYPE_CFA_CONTACT = 'CFA Contact';
    private static String PROFILE_INTEGRATION_USER = 'Integration profile';
    private static String PERMISSION_SET_EDIT_ACCOUNT_NAME = 'Edit_Account_Name';

    @TestSetup
    private static void setup() {
        CFA_TestDataFactory.createAccount('CFA Institute', 'Society', true);
        Id recordTypeIdAccountSociety = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Society').getRecordTypeId();
        Account acc = new Account(Name = 'CFA Society Toronto', RecordTypeId = recordTypeIdAccountSociety);
        insert acc;

        Map<String, Campaign> campaignByNameMap = new Map<String, Campaign>{
                'Member China Campaign' => CFA_TestDataFactory.createCampaign('Member China Campaign', false)
                , 'Member Germany Campaign' => CFA_TestDataFactory.createCampaign('Member Germany Campaign', false)
                , 'Member Ukraine Campaign' => CFA_TestDataFactory.createCampaign('Member Ukraine Campaign', false)
                , 'Member Belgium Campaign' => CFA_TestDataFactory.createCampaign('Member Belgium Campaign', false)
        };
        insert campaignByNameMap.values();

        Map<String, Account> accountByNameMap = new Map<String, Account>{
                'CFA China' => CFA_TestDataFactory.createAccount('CFA China', 'Society', false)
                , 'CFA Germany' => CFA_TestDataFactory.createAccount('CFA Germany', 'Society', false)
                , 'CFA Ukraine' => CFA_TestDataFactory.createAccount('CFA Ukraine', 'Society', false)
                , 'CFA Belgium' => CFA_TestDataFactory.createAccount('CFA Belgium', 'Society', false)
        };
        insert accountByNameMap.values();

        Map<String, NSA_Policy__c> policyByNameMap = new Map<String, NSA_Policy__c>{
                'NSA China' => CFA_TestDataFactory.createNsaPolicy(accountByNameMap.get('CFA China').Id, 'CFA_China__c', null, campaignByNameMap.get('Member China Campaign').Id, null, false)
                , 'NSA Germany' => CFA_TestDataFactory.createNsaPolicy(accountByNameMap.get('CFA Germany').Id, 'CFA_Society_Germany__c', null, campaignByNameMap.get('Member Germany Campaign').Id, null, false)
                , 'NSA Ukraine' => CFA_TestDataFactory.createNsaPolicy(accountByNameMap.get('CFA Ukraine').Id, 'CFA_Society_Ukraine__c', null, campaignByNameMap.get('Member Ukraine Campaign').Id, null, false)
                , 'NSA Belgium' => CFA_TestDataFactory.createNsaPolicy(accountByNameMap.get('CFA Belgium').Id, 'CFA_Society_Belgium__c', null, campaignByNameMap.get('Member Belgium Campaign').Id, null, false)
        };
        insert policyByNameMap.values();

        List<Contact> conChinaList = CFA_TestDataFactory.createContactRecords(RECORD_TYPE_CFA_CONTACT, 'ChinaContact', CONTACT_COUNT, false);
        List<Contact> conGermanyList = CFA_TestDataFactory.createContactRecords(RECORD_TYPE_CFA_CONTACT, 'GermanyContact', CONTACT_COUNT, false);
        List<Contact> conUkraineList = CFA_TestDataFactory.createContactRecords(RECORD_TYPE_CFA_CONTACT, 'UkraineContact', CONTACT_COUNT, false);
        List<Contact> conBelgiumList = CFA_TestDataFactory.createContactRecords(RECORD_TYPE_CFA_CONTACT, 'BelgiumContact', CONTACT_COUNT, false);

        List<Contact> conList = new List<Contact>();
        conList.addAll(conChinaList);
        conList.addAll(conGermanyList);
        conList.addAll(conUkraineList);
        conList.addAll(conBelgiumList);
        for (Contact con : conList) {
            con.AccountId = acc.Id;
        }
        insert conList;

        Test.enableChangeDataCapture();
    }

    @isTest
    private static void getTriggerNameTest() {
        String result;
        Test.startTest();
        MembershipChangeEventTriggerHandler handler = new MembershipChangeEventTriggerHandler();
        result = handler.getTriggerName();
        Test.stopTest();

        System.assertEquals('MembershipChangeEventTrigger', result);
    }

    @isTest
    private static void whenJoinMembershipIsCreatedThenCreateCampaignMember() {
        MembershipsWrapper membershipsWrapper = createMemberships('Join');

        Test.startTest();
            System.runAs(CFA_TestDataFactory.getUserWithPermission(PROFILE_INTEGRATION_USER, PERMISSION_SET_EDIT_ACCOUNT_NAME)) {
                insert membershipsWrapper.memberships;
            }
            
            Test.getEventBus().deliver();
        Test.stopTest();

        System.assertEquals(CONTACT_COUNT, [SELECT COUNT() FROM CampaignMember WHERE ContactId IN :membershipsWrapper.conChinaIds]);
        System.assertEquals(CONTACT_COUNT, [SELECT COUNT() FROM CampaignMember WHERE ContactId IN :membershipsWrapper.conBelgiumMap]);
        System.assertEquals(CONTACT_COUNT, [SELECT COUNT() FROM CampaignMember WHERE ContactId IN :membershipsWrapper.conUkraineMap]);
        System.assertEquals(CONTACT_COUNT, [SELECT COUNT() FROM CampaignMember WHERE ContactId IN :membershipsWrapper.conGermanyMap]);
    }

    @isTest
    private static void whenRenewMembershipIsCreatedThenCreateCampaignMember() {
        MembershipsWrapper membershipsWrapper = createMemberships('Renew');

        Test.startTest();
            System.runAs(CFA_TestDataFactory.getUserWithPermission(PROFILE_INTEGRATION_USER, PERMISSION_SET_EDIT_ACCOUNT_NAME)) {
                insert membershipsWrapper.memberships;
            }
            Test.getEventBus().deliver();
        Test.stopTest();

        System.assertEquals(CONTACT_COUNT, [SELECT COUNT() FROM CampaignMember WHERE ContactId IN :membershipsWrapper.conChinaIds]);
        System.assertEquals(CONTACT_COUNT, [SELECT COUNT() FROM CampaignMember WHERE ContactId IN :membershipsWrapper.conBelgiumMap]);
        System.assertEquals(CONTACT_COUNT, [SELECT COUNT() FROM CampaignMember WHERE ContactId IN :membershipsWrapper.conUkraineMap]);
        System.assertEquals(CONTACT_COUNT, [SELECT COUNT() FROM CampaignMember WHERE ContactId IN :membershipsWrapper.conGermanyMap]);
    }

    @isTest
    private static void whenMembershipTransactionTypeChangedToLapseThenRemoveCampaignMember() {
        MembershipsWrapper membershipsWrapper = createMemberships('Join');

        Test.startTest();
            System.runAs(CFA_TestDataFactory.getUserWithPermission(PROFILE_INTEGRATION_USER, PERMISSION_SET_EDIT_ACCOUNT_NAME)) {
                    insert membershipsWrapper.memberships;
                
                    Test.getEventBus().deliver();
                
                    for (Membership__c membership : membershipsWrapper.memberships) {
                        membership.Transaction_Type__c = 'Lapse';
                    }
                    update membershipsWrapper.memberships;
                }
            Test.getEventBus().deliver();
        Test.stopTest();

        System.assertEquals(0, [SELECT COUNT() FROM CampaignMember WHERE ContactId IN :membershipsWrapper.conChinaIds]);
        System.assertEquals(0, [SELECT COUNT() FROM CampaignMember WHERE ContactId IN :membershipsWrapper.conBelgiumMap]);
        System.assertEquals(0, [SELECT COUNT() FROM CampaignMember WHERE ContactId IN :membershipsWrapper.conUkraineMap]);
        System.assertEquals(0, [SELECT COUNT() FROM CampaignMember WHERE ContactId IN :membershipsWrapper.conGermanyMap]);
    }

    @isTest
    private static void whenMembershipTransactionTypeChangedToCancelThenRemoveCampaignMember() {
        MembershipsWrapper membershipsWrapper = createMemberships('Join');

        Test.startTest();
            System.runAs(CFA_TestDataFactory.getUserWithPermission(PROFILE_INTEGRATION_USER, PERMISSION_SET_EDIT_ACCOUNT_NAME)) {
                insert membershipsWrapper.memberships;
                Test.getEventBus().deliver();

                System.assertEquals(CONTACT_COUNT, [SELECT COUNT() FROM CampaignMember WHERE ContactId IN :membershipsWrapper.conChinaIds]);
                System.assertEquals(CONTACT_COUNT, [SELECT COUNT() FROM CampaignMember WHERE ContactId IN :membershipsWrapper.conBelgiumMap]);
                System.assertEquals(CONTACT_COUNT, [SELECT COUNT() FROM CampaignMember WHERE ContactId IN :membershipsWrapper.conUkraineMap]);
                System.assertEquals(CONTACT_COUNT, [SELECT COUNT() FROM CampaignMember WHERE ContactId IN :membershipsWrapper.conGermanyMap]);

                for (Membership__c membership : membershipsWrapper.memberships) {
                    membership.Transaction_Type__c = 'Cancel';
                }
                update membershipsWrapper.memberships;
            }
            Test.getEventBus().deliver();
        Test.stopTest();

        System.assertEquals(0, [SELECT COUNT() FROM CampaignMember WHERE ContactId IN :membershipsWrapper.conChinaIds]);
        System.assertEquals(0, [SELECT COUNT() FROM CampaignMember WHERE ContactId IN :membershipsWrapper.conBelgiumMap]);
        System.assertEquals(0, [SELECT COUNT() FROM CampaignMember WHERE ContactId IN :membershipsWrapper.conUkraineMap]);
        System.assertEquals(0, [SELECT COUNT() FROM CampaignMember WHERE ContactId IN :membershipsWrapper.conGermanyMap]);
    }

    private static MembershipsWrapper createMemberships(String transactionType) {
        Account accChina = [SELECT Id FROM Account WHERE Name = 'CFA China' LIMIT 1];
        Account accGermany = [SELECT Id FROM Account WHERE Name = 'CFA Germany' LIMIT 1];
        Account accUkraine = [SELECT Id FROM Account WHERE Name = 'CFA Ukraine' LIMIT 1];
        Account accBelgium = [SELECT Id FROM Account WHERE Name = 'CFA Belgium' LIMIT 1];

        Map<Id, Contact> conChinaMap = new Map<Id, Contact>([SELECT Id, AccountId FROM Contact WHERE LastName LIKE 'ChinaContact%']);
        Map<Id, Contact> conGermanyMap = new Map<Id, Contact>([SELECT Id, AccountId FROM Contact WHERE LastName LIKE 'GermanyContact%']);
        Map<Id, Contact> conUkraineMap = new Map<Id, Contact>([SELECT Id, AccountId FROM Contact WHERE LastName LIKE 'UkraineContact%']);
        Map<Id, Contact> conBelgiumMap = new Map<Id, Contact>([SELECT Id, AccountId FROM Contact WHERE LastName LIKE 'BelgiumContact%']);

        List<Membership__c> memberships = new List<Membership__c>();
        memberships.addAll(CFA_TestDataFactory.createMemberships(conChinaMap.keySet(), accChina.Id, transactionType, false));
        memberships.addAll(CFA_TestDataFactory.createMemberships(conGermanyMap.keySet(), accGermany.Id, transactionType, false));
        memberships.addAll(CFA_TestDataFactory.createMemberships(conUkraineMap.keySet(), accUkraine.Id, transactionType, false));
        memberships.addAll(CFA_TestDataFactory.createMemberships(conBelgiumMap.keySet(), accBelgium.Id, transactionType, false));

        return new MembershipsWrapper(conChinaMap.keySet(), conGermanyMap.keySet(), conUkraineMap.keySet(), conBelgiumMap.keySet(), memberships);
    }

    private class MembershipsWrapper {
        Set<Id> conChinaIds;
        Set<Id> conGermanyMap;
        Set<Id> conUkraineMap;
        Set<Id> conBelgiumMap;
        List<Membership__c> memberships;

        public MembershipsWrapper(Set<Id> conChinaIds, Set<Id> conGermanyMap, Set<Id> conUkraineMap, Set<Id> conBelgiumMap, List<Membership__c> memberships) {
            this.conChinaIds = conChinaIds;
            this.conGermanyMap = conGermanyMap;
            this.conUkraineMap = conUkraineMap;
            this.conBelgiumMap = conBelgiumMap;
            this.memberships = memberships;
        }
    }
}
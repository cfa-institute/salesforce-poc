/*****************************************************************
Name: CampaignMemberChangeEventTriggerHTest
Copyright © 2020 ITC
============================================================
Purpose: Unit test on CampaignMemberChangeEventTriggerHandler class
============================================================
History
-------
VERSION  AUTHOR          DATE         DETAIL    Description
1.0      Alona Zubenko   10.12.2020   Created   cover update Society Related Contact Fields logic
*****************************************************************/

@IsTest
private class CampaignMemberChangeEventTriggerHTest {
    private static final Integer CONTACT_COUNT = 50;

    @TestSetup
    private static void setup() {
        Test.enableChangeDataCapture();

        List<Account> accList =  CFA_TestDataFactory.createAccountRecords('TestAccount', null , 1 ,true);
        List<Contact> conList = CFA_TestDataFactory.createContactRecords('CFA Contact', 'TestContact', CONTACT_COUNT, false);

        for(Contact con : conList){
            con.AccountId = accList[0].Id;
        }
        insert conList;

        Campaign campCountry1Members = CFA_TestDataFactory.createCampaign('CFA China Members', false);
        campCountry1Members.Type = 'Society';
        campCountry1Members.Subtype__c = 'Members Only';
        insert campCountry1Members;
    }
    
    @isTest
    private static void getTriggerNameTest() {
        String result;
        Test.startTest();
            CampaignMemberChangeEventTriggerHandler handler = new CampaignMemberChangeEventTriggerHandler();
        	result = handler.getTriggerName();
        Test.stopTest();

        System.assertEquals('CampaignMemberChangeEventTrigger', result);
    }

    @IsTest
    private static void updateSocietyRelatedContactFieldsTest(){
        Map<Id,Contact> conMap = new Map<Id, Contact>([SELECT Id FROM Contact]);
        List<Campaign> campList = [SELECT Id FROM Campaign];
        Test.startTest();
        List<CampaignMember> cms = CFA_TestDataFactory.createCampaignMembers(campList[0].Id, conMap.keySet(), null, true);
        Test.getEventBus().deliver();
        Test.stopTest();

        List<CFA_Integration_Log__c> afterInsertList = [SELECT Id FROM CFA_Integration_Log__c];
        System.assert(afterInsertList.size() == 0);
        List<Contact> conAfterInsertList = [SELECT Society_Member__c, Campaign_Relations__c FROM Contact];
        System.assertEquals(true, conAfterInsertList[0].Society_Member__c);
        System.assertEquals('CFA China Members', conAfterInsertList[0].Campaign_Relations__c);
    }

    @IsTest
    private static void catchExceptionTest(){
        Map<Id,Contact> conMap = new Map<Id, Contact>([SELECT Id FROM Contact]);
        List<Campaign> campList = [SELECT Id FROM Campaign];
        List<User> users = CFA_TestDataFactory.createUsers('readOnly@user.com','System Administrator Read Only',1,true);

        Test.startTest();
        System.runAs(users[0]) {
            List<CampaignMember> cms = CFA_TestDataFactory.createCampaignMembers(campList[0].Id, conMap.keySet(), null, false);
            try {
                CampaignMemberChangeEventTriggerHelper.updateSocietyRelatedContactFields(cms);
                System.assert(false);
            }catch (Exception ex){}
        }
        Test.stopTest();
    }
}
@isTest
private class MembershipPolicyHandlerTest {
    private static Integer NUMBER_OF_CONTACTS = 80;
    private static String CFA_CHINA = 'CFA China';
    private static String CFA_GERMANY = 'CFA Germany';
    private static String PROFILE_INTEGRATION_USER = 'Integration profile';
    private static String PERMISSION_SET_EDIT_ACCOUNT_NAME = 'Edit_Account_Name';
    
    @testSetup
    private static void setup() {
        Account accCfaInstitute = CFA_TestDataFactory.createAccount('CFA Institute', 'Society', true);
        
        Map<String, Account> accountByNameMap = new Map<String, Account>{
                CFA_CHINA => CFA_TestDataFactory.createAccount(CFA_CHINA, 'Society', false),
                CFA_GERMANY => CFA_TestDataFactory.createAccount(CFA_GERMANY, 'Society', false)
        };
        insert accountByNameMap.values();
        Map<String, NSA_Policy__c> policyByNameMap = new Map<String, NSA_Policy__c>{
                'NSA China' => CFA_TestDataFactory.createNsaPolicy(accountByNameMap.get(CFA_CHINA).Id, 'CFA_China__c', null, null, null, false),
                'NSA Germany' => CFA_TestDataFactory.createNsaPolicy(accountByNameMap.get(CFA_GERMANY).Id, 'CFA_Society_Germany__c', null, null, null, false)
        };
        insert policyByNameMap.values();
        List<Contact> contacts = CFA_TestDataFactory.createContactRecords('CFA Contact', 'ContactName', NUMBER_OF_CONTACTS, false);
        for(Contact con : contacts){
            con.AccountId = accCfaInstitute.Id;
        }
        ContactTriggerHandler.triggerDisabled = true;
        insert contacts;
        
        List<Membership__c> memberships = new List<Membership__c>();
        Set<Id> conIdsPartOne = new Set<Id>();
        Set<Id> conIdsPartTwo = new Set<Id>();
        for(Integer i = 0; i < contacts.size(); i++) {
            if(i < contacts.size() / 2){
                conIdsPartOne.add(contacts[i].Id);
            } else {
                conIdsPartTwo.add(contacts[i].Id);
            } 
        }
        List<Membership__c> joinMemberships = CFA_TestDataFactory.createMemberships(conIdsPartOne,accountByNameMap.get(CFA_CHINA).Id,'Join',false);
        for(Integer i = 0; i < joinMemberships.size() / 2; i ++) {
            joinMemberships[i].Transaction_Date__c = DateTime.now().addYears(-1);
            joinMemberships[i].Transaction_Type__c = 'Lapse';
        }
        memberships.addAll(joinMemberships);
        List<Membership__c> renewMemberships = CFA_TestDataFactory.createMemberships(conIdsPartTwo,accountByNameMap.get(CFA_GERMANY).Id,'Renew',false);
        for(Integer i = 0; i < renewMemberships.size() / 2; i ++) {
            renewMemberships[i].Transaction_Date__c = DateTime.now().addYears(-1);
            renewMemberships[i].Transaction_Type__c = 'Lapse';
        }
        memberships.addAll(renewMemberships);
    	MembershipTriggerHandler.triggerDisabled = true;
        System.runAs(CFA_TestDataFactory.getUserWithPermission(PROFILE_INTEGRATION_USER,PERMISSION_SET_EDIT_ACCOUNT_NAME)) {
        	insert memberships;
        }
    }
    
    @isTest
    private static void whenJoinRenewOrLapseWithEarlyTransactionDateMemberhipThenUpdateSocietyFieldsToTrue() {
        List<Contact> contacts = [SELECT ID,CFA_China__c, CFA_Society_Germany__c FROM Contact];
        Test.startTest();
        MembershipPolicyHandler hanlder = new MembershipPolicyHandler();
        hanlder.run(
            [SELECT ID,Society_Account__c,Contact_Society_Field_API_Name__c FROM NSA_Policy__c
              WHERE Is_Active__c = TRUE
                AND Policy_Status__c = 'Approved'], 
            contacts
        );
        Test.stopTest();

        List<Contact> cfaChinaContacts = new List<Contact>();
        List<Contact> cfaGermanyContacts = new List<Contact>();
        for(Contact contact : contacts) {
            if(contact.CFA_China__c == true && contact.CFA_Society_Germany__c == false) {
                cfaChinaContacts.add(contact);
            } else if(contact.CFA_China__c == false && contact.CFA_Society_Germany__c == true) {
                cfaGermanyContacts.add(contact);
            }
        }
        System.assertEquals(NUMBER_OF_CONTACTS / 2, cfaChinaContacts.size());
        System.assertEquals(NUMBER_OF_CONTACTS / 2, cfaGermanyContacts.size());
    }
    
    @isTest
    private static void whenLapseWithLateTransactionDateOrCancelMemberhipThenNotUpdateSocietyFields() {
        List<Contact> contacts = [SELECT ID,CFA_China__c, CFA_Society_Germany__c FROM Contact];
        List<Membership__c> memberhips = [SELECT Id,Transaction_Type__c,Transaction_Date__c FROM Membership__c];
        for(Membership__c membership: memberhips) {
            if(membership.Transaction_Type__c == 'Lapse') {
                membership.Transaction_Date__c = DateTime.now().addYears(-3);
            } else {
                membership.Transaction_Type__c = 'Cancel';
                membership.Transaction_Date__c = DateTime.now().addYears(-3);
            }
        }
        MembershipTriggerHandler.triggerDisabled = true;
        System.runAs(CFA_TestDataFactory.getUserWithPermission(PROFILE_INTEGRATION_USER,PERMISSION_SET_EDIT_ACCOUNT_NAME)) {
        	update memberhips;
        }

        Test.startTest();
        MembershipPolicyHandler hanlder = new MembershipPolicyHandler();
        hanlder.run(
            [SELECT ID,Society_Account__c,Contact_Society_Field_API_Name__c FROM NSA_Policy__c
              WHERE Is_Active__c = TRUE
                AND Policy_Status__c = 'Approved'], 
            contacts
        );
        Test.stopTest();
        
        List<Contact> cfaChinaContacts = new List<Contact>();
        List<Contact> cfaGermanyContacts = new List<Contact>();
        for(Contact contact : contacts) {
            if(contact.CFA_China__c == true && contact.CFA_Society_Germany__c == false) {
                cfaChinaContacts.add(contact);
            } else if(contact.CFA_China__c == false && contact.CFA_Society_Germany__c == true) {
                cfaGermanyContacts.add(contact);
            }
        }
        System.assertEquals(0, cfaChinaContacts.size());
        System.assertEquals(0, cfaGermanyContacts.size());
    }
}
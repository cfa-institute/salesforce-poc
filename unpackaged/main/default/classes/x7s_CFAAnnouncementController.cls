/*
 * 	Created by 7Summits
 * 	Controller class for Community displayed Announcements
 */
public class x7s_CFAAnnouncementController {
	// list populated and returned by test so there is no dependency on existing metadata records
	// existing in the environment ()
    public static List<x7s_CFA_Announcement__mdt> testMetadataSamples;
        
    /*
     *  Description: return list of display announcements from Active metadata records found
     *  Parameters: None
     *  Returns: list of x7sCFAAnnouncementWrappers
     */
    @AuraEnabled (Cacheable=true)
    public static List<x7s_CFAAnnouncementWrapper> getAnnouncements(){
        List<x7s_CFAAnnouncementWrapper> announcements = new List<x7s_CFAAnnouncementWrapper>();
        for(x7s_CFA_Announcement__mdt mdtAnnouncement : queryExistingAnnouncements()){
            // create and add new instance of wrapper for each metadata Announcement found
            announcements.add(new x7s_CFAAnnouncementWrapper(mdtAnnouncement.Label, mdtAnnouncement.Announcement_Description__c, mdtAnnouncement.Alert_Type__c));
        }
        
        return announcements;
    }
    /*
     *  Description: 	return list of display announcements from Active metadata records found
	 * 					or list of metadata created during test execution 
     *  Parameters: 	None
     *  Returns: 		list of x7sCFAAnnouncementWrappers
     */
    private static List<x7s_CFA_Announcement__mdt> queryExistingAnnouncements(){
        // return query results if not in test context otherwise return test created list
        if(!Test.isRunningTest()){ return [SELECT Label, Alert_Type__c, Announcement_Description__c FROM x7s_CFA_Announcement__mdt WHERE Active__c = TRUE ORDER BY Alert_Type__c DESC]; }
        else{ return testMetadataSamples; }
    }
    
}
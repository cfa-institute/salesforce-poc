/* 
    # Created By: Shubham Dadhich
    # Created Date: 12/27/2019
    # Description: Handler for cfa_case_ada_files, Use to Fetch and Add Files For ADA Cases.

    ---------------------------------------------------------------------------------------------------------------
    # Modification inforamtion
    - Modified By                 Modified Date               Description                        Associated Task (If)
    # Shubham Dadhich             12/30/2019                  Updating Values In uploadedDate    N/A
    ---------------------------------------------------------------------------------------------------------------
*/

public with sharing class CFA_CaseFileHandler {
    

    /* 
        # Created By: Shubham Dadhich
        # Created Date: 12/27/2019
        # Description: Method to fetch all Document Related To ADA Cases.
        ---------------------------------------------------------------------------------------------------------------
        # Modification inforamtion
        - Modified By                 Modified Date               Description                        Associated Task (If)
        # Shubham Dadhich             12/30/2019                  Updating Values In uploadedDate    N/A
        ---------------------------------------------------------------------------------------------------------------
    */
    @AuraEnabled
    public static List<CFA_CaseFileHandler.ADA_Documents> getAll_ADA_CaseDocuments(String CaseId){
        List<CFA_CaseFileHandler.ADA_Documents> ADA_DocumentToBeSendList = new List<CFA_CaseFileHandler.ADA_Documents>(); // List To Be Used For Lightning Web Component 
        Set<String> CaseRecordIdSet = new Set<String>(); // Set Of Case Id To Be Used For Fetching Files
        Id ADAResourceRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Label.CFA_ADALabel).getRecordTypeId(); // Fetching Record Type Id From Case Object For ADA Request
        String ContactId = [SELECT Id, ContactId FROM Case WHERE Id =: CaseId LIMIT 1].ContactId;

        if(ContactId != null){
            for(Case caseRecord  : [SELECT Id FROM Case WHERE ContactId =: ContactId AND RecordTypeId =: ADAResourceRecordTypeId]){
                CaseRecordIdSet.add(caseRecord.Id);
            }
        }
        if(CaseRecordIdSet.size() > 0){
            for(ContentDocumentLink contentDocumentRecord : [SELECT ContentDocumentId, ContentDocument.LatestPublishedVersionId ,ContentDocument.Title, ContentDocument.FileType, ContentDocument.ContentSize, ContentDocument.CreatedDate, LinkedEntityId  FROM ContentDocumentLink WHERE LinkedEntityId IN: CaseRecordIdSet ]){
                CFA_CaseFileHandler.ADA_Documents adaDocumentFileRecord = new CFA_CaseFileHandler.ADA_Documents();
                adaDocumentFileRecord.fileId = String.valueOf(contentDocumentRecord.ContentDocumentId);
                adaDocumentFileRecord.fileName = contentDocumentRecord.ContentDocument.Title;
                adaDocumentFileRecord.parentId = String.valueOf(contentDocumentRecord.LinkedEntityId);
                adaDocumentFileRecord.size = String.valueOf(Math.round(contentDocumentRecord.ContentDocument.ContentSize/1024));
                adaDocumentFileRecord.fileType = contentDocumentRecord.ContentDocument.FileType;
                adaDocumentFileRecord.latestVersionId = String.valueOf(contentDocumentRecord.ContentDocument.LatestPublishedVersionId);
                adaDocumentFileRecord.uploadedDate = contentDocumentRecord.ContentDocument.CreatedDate.month() + '/'+ contentDocumentRecord.ContentDocument.CreatedDate.day() + '/' + contentDocumentRecord.ContentDocument.CreatedDate.year();
                ADA_DocumentToBeSendList.add(adaDocumentFileRecord);
            }
        }
        return ADA_DocumentToBeSendList;
    }

    /* 
        # Created By: Shubham Dadhich
        # Created Date: 12/27/2019
        # Description: Wrapper Class To Be Connected With Web Component With Detailed Values In It
        ---------------------------------------------------------------------------------------------------------------
        # Modification inforamtion
        - Modified By                 Modified Date               Description                     Associated Task (If)
        #
        ---------------------------------------------------------------------------------------------------------------
    */
    public class ADA_Documents{
        @AuraEnabled public String fileId;
        @AuraEnabled public String fileName;
        @AuraEnabled public String parentId;
        @AuraEnabled public String size;
        @AuraEnabled public String fileType;
        @AuraEnabled public String uploadedDate;
        @AuraEnabled public String latestVersionId;

        public ADA_Documents(){
            this.fileName = '';
            this.fileId = '';
            this.uploadedDate = '';
            this.fileType = '';
            this.size = '';
            this.parentId = '';
            this.latestVersionId='';
        }
    }
}
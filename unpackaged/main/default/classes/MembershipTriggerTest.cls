@isTest
private class MembershipTriggerTest {
    //should be divided by 4
    private static Integer NUMBER_OF_CONTACTS = 80;
    
    private static String CFA_CHINA = 'CFA China';
    private static String CFA_GERMANY = 'CFA Germany';
    private static String CFA_UKRAINE = 'CFA Ukraine';
    private static String CFA_BELGIUM = 'CFA Belgium';    
    private static String PROFILE_INTEGRATION_USER = 'Integration profile';
    private static String PERMISSION_SET_EDIT_ACCOUNT_NAME = 'Edit_Account_Name';

    @TestSetup
    private static void setup() {
        Account accCfaInstitute = CFA_TestDataFactory.createAccount('CFA Institute', 'Society', true);
        
        Map<String, Account> accountByNameMap = new Map<String, Account>{
                CFA_CHINA => CFA_TestDataFactory.createAccount(CFA_CHINA, 'Society', false),
                CFA_GERMANY => CFA_TestDataFactory.createAccount(CFA_GERMANY, 'Society', false),
                CFA_UKRAINE => CFA_TestDataFactory.createAccount(CFA_UKRAINE, 'Society', false),
                CFA_BELGIUM => CFA_TestDataFactory.createAccount(CFA_BELGIUM, 'Society', false)
        };
        insert accountByNameMap.values();
        
        Map<String, NSA_Policy__c> policyByNameMap = new Map<String, NSA_Policy__c>{
                'NSA China' => CFA_TestDataFactory.createNsaPolicy(accountByNameMap.get(CFA_CHINA).Id, 'CFA_China__c', null, null, null, false),
                'NSA Germany' => CFA_TestDataFactory.createNsaPolicy(accountByNameMap.get(CFA_GERMANY).Id, 'CFA_Society_Germany__c', null, null, null, false),
                'NSA Ukraine' => CFA_TestDataFactory.createNsaPolicy(accountByNameMap.get(CFA_UKRAINE).Id, 'CFA_Society_Ukraine__c', null, null, null, false),
                'NSA Belgium' => CFA_TestDataFactory.createNsaPolicy(accountByNameMap.get(CFA_BELGIUM).Id, 'CFA_Society_Belgium__c', null, null, null, false)
        };
        insert policyByNameMap.values();
        
        List<Contact> contacts = CFA_TestDataFactory.createContactRecords('CFA Contact', 'ContactName', NUMBER_OF_CONTACTS, false);
        for(Contact con : contacts){
            con.AccountId = accCfaInstitute.Id;
        }
        insert contacts;
        
        List<Membership__c> memberships = new List<Membership__c>();
        Set<Id> conIdsPartOne = new Set<Id>();
        Set<Id> conIdsPartTwo = new Set<Id>();
        Set<Id> conIdsPartThree = new Set<Id>();
        Set<Id> conIdsPartFour = new Set<Id>();
        for(Integer i = 0; i < contacts.size(); i++) {
            if(i < contacts.size() / 4){
                conIdsPartOne.add(contacts[i].Id);
            } else if(i < contacts.size() / 2){
                conIdsPartTwo.add(contacts[i].Id);
            } else if(i < contacts.size() / 4 * 3){ 
                conIdsPartThree.add(contacts[i].Id);
            } else {
                conIdsPartFour.add(contacts[i].Id);
            }
        }
        memberships.addAll(CFA_TestDataFactory.createMemberships(conIdsPartOne,accountByNameMap.get(CFA_CHINA).Id,'Join',false));
        memberships.addAll(CFA_TestDataFactory.createMemberships(conIdsPartTwo,accountByNameMap.get(CFA_GERMANY).Id,'Join',false));
        memberships.addAll(CFA_TestDataFactory.createMemberships(conIdsPartThree,accountByNameMap.get(CFA_UKRAINE).Id,'Renew',false));
        memberships.addAll(CFA_TestDataFactory.createMemberships(conIdsPartFour,accountByNameMap.get(CFA_BELGIUM).Id,'Renew',false));
        System.runAs(CFA_TestDataFactory.getUserWithPermission(PROFILE_INTEGRATION_USER,PERMISSION_SET_EDIT_ACCOUNT_NAME)) {
    		insert memberships;
        }
    }
    
    @isTest
    private static void whenJoinORRenewMembershipIsCreatedThenChangeContactSocietyFieldToTrue() {
        System.assertEquals(NUMBER_OF_CONTACTS / 4, [SELECT count() FROM Contact WHERE CFA_China__c = true]);
        System.assertEquals(NUMBER_OF_CONTACTS / 4, [SELECT count() FROM Contact WHERE CFA_Society_Germany__c = true]);
        System.assertEquals(NUMBER_OF_CONTACTS / 4, [SELECT count() FROM Contact WHERE CFA_Society_Ukraine__c = true]);
        System.assertEquals(NUMBER_OF_CONTACTS / 4, [SELECT count() FROM Contact WHERE CFA_Society_Belgium__c = true]);
    }
    
    @isTest
    private static void whenMembershipTransactionTypeIsCanceledORLapsedThenSetContactSocietyFieldsToFalse() {
        List<Membership__c> memberships = [SELECT ID, Transaction_Date__c, Transaction_Type__c FROM Membership__c];
        for(Integer i = 0; i < memberships.size(); i++) {
            memberships[i].Transaction_Date__c = DateTime.now().addYears(-3);
            memberships[i].Transaction_Type__c = 'Lapse';
        }
        Test.startTest();
        System.runAs(CFA_TestDataFactory.getUserWithPermission(PROFILE_INTEGRATION_USER,PERMISSION_SET_EDIT_ACCOUNT_NAME)) {
        	update memberships;
        }
        Test.stopTest();
        System.assertEquals(0, [SELECT count() FROM Contact WHERE CFA_China__c = true]);
        System.assertEquals(0, [SELECT count() FROM Contact WHERE CFA_Society_Germany__c = true]);
        System.assertEquals(0, [SELECT count() FROM Contact WHERE CFA_Society_Ukraine__c = true]);
        System.assertEquals(0, [SELECT count() FROM Contact WHERE CFA_Society_Belgium__c = true]);
    }
    
    @isTest
    private static void whenMembershipIsDeletedThenSetContactSocietyFieldsToFalse() {
        Test.startTest();
        delete [SELECT ID FROM Membership__c];
        Test.stopTest();
        
        System.assertEquals(0, [SELECT count() FROM Contact WHERE CFA_China__c = true]);
        System.assertEquals(0, [SELECT count() FROM Contact WHERE CFA_Society_Germany__c = true]);
        System.assertEquals(0, [SELECT count() FROM Contact WHERE CFA_Society_Ukraine__c = true]);
        System.assertEquals(0, [SELECT count() FROM Contact WHERE CFA_Society_Belgium__c = true]);
    }
}
/*****************************************************************
Name: RevertOutreachContactsBatch
Copyright © 2021 ITC
============================================================
Purpose:
============================================================
History
-------
VERSION  AUTHOR          DATE         DETAIL    Description
1.0      Alona Zubenko   17.05.2021   Created   CSR:
*****************************************************************/

public with sharing class RevertOutreachContactsBatch implements Database.Batchable<SObject>, Database.Stateful {
    private final String queryForContacts = '' +
            'SELECT Id\n' +
            '                FROM Contact\n' +
            '                WHERE RecordType.DeveloperName != \'CFA_Contact\'\n' +
            '                AND Id IN (\n' +
            '                        SELECT ContactId\n' +
            '                        FROM ContactHistory\n' +
            '                        WHERE Field = \'Account\'\n' +
            '                        AND CreatedDate > 2021-05-13T13:00:00Z\n' +
            '                        AND CreatedById = \'005j000000CMIo4AAH\'\n' +
            '                )\n' +
            '                AND LastModifiedDate < 2021-05-17T03:00:00Z\n' +
            '                AND LastModifiedDate > 2021-05-13T13:00:00Z\n' +
            '                ORDER BY Id DESC';
    private final Set<String> societyFieldNames;

    public RevertOutreachContactsBatch() {
        societyFieldNames = new Set<String>();

        List<NSA_Policy__c> policies = [
                SELECT Contact_Society_Field_API_Name__c
                FROM NSA_Policy__c
                WHERE Is_Active__c = TRUE
                AND Policy_Status__c = 'Approved'
        ];

        for (NSA_Policy__c policy : policies) {
            societyFieldNames.add(policy.Contact_Society_Field_API_Name__c);
        }
    }

    /**
     * @description Start
     *
     * @param context Database.BatchableContext
     *
     * @return Database.QueryLocator
     */
    public Database.QueryLocator start(Database.BatchableContext context) {
        return Database.getQueryLocator(queryForContacts);
    }

    /**
     * @description Execute
     *
     * @param context Database.BatchableContext
     * @param affectedContacts List<Contact>
     */
    public void execute(Database.BatchableContext context, List<Contact> affectedContacts) {
        Map<Id, Contact> contactsToUpdate = new Map<Id, Contact>();

        for (ContactHistory cHistory : [
                SELECT ContactId
                        , NewValue
                        , OldValue
                FROM ContactHistory
                WHERE Field = 'Account'
                AND ContactId IN :affectedContacts
                AND DataType = 'EntityId'
                AND CreatedById = '005j000000CMIo4AAH'
                ORDER BY CreatedDate ASC
        ]) {
            String accId = (String) cHistory.OldValue;
            contactsToUpdate.put(cHistory.ContactId, new Contact(Id = cHistory.ContactId, AccountId = accId));
        }

        for (Contact con : contactsToUpdate.values()) {
            for (String societyField : societyFieldNames) {
                con.put(societyField, false);
            }
        }

        List<Database.SaveResult> updateResults = Database.update(contactsToUpdate.values(), false);

        List<CFA_Integration_Log__c> exceptions = new List<CFA_Integration_Log__c>();
        Integer i = 0;
        for (Database.SaveResult result : updateResults) {
            Contact origRecord = contactsToUpdate.values()[i];
            if (!result.isSuccess()) {
                for (Database.Error error : result.getErrors()) {
                    exceptions.addAll(CFA_IntegrationLogException.logError(new RevertOutreachContactsBatchException(origRecord.Id + ' : ' + error.getMessage()), 'RevertOutreachContactsBatch', 'Revert Contacts', Datetime.now(), true, '', '', '', true, ''));
                }
            }
            i++;
        }
        if (!exceptions.isEmpty()) {
            insert exceptions;
        }

    }

    /**
     * @description Finish
     *
     * @param param1 Database.BatchableContext
     */
    public void finish(Database.BatchableContext param1) {
    }

    private class RevertOutreachContactsBatchException extends Exception {
    }

}
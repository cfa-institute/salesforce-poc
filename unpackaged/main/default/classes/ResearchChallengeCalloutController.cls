/**************************************************************************************************
* Apex Class Name   : ResearchChallengeCalloutController
* Purpose           : This class is used for making API Callout to BERT System to send updated paricipants data in Salesforce
* Version           : 1.0 Initial Version
* Organization      : 
* Created Date      : 02-AUG-2018
* Updated Date      : 11-15-2018
* Updated Date      : 17-07-2019 <removed debug statements and commented code>
***************************************************************************************************/

public with sharing class ResearchChallengeCalloutController{ 
    
    
    // This Method is used to invoke an outbound Call from 'ProcessBuilderName'
    
    @InvocableMethod(label='ParticipatUpdateCallout' description='ParticipatUpdateCallout')
    Public static void ParticipatUpdateCallout(List<Id> ParticipantID){
        JSONGenerator ReoutJson = System.JSON.createGenerator(true);
        List<RC_Participant__c> PartList = new List<RC_Participant__c>();
        
        // Get the participants from SFDC based on Participant ID's passed from Process builder
        
        If(ParticipantID != null){
            PartList = [select id,Email__c,Challenge_Status__c,Challenge_Name__c,Faculty_University_Name__r.name,
                        Faculty_Advisor__r.name,
                        Regional_Challenge__r.Name, Local_Challenge__r.Name, 
                        Local_Challenge__r.Competition_Date__c, Regional_Challenge__r.Competition_Date__c, Global_Challenge__r.Competition_Date__c,
                        Local_Challenge__r.Location__C,Global_Challenge__r.Name, Challenge_Date_To_BERT__c
                        from RC_Participant__c  where id IN :ParticipantID];
        }
           
        // Generate Json as per given request Body       
        
        ReoutJson.writeStartArray();
        for(RC_Participant__c RCP : PartList){
              
        //Assign competetion date
            DateTime CompetetionDate;
            If (RCP.Challenge_Date_To_BERT__c != null){
            CompetetionDate=RCP.Challenge_Date_To_BERT__c;
            }
            else{
                CompetetionDate = DateTime.now();
                }
            String newDate = String.valueOfGmt(CompetetionDate);

            //Assign Challenge name       
            String BERTName = RCP.Challenge_Name__c;
               BERTName = RCP.Challenge_Name__c;

            //Check Location name is not null
            
            String Location;  
            If (RCP.Local_Challenge__r.Location__C != Null){
                Location = RCP.Local_Challenge__r.Location__C;
            }
            Else {
                Location = 'Null';
            }
            
            
            //Check Faculty_Advisor name is not null
            String facAdvisor ;  
            If (RCP.Faculty_Advisor__r.Name!= Null){
                facAdvisor = RCP.Faculty_Advisor__r.Name;
            }
            Else {
                facAdvisor ='';
            }
            
            // Json Start 
            ReoutJson.writeStartObject();  
            
            ReoutJson.writeStringField('Email', RCP.Email__c);
            ReoutJson.writeStringField('Status', RCP.Challenge_Status__c);
            ReoutJson.writeStringField('ResearchChallengeName', BERTName);    
            ReoutJson.writeStringField('CompetitionDate', newDate);    
            ReoutJson.writeStringField('Location', Location);    
            ReoutJson.writeStringField('UniversityName', RCP.Faculty_University_Name__r.name); 
            ReoutJson.writeStringField('FacultyAdviser', facAdvisor );    

            ReoutJson.writeEndObject();    
        }      
        ReoutJson.writeEndArray();
        
        String ReoutJsonFinal = ReoutJson.getAsString();
        
        // Call Future Method
        try{
            MakeCalltoBert(ReoutJsonFinal);
        }
        catch(Exception e){
            
        }
          
    }
    @Future(callout=true)
    Public Static void MakeCalltoBert(String Body){
        
        // Initiate the errorCode Map

        Map<Integer, String> httpErrorCodeMap = new Map<Integer, String>();
        List<CustomerCare_ApiCodeMessages__c> listCodes = CustomerCare_ApiCodeMessages__c.getAll().values();  
        
        if(!listCodes.isEmpty()){
            // Custom setting for handling negative scenario of Error Code
            for(CustomerCare_ApiCodeMessages__c apiCode : listCodes){
                if(apiCode.CustomerCare_Error_Code__c != NULL && apiCode.CustomerCare_ErrorMessage__c != NULL)
                    httpErrorCodeMap.put(Integer.valueof(apiCode.CustomerCare_Error_Code__c),apiCode.CustomerCare_ErrorMessage__c);
            }
        }    
        
        // Build the HTTP Request and Add Certificate(MDM_Swagger_Certificate) for Authentication.     
        
        String ParticipantEP = System.Label.ResearchChallengeParticipantsLink ;    
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(ParticipantEP );
        req.setClientCertificateName(Label.MDM_Swagger_Certificate);
        req.setMethod('POST');
        req.setHeader('Content-Type', 'application/json');
        req.setHeader('Accept', 'application/json');

        //Set content length
        Integer Contentlength=body.length();
        req.setHeader('Content-Length', String.valueOf(contentlength));       
        req.setbody(body);
        
        
        HttpResponse response1 = h.send(req); 
        
        if(!httpErrorCodeMap.isEmpty() && httpErrorCodeMap.containsKey(response1.getStatusCode())){
    CFAIntegrationLog.writeLog('ResearchChallengeCalloutController.MakeCalltoBert','NA' ,String.valueOf(response1.getStatusCode()), response1.getBody());
        }
    }
    
    
}
/**************************************************************************************************************************************************
Name:  VM_CreateRoleController
Copyright © 2018  ITC
=====================================================================
Purpose: 1. Controller Class for VM_CreateRole.vfp                                                                                                
============================================================
History                                                            
-------                                                            
VERSION  AUTHOR         DATE           DETAIL          Description
1.0                  06/15/2018        Created         Created the class

****************************************************************************************************************************************************/
public class VM_CreateRoleController {
	
    private ApexPages.StandardController controller {get; set;}
    private Role__c objRole;
    public string roleId {get;set;}
    public String queryString {get;set;}
    public String recordTypeSelection {get;set;}
    public String markupJSON {get;set;}
    public Job_Application__c objJob {get;set;} 
    
    public VM_CreateRoleController(ApexPages.StandardController controller) {

        //initialize the stanrdard controller
        this.controller = controller;
        this.objRole = (Role__c)controller.getRecord();
        objJob = new Job_Application__c();
        
        PageReference thisPage = ApexPages.currentPage();
        List<String> url = thisPage.getUrl().split('\\?');
        queryString = url[1];
        
        this.roleId = objRole.Id;
    }
    
    /****************************************************************************************************************************
       Purpose:This method will fetch the all redcord type from Job_Application__c object.                         
       Parameters: 
       Returns: 
    
       History                                                            
       --------                                                           
       VERSION  AUTHOR         DATE           DETAIL          Description
       1.0      Nikhil        06/15/2018             
    ******************************************************************************************************************************/ 
    public List<selectOption> getRecordTypes(){
        
        List<selectOption> lstRecordTypes = new List<selectOption>();  
        
        for(RecordType info : [Select Id, Name from RecordType where sobjectType='Job_Application__c']){
           lstRecordTypes.add(new SelectOption(info.Id, info.Name));
        }
        
        return lstRecordTypes;
    }
    
    /****************************************************************************************************************************
       Purpose:This method will fetch the all redcord type from Job_Application__c object.                         
       Parameters: 
       Returns: 
    
       History                                                            
       --------                                                           
       VERSION  AUTHOR         DATE           DETAIL          Description
       1.0      Nikhil        06/15/2018             
    ******************************************************************************************************************************/
    public String getRecordType() {
        List<RecordType> kstrRecord = [Select Id, Name from RecordType where sobjectType='Job_Application__c'];
        return JSON.serialize(kstrRecord);
    }
    
    public PageReference dummy() {
        return null;
    }
    
     public Pagereference SaveAndNew(){
         try {
             //controller.save();
             
             upsert objRole;
             Schema.DescribeSObjectResult describeResult = controller.getRecord().getSObjectType().getDescribe();
             
             PageReference pr = new PageReference('/' + describeResult.getKeyPrefix() + '/e?' + queryString);
             pr.setRedirect(true);
             return pr;  
             
         } catch(System.DMLException e) {
             system.debug('---5656--'+e.getMessage());
             return null;
         }
  	}
    
    public class markupCLass {
        public String name ;
        public String Columns;
        public String Collapsible;
        public List<fields> field ;
    }
    
    public Class fields {
        public String field;
        public String required;
    }
}
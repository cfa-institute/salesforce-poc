public with sharing class RunPolicyController {

    @AuraEnabled
    public static void runPolicy(String policyId) {
        try {
            List<NSA_Policy__c> policies = 
                [SELECT Id, Advanced_Assignment__c, Advanced_Assignment_Handler__c, 
                 		Society_Account__c, Contact_Society_Field_API_Name__c, 
                         (SELECT Country_Name__c, State__c, Zip_Code_High__c, 
                          		 Zip_Code_Low__c, NSA_Policy__c 
                            FROM NSA_Geograhpic_Rules__r)
                  FROM NSA_Policy__c
                 WHERE Id = :policyId AND Is_Active__c = true AND Policy_Status__c = 'Approved'];
            if(policies.size() > 0) {
                List<INSAPolicyHandler> handlers = new List<INSAPolicyHandler>();
                AddressPolicyHandler addressPolicyHandler = new AddressPolicyHandler(true, true);
                MembershipPolicyHandler membershipPolicyHandler = new MembershipPolicyHandler();
                MembershipApplicationPolicyHandler membershipApplicationPolicyHandler = new MembershipApplicationPolicyHandler();
                DeceasedPolicyHandler deceasedPolicyHandler = new DeceasedPolicyHandler();
                handlers.add(addressPolicyHandler);
                handlers.add(membershipPolicyHandler);
                handlers.add(membershipApplicationPolicyHandler);
                handlers.add(deceasedPolicyHandler);
                NSAAssignmentBatch nsaAssignmentBatch = new NSAAssignmentBatch(
                    handlers,
                    policies,
                    'WHERE RecordType.DeveloperName = \'CFA_Contact\'',
                    true
                );
                Database.executeBatch(
                    nsaAssignmentBatch,
                    200
                );
            }
        } catch (Exception ex) {
            insert CFA_IntegrationLogException.logError(ex, 'Run Policy Batch','RunPolicy', DateTime.now(), true, '', '', '', true, '');
            throw ex;
        }
    }
}
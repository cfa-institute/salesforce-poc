@isTest
private class MDM_ORContacts_Affilaitions_Test {
    
    @testSetup static void setup() {
         
		Account newAccount = CFA_TestDataFactory.createAccount('CFA Society New York', null, true);

        List<Contact> newContacts = CFA_TestDataFactory.createContactRecords('Local Member', 'TestConts', 3, true);
         
		List<Affiliation__c> lstAffiliations = new List<Affiliation__c>();
		for (Contact newContact : newContacts) {
			lstAffiliations.add (
				CFA_TestDataFactory.createAffiliations(
					newAccount.Id,
					newContact.Id,
					'Advocacy Outreach',
					Date.today().addDays(-9),
					Date.today().addDays(5),
					'University',
					false
				)
			);
			lstAffiliations.add (
				CFA_TestDataFactory.createAffiliations(
					newAccount.Id,
					newContact.Id,
					'Industry Outreach',
					Date.today().addDays(-8),
					Date.today().addDays(4),
					'University',
					false
				)
			);
			lstAffiliations.add (
				CFA_TestDataFactory.createAffiliations(
					newAccount.Id,
					newContact.Id,
					'University Outreach',
					Date.today().addDays(-7),
					Date.today().addDays(3),
					'University',
					false
				)
			); 
		}
        insert lstAffiliations;
    }
    
    private static testMethod void test() {
        
        Account objAccount=[ SELECT Id FROM Account LIMIT 1];
        
        Contact objCon = [SELECT Id FROM Contact LIMIT 1];
        
        Affiliation__c objAff = [SELECT Id FROM Affiliation__c WHERE Account__c =: objAccount.Id LIMIT 1];
        
        Test.startTest();
            MDM_ORContacts_Affilaitions obj = new MDM_ORContacts_Affilaitions(new ApexPages.StandardController(objAccount));
            
            ApexPages.currentPage().getParameters().put('strContactId', objCon.Id);
            obj.editContactRecord();
            
            ApexPages.currentPage().getParameters().put('strAffiliationId', objAff.Id);
            obj.editAffiliationRecord();
            
            obj.newContact();
            obj.newAffiliation();
            
            ApexPages.currentPage().getParameters().put('strContactId', objCon.Id);
            //obj.delContactRecord();
            
            ApexPages.currentPage().getParameters().put('strAffiliationId', objAff.Id);
            obj.delAffiliationRecord();
            
           // system.assert(!obj.lstContactsToDisplay.isEmpty());
            
        Test.stopTest();
    }
}
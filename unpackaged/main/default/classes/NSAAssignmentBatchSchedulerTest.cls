/*****************************************************************
Name: NSAAssignmentBatchSchedulerTest
Copyright © 2021 ITC
============================================================
Purpose: Unit test for Schedule job for NSA Assignment Batch
============================================================
History
-------
VERSION  AUTHOR          DATE         DETAIL    Description
1.0      Alona Zubenko   15.02.2021   Created   Schedule job for NSA Assignment Batch
*****************************************************************/

@IsTest
private class NSAAssignmentBatchSchedulerTest {

    @TestSetup
    private static void init() {
        Account accCfaInstitute = CFA_TestDataFactory.createAccount('CFA Institute', 'Society', true);
    }

    @IsTest
    static void runTest() {
        Test.startTest();
        NSAAssignmentBatchScheduler.run();
        Test.stopTest();

        List<CronTrigger> jobs = [SELECT Id FROM CronTrigger WHERE CronJobDetail.Name = 'NSA Assignment Batch Schedule Job'];
        System.assert(!jobs.isEmpty());
    }

    @IsTest
    static void stopTest() {
        Test.startTest();
        NSAAssignmentBatchScheduler.run();
        NSAAssignmentBatchScheduler.stop();
        Test.stopTest();

        List<CronTrigger> jobs = [SELECT Id FROM CronTrigger WHERE CronJobDetail.Name = 'NSA Assignment Batch Schedule Job'];
        System.assert(jobs.isEmpty());
    }
}
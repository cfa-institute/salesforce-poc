/**
 *  Copyright CFA Institute
 *
 * Test the custom setting for configuration
 *
 * @group Test Layer
 **/
@isTest
private class CFA_ConfigurationTest {
    @isTest
    static void testexcludeTrigger() {
        // assign
        string excludeObject = 'Account';

        testInitializeTrigger(excludeObject);

        // act
        Boolean result = CFA_Configuration.excludeTrigger(excludeObject);


        // assert
        system.assertEquals(true, result);

    }
    @isTest
    static void testDebug() {
        // assign

        testInitializeDebug(false);

        // act
        Boolean result = CFA_Configuration.isDebug();

        // assert
        system.assertEquals(false, result);

    }
    @isTest
    static void testexcludeTriggerWithMultipleObjects() {
        // assign
        string excludeObject = 'Account,Contact,Lead,Opportunity';

        testInitializeTrigger(excludeObject);

        // act
        Boolean result = CFA_Configuration.excludeTrigger('Lead');


        // assert
        system.assertEquals(true, result);

    }

    @isTest
    static void testexcludeTriggerWithMultipleObjectsWithException() {
        // assign
        string excludeObject = 'Account,Contact,Lead,Opportunity';


        testInitializeTrigger(excludeObject);
        // cause an exception
        CFA_Configuration.m_forUnitTesting = true;
        // act
        Boolean result = CFA_Configuration.excludeTrigger('Lead');
        // assert
        system.assertEquals(false, result);
        system.assertEquals(true, cfa_ApexUtilities.containsInTestLog('Exception'));

    }

    @isTest
    static void testfindItemToExclude() {
        // assign
        string excludeObject = 'Account,Contact,Lead,Opportunity';


        testInitializeTrigger(excludeObject);
        // cause an exception
        CFA_Configuration.m_forUnitTesting = true;
        // act
        Boolean result = CFA_Configuration.findExcludeTrigger('Lead');
        // assert
        system.assertEquals(false, result);
        system.assertEquals(true, cfa_ApexUtilities.containsInTestLog('Exception'));

    }

    @isTest
    static void testexcludeValidation() {
        // assign
        string excludeObject = 'Account';

        testInitializeValidation(excludeObject);

        // act
        Boolean result = CFA_Configuration.excludeValidation(excludeObject);


        // assert
        system.assertEquals(true, result);

    }

    @isTest
    static void testexcludePB() {
        // assign
        string excludeObject = 'Account';

        testInitializePB(excludeObject);

        // act
        Boolean result = CFA_Configuration.excludePB(excludeObject);


        // assert
        system.assertEquals(true, result);

    }

    @isTest
    static void testexcludeTriggerWhichShouldNotBe() {
        // assign
        string excludeObject = 'Account';

        testInitializeTrigger(excludeObject);

        // act
        Boolean result = CFA_Configuration.excludeTrigger('Contact');

        // assert
        system.assertEquals(false, result);

    }

    @isTest
    static void testexcludePBWhichShouldNotBe() {
        // assign
        string excludeObject = 'Account';

        testInitializePB(excludeObject);

        // act
        Boolean result = CFA_Configuration.excludePb('Contact');

        // assert
        system.assertEquals(false, result);

    }

    @isTest
    static void testexcludeValidationWhichShouldNotBe() {
        // assign
        string excludeObject = 'Account';

        testInitializeValidation(excludeObject);

        // act
        Boolean result = CFA_Configuration.excludeValidation('Contact');

        // assert
        system.assertEquals(false, result);

    }
    @isTest
    static void testHandlerWhichShouldBePresent() {
        // assign
        string includeMethod = 'AccountDomain';

        testInitializeHandler(includeMethod);

        // act
        Boolean result = CFA_Configuration.findHandlerName(includeMethod);

        // assert
        system.assertEquals(true, result);

    }

    @isTest
    static void testincludeHandlerName() {
        string includeMethod = 'AccountDomain';

        testInitializeHandler(includeMethod);

        // act
        Boolean result = CFA_Configuration.includeHandlerName(includeMethod);

        // assert
        system.assertEquals(true, result);

    }
 
    @isTest
    static void testHandlerWhichShouldNotBePresent() {
        // assign
        string handler = 'Account';

        testInitializeHandler(handler);

        // act
        Boolean result = CFA_Configuration.findHandlerName(handler + 'No');

        // assert
        system.assertEquals(false, result);

    }

    @isTest
    static void testHandlerEmailNotification() {
        // assign
        string email = 'test@email.com';

        testInitializeNotify(email);

        // act
        String result = CFA_Configuration.getNotifyUserEmailAddress();

        // assert
        system.assertEquals(email, result);

    }

    @isTest
    static void testPrivate() {
        // assign

        testInitializePrivate(false);

        // act
        Boolean result = CFA_Configuration.allowPrivateContacts();
        // assert
        system.assertEquals(false, result);

    }

    @isTest
    static void testallowRolloverDummyAccounts() {
        // assign
        testInitializeRollover(false);

        // act
        Boolean result = CFA_Configuration.allowRolloverDummyAccounts();

        // assert
        system.assertEquals(false, result);

    }

    ////////////////////////////////////////////////////////////////////////
    // Private Methods
    /////////////////////////////////////////////////////////////////////////

    static void testInitializeDebug(boolean isDebug) {
        testInitialize(isDebug, '', '', '', '', '', true, true);

    }
    static void testInitializeTrigger(string data) {
        testInitialize(false, data, '', '', '', '', true, true);

    }
    static void testInitializeValidation(string data) {
        testInitialize(false, '', data, '', '', '', true, true);

    }
    static void testInitializePB(string data) {
        testInitialize(false, '', '', data, '', '', true, true);

    }
    static void testInitializeNotify(string data) {
        testInitialize(false, '', '', '', data, '', true, true);

    }
    static void testInitializeHandler(string data) {
        testInitialize(false, '', '', '', '', data, true, true);

    }
    static void testInitializePrivate(boolean isPrivate) {
        testInitialize(false, '', '', '', '', '', isPrivate, true);
    }
    static void testInitializeRollover(boolean isRollover) {
        testInitialize(false, '', '', '', '', '', true, isRollover);
    }

    static void testInitialize(boolean isDebug,
                               string excludeTrigger,
                               string excludeValidation,
                               string excludePB,
                               string notify,
                               string handler,
                               boolean allowPrivateAccounts,
                               Boolean allowAccountRollover
                              ) {

        CFA_Configuration__c config = new CFA_Configuration__c(
            SetupOwnerId = UserInfo.getUserId(),
            Debug__c = isDebug,
            Exclude_PB_by_Objects__c = excludePB,
            Exclude_Triggers_By_Objects__c = excludeTrigger,
            Exclude_Validation_by_Objects__c = excludeValidation,
            Notification__c = notify,
            Include_Handler_Names__c = handler,
            Rollover_Dummy_Accounts__c = allowAccountRollover,
            Allow_Private_Contacts__c = allowPrivateAccounts

        );

        upsert config;

    }
}
/**************************************************************************************************
* Apex Class Name   : CustomerCare_CreateEmailForContact_Test
* Purpose           : This test class is used for validating CustomerCare_ImageUrlOnET_Test    
* Version           : 1.0 Initial Version
* Organization      : Cognizant
* Created Date      : 15-Nov-2017  
***************************************************************************************************/
@isTest
public class CustomerCare_ImageUrlOnET_Test {   
    static testMethod void EmailTemplateImageURLTest(){
        CustomerCare_CommonTestData.TestDataImageurl();
        string result=CustomerCare_ImageUrlOnEmailTemplate.getImageUrl();
        system.assertEquals('https://test.com',result );
        
       
    
    }
}
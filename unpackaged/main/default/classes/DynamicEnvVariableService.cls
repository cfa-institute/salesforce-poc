global class DynamicEnvVariableService {
  //public static Map <String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
    @AuraEnabled
    public static String  genericdatafetcher(string n){
        String currentinstance = URL.getSalesforceBaseUrl().toExternalForm();    
        map<string , schema.SObjectField > mcustfields = schema.SObjectType.CFA_EnvSpecificService__mdt.fields.getMap();
        list<schema.SObjectField> fields = mcustfields.values();
        string dynamicSOQLString = 'SELECT ';
        for(schema.SObjectField s :fields ){
            String theName = s.getDescribe().getName();
            dynamicSOQLString += theName + ','; 
        }
        dynamicSOQLString = dynamicSOQLString.removeEnd(',');
        dynamicSOQLString += ' from  CFA_EnvSpecificService__mdt where QualifiedApiName= ';
        dynamicSOQLString +=  '\''+ n + '\'';
        list<CFA_EnvSpecificService__mdt> records = Database.query(dynamicSOQLString );
        String OrgURLMetadata = '';
        //Boolean isSandbox = [SELECT IsSandbox FROM Organization LIMIT 1].IsSandbox;
        String isSandbox = URL.getSalesforceBaseUrl().getHost().substringBetween('--','.');
        Boolean isFound = false;
        for (CFA_EnvSpecificService__mdt c : records){
            for(schema.SObjectField s :fields){
            if(isFound == false){
                String label = s.getDescribe().getLabel();
                String devloperName = s.getDescribe().getName();
                //System.debug('test@currentinstance :'+currentinstance);
                //System.debug('test@label :'+label);
                //System.debug('test@devloperName :'+devloperName );
                    if(currentinstance.containsIgnoreCase(label)){
                        isFound = true;
                        OrgURLMetadata  = (String)c.get(devloperName);
                        //System.debug('inside if :'+OrgURLMetadata );
                    }
                    else if(isSandBox == null){
                        OrgURLMetadata = (String)c.get('Production__c');
                        //System.debug('inside else :'+OrgURLMetadata );   
                    }
                }
            }         
        }
        //system.debug('currentinstance' +currentinstance);  
        return OrgURLMetadata;        
    }
}
public class MassRecognitionCreationClass {

    public List<Engagement_Volunteer__c> engVolList{get;set;}
    public List<EngagementVolunteerWrapper> engVolWrapperList{get;set;}
    public List<EngagementVolunteerWrapper> selectedVolunteers{get;set;}
    public List<Recognition__c> recList;
    public Recognition__c rec{get;set;}
    public boolean checkSelect{get;set;}
    public String engId;
    Public Integer size{get;set;}
    Public Integer noOfRecords{get; set;}
    public List<SelectOption> paginationSizeOptions{get;set;}
    public static final Integer QUERY_LIMIT = 10000;
    public static final Integer PAGE_SIZE = 15;
    Map<Id,EngagementVolunteerWrapper> mapHoldingSelectedRecords{get;set;}

    /*public MassRecognitionCreationClass4(ApexPages.StandardController controller) {
             
    }*/
    public MassRecognitionCreationClass(){
        mapHoldingSelectedRecords = new Map<Id, EngagementVolunteerWrapper>();
        init();
        rec=new Recognition__c();
    }
     public void init() {
         engVolWrapperList = new List<EngagementVolunteerWrapper>();
         for (Engagement_Volunteer__c vol: (List<Engagement_Volunteer__c>)setVol.getRecords()) {
             if(mapHoldingSelectedRecords != null && mapHoldingSelectedRecords.containsKey(vol.id)){
                 engVolWrapperList.add(mapHoldingSelectedRecords.get(vol.id));
            
             }
             else{
               engVolWrapperList.add(new EngagementVolunteerWrapper(vol, false));
             }
          }
    }
    public ApexPages.StandardSetController setVol{
     get {
     if(setVol == null) {
     engId=ApexPages.currentPage().getParameters().get('eng');            
     setVol = new ApexPages.StandardSetController(Database.getQueryLocator([Select Id,Engagement__c,Engagement__r.name,Role_del__c,Role_del__r.name,Contact__c,Contact__r.name from Engagement_Volunteer__c where Engagement__c=:engId and Engagement_Volunteer__c.Status__c!='Off-boarded']));
    
       // sets the number of records to show in each page view
       setVol.setPageSize(PAGE_SIZE);
     }
       return setVol;
     }
     set;
    }
    public Boolean hasNext {
 get {
   return setVol.getHasNext();
 }
 set;
 }

 /** indicates whether there are more records before the current page set.*/
 public Boolean hasPrevious {
 get {
   return setVol.getHasPrevious();
 }
 set;
 }

 /** returns the page number of the current page set*/
 public Integer pageNumber {
 get {
   return setVol.getPageNumber();
 }
 set;
 }

 /** return total number of pages for page set*/
   Public Integer getTotalPages(){
     Decimal totalSize = setVol.getResultSize();
     Decimal pageSize = setVol.getPageSize();
     Decimal pages = totalSize/pageSize;
     return (Integer)pages.round(System.RoundingMode.CEILING);
 }

 /** returns the first page of the page set*/
 public void first() {
   updateSearchItemsMap();
   setVol.first();
   init();
 }

 /** returns the last page of the page set*/
 public void last() {
   updateSearchItemsMap();
   setVol.last();
   init();
 }

 /** returns the previous page of the page set*/
 public void previous() {
   updateSearchItemsMap();
   setVol.previous();
   init();
 }

 /** returns the next page of the page set*/
 public void next() {
   updateSearchItemsMap();
   setVol.next();
   init();
 }

 //This is the method which manages to remove the deselected records, and keep the records which are selected in map.
 private void updateSearchItemsMap() {
 for(EngagementVolunteerWrapper wrp : engVolWrapperList){
     
  if(wrp.isSelected){
     mapHoldingSelectedRecords.put(wrp.engVol.id, wrp);
     system.debug('niha');
  }
  if(wrp.isSelected == false && mapHoldingSelectedRecords.containsKey(wrp.engVol.id)){
     mapHoldingSelectedRecords.remove(wrp.engVol.id);
  }
 }
 system.debug('Inside '+mapHoldingSelectedRecords);
 }

Public pagereference mydata(){
updateSearchItemsMap();
recList=new List<Recognition__c>();
String a='';
system.debug('List Values '+mapHoldingSelectedRecords);
if(mapHoldingSelectedRecords.size()<=0)
{
    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,' Please Select Atleast 1 Volunteer ')); 
                 return null;

}
for(Id recordID:mapHoldingSelectedRecords.keyset())
{

    if(mapHoldingSelectedRecords.get(recordID).isSelected==true)
    {
        a=a+mapHoldingSelectedRecords.get(recordID).engVol.Contact__r.name+','; 
        system.debug('Mummy');  
        system.debug('RRRR '+mapHoldingSelectedRecords.get(recordID));
         
                Recognition__c recognition = new Recognition__c();
                recognition.Engagement__c = mapHoldingSelectedRecords.get(recordID).engVol.Engagement__c;
                system.debug('Engagement Val '+mapHoldingSelectedRecords.get(recordID).engVol.Engagement__c);
                system.debug('Contact Val '+mapHoldingSelectedRecords.get(recordID).engVol.Contact__c);
                system.debug('Recog value '+rec.Recognition_Type__c);
                //recognition.name = 'abc';
                //recognition.recordtypeid='0122F0000008XXn';
                recognition.Engagement_Volunteer__c = mapHoldingSelectedRecords.get(recordID).engVol.Id;
                recognition.Contact__c = mapHoldingSelectedRecords.get(recordID).engVol.Contact__c; 
                recognition.Recognition_Type__c=rec.Recognition_Type__c;
                recognition.Date_Sent__c=rec.Date_Sent__c;
                recognition.Description__c=rec.Description__c;
                recList.add(recognition);   
        
    }
    
    
}
If(recList.size()>0)
         {
             system.debug('Reclist values '+recList);
             try
             {
             insert recList;
             //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Recognition successfully created'));
               pagereference pr = new pagereference('/' +engId);       
               return pr;
              
             }catch(DMLException e)
             {
                 ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,' '+e.getdmlmessage(0))); 
                 return null;    
             }       
         }   


 ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,''+a));        
 return null;
}



    /*public PageReference createrecognition()
    {
        getSelectedVolunteers(engVolWrapperList);
        
        return null;    
    }*/
    /*public PageReference applyfiler()
    {
       // getSelectedVolunteers(engVolWrapperList);
       system.debug('Inside appf');
        if(checkSelect==true)
        {
            engVolWrapperList.clear();
            for(Engagement_Volunteer__c ev:engVolList)         
            {                                              
                engVolWrapperList.add(new EngagementVolunteerWrapper(ev,true));                                    
            } 
        }
        if(checkSelect==false)
        {
            engVolWrapperList.clear();
            for(Engagement_Volunteer__c ev:engVolList)         
            {                                              
                engVolWrapperList.add(new EngagementVolunteerWrapper(ev,false));                                    
            } 
        }

        return null;    
    }

    */
    public PageReference goback()
    {
         pagereference pr = new pagereference('/' + engId);
       
        return pr;
        //return null;    
    }
    
    //public void getSelectedVolunteers(List<EngagementVolunteerWrapper> engVolWrapperList)    
    /*public PageReference createrecognition()
    {      
        selectedVolunteers = new List<EngagementVolunteerWrapper>(); 
        recList=new List<Recognition__c>();   
        for(EngagementVolunteerWrapper ev: engVolWrapperList)          
        {               
            if(ev.isSelected)               
            {                    
                
                
                Recognition__c recognition = new Recognition__c();
                recognition.Engagement__c = ev.engVol.Engagement__c;
                //recognition.name = 'abc';
                //recognition.recordtypeid='0122F0000008XXn';
                recognition.Engagement_Volunteer__c = ev.engVol.Id;
                recognition.Contact__c = ev.engVol.Contact__c; 
                recognition.Recognition_Type__c=rec.Recognition_Type__c;
                recognition.Date_Sent__c=rec.Date_Sent__c;
                recognition.Description__c=rec.Description__c;
                recList.add(recognition);                          
            }                
                   
         }
         If(recList.size()>0)
         {
             system.debug('Reclist values '+recList);
             try
             {
             insert recList;
             //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Recognition successfully created'));
               pagereference pr = new pagereference('/' +engId);       
               return pr;
              
             }catch(DMLException e)
             {
                 ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,' '+e.getdmlmessage(0))); 
                 return null;    
             }       
         }   
         else
         {
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Please Select Atleast 1 Volunteer to create Recognition'));        
             return null;
         }
     }
    */
    
    
    
     public class EngagementVolunteerWrapper    
     {         
         public Boolean isSelected{get; set;}         
         public Engagement_Volunteer__c engVol{get; set;}                  
         public EngagementVolunteerWrapper(Engagement_Volunteer__c v,Boolean flag)         
         {              
             engVol = v;              
             isSelected = flag;         
         } 
     }
}
/*********************************************************************
 * Description - Test class for CFA_IntegrationLogsDeleteBatch
 * Author - Yaswanth
 *********************************************************************/ 

@isTest
public class CFA_IntegrationLogsDeleteBatch_Test {
    static testMethod void DeleteLogTest() {
        List<CFA_Integration_Log__c> logList = new List<CFA_Integration_Log__c>();
        for(integer logNum = 0; logNum<2; logNum++){
             CFA_Integration_Log__c intLog = new CFA_Integration_Log__c ();
                 intLog.Business_Process__c = 'busProcess' + logNum;
                 intLog.Data_Source__c = 'dataSrc'+ logNum;
                 intLog.Exception__c = 'failureMessage'+ logNum;
                 intLog.Exception_Type__c = 'exceptionType'+ logNum;
            	 intLog.Request_Body_1__c = 'req1'+ logNum;		
                 intLog.Request_Body_2__c = 'req2'+ logNum;
                 intLog.Response__c = 'response'+ logNum;
            if(logNum == 0){
                 intLog.Failures_Included__c = true;
                 intLog.Valid_JSON_Payload__c = true;
                 intLog.Transaction_ID__c = '99999999-8888-7777-6666-11111111111'+ logNum;
                 intLog.Logged_at__c = System.now();
            }
            else {
                 intLog.Failures_Included__c = true;
                 intLog.Valid_JSON_Payload__c = true;
                 intLog.Transaction_ID__c = '99999999-8888-7777-6666-11111111111'+ logNum;
                 intLog.Logged_at__c = System.now()-60;
            }
            
               logList.add(intLog);
            }
        insert logList; 
        
        CFA_IntegrationLogsDeleteBatch batch = new CFA_IntegrationLogsDeleteBatch();
        Database.executeBatch(batch);            
    }
}
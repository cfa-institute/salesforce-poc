/*****************************************************************
Name: AddressPolicyHandlerTest
Copyright © 2021 ITC
============================================================
Purpose: Test for AddressPolicyTrigger
============================================================
History
-------
VERSION  AUTHOR          DATE         DETAIL    Description
1.0      Alona Zubenko   03.02.2021   Created   Cover logic for Assigning Contacts To Societies By Address
*****************************************************************/

@IsTest
private class AddressPolicyHandlerTest {
    private static String COUNTRY_ISO_CODE_CHINA = 'CHN';
    private static String COUNTRY_ISO_CODE_GERMANY = 'DEU';
    private static String COUNTRY_ISO_CODE_UKRAINE = 'UKR';
    private static String COUNTRY_ISO_CODE_BELGIUM = 'BEL';
    private static String STATE_CODE_1 = 'CA-AB';
    private static String STATE_CODE_2 = 'CA-BC';
    private static String PROFILE_INTEGRATION_USER = 'Integration profile';
    private static String PERMISSION_SET_EDIT_ACCOUNT_NAME = 'Edit_Account_Name';
    private static String SOCIETY_PARTNER_USER_ADMIN = 'Society Partner User Admin';
    private final static Integer NUMBER_OF_CONTACTS = 10;

    /**
        Geographic Rules for testing:

        Country - State - Low Zip Code - High Zip Code

        China - STATE_CODE_1 - 0000 - 0099
        China - STATE_CODE_2 - 10000 - 12000

        Germany - STATE_CODE_2 - 13000 - 13099
        Germany - null - 14100 - 15000

        Ukraine - STATE_CODE_1 - 15100 - 15199
        Ukraine - STATE_CODE_2 - null - null

        Belgium - STATE_CODE_1 - 70000 - 70099
        Belgium - null - null - null

     */
    @TestSetup
    private static void init() {
        ContactTriggerHandler.triggerDisabled = true;

        Account accCfaInstitute = CFA_TestDataFactory.createAccount('CFA Institute', 'Society', true);

        Map<String, Account> accountByNameMap = new Map<String, Account>{
                'CFA China' => CFA_TestDataFactory.createAccount('CFA China', 'Society', false)
                , 'CFA Germany' => CFA_TestDataFactory.createAccount('CFA Germany', 'Society', false)
                , 'CFA Ukraine' => CFA_TestDataFactory.createAccount('CFA Ukraine', 'Society', false)
                , 'CFA Belgium' => CFA_TestDataFactory.createAccount('CFA Belgium', 'Society', false)
        };
        insert accountByNameMap.values();

        Map<String, NSA_Policy__c> policyByNameMap = new Map<String, NSA_Policy__c>{
                'NSA China' => CFA_TestDataFactory.createNsaPolicy(accountByNameMap.get('CFA China').Id, 'CFA_China__c', null, null, null, false)
                , 'NSA Germany' => CFA_TestDataFactory.createNsaPolicy(accountByNameMap.get('CFA Germany').Id, 'CFA_Society_Germany__c', null, null, null, false)
                , 'NSA Ukraine' => CFA_TestDataFactory.createNsaPolicy(accountByNameMap.get('CFA Ukraine').Id, 'CFA_Society_Ukraine__c', null, null, null, false)
                , 'NSA Belgium' => CFA_TestDataFactory.createNsaPolicy(accountByNameMap.get('CFA Belgium').Id, 'CFA_Society_Belgium__c', null, null, null, false)
        };
        insert policyByNameMap.values();

        List<NSA_Geographic_Rule__c> geographicRules = new List<NSA_Geographic_Rule__c>();

        //China
        Integer i = 0;
        while (i < 100) {
            String highZipCode = (i < 10) ? '000' + 1 : '00' + i;
            geographicRules.add(CFA_TestDataFactory.createNsaGeographicRule(policyByNameMap.get('NSA China').Id, COUNTRY_ISO_CODE_CHINA, STATE_CODE_1, '0000', highZipCode, false));
            i++;
        }
        geographicRules.add(CFA_TestDataFactory.createNsaGeographicRule(policyByNameMap.get('NSA China').Id, COUNTRY_ISO_CODE_CHINA, STATE_CODE_2, '10000', '12000', false));

        //Germany
        i = 0;
        while (i < 100) {
            String highZipCode = (i < 10) ? '1400' + 1 : '140' + i;
            geographicRules.add(CFA_TestDataFactory.createNsaGeographicRule(policyByNameMap.get('NSA Germany').Id, COUNTRY_ISO_CODE_GERMANY, STATE_CODE_2, '13000', highZipCode, false));
            i++;
        }
        geographicRules.add(CFA_TestDataFactory.createNsaGeographicRule(policyByNameMap.get('NSA Germany').Id, COUNTRY_ISO_CODE_GERMANY, null, '14100', '15000', false));

        //Ukraine
        i = 0;
        while (i < 100) {
            String highZipCode = (i < 10) ? '1510' + 1 : '151' + i;
            geographicRules.add(CFA_TestDataFactory.createNsaGeographicRule(policyByNameMap.get('NSA Ukraine').Id, COUNTRY_ISO_CODE_UKRAINE, STATE_CODE_1, '15100', '16000', false));
            i++;
        }
        geographicRules.add(CFA_TestDataFactory.createNsaGeographicRule(policyByNameMap.get('NSA Ukraine').Id, COUNTRY_ISO_CODE_UKRAINE, STATE_CODE_2, null, null, false));

        //Belgium
        i = 0;
        while (i < 100) {
            String highZipCode = (i < 10) ? '7000' + 1 : '700' + i;
            geographicRules.add(CFA_TestDataFactory.createNsaGeographicRule(policyByNameMap.get('NSA Belgium').Id, COUNTRY_ISO_CODE_BELGIUM, STATE_CODE_1, '70000', null, false));
            i++;
        }
        geographicRules.add(CFA_TestDataFactory.createNsaGeographicRule(policyByNameMap.get('NSA Belgium').Id, COUNTRY_ISO_CODE_BELGIUM, null, null, null, false));

        insert geographicRules;
    }
    
    @IsTest
    private static void cfaContactsTest() {
        System.runAs(CFA_TestDataFactory.getUserWithPermission(PROFILE_INTEGRATION_USER, PERMISSION_SET_EDIT_ACCOUNT_NAME)) {
            List<Contact> contacts = CFA_TestDataFactory.createContactRecords('Local Member', 'Test', 1, true);
            NSAPolicyRunner policyRunner = new NSAPolicyRunner();

            Test.startTest();
            policyRunner.run(contacts, new List<INSAPolicyHandler>{
                    new AddressPolicyHandler()
            }, true);
            Test.stopTest();

            List<Contact> cfaContacts = [SELECT AccountId FROM Contact WHERE Id IN :new Map<Id,Contact>(contacts).keySet()];
            System.assertEquals(null, cfaContacts[0].AccountId);
        }
    }

    @IsTest
    private static void runEmptyPolicyTest() {
        System.runAs(CFA_TestDataFactory.getUserWithPermission(PROFILE_INTEGRATION_USER, PERMISSION_SET_EDIT_ACCOUNT_NAME)) {
            List<Contact> contacts = CFA_TestDataFactory.createContactRecords('CFA Contact', 'Test', NUMBER_OF_CONTACTS, true);
            NSAPolicyRunner policyRunner = new NSAPolicyRunner();

            Test.startTest();
            policyRunner.run(contacts, new List<INSAPolicyHandler>{
                    new AddressPolicyHandler()
            }, true);
            Test.stopTest();

            Id cfaInstituteAccountId = [SELECT Id FROM Account WHERE Name = 'CFA Institute' LIMIT 1][0].Id;
            System.assertEquals(cfaInstituteAccountId, contacts[0].AccountId);
        }
    }

    @IsTest
    private static void runPoliciesWithStateZipCodeTest() { //state zipCode
        System.runAs(CFA_TestDataFactory.getUserWithPermission(PROFILE_INTEGRATION_USER, PERMISSION_SET_EDIT_ACCOUNT_NAME)) {
            List<String> countries = new List<String>{
                    COUNTRY_ISO_CODE_CHINA, COUNTRY_ISO_CODE_GERMANY, COUNTRY_ISO_CODE_UKRAINE, COUNTRY_ISO_CODE_BELGIUM
            };
            List<String> states = new List<String>{
                    STATE_CODE_1, STATE_CODE_2
            };
            List<Contact> contacts = new List<Contact>();

            contacts.addAll(createContacts(COUNTRY_ISO_CODE_CHINA, STATE_CODE_1, '0000', NUMBER_OF_CONTACTS)); //correct
            contacts.addAll(createContacts(COUNTRY_ISO_CODE_BELGIUM, STATE_CODE_1, '70001', NUMBER_OF_CONTACTS)); //correct
            contacts.addAll(createContacts(COUNTRY_ISO_CODE_GERMANY, STATE_CODE_2, '13001', NUMBER_OF_CONTACTS)); //correct
            contacts.addAll(createContacts(COUNTRY_ISO_CODE_UKRAINE, STATE_CODE_1, '15101', NUMBER_OF_CONTACTS)); //correct

            insert contacts;

            List<NSA_Policy__c> policies = getPolicies(countries, states);
            NSAPolicyRunner policyRunner = new NSAPolicyRunner(policies);

            Test.startTest();
            policyRunner.run(contacts, new List<INSAPolicyHandler>{
                    new AddressPolicyHandler()
            }, true);
            Test.stopTest();

            Id cfaChinaAccountId = [SELECT Id FROM Account WHERE Name = 'CFA China' LIMIT 1][0].Id;
            Id cfaGermanyAccountId = [SELECT Id FROM Account WHERE Name = 'CFA Germany' LIMIT 1][0].Id;
            Id cfaUkraineAccountId = [SELECT Id FROM Account WHERE Name = 'CFA Ukraine' LIMIT 1][0].Id;
            Id cfaBelgiumAccountId = [SELECT Id FROM Account WHERE Name = 'CFA Belgium' LIMIT 1][0].Id;

            Integer numberOfChinaRecords = 0;
            Integer numberOfGermanyRecords = 0;
            Integer numberOfUkraineRecords = 0;
            Integer numberOfBelgiumRecords = 0;
            for (Contact con : contacts) {
                if (con.AccountId == cfaChinaAccountId) {
                    numberOfChinaRecords++;
                } else if (con.AccountId == cfaGermanyAccountId) {
                    numberOfGermanyRecords++;
                } else if (con.AccountId == cfaUkraineAccountId) {
                    numberOfUkraineRecords++;
                } else if (con.AccountId == cfaBelgiumAccountId) {
                    numberOfBelgiumRecords++;
                }
            }

            System.assertEquals(NUMBER_OF_CONTACTS, numberOfChinaRecords);
            System.assertEquals(NUMBER_OF_CONTACTS, numberOfUkraineRecords);
            System.assertEquals(NUMBER_OF_CONTACTS, numberOfGermanyRecords);
            System.assertEquals(NUMBER_OF_CONTACTS, numberOfBelgiumRecords);
        }
    }

    @IsTest
    private static void runPoliciesWithStateZipCodeForPartnerUserTest() { //state zipCode
        ContactTriggerHandler.triggerDisabled = true;

        List<String> countries = new List<String>{COUNTRY_ISO_CODE_CHINA, COUNTRY_ISO_CODE_GERMANY, COUNTRY_ISO_CODE_UKRAINE, COUNTRY_ISO_CODE_BELGIUM};
        List<String> states = new List<String>{STATE_CODE_1, STATE_CODE_2};
        List<Contact> contacts = new List<Contact>();
        List<Contact> chinaContacts = createContacts(COUNTRY_ISO_CODE_CHINA, STATE_CODE_1, '0000', NUMBER_OF_CONTACTS);
        contacts.addAll(chinaContacts); //correct
        contacts.addAll(createContacts(COUNTRY_ISO_CODE_BELGIUM, STATE_CODE_1, '70001', NUMBER_OF_CONTACTS)); //correct
        contacts.addAll(createContacts(COUNTRY_ISO_CODE_GERMANY, STATE_CODE_2, '13001', NUMBER_OF_CONTACTS)); //correct
        contacts.addAll(createContacts(COUNTRY_ISO_CODE_UKRAINE, STATE_CODE_1, '15101', NUMBER_OF_CONTACTS)); //correct

        insert contacts;

        CFA_TestDataFactory.createPartnerUsers(chinaContacts, SOCIETY_PARTNER_USER_ADMIN, NUMBER_OF_CONTACTS);

        System.runAs(CFA_TestDataFactory.getUserWithPermission(PROFILE_INTEGRATION_USER, PERMISSION_SET_EDIT_ACCOUNT_NAME)) {
            List<NSA_Policy__c> policies = getPolicies(countries, states);
            NSAPolicyRunner policyRunner = new NSAPolicyRunner(policies);

            Test.startTest();
            policyRunner.run(contacts, new List<INSAPolicyHandler>{
                    new AddressPolicyHandler()
            }, true);
            Test.stopTest();

            Id cfaChinaAccountId = [SELECT Id FROM Account WHERE Name = 'CFA China' LIMIT 1][0].Id;
            Id cfaGermanyAccountId = [SELECT Id FROM Account WHERE Name = 'CFA Germany' LIMIT 1][0].Id;
            Id cfaUkraineAccountId = [SELECT Id FROM Account WHERE Name = 'CFA Ukraine' LIMIT 1][0].Id;
            Id cfaBelgiumAccountId = [SELECT Id FROM Account WHERE Name = 'CFA Belgium' LIMIT 1][0].Id;

            Integer numberOfChinaRecords = 0;
            Integer numberOfGermanyRecords = 0;
            Integer numberOfUkraineRecords = 0;
            Integer numberOfBelgiumRecords = 0;
            for (Contact con : contacts) {
                if (con.AccountId == cfaChinaAccountId) {
                    numberOfChinaRecords++;
                } else if (con.AccountId == cfaGermanyAccountId) {
                    numberOfGermanyRecords++;
                } else if (con.AccountId == cfaUkraineAccountId) {
                    numberOfUkraineRecords++;
                } else if (con.AccountId == cfaBelgiumAccountId) {
                    numberOfBelgiumRecords++;
                }
            }

            System.assertEquals(0, numberOfChinaRecords);
            System.assertEquals(NUMBER_OF_CONTACTS, numberOfUkraineRecords);
            System.assertEquals(NUMBER_OF_CONTACTS, numberOfGermanyRecords);
            System.assertEquals(NUMBER_OF_CONTACTS, numberOfBelgiumRecords);
        }
    }

    @IsTest
    private static void runPoliciesWithEmptyStateEmptyZipCodeTest() { //null null
        System.runAs(CFA_TestDataFactory.getUserWithPermission(PROFILE_INTEGRATION_USER, PERMISSION_SET_EDIT_ACCOUNT_NAME)) {
            List<String> countries = new List<String>{
                    COUNTRY_ISO_CODE_BELGIUM
            };
            List<String> states = new List<String>{
                    STATE_CODE_1, STATE_CODE_2
            };
            List<Contact> contacts = new List<Contact>();

            contacts.addAll(createContacts(COUNTRY_ISO_CODE_BELGIUM, null, null, NUMBER_OF_CONTACTS)); //correct
            contacts.addAll(createContacts(COUNTRY_ISO_CODE_BELGIUM, STATE_CODE_1, '70001', NUMBER_OF_CONTACTS)); //correct
            contacts.addAll(createContacts(COUNTRY_ISO_CODE_BELGIUM, STATE_CODE_2, '70001', NUMBER_OF_CONTACTS)); //correct
            contacts.addAll(createContacts(COUNTRY_ISO_CODE_BELGIUM, STATE_CODE_1, '13500', NUMBER_OF_CONTACTS)); //correct

            insert contacts;

            List<NSA_Policy__c> policies = getPolicies(countries, states);
            NSAPolicyRunner policyRunner = new NSAPolicyRunner(policies);

            Test.startTest();
            policyRunner.run(contacts, new List<INSAPolicyHandler>{
                    new AddressPolicyHandler()
            }, true);
            Test.stopTest();

            Id cfaBelgiumAccountId = [SELECT Id FROM Account WHERE Name = 'CFA Belgium' LIMIT 1][0].Id;

            Integer numberOfCfaBelgiumRecords = 0;
            for (Contact con : contacts) {
                if (con.AccountId == cfaBelgiumAccountId) {
                    numberOfCfaBelgiumRecords++;
                }
            }
            System.assertEquals(NUMBER_OF_CONTACTS * 4, numberOfCfaBelgiumRecords);
        }
    }

    @IsTest
    private static void runPolicyWithEmptyStateTest() { //state null
        System.runAs(CFA_TestDataFactory.getUserWithPermission(PROFILE_INTEGRATION_USER, PERMISSION_SET_EDIT_ACCOUNT_NAME)) {
            List<String> countries = new List<String>{
                    COUNTRY_ISO_CODE_GERMANY
            };
            List<String> states = new List<String>{
                    STATE_CODE_1, STATE_CODE_2
            };
            List<Contact> contacts = new List<Contact>();

            contacts.addAll(createContacts(COUNTRY_ISO_CODE_GERMANY, null, '14101', NUMBER_OF_CONTACTS)); //correct
            contacts.addAll(createContacts(COUNTRY_ISO_CODE_GERMANY, STATE_CODE_1, '14101', NUMBER_OF_CONTACTS)); //correct
            contacts.addAll(createContacts(COUNTRY_ISO_CODE_GERMANY, STATE_CODE_2, '13001', NUMBER_OF_CONTACTS)); //correct
            contacts.addAll(createContacts(COUNTRY_ISO_CODE_GERMANY, STATE_CODE_2, '0000', NUMBER_OF_CONTACTS)); //not correct

            insert contacts;

            List<NSA_Policy__c> policies = getPolicies(countries, states);
            NSAPolicyRunner policyRunner = new NSAPolicyRunner(policies);

            Test.startTest();
            policyRunner.run(contacts, new List<INSAPolicyHandler>{
                    new AddressPolicyHandler()
            }, true);
            Test.stopTest();

            Id cfaInstituteAccountId = [SELECT Id FROM Account WHERE Name = 'CFA Institute' LIMIT 1][0].Id;
            Id cfaGermanyAccountId = [SELECT Id FROM Account WHERE Name = 'CFA Germany' LIMIT 1][0].Id;

            Integer numberOfCfaGermanyRecords = 0;
            Integer numberOfCfaInstituteRecords = 0;
            for (Contact con : contacts) {
                if (con.AccountId == cfaGermanyAccountId) {
                    numberOfCfaGermanyRecords++;
                } else if (con.AccountId == cfaInstituteAccountId) {
                    numberOfCfaInstituteRecords++;
                }
            }
            System.assertEquals(NUMBER_OF_CONTACTS * 3, numberOfCfaGermanyRecords);
            System.assertEquals(NUMBER_OF_CONTACTS * 1, numberOfCfaInstituteRecords);
        }
    }

    @IsTest
    private static void runPolicyWithEmptyZipCodeTest() { //state null
        System.runAs(CFA_TestDataFactory.getUserWithPermission(PROFILE_INTEGRATION_USER, PERMISSION_SET_EDIT_ACCOUNT_NAME)) {
            List<String> countries = new List<String>{
                    COUNTRY_ISO_CODE_UKRAINE
            };
            List<String> states = new List<String>{
                    STATE_CODE_1, STATE_CODE_2
            };
            List<Contact> contacts = new List<Contact>();

            contacts.addAll(createContacts(COUNTRY_ISO_CODE_UKRAINE, STATE_CODE_2, null, NUMBER_OF_CONTACTS)); //correct
            contacts.addAll(createContacts(COUNTRY_ISO_CODE_UKRAINE, STATE_CODE_1, '15101', NUMBER_OF_CONTACTS)); //correct
            contacts.addAll(createContacts(COUNTRY_ISO_CODE_UKRAINE, null, '14101', NUMBER_OF_CONTACTS)); //not correct
            contacts.addAll(createContacts(COUNTRY_ISO_CODE_UKRAINE, STATE_CODE_2, '0000', NUMBER_OF_CONTACTS)); //correct

            insert contacts;

            List<NSA_Policy__c> policies = getPolicies(countries, states);
            NSAPolicyRunner policyRunner = new NSAPolicyRunner(policies);

            Test.startTest();
            policyRunner.run(contacts, new List<INSAPolicyHandler>{
                    new AddressPolicyHandler()
            }, true);
            Test.stopTest();

            Id cfaInstituteAccountId = [SELECT Id FROM Account WHERE Name = 'CFA Institute' LIMIT 1][0].Id;
            Id cfaUkraineAccountId = [SELECT Id FROM Account WHERE Name = 'CFA Ukraine' LIMIT 1][0].Id;

            Integer numberOfCfaUkraineRecords = 0;
            Integer numberOfCfaInstituteRecords = 0;
            for (Contact con : contacts) {
                if (con.AccountId == cfaUkraineAccountId) {
                    numberOfCfaUkraineRecords++;
                } else if (con.AccountId == cfaInstituteAccountId) {
                    numberOfCfaInstituteRecords++;
                }
            }
            System.assertEquals(NUMBER_OF_CONTACTS * 3, numberOfCfaUkraineRecords);
            System.assertEquals(NUMBER_OF_CONTACTS * 1, numberOfCfaInstituteRecords);
        }
    }
    
    @IsTest
    private static void singleRunPolicyTest() {
        System.runAs(CFA_TestDataFactory.getUserWithPermission(PROFILE_INTEGRATION_USER, PERMISSION_SET_EDIT_ACCOUNT_NAME)) {
            List<String> countries = new List<String>{
                    COUNTRY_ISO_CODE_CHINA
            };
            List<String> states = new List<String>{
                    STATE_CODE_1
            };
			Id cfaInstituteAccountId = [SELECT Id FROM Account WHERE Name = 'CFA Institute' LIMIT 1][0].Id;
            Id cfaUkraineAccountId = [SELECT Id FROM Account WHERE Name = 'CFA Ukraine' LIMIT 1][0].Id;
            Id cfaChinaAccountId = [SELECT Id FROM Account WHERE Name = 'CFA China' LIMIT 1][0].Id;
            List<Contact> newContacts = createContacts(COUNTRY_ISO_CODE_CHINA, STATE_CODE_1, '0000', NUMBER_OF_CONTACTS);
            
            newContacts[0].AccountId = cfaInstituteAccountId; // should be transfered to cfa china
            
            newContacts[1].AccountId = cfaUkraineAccountId; // should not be transfered
            newContacts[1].Country_ISO_Code__c = 'UKR';
            newContacts[1].State_Province_ISO_Code__c = 'CA-BC';
            newContacts[1].MailingPostalCode = '15101';
            
            newContacts[2].AccountId = cfaChinaAccountId; // should be transfered to cfa institute
            newContacts[2].State_Province_ISO_Code__c = 'US-AK';
            
            insert newContacts;
            List<NSA_Policy__c> policies = getPolicies(countries, states);
            NSAPolicyRunner policyRunner = new NSAPolicyRunner(policies);
            Test.startTest();
            policyRunner.run(newContacts, new List<INSAPolicyHandler>{
                    new AddressPolicyHandler(true, true)
            }, true);
            Test.stopTest();
            
            Integer numberOfChinaRecords = 0;
            Integer numberOfCfaInstituteRecords = 0;
            Integer numberOfCfaUkraineRecords = 0;
            for (Contact con : newContacts) {
                if (con.AccountId == cfaChinaAccountId) {
                    numberOfChinaRecords++;
                } else if (con.AccountId == cfaUkraineAccountId) {
                    numberOfCfaUkraineRecords++;
                } else if (con.AccountId == cfaInstituteAccountId) {
                    numberOfCfaInstituteRecords++;
                }
            }

            System.assertEquals(NUMBER_OF_CONTACTS - 2, numberOfChinaRecords);
            System.assertEquals(1, numberOfCfaUkraineRecords);
            System.assertEquals(1, numberOfCfaInstituteRecords);
        }
    }

    private static List<NSA_Policy__c> getPolicies(List<String> countries, List<String> states) {
        List<NSA_Policy__c> policies = [
                SELECT Id
                        , Advanced_Assignment__c
                        , Advanced_Assignment_Handler__c
                        , Society_Account__c
                        , Contact_Society_Field_API_Name__c
                        , (
                        SELECT Country_Name__c
                                , State__c
                                , Zip_Code_High__c
                                , Zip_Code_Low__c
                                , NSA_Policy__c
                        FROM NSA_Geograhpic_Rules__r
                        WHERE Country_Name__c IN :countries
                        AND (State__c IN :states OR State__c = NULL)
                )
                FROM NSA_Policy__c
                WHERE Id IN (
                        SELECT NSA_Policy__c
                        FROM NSA_Geographic_Rule__c
                        WHERE Country_Name__c IN :countries
                        AND (State__c IN :states OR State__c = NULL)
                )
                AND Is_Active__c = TRUE
            	AND Policy_Status__c = 'Approved'
        ];
        return policies;
    }

    private static List<Contact> createContacts(String country, String state, String zipCode, Integer numberOfRecords) {
        List<Contact> contacts = new List<Contact>();
        for (Contact con : CFA_TestDataFactory.createContactRecords('CFA Contact', 'Test', numberOfRecords, false)) {
            con.Country_ISO_Code__c = country;
            con.State_Province_ISO_Code__c = state;
            con.MailingPostalCode = zipCode;
            contacts.add(con);
        }
        return contacts;
    }
}
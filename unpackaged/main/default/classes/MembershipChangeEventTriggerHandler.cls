/*****************************************************************
Name: MembershipChangeEventTriggerHandler
Copyright © 2020 ITC
============================================================
Purpose: This trigger handler is responsible for handling Membership Change Event
============================================================
History
-------
VERSION  AUTHOR          DATE         DETAIL    Description
1.0      Vadym Merkotan               Create    Apply Member Society Campaigns Changes
2.0      Alona Zubenko   10.12.2020   Update    Update Badge Visibility Dates
*****************************************************************/

public with sharing class MembershipChangeEventTriggerHandler  extends BaseChangeEventTriggerHandler {
    public static Boolean triggerDisabled = false;
    private static final String TRIGGER_NAME = 'MembershipChangeEventTrigger';

    /**
     * @description Check is trigger disabled
     *
     * @return Boolean
     */
    public override Boolean isDisabled() {
        if(Test.isRunningTest()) {
            return triggerDisabled;
        } else {
            return triggerDisabled || super.isDisabled();
        }
    }

    /**
     * @description Get Trigger Name
     *
     * @return String
     */
    public override String getTriggerName() {
        return TRIGGER_NAME;
    }

    /**
     * @description Handle Create Membership__ChangeEvent
     *
     * @param changeEvents List<Membership__ChangeEvent>
     */
    public override void handleAllCreate(List<SObject> changeEvents) {
        System.debug('handleCreate ' + changeEvents);
        Set<String> recordIds = getRecordIds(changeEvents);
        List<Membership__c> changedMembershipList = SocietyMembershipService.getSocietyMembershipsByIds(recordIds);

        MembershipChangeEventTriggerHelper.applyMemberSocietyCampaignsChanges(changedMembershipList);
    }

    /**
     * @description Handle Update Membership__ChangeEvent
     *
     * @param changeEvents List<Membership__ChangeEvent>
     */
    public override void handleAllUpdate(List<SObject> changeEvents) {
        System.debug('handleUpdate ' + changeEvents);
        Set<String> recordIds = getRecordIds(changeEvents);
        List<Membership__c> changedMembershipList = SocietyMembershipService.getSocietyMembershipsByIds(recordIds);

        MembershipChangeEventTriggerHelper.applyMemberSocietyCampaignsChanges(changedMembershipList);
    }

}
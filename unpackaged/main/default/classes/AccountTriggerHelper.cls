public with sharing class AccountTriggerHelper {
    
    public static void validateARXOnUncheck(Map<Id, Account> newItems, Map<Id, Account> oldItems) {
        List<Account> accls = [SELECT Id,ARX_Approved__c FROM Account WHERE id in (SELECT Account__c FROM Affiliation__c WHERE Products__c = 'ARX' AND Role__c = 'Content Contributor' AND Active_Affiliation__c = true and Account__c in :newItems.keyset())];
        List<Account> accntLst = new List<Account>();

        if (newItems != null && newItems.size() > 0 && oldItems != null && oldItems.size() > 0) {
           for (Account acc : accls) {
                if (oldItems.get(acc.Id).ARX_Approved__c == true && newItems.get(acc.Id).ARX_Approved__c == false) {
                    acc.adderror('You must remove the active content contributor affiliations first.');
                }
            }
        }
    }
}
/**
* @author 7Summits Inc
* @date 07-03-2019
* @description Test class for SocietyOpsCaseAgeBusinessHoursBatch
*/
@isTest

private  class  SocietyOpsCaseAgeBusinessHoursBatchTest {

    // in the test class, change the New Status to a working status.
    // Then workflow will execute.
    @isTest
    static void testBusinessHours(){

        SocietyOps_Backoffice__c societyOps= new SocietyOps_Backoffice__c();
        societyOps.name='trigger-details';
        societyOps.CaseReassignment_TemplateName__c='SocietyOps-Case Reassignment';
        societyOps.No_reply_email__c=Label.Society_Ops_NoReply;
        insert societyOps;

        Id societyTechCaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Society Tech Case').getRecordTypeId();

        // insert 200 new cases
        List<Case> newCasesList = new List<Case>();
        for(Integer i = 0; i<5; i++){
            // Case caseVariable = new Case(Status='New', createdDate.AddDays(-1));
            newCasesList.add(new Case(
                    Status='New'
                    ,RecordTypeId = societyTechCaseRecordTypeId
                    ,society_Case_Close_Comments__c='Test comments'
                    ,SuppliedEmail = 'test@test.com'
            ));

        }
        insert newCasesList;

        // setting createdDate to 2 days ago so that createdDate differs from updated date on update (see below).
        DateTime beforeDate = DateTime.now().addDays(-2);
        for(case c: newCasesList){
            Test.setCreatedDate(c.Id, beforeDate);
        }
        update newCasesList;

        // updating case statuses
        List<Case> casesList = new List<Case>();
        // using loop variable and modulo operator to update cases with a variety of statuses
        Integer loopVariable = 0;
        for(Case c: newCasesList){
            if(math.mod(loopVariable, 5) == 0){
                c.Status = 'New';
            }
            else if(math.mod(loopVariable, 3) == 0){
                c.Status = 'In-Progress';
            }else if(math.mod(loopVariable, 2) == 0){
                c.Status = 'Waiting On Customer';
            }else{
                c.Status = 'Waiting On Vendor';
            }
            loopVariable ++;
        }
        update newCasesList;


        String jobID=null;
        Test.startTest();
            SocietyOpsCaseAgeBusinessHoursBatch caseProcessor = new SocietyOpsCaseAgeBusinessHoursBatch();
            Database.executeBatch(caseProcessor,5);
        String CRON_EXP = '0 0 * ? * SUN-FRI';


        Test.stopTest();

        // assert on batch behavior
        List<Case> newCases = [SELECT Status, SOC_Age_New_Status_Hours__c,SOC_Age_Working_Status_Hours__c FROM Case];
        for(case nc : newCases){
            if(nc.status == 'New'){
                // new status hours assured to be greater than 0 because created date was set to be 2 days before current date
                System.assert(nc.SOC_Age_New_Status_Hours__c > 0);
            } else {
                // working status case. Working hours may be 0
             
                System.assert(nc.SOC_Age_Working_Status_Hours__c >= 0);
            }

        }






    }

}
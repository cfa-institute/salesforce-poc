@isTest
private class DeceasedPolicyHandlerTest {
    
    private final static Integer NUMBER_OF_CONTACTS = 200;
    
    @TestSetup
    private static void setup() {
        CFA_TestDataFactory.createAccount('CFA Institute', 'Society', true);
    }
    
    @isTest
    private static void whenDeceasedContactThenCFASocietyCheckboxesToFalse() {
        Account acc = [SELECT ID FROM Account WHERE Name = 'CFA Institute' LIMIT 1];
        List<Contact> contacts = CFA_TestDataFactory.createContactRecords('CFA Contact', 'Test', NUMBER_OF_CONTACTS, false);
        for(Contact contact: contacts) {
            contact.Deceased_Date__c = Date.today().addYears(-3);
            contact.CFA_China__c = true;
            contact.CFA_Society_Toronto__c = true;
        }
        List<NSA_Policy__c> policies = new List<NSA_Policy__c>();
        policies.add(new NSA_policy__c (Contact_Society_Field_API_Name__c = 'CFA_China__c'));
        policies.add(new NSA_policy__c (Contact_Society_Field_API_Name__c = 'CFA_Society_Toronto__c'));
        Test.startTest();
            (new DeceasedPolicyHandler()).run(policies, contacts);
        Test.stopTest();
        for(Contact contact: contacts) {
            System.assert(!contact.CFA_China__c);
            System.assert(!contact.CFA_Society_Toronto__c);
            System.assertEquals(contact.AccountId, acc.Id);
        }
    }
    
    @isTest
    private static void whenNotDeceasedContactThenNotChangeCFASocietyCheckboxesToFalse() {
        Account acc = [SELECT ID FROM Account WHERE Name = 'CFA Institute' LIMIT 1];
        List<Contact> contacts = CFA_TestDataFactory.createContactRecords('CFA Contact', 'Test', NUMBER_OF_CONTACTS, false);
        for(Contact contact: contacts) {
            contact.CFA_China__c = true;
            contact.CFA_Society_Toronto__c = true;
        }
        List<NSA_Policy__c> policies = new List<NSA_Policy__c>();
        policies.add(new NSA_policy__c (Contact_Society_Field_API_Name__c = 'CFA_China__c'));
        policies.add(new NSA_policy__c (Contact_Society_Field_API_Name__c = 'CFA_Society_Toronto__c'));
        Test.startTest();
             (new DeceasedPolicyHandler()).run(policies, contacts);
        Test.stopTest();
        for(Contact contact: contacts) {
            System.assert(contact.CFA_China__c);
            System.assert(contact.CFA_Society_Toronto__c);
            System.assertNotEquals(contact.AccountId, acc.Id);
        }
    }
}
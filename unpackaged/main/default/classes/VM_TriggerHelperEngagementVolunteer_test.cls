@isTest
public class VM_TriggerHelperEngagementVolunteer_test {  
    @isTest
    public static void OnAfterInsert_test(){
        
        User RMOwner=VM_TestDataFactory.createUser('RMTest4769@gmail.com','RMTest','Relationship Manager Staff','');
        User MDUSer=VM_TestDataFactory.createUser('MDTest4769@gmail.com','MDTest','Outreach Management User','Managing Director');
        User ReportsToUSer=VM_TestDataFactory.createUser('AnneTest4769@gmail.com','Huff','Outreach Management User','Relationship Manager');
        String ename ='t FY20';
        Engagement__c eng  = VM_TestDataFactory.createEngagement(Ename,RMOwner,MDUSer,ReportsToUSer) ;
        Account acc = VM_TestDataFactory.createAccountRecord() ;
        Contact con = VM_TestDataFactory.createContactRecord(acc) ; 
        //System.runas(t_user){
        insert eng;
        
        system.debug('Engagement inserted');
        Role__c rl = VM_TestDataFactory.createRole(eng) ;
        insert rl;
        system.debug('Role inserted');
        Group grp=new Group();
        grp.name='RM and VM'; 
        insert grp ;
        Engagement__Share eng_share = new Engagement__Share(ParentId=eng.id,UserOrGroupId=grp.id,accesslevel='Edit');
        insert eng_share; 
        Engagement_Volunteer__c engage= VM_TestDataFactory.createEngagementVolunteer(eng,rl,con) ;
        
        test.startTest();
        try{
            //System.runAs(t_user){
            //System.debug('I am trying to run as '+ t_user);
            insert engage;
            
            //}
            
        }
        
        catch (Exception e){
            System.debug(e) ;
        }
        
        
        test.stopTest();
        
        
        
    }
    
    @isTest
    public static void OnAfterInsert_test_else(){
        
        
        //User t_user =VM_TestDataFactory.createUser('t_user@gmail.com','t_user','System Administrator','');
        
        User RMOwner=VM_TestDataFactory.createUser('RMTest112@gmail.com','RMTest','Relationship Manager Staff','');
        User MDUSer=VM_TestDataFactory.createUser('MDTest112@gmail.com','MDTest','Outreach Management User','Managing Director');
        User ReportsToUSer=VM_TestDataFactory.createUser('AnneTest112@gmail.com','Huff','Outreach Management User','Relationship Manager');
        String ename ='t FY20';
        Engagement__c eng  = VM_TestDataFactory.createEngagement(Ename,RMOwner,MDUSer,ReportsToUSer) ;
        Account acc = VM_TestDataFactory.createAccountRecord() ;
        Contact con = VM_TestDataFactory.createContactRecord(acc) ;
        //System.runas(t_user){
        insert eng;
        
        //}
        system.debug('Engagement inserted');
        Role__c rl = VM_TestDataFactory.createRole(eng) ;
        rl.Confidential__c = False;
        insert rl;
        system.debug('Role inserted');
        Group grp=new Group();
        grp.name='RM and VM'; 
        insert grp ;
        Engagement__Share eng_share = new Engagement__Share(ParentId=eng.id,UserOrGroupId=grp.id,accesslevel='Read');
        //insert eng_share; 
        Engagement_Volunteer__c engage= VM_TestDataFactory.createEngagementVolunteer(eng,rl,con) ;
        
        test.startTest();
        try{
            //System.runAs(t_user){
            //System.debug('I am trying to run as '+ t_user);
            insert engage;
            
            //}
            
        }
        
        catch (Exception e){
            System.debug(e) ;
        }
        
        
        test.stopTest();
        
        
        
    }
    
    @isTest
    public static void onbeforeupdate (){
        User RMOwner=VM_TestDataFactory.createUser('RMTest567@gmail.com','RMTest','Relationship Manager Staff','');
        User MDUSer=VM_TestDataFactory.createUser('MDTest567@gmail.com','MDTest','Outreach Management User','Managing Director');
        User ReportsToUSer=VM_TestDataFactory.createUser('AnneTest567@gmail.com','Huff','Outreach Management User','Relationship Manager');
        String ename ='t FY20';
        Engagement__c eng  = VM_TestDataFactory.createEngagement(Ename,RMOwner,MDUSer,ReportsToUSer) ;
        Account acc = VM_TestDataFactory.createAccountRecord() ;
        Contact con = VM_TestDataFactory.createContactRecord(acc) ;
        insert eng;
        system.debug('Engagement inserted');
        Role__c rl = VM_TestDataFactory.createRole(eng) ;
        insert rl;
        system.debug('Role inserted');
        Group grp=new Group();
        grp.name='RM and VM'; 
        insert grp ;
        Engagement_Volunteer__c engage= VM_TestDataFactory.createEngagementVolunteer(eng,rl,con) ;
        insert engage;
        //  VM_TestDataFactory.createAssessment(eng,engage) ;
        try{
            
            engage.Status__c='Off-boarded' ;
            //rl.Available_Positions__c = 200;
            //update rl;
            update engage; 
            
        }
        catch (Exception e)
        {
            Boolean a = e.getMessage().contains('Volunteer cannot be offboarded as no Assessment exists.')?true:false;
            System.assertEquals(a,true);
        }
        
        Assessment__c tmpAs = VM_TestDataFactory.createEngagementAssessment(eng);
        tmpAs.engagement_volunteer__c = engage.Id;
        insert tmpAs;
        Recognition__c rcVM = new Recognition__c();
        rcVM.Engagement__c = eng.id;
        rcVM.Engagement_Volunteer__c = engage.Id;
        rcVM.Recognition_Type__c = 'Thank you Note';
        rcVM.Contact__c = con.ID;
        insert rcVM;
        ///delete lstAs;
        engage.Status__c='Off-boarded' ;
        engage.Reason_for_Off_boarding__c='Unacceptable';
        engage.Reason_Explanation__c = 'asda';
        //rl.Available_Positions__c = 200;
        //update rl;
        update engage; 
        
        engage.Status__c='On-boarded' ;
        engage.Reason_for_Off_boarding__c='';
        engage.Reason_Explanation__c = '';
        update engage;
    }
    
}
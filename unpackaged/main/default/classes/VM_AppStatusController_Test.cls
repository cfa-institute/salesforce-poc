@isTest
//Bhushan
public class VM_AppStatusController_Test {
    
    public static Account portalAccount;
    public static Contact portalContact;
    public static user portalUser;
    public static Engagement__c eng;
    public static Role__c role;
    public static Job_Application__c job;
    public static ContactShare conShare;
    public static Engagement__Share engShare;
    public static VM_NonConflictRules__c cs_NCR;
    public static VM_LevelThreePairs__c cs_LTP;
    public static JobApplicationRecordTypeSelection__c cs_JobAppRT;
    public static final String PARTNER_RECORDTYPE = 'CFA Contact';
    public static final String ENGAGEMENT_RECORDTYPE = 'Non Confidential';

    public static void setupdata(){
        ContactTriggerHandler.triggerDisabled = true;
        
        portalAccount = VM_TestDataFactory.createAccountRecord();
        // portalAccount.IsPartner = true;
        insert portalAccount;
        
        portalContact = VM_TestDataFactory.createContactRecord(portalAccount);        
        Schema.DescribeSObjectResult contactSchemaResult = Schema.SObjectType.Contact;
        Map<String,Schema.RecordTypeInfo> mapContactTypeInfo = contactSchemaResult.getRecordTypeInfosByName();
        Id partnerRecordTypeId = mapContactTypeInfo.get(PARTNER_RECORDTYPE).getRecordTypeId();        
        portalContact.RecordTypeId = partnerRecordTypeId;
        portalContact.MDM_Preferred_Email_for_Volunteering__c = 'abc@abc.com';
        update portalContact;
        
        //Contact checkcon = [select id, recordtype.name, recordtypeid from contact where id = :portalContact.Id];
        //system.debug('@@@@ portalContact: '+checkcon.RecordTypeId);
        //portalUser = VM_TaskCreateEditDetail_Test.createPartnerUser('volTaskCrEditDet@gmail.com','volTaskCrEditDet','Volunteer','Managing Director', portalContact.Id);
        portalUser = CFA_TestDataFactory.createPartnerUser(portalContact,'CFA Base Partner');

        User RMOwner=VM_TestDataFactory.createUser('RMTest25@gmail.com','RMTest','Relationship Manager Staff','');
        User MDUSer=VM_TestDataFactory.createUser('MDTest1@gmail.com','MDTest','Outreach Management User','Managing Director');
        User ReportsToUSer=VM_TestDataFactory.createUser('Anne1@gmail.com','Huff','Outreach Management User','Relationship Manager');
        
        eng=VM_TestDataFactory.createEngagement('t FY20',RMOwner,MDUSer,ReportsToUSer);
        Schema.DescribeSObjectResult engSchemaResult = Schema.SObjectType.Engagement__c;
        Map<String,Schema.RecordTypeInfo> mapengTypeInfo = engSchemaResult.getRecordTypeInfosByName();
        Id engRecordTypeId = mapengTypeInfo.get(ENGAGEMENT_RECORDTYPE).getRecordTypeId();
        eng.RecordTypeId = engRecordTypeId;
        eng.Engagement_Type__c='Committee';
        insert eng;
        system.debug('@@@@ eng: '+eng.recordtypeid);
        
        
        role=VM_TestDataFactory.createRole(eng); 
        role.Confidential__c = false;        
        role.Available_Positions__c = 10;
        insert role;
        
        job=VM_TestDataFactory.createJobApplication(role,portalContact);
        insert job;
        job.Status__c='Submitted';
        job.Submitted__c = true;
        update job;
        //job.status__c='Volunteer Accepted';
        //update job;
        
        ContactShare conShare = new ContactShare();
        conShare.ContactId =portalContact.Id;
        conShare.UserOrGroupId = portalUser.Id;
        conShare.ContactAccessLevel = 'Read';
        insert conShare;
//        Engagement is public read only at the moment
//        Engagement__Share engShare = new Engagement__Share();
//        engShare.ParentId =eng.Id;
//        engShare.UserOrGroupId = portalUser.Id;
//        engShare.AccessLevel = 'Read';
//        insert engShare;
        system.debug('Successful Update job');
        /*cs_NCR = new VM_NonConflictRules__c(Name ='1' ,NonConflictRolePair__c = 'Writers:Writers');
        cs_LTP = new VM_LevelThreePairs__c(Name ='1' ,Data__c = 'Writers:Writers');
        cs_JobAppRT = new JobApplicationRecordTypeSelection__c(Engagement_Type__c = 'Paid Contributors', Name = '10-8997',
                                                               JobApplication_RecordType__c = 'MemberMicro',
                                                               Role_Type__c = 'Abstractors' );
        Insert cs_NCR;
        insert cs_LTP;
        insert cs_JobAppRT;*/
    }
    
    
    public static testMethod void testAMappStatus1() {
        setupdata();
        //Task tsk = VM_TaskCreateEditDetail.getNewTask();
        
        try
        {       
            Test.startTest();
            //Engagement_Volunteer__c engV=new Engagement_Volunteer__c();
            //engV=[select Id,Engagement__c,Engagement__r.name,Role_del__c,Role_del__r.name,Contact__c,Contact__r.name, status__c from Engagement_Volunteer__c where Engagement__c=:eng.Id];
            
            System.runAs(portalUser){
                
               VM_ApplicationStatus_Controller.updateStatus(job.id, 'Agree');
                 Document doc = new Document(FolderId = UserInfo.getUserId() ,Name='Volunteer Acknowledgement Form', Body = Blob.ValueOf('Hello World'));
        insert doc;
                VM_ApplicationStatus_Controller.getacknowledgeform();
                VM_ApplicationStatus_Controller.getCheckbox(job.id, job);
                VM_ApplicationStatus_Controller.getROle(job.Id);
                VM_ApplicationStatus_Controller.getStatus(job.Id);
            }
            Test.stopTest();
        }
        catch(DMLException ex)
        {
            system.debug('testAMappStatus'+ex.getDMLMessage(0));
        }
        
    }
     public static testMethod void testAMappStatus2() {
        setupdata();
        //Task tsk = VM_TaskCreateEditDetail.getNewTask();
        
        try
        {       
            Test.startTest();
            //Engagement_Volunteer__c engV=new Engagement_Volunteer__c();
            //engV=[select Id,Engagement__c,Engagement__r.name,Role_del__c,Role_del__r.name,Contact__c,Contact__r.name, status__c from Engagement_Volunteer__c where Engagement__c=:eng.Id];
            
            System.runAs(portalUser){
                
               VM_ApplicationStatus_Controller.updateStatus(job.id, 'Reject');
            }
            job.I_agree_that_this_information_is_correct__c = false;
            update job;
            System.runAs(portalUser){
                
               VM_ApplicationStatus_Controller.updateStatus(job.id, '');
            }
            Test.stopTest();
        }
        catch(DMLException ex)
        {
            system.debug('testAMappStatus2'+ex.getDMLMessage(0));
        }
        
    }
     
}
public class cfa_vm_InviteVolunteer{
     @InvocableMethod(label='Invite Volunteer')
    public static List<Id> doSendEmail(List<String> volunteerId) {
        List<Engagement_Volunteer__c> currentRecord= [Select Role_del__c,Role_del__r.Name,id,cfa_Parent_Volunteer__c,Contact__r.FirstName,Status__c,Contact__r.MDM_Preferred_Email_for_Volunteering__c from Engagement_Volunteer__c where id IN:volunteerId];
        System.debug(volunteerId);
        System.debug(currentRecord);
        List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>();
        String taskLabel = Label.VM_CommunityHomepage;
        OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'cfavolunteers@cfainstitute.org'];
        EmailTemplate invTemplate = [ SELECT Id, Name, Subject, Body,HtmlValue FROM EmailTemplate  WHERE Name ='VM_VolunteerExtensionInvitation']; 
        
        for(Engagement_Volunteer__c each: currentRecord) {
            if( each.Contact__r.MDM_Preferred_Email_for_Volunteering__c != null) {
                string coomunityurl = taskLabel + '/s/volunteerinvitation?VolunteerId='+each.Id;
                    string subject = invTemplate.subject;
                    string body = invTemplate.HtmlValue;
                    body = body.replace('contactName',each.Contact__r.FirstName==null?'':each.Contact__r.FirstName);
                    body = body.replace('roleName',each.Role_del__r.Name);
                    body = body.replace('this link','<a href="'+coomunityurl +'">this link</a>');
                    body = body.replace('Forgot your password?','<a href="'+label.VMforgotpassword+'">Forgot your password?</a>');
                    body = body.replace('Contact us.','<a href="'+label.VMContactUs+'">Contact us.</a>');
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
                    mail.setOrgWideEmailAddressId(owea.get(0).Id);
                    mail.setSubject(subject);
                    System.debug(body);
                    mail.setHTMLBody(body);
                    mail.setSaveAsActivity(false);
                    mail.setToAddresses(new List<String>{each.Contact__r.MDM_Preferred_Email_for_Volunteering__c});
                    mailList.add(mail);
            }
        }
        Messaging.sendEmail(mailList);    
        return null;
    }
}
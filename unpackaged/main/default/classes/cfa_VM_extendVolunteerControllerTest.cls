@isTest
public class cfa_VM_extendVolunteerControllerTest {
    static testMethod void checkExtends() {
        cfa_VM_extendVolunteerController.getIconName('case');
        cfa_VM_extendVolunteerController.getIconName('Engagement_Volunteer__c');
        
        Account VM_Account = new Account();
        VM_Account.Name ='Test_VMAcc1';
        VM_Account.Phone = '222568974';
        insert VM_Account;
         
        Contact VM_Contact = new Contact();
        VM_Contact.RecordTypeID = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Local Member').getRecordTypeId();
        VM_Contact.LastName = 'Test_LVM1';
        VM_Contact.FirstName = 'Test_FVM1';
        VM_Contact.AccountId = VM_Account.ID;
        VM_Contact.Email = 'TestVM_test01@test.com';
        VM_Contact.Phone = '0123456789';
        VM_Contact.MDM_Preferred_Email_for_Volunteering__c = 'TestVM_test01@test.com';
        insert VM_Contact;
        
        Engagement__c VM_Engage=new Engagement__c();
         User RMOwner=VM_TestDataFactory.createUser('RMTest5236@gmail.com','RMTest','Relationship Manager Staff','Relationship Manager');
        User MDUSer=VM_TestDataFactory.createUser('MDTest5236@gmail.com','MDTest','Outreach Management User','Managing Director');
        User ReportsToUSer=VM_TestDataFactory.createUser('Anne5236@gmail.com','Lange','Outreach Management User','Relationship Manager');
        
        VM_Engage=VM_TestDataFactory.createEngagement('t FY20',RMOwner,MDUSer,ReportsToUSer);
        VM_Engage.Approval_Status__c = 'Approved';
        insert VM_Engage;
        
        
        Role__c VM_Role=new Role__c();
        VM_Role.Engagement_name__c=VM_Engage.id;
        VM_Role.Position_Title__c='Test_VMRole';
        VM_Role.Role_Type__c='CIPM Committee Member';
        VM_Role.Conflict_filter__c='Writers';
        VM_Role.Number_of_Positions__c=123;
        VM_Role.Start_date__c=Date.today();
        VM_Role.Recruiting_Start_Date__c=Date.today();
        VM_Role.Recruiting_End_Date__c=Date.today().addDays(10);
        VM_Role.PC_Check_required__c=true;
        VM_Role.Confidential__c=true;
        
       
        VM_Role.Volunteer_Impact__c='Test';
        VM_Role.Volunteer_Work_location__c='Test';
        VM_Role.Volunteer_Roles_and_Responsibilities__c='Test';
        VM_Role.Volunteer_Experience__c='Test';
        VM_Role.Volunteer_Compet__c='Chief Financial Officer (CFO)';
        VM_Role.Volunteer_Certifications__c='CFA Charterholder';
        VM_Role.Conflicts_Specific_To_The_Role__c='Employment and/or affiliation with prep provider prohibited';
        VM_Role.VMApprovalStatus__c=true;
        System.debug('VM_Role.name = '+VM_Role.name);
        insert VM_Role;
        
       
        System.debug([SELECT Id,Name FROM Role__c WHERE Id =: VM_ROLE.Id].Name);
        
        List<Engagement__c > lstEng = new List<Engagement__c >();
        lstEng.add(VM_Engage);
        String json1 = JSON.serialize(lstEng);
        String engId = cfa_VM_extendEngagementController.saveEngagement(json1,VM_Engage.Id);
       
       VM_Engage.MD__c = null; 
       VM_Engage.Business_Owner__c = null; 
       VM_Engage.Reports_To__c = null; 
       VM_Engage.Engagement_External_Id__c = null;
       
       update VM_Engage;
       
       Engagement_Volunteer__c engVol=new Engagement_Volunteer__c();
       engVol.Engagement__c=VM_Engage.id;
       engVol.Role_del__c=VM_Role.id;
       engVol.Contact__c=VM_Contact.id;
       engVol.Status__c='On-boarded';
       insert engVol;
       
       
       //cfa_VM_extendVolunteerController.getVolunteers(VM_Engage.id);
       cfa_VM_extendEngagementController.getIconName('case');
       cfa_VM_extendEngagementController.getIconName('Engagement_Volunteer__c');
       
       cfa_VM_extendEngagementController.getInitDetails(VM_Engage.Id);
       
       engId = cfa_VM_extendEngagementController.saveEngagement(json1,VM_Engage.Id);
       
       Engagement__c copyeng = [Select id from Engagement__c where id=:engId];
       
       cfa_VM_extendRolesController.getIconName('case');
       cfa_VM_extendRolesController.getIconName('Engagement_Volunteer__c');
       Engagement__c obE = new Engagement__c();
       obE.id = engId;
       obE .Approved__c = true;
       obE .Approval_Status__c = 'Approved';
       update obE;
       
       cfa_VM_extendRolesController.getRoles(engId );
       cfa_VM_extendRolesController.checkEligibility(engId );
       List<Role__c> lstRole = new List<Role__c>();
       lstRole.add(VM_Role);
       String jsonRole = JSON.serialize(lstRole);
       cfa_VM_extendRolesController.saveRoles(jsonRole ,engId );
       List<cfa_VM_extendRolesController.RoleWrapper> lstWrap = new List<cfa_VM_extendRolesController.RoleWrapper>();
       cfa_VM_extendRolesController.RoleWrapper obWrap = new cfa_VM_extendRolesController.RoleWrapper();
       obWrap.roles = VM_Role;
       obWrap.isChecked = true;
       obWrap.obEng = null;
       lstWrap.add(obWrap);
       jsonRole = JSON.serialize(lstWrap);
       cfa_VM_extendRolesController.saveRoles(jsonRole ,engId );
       Role__c obR = [Select id from Role__c where id !=:VM_Role.id];
       obR.VMApprovalStatus__c = true;
       update obR;
       cfa_VM_extendVolunteerController.getVolunteers(engId);
       
       cfa_VM_extendVolunteerController.checkEligibility(engId);
       List<cfa_VM_extendVolunteerController.volunteerWrapper> lstVol = new list<cfa_VM_extendVolunteerController.volunteerWrapper>();
       cfa_VM_extendVolunteerController.volunteerWrapper obVol = new cfa_VM_extendVolunteerController.volunteerWrapper();
       obVol.Volunteer = engVol;
       obVol.isChecked = true;
       lstVol.add(obVol);
       jsonRole = JSON.serialize(lstVol);
       cfa_VM_extendVolunteerController.saveVolunteers(jsonRole ,engId);
       cfa_vm_InviteVolunteer.doSendEmail(new List<String>{engVol.id});
    }
}
/**************************************************************************************************
* Apex Class Name   : EnrollmentWrapper_Test
* Purpose           : This is the wrapper class which is used for mapping Individual key of Enrollment Json Response into seperate field
* Version           : 1.0 Initial Version
* Organization      : Cognizant
* Created Date      : 27-Mar-2018  
***************************************************************************************************/
@isTest
public class EnrollmentWrapper_Test{

    public static testMethod void EnrollmentWrapper_Test(){
        EnrollmentWrapper EWrapper = new EnrollmentWrapper();
        EWrapper.Program = 'TestPr1';
        EWrapper.EnrollmentStatus = 'TestES1';
        EWrapper.EnrollmentDate = Date.today();
        EWrapper.RegistrationStatus = 'TestRS1';
        EWrapper.Level = 'TestLv1';
        EWrapper.Cycle = 'TestCy1';
        EWrapper.Year = 1;
        EWrapper.TestAreaCode = 'TestAreaCode1';
        EWrapper.TestAreaCode = 'TestAreaCode1';
        EWrapper.TestCenterCode = 'TestCenterCode1';
        EWrapper.IsRAD = true;
        EWrapper.QualificationReason = 'QualificationReason1';
        EWrapper.RegistrationDate = Date.today();
        EWrapper.CompletedDate = Date.today();
        EWrapper.CancellationDate = Date.today();
        EWrapper.ExamAttendanceStatus = 'Test EAT1';
        EWrapper.ExamResultsStatus = 'Test ERS1';
        EWrapper.ExamResultsVisibilityStatus = 'Test ERVS';
        System.assertEquals(EWrapper.Program, 'TestPr1');
    
    }
    
    
}
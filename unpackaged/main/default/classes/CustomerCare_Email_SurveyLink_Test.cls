/**************************************************************************************************
* Apex Class Name   : CustomerCare_Email_SurveyLink_Test
* Purpose           : This test class is used for validating CustomerCare_CreateEmailForContact   
* Version           : 1.0 Initial Version
* Organization      : Cognizant
* Created Date      : 15-Nov-2017   
***************************************************************************************************/
@isTest
public class CustomerCare_Email_SurveyLink_Test {
    
    static testMethod void EmailSurveyLinks() {
        boolean isSB =[SELECT IsSandbox FROM Organization LIMIT 1].IsSandbox;
        
          SocietyOps_Backoffice__c obj = new SocietyOps_Backoffice__c(name='trigger-details',CaseReassignment_TemplateName__c='SocietyOps-Case Reassignment',CaseComment_TemplateName__c='SocietyOps-New Comment Notification');
        if(isSB==true)
        {
            obj.No_reply_email__c='researchchallenge@cfainstitute.org';
         }
        else
        {
            obj.No_reply_email__c='noreply.societyoperations@cfainstitute.org';
         }
        insert obj;
        CustomerCare_CommonTestData.testDataSurveyEmailSettings();
        User usr = CustomerCare_CommonTestData.testDataUser();
        System.runAs(usr){
            Id AccId = CustomerCare_CommonTestData.testDataAccount()[0].id;
            system.assert(AccId != Null);
            Contact Con = CustomerCare_CommonTestData.testDataContact();
            
            System.debug('Contact: '+Con);
            
            Id CCRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('CustomerCare Front Office Record Type').getRecordTypeId();
            List<Case>  NewEmailCaseList = new List<Case>();
            Case deleteCase = new Case(Delete_Attachment__c=true,Status = 'New', Origin = 'Phone',RecordTypeId=CCRecordType,ContactId=Con.id, Priority='Medium', Subject = 'TestEmailTOCaseVerification', Description='Testing Email To case, CFA Institute Membership 47856985256314');
            Case emailCase = new Case(Delete_Attachment__c=true,Exam_Security__c=true,Status = 'New', Origin = 'Email',RecordTypeId=CCRecordType,ContactId=Con.id, Priority='Medium', Subject = 'TestEmailTOCaseVerification', Description='Testing Email To case, CFA Institute Membership 47856985256314');
            //system.assertEquals(emailCase.Origin,'Email');
            NewEmailCaseList.add(deleteCase);   
            NewEmailCaseList.add(emailCase);
            insert NewEmailCaseList;
                               
            ContentVersion content=new ContentVersion(); 
            content.Title='Header_Picture1'; 
            content.PathOnClient='/' + content.Title + '.jpg'; 
            Blob bodyBlob1=Blob.valueOf('Unit Test ContentVersion Body'); 
            content.VersionData=bodyBlob1; 
            //content.LinkedEntityId=sub.id;
            content.origin = 'H';
            insert content;
            
            ContentDocumentLink contentlink=new ContentDocumentLink();
            contentlink.LinkedEntityId=NewEmailCaseList[0].id;
            contentlink.contentdocumentid=[select contentdocumentid from contentversion where id =: content.id].contentdocumentid;
            contentlink.ShareType = 'V';            
            insert contentlink;
            
            Attachment attach=new Attachment();     
            attach.Name='Unit Test Attachment';
            Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
            attach.body=bodyBlob;
            attach.parentId=NewEmailCaseList[0].id;
            insert attach;
                     
            FeedItem fI_DeleteCase=  new FeedItem();
            fI_DeleteCase.ParentId = NewEmailCaseList[0].id; 
            fI_DeleteCase.Title='Phone Case feed';
            fI_DeleteCase.Body = 'This is Phone Case';
            fI_DeleteCase.Type='CreateRecordEvent';
            insert fI_DeleteCase;
            
            EmailMessage eMessage=new EmailMessage();
            eMessage.ParentId=NewEmailCaseList[1].ID;
            eMessage.subject=NewEmailCaseList[1].Subject;
            eMessage.TextBody=NewEmailCaseList[1].Description;
            eMessage.FromAddress=NewEmailCaseList[1].SuppliedEmail; 
            eMessage.ToAddress = NewEmailCaseList[1].SuppliedEmail; 
            insert eMessage;
            
            FeedItem fI_EmailCase =  new FeedItem();
            fI_EmailCase.ParentId = NewEmailCaseList[1].id; 
            fI_EmailCase.Title='Email Case feed';
            fI_EmailCase.Body = 'This is Email Case';
            fI_EmailCase.Type='CreateRecordEvent';
            insert fI_EmailCase;
            
            for(case clCase : NewEmailCaseList){
              clCase.status ='Closed';
              clCase.Resolution_Type__c='Resolved';
            }
            
            Update NewEmailCaseList;
            CustomerCare_CommonTestData.TestDataForETOfSruveyLink(); // For inserting Email Templates
            test.startTest();
            // To Cover Member contact condition
            List<Case> caseLstOfMembers = [Select Id,Delete_Attachment__c,Sub_Area__c,Area__c,Exam_Security__c,RecordTypeId,Customer_Type_Member__c,Status,ContactId,Customer_Type__c,Origin,Priority,Subject,Resolution_Type__c,Description from Case where ID IN :NewEmailCaseList];
            CustomerCare_Email_SurveyLink.ccSendSurveyLinks(caseLstOfMembers);
            
            // To Cover CFA contact condition
            Con.CFAMN__CFACandidateCode__c='1C';
            Con.CFAMN__CFAMemberCode__c = '';
            Update Con;
            List<Case> caseLstOfCFAs = [Select Id,Delete_Attachment__c,Sub_Area__c,Area__c,Exam_Security__c,RecordTypeId,Customer_Type_Member__c,Status,ContactId,Customer_Type__c,Origin,Priority,Subject,Resolution_Type__c,Description from Case where ID IN :NewEmailCaseList];
            CustomerCare_Email_SurveyLink.ccSendSurveyLinks(caseLstOfCFAs);
            
            // To Cover CIPM contact condition
            con.CFAMN__CIPMCandidateCode__c='1C';
            Con.CFAMN__CFACandidateCode__c='';
            Update Con;
            List<Case> caseLstCIPMs = [Select Id,Delete_Attachment__c,Sub_Area__c,Area__c,Exam_Security__c,RecordTypeId,Customer_Type_Member__c,Status,ContactId,Customer_Type__c,Origin,Priority,Subject,Resolution_Type__c,Description from Case where ID IN :NewEmailCaseList];
            CustomerCare_Email_SurveyLink.ccSendSurveyLinks(caseLstCIPMs);
            
            // To Cover Claritas (Investment Foundation) contact condition
            Con.CFAMN__ClaritasCandidateCode__c='1C';
            con.CFAMN__CIPMCandidateCode__c='';
            Update Con;            
            List<Case> caseLstInvFounds = [Select Id,Delete_Attachment__c,Sub_Area__c,Area__c,Exam_Security__c,RecordTypeId,Customer_Type_Member__c,Status,ContactId,Customer_Type__c,Origin,Priority,Subject,Resolution_Type__c,Description from Case where ID IN :NewEmailCaseList];
            CustomerCare_Email_SurveyLink.ccSendSurveyLinks(caseLstInvFounds);
            
            //For OptedOutOfEmail contacts
            con.HasOptedOutOfEmail=true;
            Update Con;            
            List<Case> caseLstOptedOutOfEmail = [Select Id,Delete_Attachment__c,Sub_Area__c,Area__c,Exam_Security__c,RecordTypeId,Customer_Type_Member__c,Status,ContactId,Customer_Type__c,Origin,Priority,Subject,Resolution_Type__c,Description from Case where ID IN :NewEmailCaseList];
            CustomerCare_Email_SurveyLink.ccSendSurveyLinks(caseLstOptedOutOfEmail); 
             test.stopTest();
        }
       
    }
}
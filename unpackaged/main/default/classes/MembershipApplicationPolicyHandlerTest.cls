/*****************************************************************
Name: MembershipApplicationPolicyHandlerTest
Copyright © 2021 ITC
============================================================
Purpose: Unit test for MembershipApplicationPolicyHandler
============================================================
History
-------
VERSION  AUTHOR          DATE         DETAIL    Description
1.0      Alona Zubenko   11.02.2021   Created   Cover the Logic for Assigning Society By Membership Application
*****************************************************************/

@IsTest
private class MembershipApplicationPolicyHandlerTest {
    private static Integer NUMBER_OF_CONTACTS = 10;

    @TestSetup
    private static void init() {
        MembershipApplicationTriggerHandler.triggerDisabled = true;

        Account accCfaInstitute = CFA_TestDataFactory.createAccount('CFA Institute', 'Society', true);

        Map<String, Account> accountByNameMap = new Map<String, Account>{
                'CFA China' => CFA_TestDataFactory.createAccount('CFA China', 'Society', false)
                , 'CFA Germany' => CFA_TestDataFactory.createAccount('CFA Germany', 'Society', false)
                , 'CFA Ukraine' => CFA_TestDataFactory.createAccount('CFA Ukraine', 'Society', false)
                , 'CFA Belgium' => CFA_TestDataFactory.createAccount('CFA Belgium', 'Society', false)
        };
        insert accountByNameMap.values();

        Map<String, NSA_Policy__c> policyByNameMap = new Map<String, NSA_Policy__c>{
                'NSA China' => CFA_TestDataFactory.createNsaPolicy(accountByNameMap.get('CFA China').Id, 'CFA_China__c', null, null, null, false)
                , 'NSA Germany' => CFA_TestDataFactory.createNsaPolicy(accountByNameMap.get('CFA Germany').Id, 'CFA_Society_Germany__c', null, null, null, false)
                , 'NSA Ukraine' => CFA_TestDataFactory.createNsaPolicy(accountByNameMap.get('CFA Ukraine').Id, 'CFA_Society_Ukraine__c', null, null, null, false)
                , 'NSA Belgium' => CFA_TestDataFactory.createNsaPolicy(accountByNameMap.get('CFA Belgium').Id, 'CFA_Society_Belgium__c', null, null, null, false)
        };
        insert policyByNameMap.values();

        List<Contact> contacts = new List<Contact>();

        List<Contact> chinaContacts = CFA_TestDataFactory.createContactRecords('CFA Contact', 'CFAContactChina', NUMBER_OF_CONTACTS , false);
        List<Contact> germanyContacts = CFA_TestDataFactory.createContactRecords('CFA Contact', 'CFAContactGermany', NUMBER_OF_CONTACTS, false);
        List<Contact> belgiumContacts = CFA_TestDataFactory.createContactRecords('CFA Contact', 'CFAContactBelgium', NUMBER_OF_CONTACTS, false);
        List<Contact> ukraineContacts = CFA_TestDataFactory.createContactRecords('CFA Contact', 'CFAContactUkraine', NUMBER_OF_CONTACTS, false);

        contacts.addAll(chinaContacts);
        contacts.addAll(germanyContacts);
        contacts.addAll(belgiumContacts);
        contacts.addAll(ukraineContacts);

        insert contacts;

        List<Membership_Application__c> membershipApplicationChina = CFA_TestDataFactory.createMembershipApplications(accountByNameMap.get('CFA China').Id, chinaContacts, true);
        List<Membership_Application__c> membershipApplicationBelgium = CFA_TestDataFactory.createMembershipApplications(accountByNameMap.get('CFA Belgium').Id, belgiumContacts, true);
        List<Membership_Application__c> membershipApplicationGermany = CFA_TestDataFactory.createMembershipApplications(accountByNameMap.get('CFA Germany').Id, germanyContacts, true);
        List<Membership_Application__c> membershipApplicationUkraine = new List<Membership_Application__c>();
        for (Contact con : ukraineContacts) {
            membershipApplicationUkraine.add(
                    (Membership_Application__c) CFA_TestDataFactory.create(
                            new Membership_Application__c().getSObjectType(),
                            null,
                            new Map<String,Object>{
                                'Date_Submitted__c' => Date.today().addMonths(-6).addDays(-1),
                                'Account__c' => accountByNameMap.get('CFA Ukraine').Id,
                                'Contact__c'=> con.Id
                            },
                            false
                    )
            );
        }
        insert membershipApplicationUkraine;

    }

    @IsTest
    static void runTest() {
        List<Contact> contacts = new List<Contact>();
        List<Contact> ukraineContacts = [SELECT Id,CFA_Society_Ukraine__c FROM Contact WHERE LastName LIKE 'CFAContactUkraine%'];
        List<Contact> belgiumContacts = [SELECT Id,CFA_Society_Belgium__c FROM Contact WHERE LastName LIKE 'CFAContactBelgium%'];
        List<Contact> chinaContacts = [SELECT Id,CFA_China__c FROM Contact WHERE LastName LIKE 'CFAContactChina%'];
        List<Contact> germanyContacts = [SELECT Id,CFA_Society_Germany__c FROM Contact WHERE LastName LIKE 'CFAContactGermany%'];

        contacts.addAll(ukraineContacts);
        contacts.addAll(belgiumContacts);
        contacts.addAll(chinaContacts);
        contacts.addAll(germanyContacts);

        List<NSA_Policy__c> policies = [
                SELECT Id
                        , Contact_Society_Field_API_Name__c
                        , Advanced_Assignment__c
                        , Advanced_Assignment_Handler__c
                        , Society_Account__c
                FROM NSA_Policy__c
                WHERE Is_Active__c = TRUE
            	  AND Policy_Status__c = 'Approved'
        ];

        Test.startTest();
        new MembershipApplicationPolicyHandler().run(policies,contacts);
        Test.stopTest();

        System.assertEquals(true,chinaContacts[0].CFA_China__c);
        System.assertEquals(true,germanyContacts[0].CFA_Society_Germany__c);
        System.assertEquals(false,ukraineContacts[0].CFA_Society_Ukraine__c);
        System.assertEquals(true,belgiumContacts[0].CFA_Society_Belgium__c);
    }
}
@isTest
public class Case_MarkFilesDeletion_Test {
	private static String CUSTOMERCARE_FRONT_OFFICE_RT = 'CustomerCare_Front_Office_Record_Type';

	private static String CASE_SUBJECT = 'CASE WITH FILES';

	@testSetup
	public static void testSetup() {
		Case newCase = CFA_TestDataFactory.createCaseRecord(CUSTOMERCARE_FRONT_OFFICE_RT, false);
		newCase.Subject = CASE_SUBJECT;
		insert newCase;

		EmailMessage emailMessage = CFA_TestDataFactory.createEmailMessageRecord(newCase.Id, true);

		CFA_TestDataFactory.createFirstContentVersionRecord(emailMessage.Id, true);
	}

	@isTest
	public static void getCaseFilesTest() {
		Case existingCase = [SELECT Id FROM Case WHERE Subject = :CASE_SUBJECT LIMIT 1];
		Test.startTest();
		List<Case_MarkFilesDeletion.CaseFiles> caseFiles = Case_MarkFilesDeletion.getCaseFiles(existingCase.Id);
		Test.stopTest();
		System.assertEquals(1, caseFiles.size());
	}

	@isTest
	public static void deleteFilesTest() {
		Case existingCase = [SELECT Id FROM Case WHERE Subject = :CASE_SUBJECT LIMIT 1];
		EmailMessage existingEmailMessage = [SELECT Id FROM EmailMessage WHERE RelatedToId =: existingCase.Id LIMIT 1];

		ContentDocumentLink existingDocumentLink = [SELECT Id, ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId=:existingEmailMessage.Id LIMIT 1];
		System.assertNotEquals(null, existingDocumentLink);

		Test.startTest();
		Case_MarkFilesDeletion.deleteFiles(new List<Id>{ existingDocumentLink.ContentDocumentId });
		Test.stopTest();

		Integer existingDocumentCountCheck = [SELECT count() FROM ContentDocument WHERE Id=:existingDocumentLink.Id];
		System.assertEquals(0, existingDocumentCountCheck);
	}
}
/**
   * Author: Abhijeet Upadhyay
   * Description: Handler class for the trigger on Engagement Volunteer object 
   * Date Created: 20-Sept-2017
   * Version: 0.2
   */
public class VM_TriggerHelperEngagementVolunteer {
    private boolean isExecuting=false;
    private integer batchSize=0;
    
    public VM_TriggerHelperEngagementVolunteer(boolean isExecutingTrigger, integer batchSizeTrigger){
        isExecuting = isExecutingTrigger;
        batchSize = batchSizeTrigger;
    }
    
    //for validating whether any assessment exists for engagement volunteer or not
    public void onBeforeUpdate(List<Engagement_Volunteer__c> newenggvol){
        Integer a=0;
        Map<Id,Engagement_Volunteer__c> map_engg_vol=new Map<Id,Engagement_Volunteer__c>();
        for(Engagement_Volunteer__c ev:newenggvol){
            if(ev.Status__c=='Off-boarded'){
                System.debug('off boarded');
                map_engg_vol.put(ev.Id,ev);
            }
        }
            
        for(List<AggregateResult> result : [select count(Id) total from Assessment__c where engagement_volunteer__c IN :map_engg_vol.keySet()]){
            a=Integer.valueOf(result[0].get('total'));
            if(a==0){   
                for(Engagement_Volunteer__c ev:newenggvol){
                    if(ev.Status__c=='Off-boarded'){
                        ev.addError('Volunteer cannot be offboarded as no Assessment exists.');   
                    }
                }
            }
        }
    }
    
    
    //for confidential role handling
    public void OnAfterInsert(List<Engagement_Volunteer__c> listNewEnggVol){
    
    /*******************Phase 2 : Available Position Logic******************/
    List<Role__c> roleLst=new List<Role__c>();
    List<Id> roleIdLst=new List<Id>();
    for(Engagement_Volunteer__c engVol:listNewEnggVol)
    {
      roleIdLst.add(engVol.Role_del__c);  
    }
    Map<Id,Role__c> roleMap=new Map<Id,Role__c>([select id,Available_Positions__c,Number_of_Positions__c,name from Role__c where Id IN:roleIdLst]);
    
    
    /**********************************************************************/
    
    /***********Share Engagement Volunteer with Respective Volunteers******************************************************/
    
    List<Id> conIdLst=new List<Id>();
    List<User> usrLst=new List<User>();
    Map<Id,User> volMap=new Map<Id,User>();
    for(Engagement_Volunteer__c engVol:listNewEnggVol)
    {
      conIdLst.add(engVol.Contact__c);
    }
    usrLst=[SELECT Id,ContactId FROM User WHERE ContactId IN :conIdLst LIMIT 50000];
    if(usrLst!=null)
    {
        for(User u:usrLst)
        {
            volMap.put(u.contactId,u);    
        }
    }
    /*******************************************************************************************/
        List<Engagement_Volunteer__Share> listEnggVolShare=new List<Engagement_Volunteer__Share>();
        List<Id> EngId=new List<Id>();
        String id1;
        List<Engagement__Share> list_eng_share =new List<Engagement__Share>();
        Group grp=new Group();
        grp=[SELECT id, RelatedId, Type,name FROM Group where name='RM and VM' limit 1];
        for(Engagement_Volunteer__c ev: listNewEnggVol){
            id1=String.valueOf(ev.Engagement__c).subString(0,15);
            EngId.add(id1); //EngId will now contain the list of all engagements which are part of engagement_volunteers 
        }
        list_eng_share =[select id,ParentId,UserOrGroupId,accesslevel from Engagement__Share where ParentId IN:EngId];
        for(Engagement_Volunteer__c ev:listNewEnggVol){
            if(ev.Confidential_Role__c==true){
                //for Confidential role 
                for(Engagement__Share EngAccessLvl:list_eng_share)
                {
                    if(EngAccessLvl.accesslevel=='Edit')
                    {
                        Engagement_Volunteer__Share ev1=new Engagement_Volunteer__Share();
                        ev1.parentid=ev.id;
                        ev1.UserOrGroupId =EngAccessLvl.UserOrGroupId;
                        eV1.AccessLevel = 'Read';
                        listEnggVolShare.add(ev1);
                    }
                    
                }
            } else{
            //for non-confidential role
            Engagement_Volunteer__Share evs=new Engagement_Volunteer__Share();
            evs.parentId=ev.Id;
            //evs.UserOrGroupId='00G2F000000Sefy';
            if(grp.id!=null)
            evs.UserOrGroupId=grp.id;
            evs.AccessLevel='Read';
            listEnggVolShare.add(evs);
            }
            
            /************************Share Engagement Volunteer with Respective Volunteers*******************************************************/
            if(volMap!=null)
            {
                if(volMap.get(ev.Contact__c)!=null)
                {
                    Engagement_Volunteer__Share evsnew=new Engagement_Volunteer__Share();
                    evsnew.parentId=ev.Id;  
                    evsnew.UserOrGroupId=volMap.get(ev.Contact__c).Id;
                    evsnew.AccessLevel='Read';
                    listEnggVolShare.add(evsnew);  
                }
            }
            
            
            /****************************************************************************/
            /*******************Phase 2 : Available Position Logic******************/
            if(ev.Status__c=='On-boarded')
            {
                
                if(roleMap.get(ev.Role_del__c).Available_Positions__c<=0)
                {
                    ev.addError('There are currently no Positions Available on the Role:'+roleMap.get(ev.Role_del__c).name);
                }
                else
                {
                    if(roleMap.get(ev.Role_del__c).Available_Positions__c!=null)
                    {
                        system.debug('Available Postion '+ev.Role_del__r.Available_Positions__c);
                        system.debug('New Available Position '+roleMap.get(ev.Role_del__c).Available_Positions__c);
                        
                        Role__c roleUpdate=new Role__c();
                        roleUpdate.id=ev.Role_del__c;
                        roleUpdate.Available_Positions__c=roleMap.get(ev.Role_del__c).Available_Positions__c-1;
                        roleLst.add(roleUpdate);
                    }
                }
                
            }
            
           /**********************************************************************/
        }
        system.debug('@@@@ Failed1232');
        //Database.insert(listEnggVolShare,true);
        system.debug('@@@@ Failed');
         /*******************Phase 2 : Available Position Logic******************/
         if(!roleLst.isEmpty())
         {
             update roleLst;
         }
    
    
       /**********************************************************************/

           }
           
     public void OnAfterUpdate(List<Engagement_Volunteer__c> oldEnggVolLst,List<Engagement_Volunteer__c> newEnggVolLst,Map<Id,Engagement_Volunteer__c> newEnggVolMap,Map<Id,Engagement_Volunteer__c> oldEnggVolMap){
     
        List<Role__c> roleLst=new List<Role__c>();
        List<Id> roleIdLst=new List<Id>();
        for(Engagement_Volunteer__c engVol:newEnggVolLst)
        {
          roleIdLst.add(engVol.Role_del__c);  
        }
        Map<Id,Role__c> roleMap=new Map<Id,Role__c>([select id,Available_Positions__c,Number_of_Positions__c,name from Role__c where Id IN:roleIdLst]);
         
        for(Engagement_Volunteer__c engVol:newEnggVolLst)
        {
            if(engVol.Status__c!=null)
            {
                if((engVol.Status__c!=oldEnggVolMap.get(engVol.id).Status__c)&&(engVol.Status__c=='Off-boarded'))    
                {
                    if(roleMap.get(engVol.Role_del__c).Available_Positions__c!=null)
                    {
                        system.debug('Available Postion '+engVol.Role_del__r.Available_Positions__c);
                        system.debug('New Available Position '+roleMap.get(engVol.Role_del__c).Available_Positions__c);
                        
                        Role__c roleUpdate=new Role__c();
                        roleUpdate.id=engVol.Role_del__c;
                        roleUpdate.Available_Positions__c=roleMap.get(engVol.Role_del__c).Available_Positions__c+1;
                        roleLst.add(roleUpdate);
                    }                                
                }
                else if((engVol.Status__c!=oldEnggVolMap.get(engVol.id).Status__c)&&(engVol.Status__c=='On-boarded'))    
                {
                    if(roleMap.get(engVol.Role_del__c).Available_Positions__c!=null)
                    {
                        system.debug('Available Postion '+engVol.Role_del__r.Available_Positions__c);
                        system.debug('New Available Position '+roleMap.get(engVol.Role_del__c).Available_Positions__c);
                        
                        Role__c roleUpdate=new Role__c();
                        roleUpdate.id=engVol.Role_del__c;
                        roleUpdate.Available_Positions__c=roleMap.get(engVol.Role_del__c).Available_Positions__c-1;
                        roleLst.add(roleUpdate);
                    }                                
                }
            }
        }     
 
        
         if(!roleLst.isEmpty())
         {
         
             try
             {
                 update roleLst;
              }catch(system.dmlexception e)
              {
              system.debug('OnAfterUpdate '+e.getDMLMessage(0));
              }
         }        
                     
     
     }      
           
    /*public void updateEngagementVolunteerShare( List<Engagement_Volunteer__c> lstTriggerNew, Map<Id, Engagement_Volunteer__c> mapTriggerOld ) {
        List<Engagement_Volunteer__Share> lstShareToInsert = new List<Engagement_Volunteer__Share>();
        List<Engagement_Volunteer__Share> lstShareToDelete = new List<Engagement_Volunteer__Share>();
        
        Map<Id, List<Engagement_Volunteer__Share>> mapEngVolIdToLstEngagementVolunteerShare = new Map<Id, List<Engagement_Volunteer__Share>>();
        Map<Id, Id> mapContactIdToUserId = new Map<Id, Id>();
        Set<Id> setEngagements = new Set<Id>();
        Map<Id, List<Engagement__Share>> mapEngIdToEngShare = new Map<Id, List<Engagement__Share>>();
        Group grp = [SELECT id, RelatedId, Type,name FROM Group where name='RM and VM' limit 1];
        
        for(Engagement_Volunteer__c objEngVol : lstTriggerNew ) {
            if( mapTriggerOld == null || objEngVol.Contact__c != mapTriggerOld.get(objEngVol.Id).Contact__c || objEngVol.Engagement__c != mapTriggerOld.get(objEngVol.Id).Engagement__c ) {
                mapEngVolIdToLstEngagementVolunteerShare.put( objEngVol.Id, new List<Engagement_Volunteer__Share>() );
            } 
            if( objEngVol.Contact__c != null ) {
                mapContactIdToUserId.put( objEngVol.Contact__c, null );
            }
            setEngagements.add( objEngVol.Engagement__c );
        }
        
        for( Engagement__Share objEng : [Select ParentId, AccessLevel, UserOrGroupId from Engagement__Share where ParentId In:setEngagements And AccessLevel != 'Read']) {
            if( !mapEngIdToEngShare.containsKey(objEng.ParentId) ) {
                mapEngIdToEngShare.put( objEng.ParentId, new List<Engagement__Share>{ objEng } );
            } else {
                mapEngIdToEngShare.get( objEng.ParentId).add(objEng);
            }
        }
        
        for( Engagement_Volunteer__Share objEngVOlShare : [Select Id, ParentId, UserOrGroupId, Accesslevel from Engagement_Volunteer__Share 
                                                           Where RowCause='Manual' and ParentId In:lstTriggerNew ]) {
            if( !mapEngVolIdToLstEngagementVolunteerShare.containsKey( objEngVOlShare.ParentId ) ) {
                 mapEngVolIdToLstEngagementVolunteerShare.put( objEngVOlShare.ParentId, new List<Engagement_Volunteer__Share>{ objEngVOlShare });
            } else {
                mapEngVolIdToLstEngagementVolunteerShare.get( objEngVOlShare.ParentId).add( objEngVOlShare );
            }                                                        
        }
        
        for( User objUser : [Select Id, ContactId from User where ContactId In:mapContactIdToUserId.keySet() ] ) {
            mapContactIdToUserId.put( objUser.ContactId, objUser.Id );
        }
        
        for( Engagement_Volunteer__c objEngVol: lstTriggerNew ) {
            if( mapEngVolIdToLstEngagementVolunteerShare.containsKey( objEngVol.Id ) ) {
                lstShareToDelete.addAll( mapEngVolIdToLstEngagementVolunteerShare.get( objEngVol.Id ) );
            }
            if( objEngVol.Engagement__c != null ) {
                if( mapEngIdToEngShare.containsKey(objEngVol.Engagement__c) ) {
                    for( Engagement__Share objEngShare : mapEngIdToEngShare.get(objEngVol.Engagement__c) ) {
                        lstShareToInsert.add( new Engagement_Volunteer__Share( ParentId=objEngVol.Id, UserOrGroupId=objEngShare.UserOrGroupId, AccessLevel='Read' ) );
                    }
                }
            }
            if( objEngVol.Confidential_Role__c ) {
                if( objEngVol.Contact__c != null && mapContactIdToUserId.get( objEngVol.Contact__c ) != null ) {
                    lstShareToInsert.add( new Engagement_Volunteer__Share( ParentId=objEngVol.Id, UserOrGroupId=mapContactIdToUserId.get( objEngVol.Contact__c ), AccessLevel='Read' ) );
                }
            } else {
                if( grp != null) {
                    lstShareToInsert.add( new Engagement_Volunteer__Share( ParentId=objEngVol.Id, UserOrGroupId=grp.Id, AccessLevel='Read' ) );
                }
            }
        }
        
        if( !lstShareToDelete.isEmpty() ) {
            database.delete( lstShareToDelete, false);
        }
        if( !lstShareToInsert.isEmpty() ) {
            database.insert( lstShareToInsert, false);
        }
    }   */
    
}
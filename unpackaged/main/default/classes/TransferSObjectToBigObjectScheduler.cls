/**
 * @description Scheduler to schedule batches to arhive sobject records
 */
public with sharing class TransferSObjectToBigObjectScheduler implements Schedulable {

    /**
     * Variable to mock custom metadata types in tests
     */
    @TestVisible
    private List<Big_object_data_archival_settings__mdt> testSettings;

    public void execute(SchedulableContext sc) {
        List<Big_object_data_archival_settings__mdt> settingsList = 
            [SELECT SourceSObjectApiName__c, TargetBigObjectApiName__c, SobjectQueryCriteria__c, BatchSize__c,
                    SobjectFieldsApiNames__c, BigObjectFieldsApiNames__c, DeleteSObjectRecords__c
            FROM Big_object_data_archival_settings__mdt 
            WHERE isActive__c = true WITH SECURITY_ENFORCED];
            if(Test.isRunningTest()) {
                settingsList = testSettings;
            }
        for(Big_object_data_archival_settings__mdt mdt: settingsList) {
            List<String> sobjectFields = mdt.SobjectFieldsApiNames__c.split(';');
            List<String> bigObjectFields = mdt.BigObjectFieldsApiNames__c.split(';');
            Map<String, String> mapSobjectFieldToBigObjectField = new Map<String, String>();
            for(Integer i = 0; i < (sobjectFields.size() < bigObjectFields.size() ? sobjectFields.size() : bigObjectFields.size()); i++) {
                mapSobjectFieldToBigObjectField.put(sobjectFields[i], bigObjectFields[i]);
            }
            TransferSObjectToBigObjectBatch batch = new TransferSObjectToBigObjectBatch(mdt.SourceSObjectApiName__c, mdt.TargetBigObjectApiName__c, mapSobjectFieldToBigObjectField, mdt.SobjectQueryCriteria__c, mdt.DeleteSObjectRecords__c);
            database.executebatch(batch, mdt.BatchSize__c == null ? 200 : mdt.BatchSize__c.intValue());
        }
    }

}
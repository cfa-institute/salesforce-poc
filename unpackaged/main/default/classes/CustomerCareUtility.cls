/**************************************************************************************************
* Apex Class Name   : CustomerCareUtility
* Purpose           : This class is used for declaring all the Contants Value used in CFA CC Project   
* Version           : 1.0 Initial Version
* Organization      : Cognizant
* Created Date      : 13-Nov-2017  
***************************************************************************************************/

global class CustomerCareUtility{

    global static String selfServiceCommunityOrigin = 'Web - Self Service Community';
    global static  String closedStatus = 'Closed';
    global static String ownerListValue='All Open Case'; 
    global static String queueType='Queue';
    global static String supervisorProfile='CustomerCare Supervisor';
    global static String emailToOpenCaseTempName='customerCare_Open Email Template';
    global static String frontOfficeRecordType='CustomerCare Front Office Record Type';
    global static String backOfficeRecordType='CustomerCare Back Office Record Type';
    global static String reOpenRecordType='CustomerCare Case Reopen Record Type';
    global static  String emailOrigin= 'Email';
    global static  String webOrigin = 'Web';
    /* Added for release 2 batch class */
    global static  String phoneOrigin = 'Phone';
    global static  String socialmediaOrigin = 'Social Media';
    global static  String facebookOrigin = 'Facebook';
    global static  String twitterOrigin = 'Twitter';
    
    global static  String EmailtempSurveyLinkforMember = 'CustomerCare_SurveyLink_for_Member';
    global static  String EmailtempSurveyLinkforCandidate = 'CustomerCare_SurveyLink_for_Candidate';
    global static  String EmailtempSurveyLinkforCIPMCandidate = 'CustomerCare_SurveyLink_for_CIPM_Candidate';
    global static  String EmailtempSurveyLinkforInvFoundCandidate = 'CustomerCare_SurveyLink_for_Investment_Foundation_Candidate';
    global static String escalatedStatus ='Escalated';
    global static String completedStatus='Completed';
    global static String normalPriority='Normal';
    global static String emailBackoffice='Email - Back Office'; 
    global static String frontOfficeProfile='Customer Care Front Office Agent'; 
    global static String backOfficeProfile='Customer Care Back Office Agent'; 
    global static String ccAdminProfile='CustomerCare Admin'; 
    global static String sysAdminProfile='System Administrator'; 
    global static string FrontofficeRtdevelopername = 'CustomerCare_Front_Office_Record_Type';
    global static string BackofficeRtdevelopername = 'CustomerCare_Back_Office_Record_Type';
    
    global static string QueueBoEmailWeb = 'CustomerCare - BO - Email/Web P2';
    global static string QueueFoEmailWeb = 'CustomerCare - FO - Email/Web P2';
    global static string CfainstitueRtdevelopername = 'CFA_Institute_Record_Type';
}
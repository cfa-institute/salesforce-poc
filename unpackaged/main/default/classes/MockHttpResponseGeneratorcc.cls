global class MockHttpResponseGeneratorcc implements HttpCalloutMock {
    
    @testvisible
    protected Integer code;

    public MockHttpResponseGeneratorcc(integer code){
        this.code=code;
    }
    
    global HTTPResponse respond(HTTPRequest req) {
        
        System.assertEquals('https://prdcalabrioqm.aimr.org/recordingcontrols/rest/resume', req.getEndpoint());
        System.assertEquals('POST', req.getMethod());
        //System.assertEquals('{"userdomain":"aimr","username":"Test_agent3"}', req.getBody());
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setStatusCode(code);
        return res;
    }
    
    
}
public with sharing class CampaignChangeEventTriggerHelper {
    
    /**
     * Shares campaign with territory
     * 
     * @param	recordIds				Campaign record ids share with territories
     * @param	removeOldShareRecords	if true old campaignShare records for territories and
     * 									campaigns is removed if false existing campaignshare 
     * 									records for territories would not be removed
     */
    public static void applyCampaignTerritoryVisibility(Set<String> recordIds, Boolean removeOldShareRecords) {
        try {
            if(removeOldShareRecords) {
                List<CampaignShare> existingShares = [SELECT ID FROM CampaignShare WHERE CampaignId IN :recordIds AND UserOrGroupId IN (SELECT ID FROM Group WHERE Type = 'Territory')];
                if(!existingShares.isEmpty()) {
                    delete existingShares;
                }
            }
            Map<Id, List<Campaign>> mapAccIdToCampaigns = new Map<Id, List<Campaign>>();
            for(Campaign campaign: [SELECT ID, Account_Name__c, RecordType.DeveloperName FROM Campaign WHERE ID IN :recordIds AND Account_Name__c != null]) {
                List<Campaign> campaigns = mapAccIdToCampaigns.get(campaign.Account_Name__c);
                if(campaigns == null) {
                    campaigns = new List<Campaign>();
                }
                campaigns.add(campaign);
                mapAccIdToCampaigns.put(campaign.Account_Name__c, campaigns);
            }
            List<NSA_Policy__c> policies = NSAPolicyService.getPoliciesByAccIds(mapAccIdToCampaigns.keySet());
            Map<String, Id> mapTerritoryNameToId = TerritoryService.groupTerritoryNameToId(TerritoryService.getTerritoriesByNames(SObjectService.getFieldValues(policies, 'Territory_Group_Name__c')));
            Map<Id, List<NSA_Policy__c>> mapAccIdToPolicies = NSAPolicyService.groupPoliciesByAccountId(policies);
            Map<Id, Set<Id>> mapCampaignIdToTerritoryIdsReadAcess = new Map<Id, Set<Id>>();
            Map<Id, Set<Id>> mapCampaignIdToTerritoryIdsEditAcess = new Map<Id, Set<Id>>();
            for(String accId: mapAccIdToCampaigns.keySet()) {
                List<Campaign> campaignList = mapAccIdToCampaigns.get(accId);
                List<NSA_policy__c> policiesList = mapAccIdToPolicies.get(accId);
                if(campaignList != null && policiesList != null && !campaignList.isEmpty() && !policiesList.isEmpty()) {
                    Set<Id> territoryIds = new Set<Id>();
                    for(NSA_policy__c policy: policiesList) {
                        if(mapTerritoryNameToId.get(policy.Territory_Group_Name__c) != null) {
                            territoryIds.add(mapTerritoryNameToId.get(policy.Territory_Group_Name__c));
                        }
                    }
                    for(Campaign campaign: campaignList) {
                        if(campaign.RecordType.DeveloperName == 'Campaign_Society_Type_Local') {
                            mapCampaignIdToTerritoryIdsEditAcess.put(campaign.Id, territoryIds);
                        } else {
                            mapCampaignIdToTerritoryIdsReadAcess.put(campaign.Id, territoryIds);
                        }
                    }
                }
            }
            if(!mapCampaignIdToTerritoryIdsReadAcess.isEmpty()) {
                TerritoryService.shareCampaignWithTerritories(mapCampaignIdToTerritoryIdsReadAcess, 'Read');
            }
            if(!mapCampaignIdToTerritoryIdsEditAcess.isEmpty()) {
                TerritoryService.shareCampaignWithTerritories(mapCampaignIdToTerritoryIdsEditAcess, 'Edit');
            }
        } catch (Exception ex) {
            insert CFA_IntegrationLogException.logError(ex, 'campaign territory visbility', 'CampaignChangeEventTriggerHelper', Datetime.now(), true, '', '', '', true, '');
            throw ex;
        }
    }

}
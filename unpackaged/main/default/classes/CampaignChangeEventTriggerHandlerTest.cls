@isTest
private class CampaignChangeEventTriggerHandlerTest {
    
    private static Integer CAMPAIGN_NUMBER = 40;
    private static ID CAMPAIGN_SOCIETY_TYPE_CANDIDATES = 
        Schema.SObjectType.Campaign.getRecordTypeInfosByDeveloperName()
    		.get('Campaign_Society_Type_Candidates').getRecordTypeId();
    private static ID CAMPAIGN_SOCIETY_TYPE_LOCAL = 
        Schema.SObjectType.Campaign.getRecordTypeInfosByDeveloperName()
    		.get('Campaign_Society_Type_Local').getRecordTypeId();
    
    private static String CFA_CHINA = 'CFA China';
    private static String CFA_GERMANY = 'CFA Society Germany';
    
    @TestSetup
    private static void setup() {
        
        Map<String, Account> accountByNameMap = new Map<String, Account>{
                CFA_CHINA => CFA_TestDataFactory.createAccount(CFA_CHINA, 'Society', false),
                CFA_GERMANY => CFA_TestDataFactory.createAccount(CFA_GERMANY, 'Society', false)
        };
        insert accountByNameMap.values();
        
        Map<String, NSA_Policy__c> policyByNameMap = new Map<String, NSA_Policy__c>{
                'NSA China' => CFA_TestDataFactory.createNsaPolicy(accountByNameMap.get(CFA_CHINA).Id, 'CFA_China__c', null, null, CFA_CHINA, false),
                'NSA Germany' => CFA_TestDataFactory.createNsaPolicy(accountByNameMap.get(CFA_GERMANY).Id, 'CFA_Society_Germany__c', null, null, CFA_GERMANY, false)
        };
        insert policyByNameMap.values();
    }

    @isTest
    private static void whenCreateCampaignThenShareWithTerritory() {
        Test.enableChangeDataCapture();
        
        Test.startTest();
        insert createDefCampaigns();
        Test.stopTest();
        
        Group chinaTerritoryGroup = 
            [SELECT ID
               FROM Group WHERE Type = 'Territory' 
                AND relatedId IN (SELECT ID FROM Territory2 WHERE Name = :CFA_CHINA) LIMIT 1];
        Group germanTerritoryGroup = 
            [SELECT ID
               FROM Group WHERE Type = 'Territory' 
                AND relatedId IN (SELECT ID FROM Territory2 WHERE Name = :CFA_GERMANY) LIMIT 1];
        System.assertEquals(CAMPAIGN_NUMBER / 2, [SELECT count() FROM CampaignShare WHERE UserOrGroupId = :chinaTerritoryGroup.Id AND CampaignAccessLevel = 'Read']);
        System.assertEquals(CAMPAIGN_NUMBER / 2, [SELECT count() FROM CampaignShare WHERE UserOrGroupId = :germanTerritoryGroup.Id AND CampaignAccessLevel = 'Edit']);
    }
    
    @isTest
    private static void whenUpdateCampaignAccountThenShareWithNewTerritory() {
        Test.enableChangeDataCapture();
        List<Campaign> campaigns = createDefCampaigns();
        insert campaigns;
        Test.getEventBus().deliver();
        
        Group chinaTerritoryGroup = 
            [SELECT ID
               FROM Group WHERE Type = 'Territory' 
                AND relatedId IN (SELECT ID FROM Territory2 WHERE Name = :CFA_CHINA) LIMIT 1];
        Group germanTerritoryGroup = 
            [SELECT ID
               FROM Group WHERE Type = 'Territory' 
                AND relatedId IN (SELECT ID FROM Territory2 WHERE Name = :CFA_GERMANY) LIMIT 1];
        
        Account chinaSociety = [SELECT Id FROM Account WHERE Name = :CFA_CHINA LIMIT 1];
        Account germanySociety = [SELECT Id FROM Account WHERE Name = :CFA_GERMANY LIMIT 1];
        List<Campaign> germanyCampaigns = new List<Campaign>();
        List<Campaign> chinaCampaigns = new List<Campaign>();
        for(Integer i = 0; i < campaigns.size(); i++) {
            if(campaigns[i].Account_name__c == chinaSociety.Id) {
                campaigns[i].Account_name__c = germanySociety.Id;
                germanyCampaigns.add(campaigns[i]);
            } else {
                campaigns[i].Account_name__c = chinaSociety.Id;
                chinaCampaigns.add(campaigns[i]);
            }
        }
        Test.startTest();
        update campaigns;
        Test.stopTest();
        
        System.assertEquals(CAMPAIGN_NUMBER / 2, [SELECT count() FROM CampaignShare WHERE UserOrGroupId = :chinaTerritoryGroup.Id AND CampaignId IN :chinaCampaigns AND CampaignAccessLevel = 'Edit']);
        System.assertEquals(CAMPAIGN_NUMBER / 2, [SELECT count() FROM CampaignShare WHERE UserOrGroupId = :germanTerritoryGroup.Id AND CampaignId IN :germanyCampaigns AND CampaignAccessLevel = 'Read']);
    }
    
    @isTest
    private static void whenGetTriggerNameThenReturnTriggerName() {
        System.assertEquals('CampaignChangeEventTrigger', (new CampaignChangeEventTriggerHandler()).getTriggerName());
    }
    
    private static List<Campaign> createDefCampaigns() {
        Account chinaSociety = [SELECT Id FROM Account WHERE Name = :CFA_CHINA LIMIT 1];
        Account germanySociety = [SELECT Id FROM Account WHERE Name = :CFA_GERMANY LIMIT 1];
        List<Campaign> chinaCampaigns = new List<Campaign>();
        List<Campaign> germanyCampaigns = new List<Campaign>();
        for(Integer i = 0; i < CAMPAIGN_NUMBER; i++) {
            String accId;
            if(i < CAMPAIGN_NUMBER / 2){
                Campaign campaign = CFA_TestDataFactory.createCampaign('Campaign' + i, false);
                campaign.Account_name__c = chinaSociety.Id;
                campaign.RecordTypeId = CAMPAIGN_SOCIETY_TYPE_CANDIDATES;
                chinaCampaigns.add(campaign);
            } else {
                Campaign campaign = CFA_TestDataFactory.createCampaign('Campaign' + i, false);
                campaign.Account_name__c = germanySociety.Id;
                campaign.RecordTypeId = CAMPAIGN_SOCIETY_TYPE_LOCAL;
                germanyCampaigns.add(campaign);
            }
        }
        List<Campaign> campaigns = new List<Campaign>();
        campaigns.addAll(chinaCampaigns);
        campaigns.addAll(germanyCampaigns);
        return campaigns;
    }
    
    
}
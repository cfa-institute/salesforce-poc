public abstract class BaseChangeEventTriggerHandler implements ICDCTriggerHandler {
    
    /**
     * @description Checks if trigger is disabled
     *
     * @return Boolean
     *
     */
    public virtual Boolean isDisabled() {
        return DynamicTriggerStatus.getTriggerStatus(getTriggerName());
    }
    
    /**
     * @description Handles CREATE events
     *
     * @param changeEvents List<sObject>
     *
     */
    public virtual void handleCreate(List<sObject> changeEvents) {
    }

    /**
     * @description Handles GAP_CREATE events
     *
     * @param changeEvents List<sObject>
     *
     */
    public virtual void handleGAPCreate(List<sObject> changeEvents) {
    }

    /**
     * @description Handles CREATE and GAP_CREATE events
     *
     * @param changeEvents List<sObject>
     *
     */
    public virtual void handleAllCreate(List<sObject> changeEvents) {
    }
    
    /**
     * @description Handles UPDATE events
     *
     * @param changeEvents List<sObject>
     *
     */
    public virtual void handleUpdate(List<sObject> changeEvents) {
    }
    
    /**
     * @description Handles GAP_UPDATE events
     *
     * @param changeEvents List<sObject>
     *
     */
    public virtual void handleGAPUpdate(List<sObject> changeEvents) {
    }

    /**
     * @description Handles UPDATE and GAP_UPDATE events
     *
     * @param changeEvents List<sObject>
     *
     */
    public virtual void handleAllUpdate(List<sObject> changeEvents) {
    }
    
    /**
     * @description Handles DELETE events
     *
     * @param changeEvents List<sObject>
     *
     */
    public virtual void handleDelete(List<sObject> changeEvents) {
    }
    
    /**
     * @description Handles GAP_DELETE events
     *
     * @param changeEvents List<sObject>
     *
     */
    public virtual void handleGAPDelete(List<sObject> changeEvents) {
    }

    /**
     * @description Handles DELETE and GAP_DELETE events
     *
     * @param changeEvents List<sObject>
     *
     */
    public virtual void handleAllDelete(List<sObject> changeEvents) {
    }
    
    /**
     * @description Handles UNDELETE events
     *
     * @param changeEvents List<sObject>
     *
     */
    public virtual void handleUndelete(List<sObject> changeEvents) {
    }
    
    /**
     * @description Handles GAP_UNDELETE events
     *
     * @param changeEvents List<sObject>
     *
     */
    public virtual void handleGAPUndelete(List<sObject> changeEvents) { 
    }

    /**
     * @description Handles UNDELETE and GAP_UNDELETE events
     *
     * @param changeEvents List<sObject>
     *
     */
    public virtual void handleAllUndelete(List<sObject> changeEvents) {
    }
    
    /**
     * @description Handles OVERFLOW events
     *
     * @param changeEvents List<sObject>
     *
     */
    public virtual void handleOverflow(List<sObject> changeEvents) {
        List<String> toAddresses = new List<String>();

        for (ApexEmailNotification notif : [ SELECT UserId, Email FROM ApexEmailNotification LIMIT 100 ]) {
            if(!String.isEmpty(notif.Email) ) {
                toAddresses.add(notif.Email);
            } else if (!String.isEmpty(notif.UserId)) {
                toAddresses.add(notif.UserId);
            }
        }

        if(!toAddresses.isEmpty()) {
            Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
            message.toAddresses = toAddresses;
            message.subject = 'CDC Overflow Event Occured';
            message.plainTextBody = 'CDC Overflow event occured in the ' + getTriggerName() + ' class. Please check https://developer.salesforce.com/docs/atlas.en-us.change_data_capture.meta/change_data_capture/cdc_replication_steps.htm for more information.';

            Messaging.sendEmail( new Messaging.SingleEmailMessage[] { message } );
        }
    }

    /**
     * @description Get trigger name
     *
     * @return String
     */
    public abstract String getTriggerName();
	
    @TestVisible
    protected virtual Set<String> getRecordIds(List<sObject> changeEvents) {
        Set<String> recordIds = new Set<String>();
        for (sObject event : changeEvents) {
            EventBus.ChangeEventHeader header = (EventBus.ChangeEventHeader) event.get('ChangeEventHeader');
            recordIds.addAll(header.getRecordIds());
        }
        return recordIds;
    }

    @TestVisible
    protected virtual Set<String> getRecordIds(List<sObject> changeEvents, Set<String> changedFields) {
        Set<String> recordIds = new Set<String>();
        for (SObject event : changeEvents) {
            EventBus.ChangeEventHeader header = (EventBus.ChangeEventHeader) event.get('ChangeEventHeader');

            for(String fieldName : header.changedFields) {
                if (changedFields.contains(fieldName)) {
                    recordIds.addAll(header.getRecordIds());
                }
            }
        }
        return recordIds;
    }
}
public without sharing class Case_MarkFilesDeletion {
	public class CaseFiles {
		@AuraEnabled
		public Id contentDocumentId;
		@AuraEnabled
		public String title;
		@AuraEnabled
		public String owner;
		@AuraEnabled
		public String source;
		@AuraEnabled
		public String size;

		public CaseFiles(String contentDocumentId, String title, String owner, String source, String size) {
			this.contentDocumentId = contentDocumentId;
			this.title = title;
			this.owner = owner;
			this.source = source;
			this.size = size;
		}

		public CaseFiles() {
		}
	}

	@AuraEnabled
	public static List<CaseFiles> getCaseFiles(Id caseId) {
		Set<Id> linkedEntityIds = new Set<Id>{ caseId };
		List<EmailMessage> emailMessages = [SELECT id FROM EmailMessage WHERE ParentId = :caseId];
		for (EmailMessage em : emailMessages) {
			linkedEntityIds.add(em.Id);
		}
		List<CaseFiles> caseFiles = new List<CaseFiles>();

		List<ContentDocumentLink> contentLinks = [
			SELECT Id, ContentDocumentId, ContentDocument.Title, LinkedEntity.Name, ContentDocument.ContentSize, ContentDocument.Owner.Name
			FROM ContentDocumentLink
			WHERE LinkedEntityId IN :linkedEntityIds
		];
		if (!contentLinks.IsEmpty()) {
			for (ContentDocumentLink cdl : contentLinks) {
				caseFiles.add(
					new CaseFiles(
						cdl.ContentDocumentId,
						cdl.ContentDocument.Title,
						cdl.ContentDocument.Owner.Name,
						cdl.LinkedEntity.Name,
						(cdl.ContentDocument.ContentSize / 1024) + 'KB'
					)
				);
			}
		}

		return caseFiles;
	}

	@AuraEnabled
	public static void deleteFiles(List<Id> fileIds) {
		delete [SELECT Id FROM ContentDocument WHERE Id IN :fileIds];
	}
}
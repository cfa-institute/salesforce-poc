/*****************************************************************
Name: TestDataWrappers
Copyright © 2021 ITC
============================================================
Purpose: Factory for creation SObjects for UnitTests
============================================================
History
-------
VERSION  AUTHOR          DATE         DETAIL    Description
1.0      Alona Zubenko   25.02.2021   Created   Factory for creation SObjects
*****************************************************************/

public with sharing class TestDataWrappers {
    private static Set<String> usedRandomValue = new Set<String>();
    private static Map<String, Schema.SObjectType> globalDescribeMap;
        
    private static SObjectType getSobjectTypeByObjectName(String objectName) {
        if(globalDescribeMap == null) {
            globalDescribeMap = Schema.getGlobalDescribe();
        }
        return globalDescribeMap.get(objectName);
    }

    public abstract class SObjectWrapper{
        protected Map<String,Object> fieldsWithValuesMap = new Map<String,Object>();
        protected Map<String,Integer> uniqueFieldNamesWithLength = new Map<String,Integer>();
        protected String objectName;
        
        public List<SObject> createList(String recordTypeDeveloperName, Map<String,Object> fieldsWithValues, Integer numberOfRecords){
            Id recordTypeId;
            SObjectType objType = TestDataWrappers.getSobjectTypeByObjectName(objectName);
            if(fieldsWithValues != null) {
                fieldsWithValuesMap.putAll(fieldsWithValues);
            }

            if(recordTypeDeveloperName != null){
                List<RecordType> recordTypes = [SELECT Id FROM RecordType WHERE DeveloperName = :recordTypeDeveloperName OR Name = :recordTypeDeveloperName];
                if(recordTypes.isEmpty()){
                    throw new TestDataFactoryException('Incorrect Record Type');
                }
                recordTypeId = recordTypes[0].Id;
            }

            List<SObject> objList = new List<SObject>();
            for(Integer i=0; i < numberOfRecords; i++) {
                SObject obj = objType.newSObject();

                if(recordTypeId != null){
                    obj.put('RecordTypeId',recordTypeId);
                }
                for (String fieldName : fieldsWithValuesMap.keySet()) {
                    Object fieldValue = fieldsWithValuesMap.get(fieldName);

                    if(fieldValue instanceof String) {

                        if (fieldName.toLowerCase() == 'email' || fieldName.toLowerCase() == 'username') {
                            List<String> valueArr = ((String)fieldValue).split('@');
                            fieldValue = valueArr[0] + getRandomValue(uniqueFieldNamesWithLength, fieldName) + '@' + valueArr[1];
                        } else {
                            fieldValue = fieldValue + getRandomValue(uniqueFieldNamesWithLength, fieldName);
                        }
                    }
                    obj.put(fieldName, fieldValue);
                }
                objList.add(obj);
            }
            return objList;
        }
    }

    /**
     * @description Get Random Value
     *
     * @param uniqueFieldNames Set<String>
     * @param fieldName String
     * @param lengthFiled Integer
     *
     * @return String
     */
    private static String getRandomValue(Map<String,Integer> uniqueFieldNames, String fieldName){
        String randomVal = '';
        if(uniqueFieldNames.containsKey(fieldName)){
            randomVal = generateRandomString(uniqueFieldNames.get(fieldName));
        }
        return randomVal;
    }

    /**
     * @description Generate Random String
     *
     * @param size Integer
     *
     * @return String
     */
    public static String generateRandomString(Integer size) {
        final String chars = '1234567890ABCDEFGJKLMNOPRSTabcdefjgklmncawertgfdwDEFGH';
        String newRandomValue = '';

        while (newRandomValue == '' || usedRandomValue.contains(newRandomValue)) {
            while (newRandomValue.length() < size) {
                Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), chars.length());
                newRandomValue += chars.substring(idx, idx + 1);
            }
        }
        usedRandomValue.add(newRandomValue);
        return newRandomValue;
    }

    public class TestDataFactoryException extends Exception{}

    /* ***************************** OBJECT WRAPPERS ****************************** */

    public class AccountWrapper extends SObjectWrapper{
        protected String objectName = 'Account';
        protected Map<String,Object> fieldsWithValuesMap = new Map<String,Object>{
                'Name' => 'Test'
        };
        protected Map<String,Integer> uniqueFieldNamesWithLength = new Map<String,Integer>{
                'Name' => 10
        };

        public AccountWrapper(){
            super.objectName = this.objectName;
            super.fieldsWithValuesMap = this.fieldsWithValuesMap;
            super.uniqueFieldNamesWithLength = this.uniqueFieldNamesWithLength;
        }
    }

    public class LeadWrapper extends SObjectWrapper{
        protected String objectName = 'Lead';
        protected Map<String,Object> fieldsWithValuesMap = new Map<String,Object>{
                'LastName' => 'Test',
                'FirstName' => 'Test',
                'Email' => '@gmail.com'
        };
        protected Map<String,Integer> uniqueFieldNamesWithLength = new Map<String,Integer>{
                'LastName' => 10,
                'Email' => 8
        };

        public LeadWrapper(){
            super.objectName = this.objectName;
            super.fieldsWithValuesMap = this.fieldsWithValuesMap;
            super.uniqueFieldNamesWithLength = this.uniqueFieldNamesWithLength;
        }
	}

    public class ContactWrapper extends SObjectWrapper{
        protected String objectName = 'Contact';
        protected Map<String,Object> fieldsWithValuesMap = new Map<String,Object>{
                'LastName' => 'Test',
                'FirstName' => 'Test',
                'Email' => '@gmail.com',
                'Person_Id__c' => '',
                'Partner_Id__c' => ''
        };
        protected Map<String,Integer> uniqueFieldNamesWithLength = new Map<String,Integer>{
                'LastName' => 10,
                'Partner_Id__c' => 5,
                'Person_Id__c' => 5,
                'Email' => 8
        };

        public ContactWrapper(){
            super.objectName = this.objectName;
            super.fieldsWithValuesMap = this.fieldsWithValuesMap;
            super.uniqueFieldNamesWithLength = this.uniqueFieldNamesWithLength;
        }
    }

    public class CaseWrapper extends SObjectWrapper{
        protected String objectName = 'Case';
        protected Map<String,Object> fieldsWithValuesMap = new Map<String,Object>{
                'Status' => 'New'
        };
        protected Map<String,Integer> uniqueFieldNamesWithLength = new Map<String,Integer>{};

        public CaseWrapper(){
            super.objectName = this.objectName;
            super.fieldsWithValuesMap = this.fieldsWithValuesMap;
            super.uniqueFieldNamesWithLength = this.uniqueFieldNamesWithLength;
        }
    }

    public class TaskWrapper extends SObjectWrapper{
        protected String objectName = 'Task';
        protected Map<String,Object> fieldsWithValuesMap = new Map<String,Object>{
                'Status' => 'Not Started',
                'Status' => 'Normal'
        };
        protected Map<String,Integer> uniqueFieldNamesWithLength = new Map<String,Integer>{};

        public TaskWrapper(){
            super.objectName = this.objectName;
            super.fieldsWithValuesMap = this.fieldsWithValuesMap;
            super.uniqueFieldNamesWithLength = this.uniqueFieldNamesWithLength;
        }
    }

    public class MembershipWrapper extends SObjectWrapper{
        protected String objectName = 'Membership__c';
        protected Map<String,Object> fieldsWithValuesMap = new Map<String,Object>{
                'Membership_Type__c' => 'Society'
        };
        protected Map<String,Integer> uniqueFieldNamesWithLength = new Map<String,Integer>{};

        public MembershipWrapper(){
            super.objectName = this.objectName;
            super.fieldsWithValuesMap = this.fieldsWithValuesMap;
            super.uniqueFieldNamesWithLength = this.uniqueFieldNamesWithLength;
        }
    }

    public class CampaignWrapper extends SObjectWrapper{
        protected String objectName = 'Campaign';
        protected Map<String,Object> fieldsWithValuesMap = new Map<String, Object>{
                'Name' => 'CampaignName',
                'IsActive' => true
        };
        protected Map<String,Integer> uniqueFieldNamesWithLength = new Map<String,Integer>{};

        public CampaignWrapper(){
            super.objectName = this.objectName;
            super.fieldsWithValuesMap = this.fieldsWithValuesMap;
            super.uniqueFieldNamesWithLength = this.uniqueFieldNamesWithLength;
        }
    }

    public class EmailMessageWrapper extends SObjectWrapper{
        protected String objectName = 'EmailMessage';
        protected Map<String,Object> fieldsWithValuesMap = new Map<String, Object>{};
        protected Map<String,Integer> uniqueFieldNamesWithLength = new Map<String,Integer>{};

        public EmailMessageWrapper(){
            super.objectName = this.objectName;
            super.fieldsWithValuesMap = this.fieldsWithValuesMap;
            super.uniqueFieldNamesWithLength = this.uniqueFieldNamesWithLength;
        }
    }

    public class CampaignMemberWrapper extends SObjectWrapper{
        protected String objectName = 'CampaignMember';
        protected Map<String,Object> fieldsWithValuesMap = new Map<String, Object>{};
        protected Map<String,Integer> uniqueFieldNamesWithLength = new Map<String,Integer>{};

        public CampaignMemberWrapper(){
            super.objectName = this.objectName;
            super.fieldsWithValuesMap = this.fieldsWithValuesMap;
            super.uniqueFieldNamesWithLength = this.uniqueFieldNamesWithLength;
        }
    }

    public class UserWrapper extends SObjectWrapper{
        protected String objectName = 'User';
        protected Map<String,Object> fieldsWithValuesMap = new Map<String, Object>{
                'Alias' => ''
                ,'Email' => '@testorg.com'
                ,'EmailEncodingKey' => 'UTF-8'
                ,'LastName' => 'LName'
                ,'LanguageLocaleKey' => 'en_US'
                ,'LocaleSidKey' => 'en_US'
                ,'TimeZoneSidKey' => 'America/Los_Angeles'
                ,'UserName' => '@testorg.com'
        };
        protected Map<String,Integer> uniqueFieldNamesWithLength = new Map<String,Integer>{
                'Alias' => 8
                ,'LastName' => 10
                ,'UserName' => 10
                ,'Email' => 8
        };

        public UserWrapper(){
            super.objectName = this.objectName;
            super.fieldsWithValuesMap = this.fieldsWithValuesMap;
            super.uniqueFieldNamesWithLength = this.uniqueFieldNamesWithLength;
        }
    }

    public class GroupWrapper extends SObjectWrapper{
        protected String objectName = 'Group';
        protected Map<String,Object> fieldsWithValuesMap = new Map<String, Object>{
                'Name' => 'GName'
        };
        protected Map<String,Integer> uniqueFieldNamesWithLength = new Map<String,Integer>{
                'Name' => 5
        };

        public GroupWrapper(){
            super.objectName = this.objectName;
            super.fieldsWithValuesMap = this.fieldsWithValuesMap;
            super.uniqueFieldNamesWithLength = this.uniqueFieldNamesWithLength;
        }
    }

    public class QueueSobjectWrapper extends SObjectWrapper{
        protected String objectName = 'QueueSobject';
        protected Map<String,Object> fieldsWithValuesMap = new Map<String, Object>{ };
        protected Map<String,Integer> uniqueFieldNamesWithLength = new Map<String,Integer>{};

        public QueueSobjectWrapper(){
            super.objectName = this.objectName;
            super.fieldsWithValuesMap = this.fieldsWithValuesMap;
            super.uniqueFieldNamesWithLength = this.uniqueFieldNamesWithLength;
        }
    }

    public class NSAGeographicRuleWrapper extends SObjectWrapper{
        protected String objectName = 'NSA_Geographic_Rule__c';
        protected Map<String,Object> fieldsWithValuesMap = new Map<String, Object>{ };
        protected Map<String,Integer> uniqueFieldNamesWithLength = new Map<String,Integer>{};

        public NSAGeographicRuleWrapper(){
            super.objectName = this.objectName;
            super.fieldsWithValuesMap = this.fieldsWithValuesMap;
            super.uniqueFieldNamesWithLength = this.uniqueFieldNamesWithLength;
        }
    }

    public class NSAPolicyWrapper extends SObjectWrapper{
        protected String objectName = 'NSA_Policy__c';
        protected Map<String,Object> fieldsWithValuesMap = new Map<String, Object>{
                'Advanced_Assignment__c' => false
                ,'Is_Active__c' => true
                ,'Territory_Group_Name__c' => 'CFA China'
        };
        protected Map<String,Integer> uniqueFieldNamesWithLength = new Map<String,Integer>{};

        public NSAPolicyWrapper(){
            super.objectName = this.objectName;
            super.fieldsWithValuesMap = this.fieldsWithValuesMap;
            super.uniqueFieldNamesWithLength = this.uniqueFieldNamesWithLength;
        }
    }

    public class MembershipApplicationWrapper extends SObjectWrapper{
        protected String objectName = 'Membership_Application__c';
        protected Map<String,Object> fieldsWithValuesMap = new Map<String, Object>{
                'Membership_Application_Number__c' => ''
                ,'Date_Submitted__c' => Date.today().addDays(-1)
        };
        protected Map<String,Integer> uniqueFieldNamesWithLength = new Map<String,Integer>{
                'Membership_Application_Number__c' => 10
        };

        public MembershipApplicationWrapper(){
            super.objectName = this.objectName;
            super.fieldsWithValuesMap = this.fieldsWithValuesMap;
            super.uniqueFieldNamesWithLength = this.uniqueFieldNamesWithLength;
        }
    }

    public class ContentVersionWrapper extends SObjectWrapper{
        protected String objectName = 'ContentVersion';
        protected Map<String,Object> fieldsWithValuesMap = new Map<String, Object>{};
        protected Map<String,Integer> uniqueFieldNamesWithLength = new Map<String,Integer>{};

        public ContentVersionWrapper(){
            super.objectName = this.objectName;
            super.fieldsWithValuesMap = this.fieldsWithValuesMap;
            super.uniqueFieldNamesWithLength = this.uniqueFieldNamesWithLength;
        }
    }

    public class et4ae5SendDefinitionWrapper extends SObjectWrapper{
        protected String objectName = 'et4ae5__SendDefinition__c';
        protected Map<String,Object> fieldsWithValuesMap = new Map<String, Object>{};
        protected Map<String,Integer> uniqueFieldNamesWithLength = new Map<String,Integer>{};

        public et4ae5SendDefinitionWrapper(){
            super.objectName = this.objectName;
            super.fieldsWithValuesMap = this.fieldsWithValuesMap;
            super.uniqueFieldNamesWithLength = this.uniqueFieldNamesWithLength;
        }
    }

    public class et4ae5BusinessUnitWrapper extends SObjectWrapper{
        protected String objectName = 'et4ae5__Business_Unit__c';
        protected Map<String,Object> fieldsWithValuesMap = new Map<String, Object>{
                'et4ae5__Enabled__c' => true
        };
        protected Map<String,Integer> uniqueFieldNamesWithLength = new Map<String,Integer>{};

        public et4ae5BusinessUnitWrapper(){
            super.objectName = this.objectName;
            super.fieldsWithValuesMap = this.fieldsWithValuesMap;
            super.uniqueFieldNamesWithLength = this.uniqueFieldNamesWithLength;
        }
    }
}
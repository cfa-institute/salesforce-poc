@isTest
private class NSAPolicyServiceTest {
    
    private static String CFA_CHINA = 'CFA China';
    private static String CFA_GERMANY = 'CFA Germany';
    private static String CFA_UKRAINE = 'CFA Ukraine';
    private static String CFA_BELGIUM = 'CFA Belgium';
    
    private static String CFA_INSTITUTE = 'CFA Institute';
    
    @TestSetup
    private static void setup() {
        
        Account accCfaInstitute = CFA_TestDataFactory.createAccount(CFA_INSTITUTE, 'Society', true);
        
        Map<String, Account> accountByNameMap = new Map<String, Account>{
                CFA_CHINA => CFA_TestDataFactory.createAccount(CFA_CHINA, 'Society', false),
                CFA_GERMANY => CFA_TestDataFactory.createAccount(CFA_GERMANY, 'Society', false),
                CFA_UKRAINE => CFA_TestDataFactory.createAccount(CFA_UKRAINE, 'Society', false),
                CFA_BELGIUM => CFA_TestDataFactory.createAccount(CFA_BELGIUM, 'Society', false)
        };
        insert accountByNameMap.values();
        
        Map<String, NSA_Policy__c> policyByNameMap = new Map<String, NSA_Policy__c>{
                'NSA China' => CFA_TestDataFactory.createNsaPolicy(accountByNameMap.get(CFA_CHINA).Id, 'CFA_China__c', null, null, 'CFA_China', false),
                'NSA Germany' => CFA_TestDataFactory.createNsaPolicy(accountByNameMap.get(CFA_GERMANY).Id, 'CFA_Society_Germany__c', null, null, 'CFA_Society_Germany', false),
                'NSA Ukraine' => CFA_TestDataFactory.createNsaPolicy(accountByNameMap.get(CFA_UKRAINE).Id, 'CFA_Society_Ukraine__c', null, null, 'CFA_Society_Ukraine', false),
                'NSA Belgium' => CFA_TestDataFactory.createNsaPolicy(accountByNameMap.get(CFA_BELGIUM).Id, 'CFA_Society_Belgium__c', null, null, 'CFA_Society_Belgium', false)
        };
        insert policyByNameMap.values();
        CFA_TestDataFactory.createContact('ContactName',accCfaInstitute.Id,'CFA Contact', true);
    }
    
    @isTest
    private static void whenGetPoliciesByAccIdsThenReturnPolicies() {
        Set<Id> accIds = new Set<Id>();
        for(Account acc: [SELECT ID FROM Account WHERE NAME != :CFA_INSTITUTE]) {
            accIds.add(acc.Id);
        }
        Test.startTest();
        List<NSA_Policy__c> policies = NSAPolicyService.getPoliciesByAccIds(accIds);
        Test.stopTest();
        System.assertEquals(4, policies.size());
    }
    
    @isTest
    private static void whenGetPoliciesByTerritoryGroupNamesThenReturnPolicies() {
        Test.startTest();
        List<NSA_Policy__c> policies = NSAPolicyService.getPoliciesByTerritoryGroupNames(
            new Set<String>{'CFA_China','CFA_Society_Germany','CFA_Society_Ukraine','CFA_Society_Belgium'}
        );
        Test.stopTest();
        System.assertEquals(4, policies.size());
    }
    
    @isTest
    private static void whenGroupPoliciesByAccountIdThenReturnMap() {
        Test.startTest();
        Map<Id, List<NSA_Policy__c>> result = NSAPolicyService.groupPoliciesByAccountId([SELECT ID,Society_Account__c FROM NSA_Policy__c WHERE Is_Active__c = true AND Policy_Status__c = 'Approved']);
        Test.stopTest();
        System.assertEquals(4, result.size());
    }
    
    @isTest
    private static void whenGroupPoliciesByTerritoryGroupNameThenReturnMap() {
        Test.startTest();
        Map<String, List<NSA_Policy__c>> result = NSAPolicyService.groupPoliciesByTerritoryGroupName([SELECT ID,Territory_Group_Name__c FROM NSA_Policy__c WHERE Is_Active__c = true AND Policy_Status__c = 'Approved']);
        Test.stopTest();
        System.assertEquals(4, result.size());
    }
    
    @isTest
    private static void whenChangeContactCFACheckboxesThenUpdateContactCFAFields() {
        Contact contact = [SELECT ID,CFA_China__c,CFA_Society_Germany__c,CFA_Society_Ukraine__c,CFA_Society_Belgium__c FROM Contact LIMIT 1];
        Test.startTest();
        Boolean result = NSAPolicyService.changeContactCFACheckboxes(contact, [SELECT Contact_Society_Field_API_Name__c FROM NSA_Policy__c WHERE Is_Active__c = true AND Policy_Status__c = 'Approved'], true);
        Test.stopTest();
        System.assert(result);
        System.assert(contact.CFA_China__c);
        System.assert(contact.CFA_Society_Germany__c);
        System.assert(contact.CFA_Society_Ukraine__c);
        System.assert(contact.CFA_Society_Belgium__c);
    }
    
    @isTest
    private static void whenGetContactSocietyFieldAPINamesFromPoliciesThenReturnCFASocietyFields() {
        Test.startTest();
        Set<String> result = NSAPolicyService.getContactSocietyFieldAPINamesFromPolicies();
        Test.stopTest();
        System.assertEquals(4, result.size());
    }

}
public inherited sharing class SObjectService {
    
    /**
     * Returns values from specific field from sobject list.
     * 
     * @param	sobjects	sObject records to export field values from
     * @param	fieldName	field ApiName to extract value
     * @return	field values for specific field api name
     * 
     */
    public static Set<String> getFieldValues(List<sObject> sobjects, String fieldName) {
        Set<String> result = new Set<String>();
        if(sobjects != null && !sobjects.isEmpty() && String.isNotEmpty(fieldName)) {
            for(sObject sobj: sobjects) {
                result.add((String) sobj.get(fieldName));
            }
        }
        return result;
    }

}
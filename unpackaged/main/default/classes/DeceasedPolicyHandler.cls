public without sharing class DeceasedPolicyHandler implements INSAPolicyHandler {
    
    /**
     * Updates CFA * Society checkboxes, assign to CFA Institute Account for deceased contacts
     * 
     * @param	policies	policies not used in calculations
     * @param	contacts	contacts to count and apply deceased logic
     * 
     * */
    public void run(List<NSA_Policy__c> policies, List<Contact> contacts) {
        Set<String> contactFieldApiNames = SObjectService.getFieldValues(policies, 'Contact_Society_Field_API_Name__c');
        List<Account> cfaInstituteAccounts = [SELECT Id FROM Account WHERE Name = 'CFA Institute' LIMIT 1];
        for(Contact contact: contacts) {
            if(contact.Deceased_Date__c != null && contact.Deceased_Date__c.addYears(2) < Date.today()) {
                for(String fName : contactFieldApiNames) {
                	contact.put(fName, false);
            	}
                if(!cfaInstituteAccounts.isEmpty()) {
                    contact.AccountId = cfaInstituteAccounts[0].Id;
                }
            }
        }
    }
}
@isTest
public class EmailMessageTriggerHelperTest {
    
    @isTest
    private static void testHandleCreditCardMasking(){
        boolean isSB =[SELECT IsSandbox FROM Organization LIMIT 1].IsSandbox;
        SocietyOps_Backoffice__c obj = new SocietyOps_Backoffice__c(name='trigger-details',CaseReassignment_TemplateName__c='SocietyOps-Case Reassignment',CaseComment_TemplateName__c='SocietyOps-New Comment Notification');
        if(isSB==true)
        {
            obj.No_reply_email__c='researchchallenge@cfainstitute.org';
         }
        else
        {
            obj.No_reply_email__c='noreply.societyoperations@cfainstitute.org';
         }
        insert obj;
        EmailMessage newEmail = new EmailMessage();
        newEmail.FromAddress = 'test@abc.org';
        newEmail.Incoming = True;
        newEmail.ToAddress= 'hello@670ocglw7xh4oyr5yw2zvf.8kp7yeag.8.case.salesforce.com';
        newEmail.Subject = 'Test email';
        newEmail.TextBody = '[0-9] with "-" --> 14dCC : 1234-123456-1234, 15dCC : 1234-123456-12345, 16dCC : 1234-1234-1234-1234 ==> [0-9] without spaces --> 14dCC : 12341234561234, 15dCC : 123412345612345, 16dCC : 1234123412341234 ==> [0-9] with spaces --> 14dCC : 1234 123456 1234, 15dCC : 1234 123456 12345, 16dCC : 1234 1234 1234 1234 ';
        newEmail.ParentId = CustomerCare_CommonTestData.TestDataEmailCase().Id;            
        
        insert newEmail;
        
        EmailMessage emailInserted = [SELECT TextBody FROM EmailMessage WHERE Id = :newEmail.Id ];
        System.assertEquals( true, emailInserted.TextBody.contains( '16dCC : xxxx-xxxx-xxxx-xxxx' ) );
    }

    @isTest
    private static void testHandleCreditCardMaskingNegative(){
        boolean isSB =[SELECT IsSandbox FROM Organization LIMIT 1].IsSandbox;
        SocietyOps_Backoffice__c obj = new SocietyOps_Backoffice__c(name='trigger-details',CaseReassignment_TemplateName__c='SocietyOps-Case Reassignment',CaseComment_TemplateName__c='SocietyOps-New Comment Notification');
        if(isSB==true)
        {
            obj.No_reply_email__c='researchchallenge@cfainstitute.org';
         }
        else
        {
            obj.No_reply_email__c='noreply.societyoperations@cfainstitute.org';
         }
        insert obj;
        EmailMessage newEmail = new EmailMessage();
        newEmail.FromAddress = 'test@abc.org';
        newEmail.Incoming = True;
        newEmail.ToAddress= 'hello@670ocglw7xh4oyr5yw2zvf.8kp7yeag.8.case.salesforce.com';
        newEmail.Subject = 'Test email';
        newEmail.ParentId = CustomerCare_CommonTestData.TestDataEmailCase().Id;  
        
        insert newEmail;
        
        EmailMessage emailInserted = [SELECT Subject FROM EmailMessage WHERE Id = :newEmail.Id ];
        System.assertEquals( newEmail.Subject, emailInserted.Subject );
    }

    @isTest
    private static void testHandleApproverStatusChangedToApproved(){
        boolean isSB =[SELECT IsSandbox FROM Organization LIMIT 1].IsSandbox;
        SocietyOps_Backoffice__c obj = new SocietyOps_Backoffice__c(name='trigger-details',CaseReassignment_TemplateName__c='SocietyOps-Case Reassignment',CaseComment_TemplateName__c='SocietyOps-New Comment Notification');
        if(isSB==true)
        {
            obj.No_reply_email__c='researchchallenge@cfainstitute.org';
         }
        else
        {
            obj.No_reply_email__c='noreply.societyoperations@cfainstitute.org';
         }
        insert obj;
        EmailMessage newEmail = new EmailMessage();
        newEmail.FromAddress = 'test@abc.org';
        newEmail.Incoming = True;
        newEmail.ToAddress= 'hello@670ocglw7xh4oyr5yw2zvf.8kp7yeag.8.case.salesforce.com';
        newEmail.Subject = 'Test email';
        newEmail.ParentId = CustomerCare_CommonTestData.TestDataEmailCase().Id;            
        
        insert newEmail;

        Test.startTest();
            newEmail.Approver_status__c = 'Approved';
            update newEmail;
            System.assertEquals( 1, Limits.getEmailInvocations() );
        Test.stopTest();
    }

    @isTest
    private static void testHandleApproverStatusChangedToRejected(){
        boolean isSB =[SELECT IsSandbox FROM Organization LIMIT 1].IsSandbox;
        SocietyOps_Backoffice__c obj = new SocietyOps_Backoffice__c(name='trigger-details',CaseReassignment_TemplateName__c='SocietyOps-Case Reassignment',CaseComment_TemplateName__c='SocietyOps-New Comment Notification');
        if(isSB==true)
        {
            obj.No_reply_email__c='researchchallenge@cfainstitute.org';
         }
        else
        {
            obj.No_reply_email__c='noreply.societyoperations@cfainstitute.org';
         }
        insert obj;
        EmailMessage newEmail = new EmailMessage();
        newEmail.FromAddress = 'test@abc.org';
        newEmail.Incoming = True;
        newEmail.ToAddress= 'hello@670ocglw7xh4oyr5yw2zvf.8kp7yeag.8.case.salesforce.com';
        newEmail.Subject = 'Test email';
        newEmail.ParentId = CustomerCare_CommonTestData.TestDataEmailCase().Id;            
        
        insert newEmail;

        Test.startTest();
            newEmail.Approver_status__c = 'Rejected';
            update newEmail;
            System.assertEquals( 1, Limits.getEmailInvocations() );
        Test.stopTest();
    }
}
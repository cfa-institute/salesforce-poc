public with sharing class DataPrivacyNoticeCtrl  {

    @AuraEnabled
    public static boolean acceptPrivacyPolicy() {
        try{
            User u = new User();
            u.id = userInfo.getUserId();
            u.cfa_GDPR_Accepted_Date__c = System.today();
            update u;  
            return true;
        } catch(Exception e) {
            insert CFA_IntegrationLogException.logError(e, 'User Accepts Privacy Policy', 'dataPrivacyNoticeCtrl', Datetime.now(), true, '', '', '', true, '');
            return false;
        }
    }
}
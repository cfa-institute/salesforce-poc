/*****************************************************************
Name: CampaignMemberChangeEventTriggerHandler
Copyright © 2020 ITC
============================================================
Purpose: Handler for Trigger CampaignMember Change Event
============================================================
History
-------
VERSION  AUTHOR          DATE         DETAIL    Description
1.0      Alona Zubenko   09.12.2020   Created   Update Society Related Contact Fields
*****************************************************************/

public with sharing class CampaignMemberChangeEventTriggerHandler extends BaseChangeEventTriggerHandler {
    public static Boolean triggerDisabled = false;
    private static final String TRIGGER_NAME = 'CampaignMemberChangeEventTrigger';

    /**
     * @description Check is trigger disabled
     *
     * @return Boolean
     */
    public override Boolean isDisabled() {
        if(Test.isRunningTest()) {
            return triggerDisabled;
        } else {
            return triggerDisabled || super.isDisabled();
        }
    }

    /**
     * @description Get Trigger Name
     *
     * @return String
     */
    public override String getTriggerName() {
        return TRIGGER_NAME;
    }

    /**
     * @description Handle Create Change Events
     *
     * @param changeEvents List<SObject>
     */
    public override void handleAllCreate(List<SObject> changeEvents) {
        Set<String> recordIds = getRecordIds(changeEvents);
        List<CampaignMember> campaignMembers = SocietyCampaignMemberService.getCampaignMembers(recordIds);

        CampaignMemberChangeEventTriggerHelper.updateSocietyRelatedContactFields(campaignMembers);
    }
}
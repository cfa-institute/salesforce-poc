@isTest
public class Engagement_Test
{
    public static testMethod void validateOnBeforeUpdate(){
        List<Engagement__c> englst = new List<Engagement__c>();
        User RMOwner1=VM_TestDataFactory.createUser('RMTest1@gmail.com','RMTest1','Relationship Manager Staff','');
        User MDUSer1=VM_TestDataFactory.createUser('MDTest1@gmail.com','MDTest1','Outreach Management User','Managing Director');
        User ReportsToUSer1=VM_TestDataFactory.createUser('Anne1@gmail.com','Huff','Outreach Management User','Relationship Manager');
        Engagement__c eng1 =VM_TestDataFactory.createEngagement('t FY20',RMOwner1,MDUSer1,ReportsToUSer1);
        englst.add(eng1);
        //User RMOwner2=VM_TestDataFactory.createUser('RMTest2@gmail.com','RMTest1','Relationship Manager Staff','');
        //User MDUSer2=VM_TestDataFactory.createUser('MDTest2@gmail.com','MDTest2','Outreach Management User','Managing Director');
        // User ReportsToUSer2=VM_TestDataFactory.createUser('Anne2@gmail.com','Huff','Outreach Management User','Relationship Manager');
        //Engagement__c eng2 =VM_TestDataFactory.createEngagement('t FY20',RMOwner2,MDUSer2,ReportsToUSer2);
        //englst.add(eng2);
        
       
        
        
        Engagement__c eng =VM_TestDataFactory.createEngagement('t FY20',RMOwner1,MDUSer1,ReportsToUSer1);
        //Engagement__c eng = VM_TestDataFactory.createEngagement(String Ename,User RMOwner,User MDUSer,User ReportsToUSer);
        eng.Status__c = 'New';
        eng.Name='testNameFY20';
        insert eng;
        eng.Status__c = 'Deactivated';
        eng.Deactivation_date__c = Date.today();
        eng.Reason_for_deactivation__c = 'Work reassigned to another group';
        Account acct = new Account();
		acct.Name = 'testchildqueriesacct';
		insert acct;
        
        List<Task> taskList= new List<Task>();
        Task task1 = VM_TestDataFactory.createTask();
        task1.Status = 'Deferred';
        task1.Engagement__c = eng.Id;
        task1.whatid=acct.id;
        Task task2 = VM_TestDataFactory.createTask();
        task2.Engagement__c = eng.Id;
         task2.whatid=acct.id;
        taskList.add(task1);
        taskList.add(task2);
        
        insert taskList;
        
        test.startTest();
        
        List<Engagement__c> engList = new List<Engagement__c>();
        engList.add(eng);
        
        TriggerHandlerEngagement helper = new TriggerHandlerEngagement(true,1);
        helper.onBeforeUpdate(engList);
        
        try{
            update eng;
        }
        catch(Exception e){
            Boolean expectedExceptionThrown =  (e.getMessage().contains('Please complete the incomplete Tasks left before deactivation.')) ? true : false;
            System.AssertEquals(expectedExceptionThrown, true);
        }
        
        test.stopTest();
    }
    
    public static testmethod void validateOnBeforeInsert(){
        User RMOwner1=VM_TestDataFactory.createUser('RMTest1@gmail.com','RMTest1','Relationship Manager Staff','');
        User MDUSer1=VM_TestDataFactory.createUser('MDTest1@gmail.com','MDTest1','Outreach Management User','Managing Director');
         User ReportsToUSer1=VM_TestDataFactory.createUser('Anne1@gmail.com','Huff','Outreach Management User','Relationship Manager');
        Engagement__c e1 = VM_TestDataFactory.createEngagement('testnameFY17',RMOwner1,MDUSer1,ReportsToUSer1);
        try{
        insert e1;
        }
        catch(DMLException ex){
        System.debug(ex);
          }
       Engagement__c e2 = e1.clone(false,true,false,false);
        e2.Name='tesytAbcFY17';
          e2.Engagement_External_Id__c='456';
        e2.Business_Owner_Cost_Center__c='Abc';
        e2.Start_Date__c=date.parse('11/11/2019');
        try{
        insert e2;
        }
        catch(DMLException ex){
        System.debug(ex);
          }
        List<Engagement__c> englist= new List<Engagement__c>();
        engList.add(e2);
        test.starttest();
        TriggerHandlerEngagement helper = new TriggerHandlerEngagement(true,1);
        helper.onBeforeInsert(englist);
       try{
           insert e2;
        }
        catch(Exception e){
            Boolean expectedExceptionThrown =  (e.getMessage().contains('Please make atleast one change!')) ? true : false;
            System.AssertEquals(expectedExceptionThrown, false);
        }
        
        test.stopTest(); 
    }
}
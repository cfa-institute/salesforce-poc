public class VM_VolunteerEngagementAssociationFilter {
	@AuraEnabled
	public static String checkAllConditions(Id roleId) {
		Id UserId = userinfo.getUserID();
		User u = [SELECT id, contactid FROM User WHERE Id = :UserId LIMIT 1];

		String check = VM_VolunteerEngagementAssociationFilter.checkSameEngagementEnrollment(roleId, u.contactId);
		if (check != null) {
			return check;
		} else {
			check = VM_VolunteerEngagementAssociationFilter.checkCertification(roleId, u.contactId);

			if (check != null) {
				return check;
			} else {
				check = VM_VolunteerEngagementAssociationFilter.checkConflictFilterNPPApproach(roleId, u.contactId);

				if (check != null) {
					return check;
				} else {
					String engAssociation = VM_VolunteerEngagementAssociationFilter.getRecordTypeDetail(roleId);
					return engAssociation;
				}
				//return 'Can successfully Apply';
			}
		}
	}

	@AuraEnabled
	public static String getRecordTypeDetail(Id roleId) {
		Id UserId = userinfo.getUserID();
		User u = [SELECT id, contactid FROM User WHERE Id = :UserId LIMIT 1];
		Role__c futureRole = [
			SELECT id, Engagement_name__c, Engagement_name__r.Engagement_Type__c, Role_Type__c
			FROM Role__c
			WHERE Id = :roleId
		];
		List<JobApplicationRecordTypeSelection__c> jobselectionlst = JobApplicationRecordTypeSelection__c.getall().values();

		for (JobApplicationRecordTypeSelection__c jobselect : jobselectionlst) {
			if (
				(futureRole.Engagement_name__r.Engagement_Type__c == jobselect.Engagement_Type__c) &&
				(futureRole.Role_Type__c == jobselect.Role_Type__c)
			) {
				//Retrieve the record type id by name
				//String recordTypeId = recordTypeInfo.get(jobselect.JobApplication_RecordType__c).getRecordTypeId();
				String recordTypeId = Schema.SObjectType.Job_Application__c.getRecordTypeInfosByName()
					.get(jobselect.JobApplication_RecordType__c)
					.getRecordTypeId();

				if (recordTypeId != null) {
					return 'success,' + u.contactId + ',' + roleId + ',' + jobselect.JobApplication_RecordType__c;
				}
			}
		}

		//String recordTypeId =Schema.SObjectType.Job_Application__c.getRecordTypeInfosByName().get('Default').getRecordTypeId();
		return 'success,' + u.contactId + ',' + roleId + ',' + 'Default';
		//return u.contactId+','+roleId+','+'Default';
	}

	@AuraEnabled
	public static String checkSameEngagementEnrollment(Id roleId, Id contactId) {
		List<Engagement_Volunteer__c> ev = new List<Engagement_Volunteer__c>();
		List<Job_Application__c> job = new List<Job_Application__c>();
		List<Engagement_Volunteer__c> eng = new List<Engagement_Volunteer__c>();
		Role__c newrole = [
			SELECT Engagement_name__c, Available_Positions__c, name, Relationship_Manager_Email_a__c
			FROM Role__c
			WHERE Id = :roleId
		];
		ev = [SELECT id FROM Engagement_Volunteer__c WHERE Role_del__c = :roleId AND Contact__c = :contactId AND Status__c = 'On-boarded'];

		job = [
			SELECT id
			FROM Job_Application__c
			WHERE Position__c = :roleId AND Contact__c = :contactId AND Status__c != 'Volunteer Accepted'
		];
		eng = [
			SELECT id
			FROM Engagement_Volunteer__c
			WHERE Engagement__c = :newrole.Engagement_name__c AND Contact__c = :contactId AND Status__c = 'On-boarded'
		];

		/**************************************************************************/

		/***************************************************************************/

		if (newrole.Available_Positions__c <= 0) {
			return 'There are currently no Available Positions in this Role';
		}
		if (!ev.isEmpty()) {
			return 'You are currently working in this Role<br/>For more information, please contact this Opportunity\'s relationship manager :<a>' +
				newrole.Relationship_Manager_Email_a__c +
				'</a>';
		}

		if (!job.isEmpty()) {
			return 'You have already applied for this Opportunity.<br/>For more information, please contact this Opportunity\'s relationship manager :<a>' +
				newrole.Relationship_Manager_Email_a__c +
				'</a>';
		}
		if (!eng.isEmpty()) {
			return 'You are currently working in this Engagement in another role<br/>For more information, please contact this Opportunity\'s relationship manager :<a>' +
				newrole.Relationship_Manager_Email_a__c +
				'</a>';
		}

		return null;
	}
	@AuraEnabled
	public static String checkCertification(Id roleId, Id contactId) {
		Role__c newRole = [
			SELECT Engagement_name__c, name, Volunteer_Certifications__c, Relationship_Manager_Email_a__c
			FROM Role__c
			WHERE Id = :roleId
		];
		Contact volContact = new contact();
		try {
			volContact = [
				SELECT CFA_Candidate_Code__c, CIPM_Candidate_Code__c, Investment_Foundations_Candidate_Code__c, name
				FROM Contact
				WHERE ID = :contactId
			];
		} catch (exception excp) {
		}

		//if((newRole.Volunteer_Certifications__c!=null)&&(volContact.CFAMN__CFACandidateCode__c)==null)
		if (newRole.Volunteer_Certifications__c != null) {
			if ((newRole.Volunteer_Certifications__c.contains('CFA Charterholder')) && (volContact.CFA_Candidate_Code__c != 'GRAD')) {
				return 'You do not have the required Certifications to apply for this Opportunity.<br/>For more information, please contact this Opportunity\'s relationship manager :<a>' +
					newrole.Relationship_Manager_Email_a__c +
					'</a>';
			} else if (
				newRole.Volunteer_Certifications__c.contains('CIPM certificant holder') &&
				(volContact.CFAMN__CIPMCandidateCode__c != 'GRAD')
			) {
				return 'You do not have the required Certifications to apply for this Opportunity.<br/>For more information, please contact this Opportunity\'s relationship manager :<a>' +
					newrole.Relationship_Manager_Email_a__c +
					'</a>';
			}
			if (
				(newRole.Volunteer_Certifications__c.contains('Investment Foundations certificate holder')) &&
				(volContact.Investment_Foundations_Candidate_Code__c != 'GRAD')
			) {
				return 'You do not have the required Certifications to apply for this Opportunity.<br/>For more information, please contact this Opportunity\'s relationship manager :<a>' +
					newrole.Relationship_Manager_Email_a__c +
					'</a>';
			}
		}
		return null;
	}

	@AuraEnabled
	public static string checkConflictFilterNPPApproach(Id roleId, Id contactId) {
		Set<String> FutureRoleNonConflictSet = new Set<String>();
		Set<String> LevelThreeSet = new Set<String>();
		Contact con = new Contact();
		for (VM_NonConflictRules__c rules : VM_NonConflictRules__c.getAll().values()) {
			FutureRoleNonConflictSet.add(rules.NonConflictRolePair__c);
		}
		for (VM_LevelThreePairs__c pair : VM_LevelThreePairs__c.getAll().values()) {
			LevelThreeSet.add(pair.Data__c);
		}

		List<Engagement_Volunteer__c> engVolLst = new List<Engagement_Volunteer__c>();

		try {
			con = [SELECT id, CFAMN__CFACandidateCode__c FROM Contact WHERE Id = :contactId];
			engVolLst = [
				SELECT
					id,
					name,
					Engagement__c,
					Engagement__r.Start_Date__c,
					Engagement__r.Expiration_Date__c,
					Role_del__c,
					Role_del__r.name,
					Contact__c
				FROM Engagement_Volunteer__c
				WHERE Contact__c = :contactId AND Status__c = 'On-boarded'
			];
		} catch (exception excp) {
		}

		List<Id> currentRoleIDLst = new List<Id>();
		Role__c futureRoleDetail = new Role__c();
		futureRoleDetail = [
			SELECT
				id,
				name,
				Relationship_Manager_Email_a__c,
				Conflict_filter__c,
				Engagement_name__r.Start_Date__c,
				Engagement_name__r.Expiration_Date__c
			FROM Role__c
			WHERE ID = :roleId
		];

		if (engVolLst != null) {
			for (Engagement_Volunteer__c engVol : engVolLst) {
				currentRoleIDLst.add(engVol.Role_del__c);
			}
			Map<Id, Role__c> currentRoleMap = new Map<Id, Role__c>(
				[SELECT id, Conflict_filter__c FROM Role__c WHERE ID IN :currentRoleIDLst]
			);
			for (Engagement_Volunteer__c engVol : engVolLst) {
				String currentRoleFilter = currentRoleMap.get(engVol.Role_del__c).Conflict_filter__c;
				String futureRoleFilter = futureRoleDetail.Conflict_filter__c;
				//String customSettingvalues=VM_ConflictNew__c.getValues(currentRoleFilter).FutureConflict__c;

				//FutureRoleConflictSet=new Set<String>(VM_ConflictNew__c.getValues(currentRoleFilter).FutureConflict__c.split(','));
				String combinedPair = currentRoleFilter + ':' + futureRoleFilter;
				if (!FutureRoleNonConflictSet.contains(combinedPair)) {
					if (futureRoleDetail.Engagement_name__r.Start_Date__c <= engVol.Engagement__r.Expiration_Date__c) {
						return 'The role \"<b><i>' +
							engVol.Role_del__r.name +
							'</i></b>\" has conflict with \"<b><i>' +
							futureRoleDetail.name +
							'</i></b>\"<br/>For more information, please contact this Opportunity\'s relationship manager :<a>' +
							futureRoleDetail.Relationship_Manager_Email_a__c +
							'</a>';
					}
				} else {
					if (LevelThreeSet.contains(combinedPair)) {
						if (con.CFAMN__CFACandidateCode__c != '3C') {
							return 'The role \"<b><i>' +
								engVol.Role_del__r.name +
								'</i></b>\" has conflict with \"<b><i>' +
								futureRoleDetail.name +
								'</i></b>\" Relating to 3C Certificate';
						}
					}
				}
			}
		}
		return null;
	}
}
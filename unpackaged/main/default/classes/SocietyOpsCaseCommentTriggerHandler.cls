/**
* @author 7Summits Inc
* @date 06-21-2019
* @description This trigger handler is responsible for sending emails when the case comments are either added or updated
* by other than the owner of the case. This is only applicable for Society Tech Case record type
*/
public with sharing class SocietyOpsCaseCommentTriggerHandler {


    public static void sendEmail(){

        //change only applicable to society tech record type
        Id societyTechCaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Society Tech Case').getRecordTypeId();

        //Gather all the case comments that are in open statuses
        List<CaseComment> caseComments=
        [select  Parent.id, Parent.RecordTypeId, LastModifiedById, Parent.OwnerId,Parent.Status,Parent.Owner.email,
                Parent.Owner.Name,Parent.CaseNumber,Parent.Subject,Parent.SOC_ReadOnlyOwner__c  from CaseComment
        where id in : Trigger.New and Parent.RecordTypeId= :societyTechCaseRecordTypeId
        and Parent.Status!='Completed' and Parent.Status!='Resolved - Known Issue'
        and Parent.Status!='Closed - Out of Scope'];



        cfa_ApexUtilities.log('caseComments.size()'+caseComments.size());
        if(caseComments.size()>0){
            SocietyOps_Backoffice__c societyOps= SocietyOps_Backoffice__c.getValues('trigger-details');
            String caseCommentTemplateName=   societyOps.CaseComment_TemplateName__c;
            String noreplyAddress=   societyOps.No_reply_email__c;
            OrgWideEmailAddress owa = [select id,  Address from OrgWideEmailAddress where Address = :noreplyAddress];
            EmailTemplate emailTemplate = [Select id,Subject, Body from EmailTemplate where name = :caseCommentTemplateName];


            List<Messaging.SingleEmailMessage> allmsg = new List<Messaging.SingleEmailMessage>();

            for(CaseComment caseComment:caseComments) {
                cfa_ApexUtilities.log('caseComment.LastModifiedById=' + caseComment.LastModifiedById);
                //check to see if the case comments updated by other than the owner of the case.
                if (caseComment.LastModifiedById != caseComment.Parent.OwnerId) {

                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    mail.setTemplateID(emailTemplate.Id);
                    mail.setToAddresses(new String[]{caseComment.Parent.Owner.email});
                    mail.setTargetObjectId(caseComment.Parent.Owner.id);
                    String subject = emailTemplate.Subject;
                    subject = subject.replace('{!Case.CaseNumber}', caseComment.Parent.CaseNumber);
                    String plainBody = emailTemplate.Body;
                    plainBody = plainBody.replace('{!Case.CaseNumber}', caseComment.Parent.CaseNumber);
                    plainBody = plainBody.replace('{!Case.Subject}', caseComment.Parent.Subject);
                    plainBody = plainBody.replace('{!Case.Owner}', caseComment.Parent.SOC_ReadOnlyOwner__c);
                    plainBody = plainBody.replace('{!Case.Link}', System.URL.getOrgDomainUrl().toExternalForm() + '?Id=' + caseComment.Parent.id);

                    mail.setSubject(subject);
                    mail.setPlainTextBody(plainBody);
                    mail.setOrgWideEmailAddressId(owa.id);
                    mail.setSaveAsActivity(false);
                    mail.setReplyTo(owa.Address);
                    allmsg.add(mail);
                }
            }
            if (!allmsg.isEmpty()) {
                Messaging.SendEmailResult[] sampleResult = Messaging.sendEmail(allmsg, false);
                cfa_ApexUtilities.log('Send email='+sampleResult);

            }
        }


    }
}
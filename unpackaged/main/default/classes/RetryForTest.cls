/**
 * When tests are run in parallel, UNABLE_TO_LOCK_ROW errors occur where tests insert/update the same record or lock the tables with long calculations.
 * This class aims to get around that by retrying.
 */
public class RetryForTest {
	// Typically zero or one retry so this should be plenty unless there is some kind of deadlock
	private static final Integer TRIES = 50;
	private static final Integer SLEEP_MS = 100;

	public class RetryException extends Exception {
	}

	public static void upsertOnUnableToLockRow(SObject sob) {
		upsertOnUnableToLockRow(new List<SObject>{ sob });
	}

	public static void upsertOnUnableToLockRow(SObject[] sobs) {
		if (sobs.size() == 0) {
			return;
		}

		Long start = System.currentTimeMillis();
		Exception lastException;

		for (Integer i = 0; i < TRIES; i++) {
			try {
				SObject[] inserts = new List<SObject>{};
				SObject[] updates = new List<SObject>{};
				for (SObject sob : sobs) {
					if (sob.Id != null) {
						updates.add(sob);
					} else {
						inserts.add(sob);
					}
				}
				if (!inserts.isEmpty()) {
					insert inserts;
				}
				if (!updates.isEmpty()) {
					update updates;
				}
				return;
			} catch (DmlException e) {
				// Immediately throw if an unrelated problem
				System.debug(e.getMessage());
				if (!e.getMessage().contains('UNABLE_TO_LOCK_ROW')) {
					throw e;
				}
				lastException = e;
				sleep(SLEEP_MS);
			}
		}

		Long finish = System.currentTimeMillis();
		throw new RetryException(
			'Retry.upsertOnUnableToLockRow failed first id=' +
			sobs[0].Id +
			' of ' +
			sobs.size() +
			' records after ' +
			TRIES +
			' tries taking ' +
			(finish - start) +
			' ms with exception ' +
			lastException
		);
	}

	private static void sleep(Integer ms) {
		Long start = System.currentTimeMillis();
		while (System.currentTimeMillis() < start + ms) {
			// Throw away CPU cycles
		}
	}
}
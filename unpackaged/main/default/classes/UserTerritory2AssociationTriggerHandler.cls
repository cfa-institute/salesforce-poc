public with sharing class UserTerritory2AssociationTriggerHandler implements ITriggerHandler {
    // Allows unit tests (or other code) to disable this trigger for the transaction
    public static Boolean triggerDisabled = false;
    
    //Checks to see if the trigger has been disabled either by custom metadata or by running code
    public Boolean isDisabled() {
        if (Test.isRunningTest()) {
            return triggerDisabled;
        } else {
            return DynamicTriggerStatus.getTriggerStatus('UserTerritory2AssociationTrigger') || triggerDisabled;
        }
    }
    
    public void beforeInsert(List<SObject> newItems) {}
    
    public void beforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {}
    
    public void afterInsert(Map<Id, SObject> newItems) {
        UserTerritory2AssociationTriggerHelper.shareLibrariesWithUsers(newItems.values());
    }

    public void afterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {}
    
    public void afterDelete(Map<Id, SObject> oldItems) {
        UserTerritory2AssociationTriggerHelper.unshareLibrariesFromUsers(oldItems.values());
    }

    public void afterUndelete(Map<Id, SObject> oldItems) {}

    public void beforeDelete(Map<Id, SObject> oldItems) {}

}
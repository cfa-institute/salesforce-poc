/**************************************************************************************************
* Apex Class Name   : ResearchChallengeCalloutControllerTest
* Purpose           : This is the test class which is used for ResearchChallengeCalloutController class.
* Version           : 1.0 Initial Version
* Organization      : Geetha Chadipiralla-- Lightning Team
* Created Date      : 08-November-2018   
* Notes             : All testrecords have been newly created to remove dependency on CustomCare data. Futher Upon bulkification of Research Challenge 
these records can be moved to new Datafactory 
***************************************************************************************************/
@isTest
private class ResearchChallengeInboundServiceTest {

	private static String UNIVERSITY_ACCOUNT_RT = 'University';
	private static String LOCAL_RESEARCH_CHALLENGE_RT_ID = Schema.SObjectType.Research_Challenge__c.getRecordTypeInfosByName()
		.get('Local Research Challenge')
		.getRecordTypeId();
	private static String CONTACTNAME = 'Test Contact for Callout';

	@testSetup
	static void setup() {
		Test.setMock(HttpCalloutMock.class, new ResearchChallengeMockClass());

		Account newAccount = CFA_TestDataFactory.createAccount('Test Account', UNIVERSITY_ACCOUNT_RT, true);
		Contact newContact = CFA_TestDataFactory.createCFAContact(CONTACTNAME, newAccount.Id, false);
		newContact.Partner_Id__c = '14088';
		RetryForTest.upsertOnUnableToLockRow(newContact);

		Research_Challenge__c researchChallenge = new Research_Challenge__c();
		researchChallenge.Name = 'Local Challenge';
		researchChallenge.Competition_Date__c = Date.today();
		researchChallenge.Location__c = 'Test Location';
		researchChallenge.RecordTypeId = LOCAL_RESEARCH_CHALLENGE_RT_ID;
		RetryForTest.upsertOnUnableToLockRow(researchChallenge);

		RC_Participant__c testParticipant = new RC_Participant__c(
			Email__c = 'testemail@gmail.com',
			Challenge_Status__c = 'Invited',
			Faculty_University_Name__c = newAccount.Id,
			Faculty_Advisor__c = newContact.Id,
			Local_Challenge__c = researchChallenge.Id,
			Participant_First_Name__c = 'TestFirstName',
			Participant_Last_Name__c = 'TestLastName',
			Challenge_Name__c = 'Test Challenge'
		);
		RetryForTest.upsertOnUnableToLockRow(testParticipant);
	}

	@isTest
	public static void researchchallengeTest() {
		Test.setMock(HttpCalloutMock.class, new ResearchChallengeMockClass());

		String jsonRequest = '{"TransactionId":"125457c6-054e-431f-9857-143afa9abc1e","Code":null,"Message":null,"Applications":[{"ErrorMessage":null,"Email":"1023a@aimr.org","PartnerID":"14088","EducationEligibilityResponse":"string","EmploymentEligibilityResponse":"string","SponsorOptIn":"Geetha","CFAProspect":"string","Degree":"TEST","UniversityName":"string","Major":"string","GraduationDate":"1956-09-03T00:00:00Z"},{"ErrorMessage":null,"Email":"1023a@test.org","PartnerID":"14089","EducationEligibilityResponse":"string","EmploymentEligibilityResponse":"string","SponsorOptIn":"Geetha","CFAProspect":"string","Degree":"MD","UniversityName":"string","Major":"string","GraduationDate":"1956-09-03T00:00:00Z"},{"ErrorMessage":null,"Email":"1023a@aimr.org","PartnerID":"14088","EducationEligibilityResponse":"string","EmploymentEligibilityResponse":"string","SponsorOptIn":"Geetha","CFAProspect":"string","Degree":"TEST","UniversityName":"string","Major":"string","GraduationDate":"1956-09-03T00:00:00Z"}]}';

		RestRequest req = new RestRequest();
		RestResponse res = new RestResponse();

		req.requestURI = '/services/apexrest/inboundresearchchallenge/'; //Request URL
		req.httpMethod = 'POST'; //HTTP Request Type
		req.requestBody = Blob.valueof(jsonRequest);

		Test.startTest();
		ResearchChallengeInboundService.researchchallenge();
		RestContext.request = req;
		RestContext.response = res;
		ResearchChallengeInboundService.researchchallenge();
		Test.stopTest();
	}
}
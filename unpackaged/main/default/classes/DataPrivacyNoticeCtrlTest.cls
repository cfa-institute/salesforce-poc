@isTest
private class DataPrivacyNoticeCtrlTest  {

    @isTest
    private static void acceptPrivacyPolicyTest() {

        List<User> usersList = CFA_TestDataFactory.createUsers('DataPrivacyNoticeCtrlTest','CFA Base', 1, false);
        for (User user : usersList) {
            user.isActive = true;
        }
        insert usersList;

        User user = usersList[0];
        System.assertNotEquals(null, user, 'Test user not found');
        System.assertEquals(null, user.cfa_GDPR_Accepted_Date__c, 'cfa_GDPR_Accepted_Date__c must be empty');

        Test.startTest();
        System.runAs(user) {
            Boolean acceptResult = DataPrivacyNoticeCtrl.acceptPrivacyPolicy();
            System.assert(acceptResult, 'Error in accepting Privacy Policy');
        }
        Test.stopTest();

        User userCheck = [SELECT Id, cfa_GDPR_Accepted_Date__c FROM user WHERE Id = :user.Id limit 1];
		System.runAs(user) {
			System.assertEquals(Date.Today(), userCheck.cfa_GDPR_Accepted_Date__c, 'cfa_GDPR_Accepted_Date__c must be Today');
		}
    }
}
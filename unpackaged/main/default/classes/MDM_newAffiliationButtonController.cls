/**************************************************************************************************************************************************
Name:  MDM_newAffiliationButtonController
Copyright © 2017  ITC
=====================================================================
Purpose: 
1. Controller class for New affiliation Button

============================================================
History                                                            
-------                                                            
VERSION  AUTHOR         DATE           DETAIL                                           Description
1.0      Nikhil                        Created MDM_newAffiliationButtonController       Created this class   

****************************************************************************************************************************************************/
public class MDM_newAffiliationButtonController {
    
    public MDM_newAffiliationButtonController(ApexPages.StandardSetController controller) { } 
    public MDM_newAffiliationButtonController() { }
     
    public PageReference newAffiliation(){  
        
         String retURL = EncodingUtil.urlDecode(ApexPages.currentPage().getParameters().get('retURL'), 'UTF-8');
         String recordId = String.valueOf(retURL.substring(retURL.indexOf('/')+1, 16));
         
         if(recordId.startsWith('001')){ 
             String acctName = [SELECT Name FROM Account where Id=: recordId].Name;
             String strRecordTypeName = [SELECT RecordType.Name FROM Account WHERE Id=: recordId].RecordType.Name;
             //PageReference redirectLink = new PageReference('/a1f/e?ent=CF00Nj0000009ePPQ&CF00Nj0000009ePPQ='+acctName+'&retURL=/'+recordId+'&00Nj0000009ePQM='+strRecordTypeName+'&CF00Nj0000009ePPQ_lkid='+recordId+'&save_new_url=/'+recordId);
             return new PageReference('/a1f/e?ent=CF00Nj0000009ePPQ&CF00Nj0000009ePPQ='+EncodingUtil.urlEncode(acctName,'UTF-8')+'&retURL=/'+recordId+'&00Nj0000009ePQM='+strRecordTypeName+'&CF00Nj0000009ePPQ_lkid='+recordId+'&save_new_url=/'+recordId); 
         }else{
            Contact objContact = [SELECT Id, Name, AccountId, Account.Name, Account.RecordType.Name FROM Contact where Id=: recordId];
            //PageReference redirectLink = new PageReference('/a1f/e?CF00Nj0000009ePPQ='+objContact.Account.Name+'&CF00Nj0000009ePPQ_lkid='+objContact.AccountId+'&00Nj0000009ePQM='+objContact.Account.RecordType.Name+'&CF00Nj0000009ePPS='+objContact.Name+'&CF00Nj0000009ePPS_lkid='+objContact.Id+'&retURL=%2F'+objContact.Id+'&saveURL='+objContact.Id);
            return new PageReference('/a1f/e?CF00Nj0000009ePPQ='+EncodingUtil.urlEncode(objContact.Account.Name,'UTF-8')+'&CF00Nj0000009ePPQ_lkid='+objContact.AccountId+'&00Nj0000009ePQM='+objContact.Account.RecordType.Name+'&CF00Nj0000009ePPS='+objContact.Name+'&CF00Nj0000009ePPS_lkid='+objContact.Id+'&retURL=%2F'+objContact.Id+'&saveURL='+objContact.Id);
        }
    }
}
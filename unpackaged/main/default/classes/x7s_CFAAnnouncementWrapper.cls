/*
 * 	Created by 7Summits
 * 	Wrapper class for Community displayed Announcements
 * 	Source is x7s_CFA_Announcement custom metadata records
 */
global without sharing class x7s_CFAAnnouncementWrapper {
	@AuraEnabled
    public String Title {get; set;}
    @AuraEnabled
    public String Description {get; set;}
    @AuraEnabled
    public String Type {get; set;}
    
    public x7s_CFAAnnouncementWrapper(String announcementTitle, String announcementDescription, String alertType){
        this.Title = announcementTitle;
        this.Description = announcementDescription;
        this.Type = alertType;
    }
}
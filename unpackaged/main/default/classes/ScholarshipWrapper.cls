/**************************************************************************************************
* Apex Class Name   : ScholarshipWrapper
* Purpose           : This is the wrapper class which is used for mapping Individual key of Scholarship Json Response into seperate field
* Version           : 1.0 Initial Version
* Organization      : Cognizant
* Created Date      : 22-Jan-2018  
***************************************************************************************************/
public class ScholarshipWrapper{ 
    public String Program{get; set;}
    public String Type{get; set;}
    public String ProgramLevel{get;set;}
    public String ExamCycle{get;set;}
    public Integer Year{get;set;}
    public DateTime ScholarshipApplicationDate{get; set;}
    public DateTime CommunicationDate{get; set;}
    public DateTime ScholarshipApplicationDeadline{get;set;}
    public String Status{get; set;}
    public String Reason{get; set;}
    public String Society{get; set;}
}
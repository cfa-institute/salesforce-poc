/**
   * Author: Abhijeet Upadhyay
   * Description: Handler class for the trigger on Engagement Volunteer object
   * Date Created: 20-Sept-2017
   * Version: 0.2
   */
public class TriggerHandlerEngagement {
	private boolean isExecuting=false;
    private integer batchSize=0;
    
    public TriggerHandlerEngagement(boolean isExecutingTrigger, integer batchSizeTrigger){
        isExecuting = isExecutingTrigger;
        batchSize = batchSizeTrigger;
    }
    
	 //for validating whether any incomplete task left on engagement before deactivation
    public void onBeforeUpdate(List<Engagement__c> newengg){
                   
        Map<Id,Engagement__c> engMap = new Map<Id,Engagement__c>();
        Integer a=0, b=0; 
        for(Engagement__c eng: newengg){
            if(eng.status__c=='Deactivated'){
                engMap.put(eng.Id,eng);   
            }
        }
        
        for(Task tsk:[select Id, Status,ActivityDate from Task where engagement__c IN :engMap.keySet()]){
            if(tsk.Status!='Completed'){
                a=1;
            
            }
            else if(tsk.activitydate>System.today()){
                b=1;
            }
            else{
                System.debug('no error');
            }
        }
        if(a==1){
            for(Engagement__c eng: newengg){
                if(eng.status__c=='Deactivated'){
                    eng.addError('Please complete the incomplete Tasks left before deactivation.' );
                }
            }
        }
        else if(b==1){
            for(Engagement__c eng: newengg){
                if(eng.status__c=='Deactivated'){
                    eng.addError('All Volunteer end date should be prior to Engagement Deactivation Date.' );
                }
            }
        }
        else{
            System.debug('No error!');
        }
	}
    
    
    //validate clone trigger
    public void onBeforeInsert(List<Engagement__c> newengg){
    	Integer c=0;
	    for(Engagement__c e : newengg){
    	    if(e.getCloneSourceId()<>null){
				String id1=String.valueOf(e.getCloneSourceId()).subString(0,15);
            
            	Engagement__c e1=[Select Name,Engagement_Type__c,Start_Date__c,Expiration_Date__c,	
                              Status__c,Business_Owner__c,MD__c,Reports_To__c,Aligns_to_which_Strategic_Objective__c,
                              Business_Objective__c,Business_Owner_Cost_Center__c,Estimated_Cost__c from Engagement__c 
                              where Id =:id1];
            	if(e.Name<>e1.Name||e.Engagement_Type__c<>e1.Engagement_Type__c||
               		e.Start_Date__c<>e1.Start_Date__c||e.Expiration_Date__c<>e1.Expiration_Date__c||e.Status__c<>e1.Status__c||
              		e.Business_Owner__c<>e1.Business_Owner__c||e.MD__c<>e1.MD__c||e.Reports_To__c<>e1.Reports_To__c||
              		e.Aligns_to_which_Strategic_Objective__c<>e1.Aligns_to_which_Strategic_Objective__c||
               		e.Business_Objective__c<>e1.Business_Objective__c||e.Business_Owner_Cost_Center__c<>e1.Business_Owner_Cost_Center__c||
               		e.Estimated_Cost__c<>e1.Estimated_Cost__c){
                		c=1;
	            }
	            if(c==0){
                   e.addError('Please make atleast one change!');                
    	        }
        	}            
    	}
    }

    
    
}
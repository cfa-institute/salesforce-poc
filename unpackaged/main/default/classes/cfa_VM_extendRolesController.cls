public class cfa_VM_extendRolesController {
    @AuraEnabled
    public static String getIconName(String sObjectName){ 
        String u;
        List<Schema.DescribeTabSetResult> tabSetDesc = Schema.describeTabs();
        List<Schema.DescribeTabResult> tabDesc = new List<Schema.DescribeTabResult>();
        List<Schema.DescribeIconResult> iconDesc = new List<Schema.DescribeIconResult>();
        
        for(Schema.DescribeTabSetResult tsr : tabSetDesc) { tabDesc.addAll(tsr.getTabs()); }
        
        for(Schema.DescribeTabResult tr : tabDesc) {
            if( sObjectName == tr.getSobjectName() ) {
                if( tr.isCustom() == true ) {
                    iconDesc.addAll(tr.getIcons());
                } else {
                    u = 'standard:' + sObjectName.toLowerCase();
                }
            }
        }
        for (Schema.DescribeIconResult ir : iconDesc) {
            if (ir.getContentType() == 'image/svg+xml'){
                u = 'custom:' + ir.getUrl().substringBetween('custom/','.svg').substringBefore('_');
                break;
            }
        }
        return u;
    }
    
    public class RoleWrapper {
        @AuraEnabled
        public Role__c roles;
        @AuraEnabled
        public Boolean isChecked;
        @AuraEnabled
        public Engagement__c obEng;
    }   
    @AuraEnabled
    public static List<RoleWrapper> getRoles(String engagementId){ 
        List<RoleWrapper> custWrap = new List<RoleWrapper>();
        List<Engagement__c> lstengage = [Select cfa_Parent_Engagement__c from Engagement__c where id =:engagementId];
        if(!lstengage.isEmpty()) {
            List<Role__c> lstOfRoles = [Select id,Name,Engagement_name__r.Name,Engagement_name__r.id, Start_date__c, Recruiting_Start_Date__c, Recruiting_End_Date__c,Number_of_Positions__c,
                                        Role_Type__c,Conflict_filter__c, VM_Conflicts_Specific_To_The_Role__c,Available_Positions__c,VMApprovalStatus__c
                                        from Role__c where Engagement_name__c =:lstengage[0].cfa_Parent_Engagement__c AND VMApprovalStatus__c = true];
            for(Role__c each: lstOfRoles) {
                RoleWrapper ob = new RoleWrapper(); 
                ob.roles = each;
                ob.isChecked = false;
                Engagement__c obe = new Engagement__c();
                obe.Id = each.Engagement_name__r.id;
                obe.Name = each.Engagement_name__r.Name;
                ob.obEng = obe;
                custWrap.add(ob);
            }
            return custWrap;
        } else {
            return null;
        }
    }
    @AuraEnabled
    public static String checkEligibility(String engagementId) {
        List<Engagement__c> lstengage = [Select cfa_Parent_Engagement__c,Approval_Status__c,Approved__c from Engagement__c where id =:engagementId];
        if(!lstengage.isEmpty() && lstengage[0].cfa_Parent_Engagement__c == null) {
            return 'Parent Engagement is missing to extend the roles';
        } else if(!lstengage.isEmpty() &&  (lstengage[0].Approval_Status__c != 'Approved' || !lstengage[0].Approved__c )) {
            return 'Engagement is not approved to extend the roles';
        }
        List<Role__c> lstOfRole;
        boolean checkStatus = false;
        if(!lstengage.isEmpty() && lstengage[0].cfa_Parent_Engagement__c !=null) {
            
            lstOfRole = [Select id,VMApprovalStatus__c from Role__c where Engagement_name__c =:lstengage[0].cfa_Parent_Engagement__c];
            for(Role__c each: lstOfRole) {
                if(each.VMApprovalStatus__c) {
                    checkStatus = true;
                    break;
                }
            }
        }
        
        if(!checkStatus) {
            return 'None of your roles are approved';
        }
        
        
        
        return null;
    }
    @AuraEnabled
    public static List < sObject > fetchLookUpValues(String searchKeyWord, String ObjectName) {
        system.debug('ObjectName-->' + ObjectName);
        String searchKey = searchKeyWord + '%';
        
        List < sObject > returnList = new List < sObject > ();
        
        // Create a Dynamic SOQL Query For Fetch Record List with LIMIT 5   
        String sQuery =  'select id, Name from ' +ObjectName + ' where Name LIKE: searchKey order by createdDate DESC limit 5';
        List < sObject > lstOfRecords = Database.query(sQuery);
        
        for (sObject obj: lstOfRecords) {
            returnList.add(obj);
        }
        return returnList;
    }
    
    @AuraEnabled
    public static string saveRoles(String strlstOfRole,String engId) {
        try{
            List<RoleWrapper> lstOfRole = (List<RoleWrapper>)JSON.deserialize(strlstOfRole, List<RoleWrapper>.class);
            System.debug(lstOfRole);
            System.debug(engId);
            List<Role__c> finalRoleList = new List<Role__c>();
            for(RoleWrapper each: lstOfRole) {
                if(each.isChecked) {
                    Role__c obr = each.roles.clone(false);
                    obr.cfa_Parent_Role__c = each.roles.id;
                    obr.Engagement_name__c = engId;
                    obr.VMApprovalStatus__c = false;
                    obr.Available_Positions__c = null;
                    finalRoleList.add(obr);
                    
                }
            }
            
            System.debug(finalRoleList);
            insert finalRoleList;
            return engId;
        } catch(Exception e) {
            String errorStr = e.getMessage().substringBetween('FIELD_CUSTOM_VALIDATION_EXCEPTION,',':');
            System.debug(errorStr);
            return errorStr;
        }        
    }
    
}
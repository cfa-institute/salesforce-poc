@isTest
private class TaskTriggerHelperTest {
	@IsTest
	private static void handleCompletedTasks_SingleTaskTest() {
		Id cbtDaRequestId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('CBT DA Request').getRecordTypeId();

		Case testCase = new Case(Subject = 'test tasktriggerhandler', RecordTypeId = cbtDaRequestId);

		insert testCase;

		Task testTask = new Task(WhatId = testCase.Id);

		insert testTask;

		testTask.Status = 'Completed';

		Test.startTest();
		update testTask;
		Test.stopTest();

		System.assertNotEquals(0, Limits.getEmailInvocations());
	}

	@IsTest
	private static void handleCompletedTasks_MultipleTasksOnlyOneCompleteTest() {
		Id cbtDaRequestId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('CBT DA Request').getRecordTypeId();

		Case testCase = new Case(Subject = 'test tasktriggerhandler', RecordTypeId = cbtDaRequestId);

		insert testCase;

		Task testTask = new Task(WhatId = testCase.Id);

		Task testTask2 = new Task(WhatId = testCase.Id);

		insert new List<Task>{ testTask, testTask2 };

		testTask.Status = 'Completed';

		Test.startTest();
		update testTask;
		Test.stopTest();

		System.assertEquals(0, Limits.getEmailInvocations() - 1);
	}

	@IsTest
	private static void handleCompletedTasks_MultipleTasksAllCompleteTest() {
		Id cbtDaRequestId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('CBT DA Request').getRecordTypeId();
		Id externalId = Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get('TaskRecordExternal').getRecordTypeId();

		Case testCase = new Case(Subject = 'test tasktriggerhandler', RecordTypeId = cbtDaRequestId);

		insert testCase;

		List<Task> testTasks = new List<Task>();

		testTasks.add(
			new Task(WhatId = testCase.Id, recordTypeId = externalId, VM_Volunteer_Preferred_Email__c = 'test@cfainstitute.test')
		);

		testTasks.add(new Task(WhatId = testCase.Id));

		insert testTasks;

		for (Task t : testTasks) {
			t.Status = 'Completed';
		}

		Test.startTest();
		update testTasks;
		Test.stopTest();

		System.assertNotEquals(0, Limits.getEmailInvocations());

		delete testTasks;
	}
}
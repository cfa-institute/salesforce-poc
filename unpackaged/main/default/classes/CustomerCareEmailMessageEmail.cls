public with sharing class CustomerCareEmailMessageEmail{
    
    public static void sendApproval(Map<Id,EmailMessage> idToApproveMessagemap){
         Id CCFORecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('CustomerCare Front Office Record Type').getRecordTypeId();
         Id CCBORecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('CustomerCare Back Office Record Type').getRecordTypeId();
         Id CCReOpenRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('CustomerCare Case Reopen Record Type').getRecordTypeId();
         List<Case> caseAppList = [SELECT id,CaseNumber,recordTypeID,OwnerID,status,Owner.Name,Owner.Email FROM Case where ID IN:idToApproveMessagemap.keySet() and recordTypeID In (:CCFORecordType,:CCBORecordType,:CCReOpenRecordType)];
         try{
             if(!caseAppList.isEmpty()){
                Messaging.SingleEmailMessage[] mails = new List<Messaging.SingleEmailMessage>();      
                for(Case cp : caseAppList){
                    
                    Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                    List<string> toAddresses=new List<string>();                
                    String body;
                    String subject;
                    if(cp.Owner.email != null && cp.Owner.email != '')
                        toAddresses.add(cp.Owner.email);
                    
                    subject='Send Email request is Approved for your Case '+cp.caseNumber;                 
                    body=system.label.Approval_email_template+'<br><a href="'+URL.getSalesforceBaseUrl().toExternalForm()+'/'+cp.ID+'">click to open detail page</a><br><br>Thanks,';
                    email.setSubject(subject);
                    email.setHtmlBody(body);
                    email.setToAddresses(toAddresses);
                    mails.add(email);
                }
                
                    if(!mails.isEmpty())
                        Messaging.sendEmail(mails);
             }
         }catch(Exception e){
            System.debug('Error in sending email'+ e.getMessage());
         }
         
    }
    
    public static void sendRejection(Map<Id,EmailMessage> idToRejectMessagemap){
         Id CCFORecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('CustomerCare Front Office Record Type').getRecordTypeId();
         Id CCBORecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('CustomerCare Back Office Record Type').getRecordTypeId();
         Id CCReOpenRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('CustomerCare Case Reopen Record Type').getRecordTypeId();
         List<Case> caseRejList = [SELECT id,CaseNumber,recordTypeID,OwnerID,status,Owner.Name,Owner.Email FROM Case where ID In:idToRejectMessagemap.keySet() and recordTypeID In (:CCFORecordType,:CCBORecordType,:CCReOpenRecordType)];
         try{   
             if(!caseRejList.isEmpty()){
                Messaging.SingleEmailMessage[] mails = new List<Messaging.SingleEmailMessage>();      
                for(Case cp1 : caseRejList){
                    
                    Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                    List<string> toAddresses=new List<string>();                
                    String body;
                    String subject;
                    if(cp1.Owner.email != null && cp1.Owner.email != '')
                        toAddresses.add(cp1.Owner.email);
                    
                    subject='Send Email request is Rejected for your Case '+cp1.caseNumber;               
                    body=system.label.Rejection_Email_Template+'<br><a href="'+URL.getSalesforceBaseUrl().toExternalForm()+'/'+cp1.ID+'">click to open detail page</a><br><br>Thanks,';
                    email.setSubject(subject);
                    email.setHtmlBody(body);
                    email.setToAddresses(toAddresses);
                    mails.add(email);
                }
                if(!mails.isEmpty())
                        Messaging.sendEmail(mails);
                        
             }
         }catch(Exception e){
            System.debug('Error in sending email'+ e.getMessage());
         }
    }
}
global without sharing class EngagementExpirationDailyScheduler Implements Schedulable
    {
        global void execute(SchedulableContext sc)
        {
            setExpiration();
        }

        public void setExpiration()
        {
            List<Task> tskLst=new List<Task>();
            List<Engagement__c> listEngagements =new List<Engagement__c>();
            if(!Test.isRunningTest())
            {
            listEngagements = [SELECT ID,name,ownerid,Business_Owner__c FROM Engagement__c WHERE Expiration_Date__c=TODAY and Status__c<>'Deactivated'];
            }
            else
            {
            
            listEngagements = [SELECT ID,name,ownerid,Business_Owner__c FROM Engagement__c WHERE Expiration_Date__c=tomorrow and Status__c<>'Deactivated'];
            }
            
            if(listEngagements!=null)
            {
                for(Engagement__c eng:listEngagements)
                {
                    Task t = new Task();
                    t.OwnerId = eng.ownerid;
                    t.Subject = 'Engagement Expiration';
                    t.Description = 'Engagement will Expire Today Kindly review the Engagement';
                    t.Status = 'Open';
                    t.Priority = 'High';
                    t.WhatId = eng.Id;
                    tskLst.add(t);
                }    
            }
            try{
            if(tskLst.size()>0)
            insert tskLst;
            }catch(System.DMLException ex)
            {
                system.debug('Exception in EngagementExpirationDailyScheduler '+ex.getDMLMessage(0));
            }
        }
    }
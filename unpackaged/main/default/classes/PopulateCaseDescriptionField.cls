public class PopulateCaseDescriptionField implements Database.Batchable<SObject> {
    
    public Database.QueryLocator start(Database.BatchableContext context) {
        return Database.getQueryLocator([SELECT Id,CustomerCare_Description__c,Description FROM Case]);
    }
    
    public void execute(Database.BatchableContext context, List<Case> cases) {
        List<Case> updatedCases = new List<Case>();
        for(Case cs : cases){
            if(cs.Description == null && cs.CustomerCare_Description__c != null){
                cs.Description = cs.CustomerCare_Description__c;
                cs.CustomerCare_Description__c = null;
                updatedCases.add(cs);
            }
        }
        
        if(!updatedCases.isEmpty()){
            Database.update(updatedCases,false);
        }
    }
    
    public void finish(Database.BatchableContext BC) {}
}
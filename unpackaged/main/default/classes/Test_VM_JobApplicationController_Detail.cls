@isTest
public class Test_VM_JobApplicationController_Detail{
    
    public static void prepareData(){
        //create account      
        Account VM_Account = VM_TestDataFactory.createAccountRecord();
        
        //create contact    
        Contact VM_Contact = new Contact();
        VM_Contact.RecordTypeID = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Local Member').getRecordTypeId();
        VM_Contact.LastName = 'Test_LVM1';
        VM_Contact.FirstName = 'Test_FVM1';
        VM_Contact.AccountId = VM_Account.ID;
        VM_Contact.Email = 'TestVM_test01@test.com';
        VM_Contact.Phone = '0123456789';
        insert VM_Contact;
        //create engament    
        Engagement__c VM_Engage=new Engagement__c();
        User RMOwner=VM_TestDataFactory.createUser('RMTest11' + System.now().millisecond() + '@gmail.com','RMTest','Relationship Manager Staff','');
        User MDUSer=VM_TestDataFactory.createUser('MDTest' + System.now().millisecond() + '@gmail.com','MDTest','Outreach Management User','Managing Director');
        User ReportsToUSer=VM_TestDataFactory.createUser('Anne' + System.now().millisecond() +'@gmail.com','Lange','Outreach Management User','Relationship Manager');
        
        VM_Engage=VM_TestDataFactory.createEngagement('t FY21',RMOwner,MDUSer,ReportsToUSer);
        VM_Engage.Approval_Status__c = 'Approved';
        insert VM_Engage;
        
        //create Role
        Role__c VM_Role=new Role__c();
        VM_Role.Engagement_name__c=VM_Engage.id;
        VM_Role.Position_Title__c='Test_VMRole';
        VM_Role.Role_Type__c='CIPM Committee Member';
        VM_Role.Conflict_filter__c='Writers';
        VM_Role.Number_of_Positions__c=123;
        VM_Role.Start_date__c=Date.today();
        VM_Role.Recruiting_Start_Date__c=Date.today();
        VM_Role.Recruiting_End_Date__c=Date.today().addDays(10);
        VM_Role.PC_Check_required__c=true;
        VM_Role.Confidential__c=true;
        //VM_Role.ByPassApproval__c=true;
        VM_Role.Volunteer_Impact__c='Test';
        VM_Role.Volunteer_Work_location__c='Test';
        VM_Role.Volunteer_Roles_and_Responsibilities__c='Test';
        VM_Role.Volunteer_Experience__c='Test';
        VM_Role.Volunteer_Compet__c='Chief Financial Officer (CFO)';
        VM_Role.Volunteer_Certifications__c='CFA Charterholder';
        VM_Role.Conflicts_Specific_To_The_Role__c='Employment and/or affiliation with prep provider prohibited';
        VM_Role.VMApprovalStatus__c=true;
        insert VM_Role;
        //create Job Application
        Job_Application__c VM_Job = new Job_Application__c();
        VM_Job.RecordTypeID = Schema.SObjectType.Job_Application__c.getRecordTypeInfosByName().get('CIPMCommittee').getRecordTypeId();
        VM_Job.Position__c=VM_Role.id;
        VM_Job.Contact__c=VM_Contact.id;
        VM_Job.Status__c='Applied';
        VM_Job.Alternate_E_mail_Address__c='TestVM_test@test.com';
        VM_Job.Professional_Affiliations__c='Test';
        VM_Job.Volunteered_before__c='Yes';
        VM_Job.If_YES_list_experiences__c='Yes';
        VM_Job.Disciplinary_Action_Imposed__c='Yes';
        VM_Job.Disciplinary_action_Explain__c='Test';
        VM_Job.What_interests_you_about_this_role__c='Test';
        VM_Job.How_can_you_contribute_to_this_role__c='Test';
        VM_Job.Why_are_you_best_for_this_role__c='Test';
        VM_Job.Have_you_presented_on_these_topics__c='Test';
        VM_Job.Do_you_have_employer_support__c='Yes';
        VM_Job.Select_all_competencies__c='Central Banking 10 plus years';
        VM_Job.Degree1__c='Test';
        VM_Job.Degree2__c='Test';
        VM_Job.Degree3__c='Test';
        VM_Job.Reference_Name1__c='Test';
        VM_Job.Reference_Name2__c='Test';
        VM_Job.Reference_Name3__c='Test';
        VM_Job.Email1__c='TestVM56@test.com';
        VM_Job.Email2__c='TestVM57@test.com';
        VM_Job.Email3__c='TestVM58@test.com';
        VM_Job.Phone1__c='1234567897';
        VM_Job.Phone2__c='1234567897';
        VM_Job.Phone3__c='1234567897';
        VM_Job.Address1__c='Test';
        VM_Job.Address2__c='Test';
        VM_Job.Address3__c='Test';
        VM_Job.Personal_gain_or_benefit__c='Yes';
        VM_Job.Inappropriate_influence__c='Yes';
        VM_Job.Relationship_to_Prep_Provider__c='Yes';
        VM_Job.Relationship_with_Testing_Organization__c='Yes';
        VM_Job.Relationship_with_Candidate__c='Yes';
        VM_Job.I_have_read_the_disclosed_conflicts__c=true;
        VM_Job.I_Agree_to_the_Conflict_Policy__c=true;
        VM_Job.Other_potential_conflicts__c='Test';
        insert VM_Job;
        
        //create document
        Document vm_document = new Document();
        vm_document.AuthorId = UserInfo.getUserId();
        vm_document.name = 'Volunteer Acknowledgement Form';
        vm_document.FolderId = UserInfo.getUserId();
        insert vm_document;
        
        List<Attachment> attachments = new List<Attachment>();
        
        Attachment attach1 = new Attachment();
        attach1.name = 'Test Attachment.jpg';
        attach1.Body = blob.valueOf('Test Attachment');
        attach1.parentID = VM_Job.Id;
        attachments.add(attach1);
        
        Attachment attach2 = new Attachment();
        attach2.name = 'Test Attachment.docx';
        attach2.Body = blob.valueOf('Test Attachment2');
        attach2.parentID = VM_Job.Id;
        attachments.add(attach2);
        
        Attachment attach3 = new Attachment();
        attach3.name = 'Test Attachment.pdf';
        attach3.Body = blob.valueOf('Test Attachment2');
        attach3.parentID = VM_Job.Id;
        attachments.add(attach3);
        
        Attachment attach4 = new Attachment();
        attach4.name = 'Test Attachment.msg';
        attach4.Body = blob.valueOf('Test Attachment2');
        attach4.parentID = VM_Job.Id;
        attachments.add(attach4);
        
        insert attachments;
        
    }
    
    @TestSetup
    static void setUp() {
       prepareData();
    }
    
    public static testMethod void test_JobAppDetail(){
        Job_Application__c VM_Job = [SELECT ID,RecordTypeID FROM Job_Application__c LIMIT 1];
        Test.setCurrentPage(Page.VM_VolunteerApplication_Detail);
        //parse URL
        string str = '?key1='+VM_Job.Id+'&job-application='+VM_Job.Id;
        ApexPages.currentPage().getHeaders().put('referer',str);
        Test.startTest();
        VM_JobApplicationController_Detail vmController = new VM_JobApplicationController_Detail(new ApexPages.StandardController(VM_Job));
        vmController.getQuestionsForRecordType();
        vmController.getItems();
        vmController.getItems1();
        vmController.redirectToEdit();
        vmController.submitApplication();
        //For CMPCCommittee recordType.
        VM_Job.RecordTypeID = Schema.SObjectType.Job_Application__c.getRecordTypeInfosByName().get('CMPCCommittee').getRecordTypeId();
        Update VM_Job;
        VM_JobApplicationController_Detail vmController2 = new VM_JobApplicationController_Detail(new ApexPages.StandardController(VM_Job));
        vmController2.getQuestionsForRecordType();
        
        VM_Job.RecordTypeID = Schema.SObjectType.Job_Application__c.getRecordTypeInfosByName().get('DRCCommittee').getRecordTypeId();
        Update VM_Job;
        VM_JobApplicationController_Detail vmController5 = new VM_JobApplicationController_Detail(new ApexPages.StandardController(VM_Job));
        vmController5.getQuestionsForRecordType();
        
        //For EACCommittee recordType.
        VM_Job.RecordTypeID = Schema.SObjectType.Job_Application__c.getRecordTypeInfosByName().get('EACCommittee').getRecordTypeId();
        Update VM_Job;
        VM_JobApplicationController_Detail vmController6 = new VM_JobApplicationController_Detail(new ApexPages.StandardController(VM_Job));
        vmController6.getQuestionsForRecordType();
        
        //For Exam Related Advisor recordType.
        VM_Job.RecordTypeID = Schema.SObjectType.Job_Application__c.getRecordTypeInfosByName().get('Exam Related Advisor').getRecordTypeId();
        Update VM_Job;
        VM_JobApplicationController_Detail vmController7 = new VM_JobApplicationController_Detail(new ApexPages.StandardController(VM_Job));
        vmController7.getQuestionsForRecordType();
        
        //For GIPSCommittee recordType.
        VM_Job.RecordTypeID = Schema.SObjectType.Job_Application__c.getRecordTypeInfosByName().get('GIPSCommittee').getRecordTypeId();
        Update VM_Job;
        VM_JobApplicationController_Detail vmController8 = new VM_JobApplicationController_Detail(new ApexPages.StandardController(VM_Job));
        vmController8.getQuestionsForRecordType();
        
        //For MemberMicro recordType.
        VM_Job.RecordTypeID = Schema.SObjectType.Job_Application__c.getRecordTypeInfosByName().get('MemberMicro').getRecordTypeId();
        Update VM_Job;
        VM_JobApplicationController_Detail vmController9 = new VM_JobApplicationController_Detail(new ApexPages.StandardController(VM_Job));
        vmController9.getQuestionsForRecordType();
        
        //For SPCCommittee recordType.
        VM_Job.RecordTypeID = Schema.SObjectType.Job_Application__c.getRecordTypeInfosByName().get('SPCCommittee').getRecordTypeId();
        Update VM_Job;
        VM_JobApplicationController_Detail vmController10 = new VM_JobApplicationController_Detail(new ApexPages.StandardController(VM_Job));
        vmController10.getQuestionsForRecordType();
        
        //For Non Exam related Advisory Council recordType.
        VM_Job.RecordTypeID = Schema.SObjectType.Job_Application__c.getRecordTypeInfosByName().get('Non Exam related Advisory council').getRecordTypeId();
        Update VM_Job;
        
        //Attachment functionality
        vmController10.relatedTopics = 'Test related topics';
        vmController10.Agree_to_the_Conflict_Policy = 'true';
        vmController10.disclosed_conflicts = 'true';
        vmController10.fileName = 'test.doc';
        vmController10.fileName1 = 'test1.jpeg';
        vmController10.fileName2 = 'test2.pdf';
        vmController10.bl = Blob.valueOf('Unit Test Attachment Body');
        vmController10.bl1 = Blob.valueOf('Unit Test Attachment Body');
        vmController10.bl2 = Blob.valueOf('Unit Test Attachment Body');
        vmController10.submitApplication(); 
        Test.stopTest();
        
    }
    
    public static testMethod void test_JobAppDetailTest() {
        Job_Application__c VM_Job = [SELECT ID,RecordTypeID FROM Job_Application__c LIMIT 1];
        Test.setCurrentPage(Page.VM_VolunteerApplication_Detail);
        //parse URL
        string str = '?key1='+VM_Job.Id+'&job-application='+VM_Job.Id;
        ApexPages.currentPage().getHeaders().put('referer',str);
        Test.startTest();
        VM_JobApplicationController_Detail vmController11 = new VM_JobApplicationController_Detail(new ApexPages.StandardController(VM_Job));
        vmController11.getQuestionsForRecordType();
        //Attachement -ve functionality
        
        vmController11.relatedTopics = 'Test related topics';
        vmController11.Agree_to_the_Conflict_Policy = 'true';
        vmController11.disclosed_conflicts = 'true';
        vmController11.fileName = 'test.xls';
        vmController11.fileName1 = 'test1.jpeg';
        vmController11.fileName2 = 'test2.pdf';
        vmController11.bl = Blob.valueOf('Unit Test Attachment Body');
        vmController11.bl1 = Blob.valueOf('Unit Test Attachment Body');
        vmController11.bl2 = Blob.valueOf('Unit Test Attachment Body');
        vmController11.submitApplication();         
        vmController11.fileName = 'test.docx';
        vmController11.submitApplication();
        
        
        //Attachment functionality
        VM_Job.RecordTypeID = Schema.SObjectType.Job_Application__c.getRecordTypeInfosByName().get('SPCCommittee').getRecordTypeId();
        Update VM_Job;
        VM_JobApplicationController_Detail vmController12 = new VM_JobApplicationController_Detail(new ApexPages.StandardController(VM_Job));
        
        vmController12.relatedTopics = 'Test related topics';
        vmController12.Agree_to_the_Conflict_Policy = 'true';
        vmController12.disclosed_conflicts = 'true';
        
        List<Attachment> attachments = [SELECT ID,Body,Name,ParentId FROM Attachment WHERE Name LIKE 'Test Attachment%' AND ParentID = :VM_Job.Id];
        
        vmController12.att1 = attachments[0];
        vmController12.att2 = attachments[1];
        vmController12.att3 = attachments[2];
        vmController12.submitApplication(); 
        
        //Attachement -ve functionality
        VM_Job.RecordTypeID = Schema.SObjectType.Job_Application__c.getRecordTypeInfosByName().get('Non Exam related Advisory council').getRecordTypeId();
        Update VM_Job;
        
        Test.stopTest();
    }
    
    
}
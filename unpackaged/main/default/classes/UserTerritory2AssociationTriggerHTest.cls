@isTest
private class UserTerritory2AssociationTriggerHTest {
    
    private static final String AMER_TERRITORY_GROUP_NAME = 'AMER (Americas)';
    private static final String APAC_TERRITORY_GROUP_NAME = 'APAC (Asia Pacific)';
    
    @testSetup
    private static void setup() {
        List<ContentWorkspace> cwsList = new List<ContentWorkspace>();
        ContentWorkspace cws1 = new ContentWorkspace(Name = AMER_TERRITORY_GROUP_NAME);
        ContentWorkspace cws2 = new ContentWorkspace(Name = APAC_TERRITORY_GROUP_NAME);
        cwsList.add(cws1);
        cwsList.add(cws2);
        insert cwsList;
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs (thisUser) {
            Account accCfaInstitute = CFA_TestDataFactory.createAccount('CFA Institute', 'Society', true);
            List<NSA_Policy__c> policies = new List<NSA_Policy__c>();
            NSA_Policy__c policy1 = CFA_TestDataFactory.createNsaPolicy(accCfaInstitute.Id, 'CFA_China__c', null, null, AMER_TERRITORY_GROUP_NAME, false);
            policy1.Library_Id__c = cws1.Id;
            NSA_Policy__c policy2 = CFA_TestDataFactory.createNsaPolicy(accCfaInstitute.Id, 'CFA_Society_Germany__c', null, null, APAC_TERRITORY_GROUP_NAME, false);
            policy2.Library_Id__c = cws2.Id;
            policies.add(policy1);
            policies.add(policy2);
            RetryForTest.upsertOnUnableToLockRow(policies);
        }
    }
    
    @isTest
    private static void whenUserAssignedToTerritoryThenShareLibrary() {
        User user = [SELECT ID FROM User WHERE Profile.Name = 'System Administrator' AND isActive = true LIMIT 1];
        Set<String> territoryNames = new Set<String>{AMER_TERRITORY_GROUP_NAME, APAC_TERRITORY_GROUP_NAME};
        List<Territory2> territories = [SELECT ID FROM Territory2 WHERE Name IN :territoryNames];
        Test.startTest();
        List<UserTerritory2Association> associationList = new List<UserTerritory2Association>();
        associationList.add(new UserTerritory2Association(Territory2Id = territories[0].Id, UserId = user.Id));
        associationList.add(new UserTerritory2Association(Territory2Id = territories[1].Id, UserId = user.Id));
        RetryForTest.upsertOnUnableToLockRow(associationList);
        Test.stopTest();
        System.assertEquals(1, [SELECT count() FROM ContentWorkspaceMember WHERE ContentWorkspaceId in (SELECT ID FROM ContentWorkspace WHERE Name = :AMER_TERRITORY_GROUP_NAME) AND MemberId = :user.Id AND ContentWorkspacePermission.Name = 'Author']);
        System.assertEquals(1, [SELECT count() FROM ContentWorkspaceMember WHERE ContentWorkspaceId in (SELECT ID FROM ContentWorkspace WHERE Name = :APAC_TERRITORY_GROUP_NAME) AND MemberId = :user.Id AND ContentWorkspacePermission.Name = 'Author']);
    }
    
    @isTest
    private static void whenUserUnassignedFromTerritoryThenRemoveShareLibrary() {
        User user = [SELECT ID FROM User WHERE Profile.Name = 'System Administrator' AND isActive = true LIMIT 1];
        Territory2 territory = [SELECT ID FROM Territory2 WHERE Name = :AMER_TERRITORY_GROUP_NAME];
        Test.startTest();
        UserTerritory2Association uta = new UserTerritory2Association(Territory2Id = territory.Id, UserId = user.Id);
        RetryForTest.upsertOnUnableToLockRow(uta);
        delete uta;
        Test.stopTest();
        System.assertEquals(0, [SELECT count() FROM ContentWorkspaceMember WHERE ContentWorkspaceId in (SELECT ID FROM ContentWorkspace WHERE Name = :AMER_TERRITORY_GROUP_NAME) AND MemberId = :user.Id AND ContentWorkspacePermission.Name = 'Author']);
    }
    
    @isTest
    //test was added to increase test coverage for handler class and cover update event
    private static void whenUpdateUserTerritoryAssignmentThenStayShareLibrary() {
        User user = [SELECT ID FROM User WHERE Profile.Name = 'System Administrator' AND isActive = true LIMIT 1];
        Territory2 territory = [SELECT ID FROM Territory2 WHERE Name = :AMER_TERRITORY_GROUP_NAME];
        Test.startTest();
        UserTerritory2Association uta = new UserTerritory2Association(Territory2Id = territory.Id, UserId = user.Id);
        RetryForTest.upsertOnUnableToLockRow(uta);
        uta.RoleInTerritory2 = 'Sales Rep';
        RetryForTest.upsertOnUnableToLockRow(uta);
        Test.stopTest();
        System.assertEquals(1, [SELECT count() FROM ContentWorkspaceMember WHERE ContentWorkspaceId in (SELECT ID FROM ContentWorkspace WHERE Name = :AMER_TERRITORY_GROUP_NAME) AND MemberId = :user.Id AND ContentWorkspacePermission.Name = 'Author']);
    }
}
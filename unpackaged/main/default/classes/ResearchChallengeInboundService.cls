/**************************************************************************************************
* Apex Class Name   : ResearchChallengeInboundService
* Purpose           : This is the Inbound class to update Participant responses.
* Version           : 1.0 Initial Version
* Organization      : Vijay Vemuru -- Lightning Team
* Version 2         : Geetha Chadipiralla --- Changed the Payload  
* Created Date      : 08-August-2018   
* Modified Date     : 01-November-2018
***************************************************************************************************/

@RestResource(urlMapping='/inboundresearchchallenge/*')
global class ResearchChallengeInboundService{
    
    //Request/Response Wrapper
    global class ResearchChallenge{
        public String Code;
        public String Message;
        public String TransactionId;
        public List<Application> Applications;
    }
    
    global class Application{
        public String EducationEligibilityResponse;
        public String EmploymentEligibilityResponse;
        public String SponsorOptIn;
        public String CFAProspect;
        public String PartnerId;
        public String Degree;
        public String UniversityName;
        public String Major;
        public String GraduationDate;
        public String Email;
        public String ErrorMessage;
        public String Success;
    }
    
   //Deserilaize List for request Body
    public static ResearchChallenge researchchallenge = new  ResearchChallenge();
    
    @HttpPost
    //HttpPost for Inbound Service
    global static ResearchChallenge researchchallenge(){
        //rcmap to access RC with email
        Map<String, Application> rcMap = new Map<String, Application>();
        try{
            System.debug('Request :'+RestContext.request.requestBody.toString());
            researchchallenge = (ResearchChallenge)JSON.deserialize(RestContext.request.requestBody.toString(), ResearchChallenge.class);
            System.debug('researchchallengeRequest :'+researchchallenge);

            List<String> partnerIdList = new List<String>();
            List<String> emailIdList = new List<String>();
            //Iterating RC List
            for(Application rc : researchchallenge.Applications){
                //Duplicate Email Check
                if(emailIdList.indexOf(rc.Email) != -1){
                    System.debug('Email :'+rc.Email);
                   rc.ErrorMessage = 'Duplicate Email'; 
                    rc.Success = 'Failure';
                    researchchallenge.Code = '200';
                    researchchallenge.Message = 'Success';
                }
                else{
                emailIdList.add(rc.Email);
                System.debug('RC :'+ rc);
                rcMap.put(rc.Email, rc);
                partnerIdList.add(rc.PartnerId);
                researchchallenge.Code = '200';
                researchchallenge.Message = 'Success';
                rc.Success = 'Success';
                }
            }
            List<Contact> contactList = [SELECT Id,Name,Email,Partner_Id__c FROM Contact WHERE Partner_Id__c IN: partnerIdList];
            List<RC_Participant__c> rcpList = [SELECT Id, Name,Email__c FROM RC_Participant__c WHERE Email__c IN: emailIdList];
            // Iterating RC Participant Object
            for(RC_Participant__c rcp :rcpList){
                mapRCParticipant(rcp, rcMap.get(rcp.Email__c));
                Contact cnt = getContact(rcMap.get(rcp.Email__c).PartnerId,contactList);
                //Removing Email ID from List to check Invalid Emails
                emailIdList.remove(emailIdList.IndexOf(rcp.Email__c));
                if(cnt != null){
                    System.debug('RC :'+ cnt);
                    rcp.Participant_Contact__c = cnt.Id;
                }
                else{
                    System.debug('RC :'+ rcMap.get(rcp.Email__c));
                    rcMap.get(rcp.Email__c).Success = 'Failure';
                    rcMap.get(rcp.Email__c).ErrorMessage = 'Invalid Partner Id';
                    researchchallenge.Code = '200';
                    researchchallenge.Message = 'Sucess';
                    CFAIntegrationLog.writeLog('researchchallenge','NA' ,'Invalid PartnerID', 'Invalid PartnerID');
                }

            }
            if(!emailIdList.isEmpty())
                for(String email : emailIdList){
                    rcMap.get(email).Success = 'Failure';
                    rcMap.get(email).ErrorMessage = 'Invalid Email';
                    researchchallenge.Code ='200';
                    researchchallenge.Message = 'Success';
                    CFAIntegrationLog.writeLog('researchchallenge','NA' ,'Invalid Email', 'Invalid Email');
                }
            //Updating RC Participant Records
            Database.SaveResult[] saveResults = Database.update(rcpList, false);
            for(Integer i=0;i<saveResults.size();i++){
                if (!saveResults.get(i).isSuccess()){
                    //saveResults.get(i).getId()
                    System.debug('RC :'+saveResults.get(i));
                    Database.Error error = saveResults.get(i).getErrors().get(0);
                    rcmap.get(rcpList[i].Email__c).Success = 'Failure';
                   researchchallenge.Code ='200';
                   researchchallenge.Message = 'Success';
                   // if(String.valueOf(error.getStatusCode()) == 'INVALID_OR_NULL_FOR_RESTRICTED_PICKLIST' && error.getFields()[0] == 'Degree__c'){
                       // rcmap.get(rcpList[i].Email__c).ErrorMessage = 'Invalid Degree';
                        //CFAIntegrationLog.writeLog('researchchallenge','NA' ,String.valueOf(error.getStatusCode()), error.getMessage());
                    //}
                }
            }
            // update rcpList;
            // researchchallengeResponse = getResponse();
            
        } 
        
        catch(Exception e){
            System.debug('Exception :'+e);
            System.debug('Exception :'+e.getStackTraceString());
			CFAIntegrationLog.writeLog('researchchallenge',String.valueOf(e.getLineNumber()) , e.getTypeName(), e.getMessage());
        }
        /*if(!logs.isEmpty())
        CFAIntegrationLog.writeLog(logs);*/
        //researchchallenge.Applications = rcMap.values();
        return researchchallenge;
        
    }
    //Get Contact for RC Participant
    public static Contact getContact(String partnerId, List<Contact> contactList){
        Contact cnt;
        for(Contact c :contactList){
            if(c.Partner_Id__c == partnerId){
                cnt = c;
            }
        }
        return cnt;
    }
    
    
    // Mapping Salesforce and BERT Fields 
    public static RC_Participant__c mapRCParticipant(RC_Participant__c rcp ,Application rcr){
        rcp.Major__c = rcr.Major;
        System.debug('rcr :'+rcr.GraduationDate);
        rcp.Challenge_Status__c = 'Applied';
        rcp.Graduation_Date__c = Date.valueOf(rcr.GraduationDate.replace('T', ' '));
        rcp.Participant_University_Name__c = rcr.UniversityName;
        rcp.Meets_Education_Eligibility__c  = rcr.EducationEligibilityResponse;
        rcp.Meets_Employment_Eligibility__c = rcr.EmploymentEligibilityResponse;
        rcp.Sponsor_Contact_Opt_In__c = rcr.SponsorOptIn;
        rcp.CFA_Program_Prospect__c = rcr.CFAProspect;
        rcp.Degree__c = rcr.Degree;
        rcp.Email__c = rcr.Email;
        
        return rcp;
    }
    
}
/**************************************************************************************************
* Apex Class Name   : CustomerCare_CreateEmailForContact_Test
* Purpose           : This test class is used for validating CustomerCare_CreateEmailForContact   
* Version           : 1.0 Initial Version
* Organization      : Cognizant
* Created Date      : 15-Nov-2017  
***************************************************************************************************/
@isTest
public class CustomerCare_CreateEmailForContact_Test {
    static testMethod void CreatingEmailAsActivityTest() {
        
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        
        Messaging.InboundEmail email = new Messaging.InboundEmail() ;
        email.subject = 'Test Contact Applicant'; 
        list<String>  Newlist=new list<String>{CustomerCare_CommonTestData.TestDataContact().Email}; 
        email.toAddresses = Newlist;
        
        
        // call the email service class and test it with the data in the testMethod
        CustomerCare_CreateEmailForContact emailProcess = new CustomerCare_CreateEmailForContact();
        Messaging.InboundEmailResult result= emailProcess.handleInboundEmail(email, env); 
        System.assertEquals( true,result.success );
        
    }
}
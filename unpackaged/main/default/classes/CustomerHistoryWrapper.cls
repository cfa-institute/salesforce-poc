/**************************************************************************************************
* Apex Class Name   : CustomerHistoryWrapper
* Purpose           : This is the wrapper class which is used for mapping Individual key of CustomerHistoryWrapper Json Response into seperate field
* Version           : 1.0 Initial Version
* Organization      : CFA
***************************************************************************************************/

public class CustomerHistoryWrapper {
    public String PersonId{get;set;}
    public String GivenName{get;set;}
    public String Surname{get;set;}
    public DateTime DateOfBirth{get;set;}
    public String DateOfBirthFormatted{get;set;}
    public String PassportNumber{get;set;}
    public DateTime PassportExpirationDate{get;set;}
    public String PassportExpirationDateFormatted{get;set;}
    public String ModifiedBy{get;set;} 
    public DateTime ValidFrom{get;set;}
    public string ValidFromFormatted{get;set;}
    
}
@isTest
public class EngagementExpirationDailyScheduler_Test{
    
    public static testmethod void first1(){
        User RMOwner=VM_TestDataFactory.createUser('RMTest3' + System.now().millisecond() + '@gmail.com','RMTest3','Relationship Manager Staff','');
        User MDUSer=VM_TestDataFactory.createUser('MDTes1' + System.now().millisecond() + '@gmail.com','MDTest','Outreach Management User','Managing Director');
        User ReportsToUSer=VM_TestDataFactory.createUser('Ann1' + System.now().millisecond() + '@gmail.com','Huff','Outreach Management User','Relationship Manager');
        Engagement__c eng=VM_TestDataFactory.createEngagement('t FY21',RMOwner,MDUSer,ReportsToUSer);
        try
        {  
            eng.Expiration_Date__c=date.today().adddays(1);
            insert eng;
            
            system.debug('Successful Exe');
        }catch(DMLException ex)
        {
            system.debug('Npp eng'+ex.getDMLMessage(0));
        }
        Test.startTest();
        EngagementExpirationDailyScheduler scheUpdateAccount = new EngagementExpirationDailyScheduler();
        String sch ='0 48 * * * ?'; 
        System.schedule('Schedule to update Account Name', sch,scheUpdateAccount);
        Test.stopTest();
    }
    
}
/**
 * Encapsulates all behaviour logic relating to the testing apex functions
 *
 * For more guidelines and details see
 *   https://developer.salesforce.com/page/Apex_Enterprise_Patterns_-_Domain_Layer
 * @group Test Layer
 **/
@isTest
private class cfa_ApexUtilitiesTest {
	@isTest
	static void testgenerateRandomString() {
		// assign
		for ( Integer inx = 2; inx < 15; ++inx) {
			// act / assert
			System.assertEquals(inx, cfa_ApexUtilities.generateRandomString(inx).Length());
		}
	}

	@isTest
	static void testgetIds() {
		// assign
		List<SObject> mySObjects = new List<SObject> {
			new Account (
			    Name = 'test-1',
			    CurrencyIsoCode = 'AED',
			    type = 'Unknown')
			,	new Account (
			    Name = 'test-2',
			    CurrencyIsoCode = 'AED',
			    type = 'Unknown')
			,	new Account (
			    Name = 'test-3',
			    CurrencyIsoCode = 'AED',
			    type = 'Unknown')
			,	new Account (
			    Name = 'test-3',
			    CurrencyIsoCode = 'AED',
			    type = 'Unknown')
			, new Contact(
			    Email =  'afdsdfads@cfainstitute.org',
			    Firstname = 'asdfsdfa',
			    Lastname = 'adsfasdfafdsafdaf'
			)
		};
		insert mySObjects;

		Set<Id> ids = cfa_ApexUtilities.getIds(mySObjects);
		// assert
		System.assertEquals(ids.size(), mySObjects.size());
		ids = cfa_ApexUtilities.getIds(null);
		// assert
		System.assertEquals(0, ids.size());

	}
	@isTest
	static void testgetEnvironment() {
		// act/assign/assert
		System.assertEquals(cfa_ApexUtilities.TEST_ENVIRONMENT, cfa_ApexUtilities.getEnvironment());

	}
	@isTest static void testgetTypeFromSObject() {
		//assign
		SObject sob = new account(Name = 'Acme', Description = 'Acme Account');
		Type expected = Account.class;
		// act
		Type ns = cfa_ApexUtilities.getTypeFromSObject(sob);
		// assert
		System.assertEquals(expected, ns);
	} // end of testgetTypeFromSObject

	@isTest static void testgetTypeFromSObjectList() {
		//assign
		Account sob = new account(Name = 'Acme', Description = 'Acme Account');
		List<Account> sobs = new List<Account> { sob};
		Type expected = Account.class;
		// act
		Type ns = cfa_ApexUtilities.getTypeFromSObject(sobs);
		// assert
		System.assertEquals(expected, ns);
	} // end of testgetTypeFromSObject

	@isTest static void testgetSobjectTypeFromSObject() {
		//assign
		SObject sob = new account(Name = 'Acme', Description = 'Acme Account');
		Schema.SObjectType expected = Account.getSobjectType();
		// act
		Schema.SObjectType ns = cfa_ApexUtilities.getSobjectTypeFromSObject(sob);
		// assert
		System.assertEquals(expected, ns);
	} // end of testgetTypeFromSObject
	@isTest
	static void testisSandbox() {
		// act/assign/assert
		Boolean isProd = cfa_ApexUtilities.PROD_ENVIRONMENT == cfa_ApexUtilities.getEnvironment();
		Boolean expected = isProd ? false : true;


	}
	@isTest
	static void testwriteDataForTesting() {
		final string text = 'some value';
		cfa_ApexUtilities.writeDataForTesting(text);
		System.assertEquals(true, cfa_ApexUtilities.logTestData.contains(text));
	}

	@isTest
	static void testlog() {
		// assign
		final String  mystring = '1234567890abcdefghijklmnop';

		// act
		cfa_ApexUtilities.log(mystring, true);
		// assert
		System.assertEquals(true, cfa_ApexUtilities.containsInTestLog(mystring));

	}
	@isTest
	static void testlogWithControl() {
		// assign
		final String  mystring = '1234567890abcdefghijklmnop';

		// act
		cfa_ApexUtilities.log(mystring, false);
		// assert
		System.assertEquals(false, cfa_ApexUtilities.containsInTestLog(mystring));

	}
	@isTest
	static void testlogException() {
		// assign
		String findString = 'My Exception';
		Exception expected = new CFAException(findString);
		// act
		cfa_ApexUtilities.log(expected);
		// assert
		System.assertEquals(true, cfa_ApexUtilities.containsInTestLog(findString));

	}

	private class CfaException extends Exception {}

	// 0689912549, +33698912549, +33 6 79 91 25 49,+33-6-79-91-25-49 ,(555)-555-5555
	// 555-555-5555, +1-238 6 79 91 25 49, +1-555-532-3455, +15555323455, 55555555555555555555555555
	// +7 06 79 91 25 49
	@isTest
	static void testAllTelephoneNumbers() {
		// assign
		Map<String, Boolean> testOracle = new Map<String, Boolean> {
			'+33698912549' => true,
			'+33 6 79 91 25 49' => true,
			'+33-6-79-91-25-49' => true,
			'(555)-555-5555' => true,
			'555-555-5555' => true,
			'+1-238 6 79 91 25 49' => true,
			'+1-555-532-3455' => true,
			'+15555323455' => true,
			'0689912549' => true,
			'-1-238 6 79 91 25 49' => false,
			'+1-555-532-' => false,
			'+1555' => false,
			'+155' => false,
			'+15' => false,
			'+1' => false,
			'+' => false
		};
		integer inx = 0;
		//  act
		for ( string input : testOracle.keyset() ) {
			Boolean result = cfa_ApexUtilities.isValidTelephoneUSorInternational(input);
			// assert
			system.assertEquals(testOracle.get(input), result, 'error at ' + inx);
			inx++;
		}
	} // end of testAllTelephoneNumbers
	@isTest
	static void testgetExecutingClassName() {
		// assign/act/assert
		System.assertNotEquals(cfa_ApexUtilities.getExecutingClassName(), '');
	} // end of testgetExecutingClassName

	@isTest
	static void testgetExecutingMethod() {
		// assign/act/assert
		System.assertEquals(cfa_ApexUtilities.getExecutingMethod(), 'testgetExecutingMethod');

	}// end of testgetExecutingMethod
	
	@isTest
	static void testgetSetElement() {
		Set<String> sets = new Set<String> { '12', '23', '34'};
		Integer inx = 0;
		// act
		for (String element : sets) {
			// assert
			System.assertEquals(element, cfa_ApexUtilities.getSetElement(sets, inx++));
		}
	} // end of testgetSetElement

	@isTest
	static void testValidEmail() {
		// assign
		Map<String, Boolean> testOracle = new Map<String, Boolean> {
			'asd@me.com' => true,
			'asdadsa@asdsadasad.com' => true,
			'asdas' => false,
			'sadasdsa@' => false,
			'' => false,
			'      ' => false,
			'asdadsa@ some value' => false

		};
		integer inx = 0;
		//  act
		for ( string input : testOracle.keyset() ) {
			Boolean result = cfa_ApexUtilities.isValidEmail(input);
			// assert
			system.assertEquals(testOracle.get(input), result, 'error at ' + inx);
			inx++;
		}
	} // end of testValidEmail

	//////////////////////////////////////////////////////////////////////////////////
	//Setup
	//////////////////////////////////////////////////////////////////////////////////
	@TestSetup
	static void testInitialize() {
		CFA_Configuration__c config = new CFA_Configuration__c();
		config.Debug__c = true;
		config.Exclude_PB_by_Objects__c = '';
		config.Exclude_Triggers_By_Objects__c = '';
		config.Exclude_Validation_by_Objects__c = '';
		config.Notification__c = '';
		config.Include_Handler_Names__c = '';

		insert config;
	}
} // end of cfa_ApexUtilitiesTest
/**************************************************************************************************
* Apex Class Name   : CustomerCare_CreateEmailForContact
* Purpose           : This class is used to send email to all open case.   
* Version           : 1.0 Initial Version
* Organization      : Cognizant
* Created Date      : 10-Oct-2017  
***************************************************************************************************/

global class CustomerCare_EmailToOpenCaseContacts{
    /*This method used to check profile of current user is CustomerCare Supervisor*/
    webservice static String validateUser(){ 
        Id loggedInUserProfileId = UserInfo.getProfileId();
        Profile profile;
        if(loggedInUserProfileId!=null){
            profile = [Select Name from Profile where Id=:loggedInUserProfileId limit 1];
        }
        String profileName;
        if(profile!=null)
            profileName=profile.Name;
        
        if(profileName==CustomerCareUtility.supervisorProfile || profileName==CustomerCareUtility.ccAdminProfile || profileName==CustomerCareUtility.sysAdminProfile){
            return 'true';
        }else{
            return 'false';
        }
    }
    
    /*This is method is used send email to all open cases*/
    webservice static String emailToOpenCaseContacts(List<id> caseids){ 
        Id loggedInUserId=UserInfo.getUserId();
        User user;
        
        if(loggedInUserId!=null)
            user=[Select Email from User where id=:loggedInUserId];
        
        String loggedInUserEmail;
        if(user!=null)
            loggedInUserEmail=user.email;
        
        Id CCFOCaseRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get(CustomerCareUtility.frontOfficeRecordType).getRecordTypeId();
        Id CCBOCaseRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get(CustomerCareUtility.backOfficeRecordType).getRecordTypeId();
        Id CCReopenCaseRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get(CustomerCareUtility.reOpenRecordType).getRecordTypeId();
        List<Case> listOpenCases = new List<Case>();
        
        OrgWideEmailAddress[] fromAddress = [SELECT Address,DisplayName,Id FROM OrgWideEmailAddress where DisplayName = 'CFA Institute Customer Service (Do Not Reply)'];
        //List<EmailServicesAddress> fromAddress = [SELECT EmailDomainName,IsActive,LocalPart,ID FROM EmailServicesAddress where LocalPart = 'vijay.vemuru'];
        
        for(Case emailToCase : [SELECT id,CaseNumber,Origin,Status,ContactEmail,contactId,Contact.Name,SuppliedEmail FROM Case WHERE id IN :caseids AND(ContactEmail!= null AND contactId!=null) 
                                AND (RecordTypeId= :CCFOCaseRecordType  OR RecordTypeId = :CCBOCaseRecordType OR RecordTypeId= :CCReopenCaseRecordType)]){
        listOpenCases.add(emailToCase);
        }
        String templateName = System.Label.CustomerCare_EmailTemplateForEmailToListView;
        EmailTemplate emailTemplate = [select Id,subject,htmlvalue from EmailTemplate 
                                       where name=:templateName limit 1];
        Messaging.SingleEmailMessage[] mails = new List<Messaging.SingleEmailMessage>();
        if(listOpenCases!=null && !listOpenCases.isEmpty() && emailTemplate!=null){
            for(Case caseObj :listOpenCases){
                Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                email.setTemplateId(emailTemplate.id);                
                email.setTargetObjectId(caseObj.contactId);
                if (!fromAddress.isEmpty() && fromAddress.size() > 0) {
                    email.setOrgWideEmailAddressId(fromAddress.get(0).Id);
                }
                email.setWhatId(caseObj.Id);
                mails.add(email);       
            }
        }
        system.debug('***mails***'+mails);
        if(mails.size()>0){
            Messaging.sendEmail(mails);
        }
        
        return 'true';  
   }
}
/* 
        # Created By: Shubham Dadhich
        # Created Date: 01/27/2020
        # Description: Apex Handler for CFA_UpdateDARequestCaseStatus Lightning Component
        ---------------------------------------------------------------------------------------------------------------
        # Modification inforamtion
        - Modified By                 Modified Date               Description                     Associated Task (If)
        # Dhana Prashad               02/14/2020                  Added One Method Check Status
        # Shubham Dadhich             03/19/2020                  Code Optimized
        # Dhana Prashad               04/20/2020                  made updateCaseStatus  method to return to response
        ---------------------------------------------------------------------------------------------------------------
    */
public without sharing class CFA_UpdateDARequestCaseStatus_Handler {
     /* 
        # Created By: Shubham Dadhich
        # Created Date: 01/27/2020
        # Description: Method to Update Case DA Request Status to Submitted
        ---------------------------------------------------------------------------------------------------------------
        # Modification inforamtion
        - Modified By                 Modified Date               Description                     Associated Task (If)
        # Dhana Prashad               04/20/2020                  made method to return to response
        ---------------------------------------------------------------------------------------------------------------
    */
    @AuraEnabled
    public static String updateCaseStatus(String recordId){
        Case caseRecord = [SELECT Id, Status FROM Case WHERE Id=: recordId];
        caseRecord.Status = 'Submitted';
        update caseRecord;
        return 'success';
    }
     /* 
        # Created By: Dhana Prashad
        # Created Date: 02/14/2020
        # Description: Method to test updateCaseStatus method of CFA_UpdateDARequestCaseStatus_Handler class
        ---------------------------------------------------------------------------------------------------------------
        # Modification inforamtion
        - Modified By                 Modified Date               Description                     Associated Task (If)
        # Shubham Dadhich             03/19/2020                  Code Optimized
        ---------------------------------------------------------------------------------------------------------------
    */
    @AuraEnabled
    public static string checkStatus(String recordId){
        Case caseRecord = [SELECT Id, Status FROM Case WHERE Id=: recordId];
        return caseRecord.Status;
    }
}
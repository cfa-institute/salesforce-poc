/**
* @author 7Summits Inc
* @date 06-21-2019
* @description This trigger handler is responsible for sending emails to the original case owner when the ownership is changed
* by someone other than the case owner.
*/

public with sharing class SocietyOpsCaseTriggerHandler {


    public static void sendEmail(Map<ID, Case> oldCaseOwnerMap,Map<ID, Case> newCaseOwnerMap){

        //change only applicable to society tech record type
        Id societyTechCaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Society Tech Case').getRecordTypeId();

        Messaging.SingleEmailMessage[] mails = new List<Messaging.SingleEmailMessage>();

        List<Id> changeCaseOwnerIds = new List<Id>();
        Map<Id, Messaging.SingleEmailMessage> emailMap = new Map<Id, Messaging.SingleEmailMessage>();

        //read from the custom setting SocietyOps_Backoffice__c
        SocietyOps_Backoffice__c societyOps= SocietyOps_Backoffice__c.getValues('trigger-details');
        String caseReassignmentTemplateName=   societyOps.CaseReassignment_TemplateName__c;
        String noreplyAddress=   societyOps.No_reply_email__c;
        OrgWideEmailAddress owa = [select id,  Address from OrgWideEmailAddress where Address = :noreplyAddress];
        EmailTemplate emailTemplate = [Select id,Subject, Body from EmailTemplate where name = :caseReassignmentTemplateName];



        for (ID oldCaseOwnerKey : oldCaseOwnerMap.keyset()) {
            Case oldCaseOwner = oldCaseOwnerMap.get(oldCaseOwnerKey);
            Case newCaseOwner = newCaseOwnerMap.get(oldCaseOwnerKey);
            cfa_ApexUtilities.log('oldCaseOwner.ownerId'+oldCaseOwner.ownerId);
            cfa_ApexUtilities.log('newCaseOwner.ownerId'+newCaseOwner.ownerId);

            //oldCaseOwner.ownerId.substring(0,3)!='00G' if the previous owner is a queue don't send emails.
            if (oldCaseOwner.ownerId != newCaseOwner.ownerId
                    && newCaseOwner.LastModifiedById != oldCaseOwner.ownerId
                    && oldCaseOwner.RecordTypeId==societyTechCaseRecordTypeId && ((String)oldCaseOwner.ownerId).substring(0,3)!='00G') {


                Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                email.setTemplateID(emailTemplate.Id);
                email.setTargetObjectId(oldCaseOwner.Owner.id);
                String subject = emailTemplate.Subject;
                subject = subject.replace('{!Case.CaseNumber}', newCaseOwner.CaseNumber);
                String plainBody = emailTemplate.Body;
                plainBody = plainBody.replace('{!Case.CaseNumber}', newCaseOwner.CaseNumber);
                plainBody = plainBody.replace('{!Case.Subject}', newCaseOwner.Subject);
                plainBody = plainBody.replace('{!Case.Link}', System.URL.getOrgDomainUrl().toExternalForm() + '?Id=' + newCaseOwner.id);
                plainBody = plainBody.replace('{!Case.Owner}',newCaseOwner.SOC_ReadOnlyOwner__c);
                email.setSubject(subject);
                email.setPlainTextBody(plainBody);
                email.setOrgWideEmailAddressId(owa.id);
                email.setSaveAsActivity(false);
                email.setReplyTo(owa.Address);
                emailMap.put(oldCaseOwner.ownerId, email);
                changeCaseOwnerIds.add(oldCaseOwner.ownerId);
                mails.add(email);
            }
        }



        Map<Id, User> userEmailMap = new Map<Id, User>([select name,email from User where id in :changeCaseOwnerIds]);



        for (Id caseOwnerIdKey : emailMap.keySet()) {
            Messaging.SingleEmailMessage email = emailMap.get(caseOwnerIdKey);
            List<string> toAddresses = new List<string>();
            toAddresses.add(((User) userEmailMap.get(caseOwnerIdKey)).Email);
            email.setToAddresses(toAddresses);


        }

        if (!mails.isEmpty()) {
            Messaging.SendEmailResult[] sampleResult = Messaging.sendEmail(mails, false);
            cfa_ApexUtilities.log('Send email='+sampleResult);


        }



    }


}
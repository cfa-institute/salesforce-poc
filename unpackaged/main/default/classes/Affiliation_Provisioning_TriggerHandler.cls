/**************************************************************************************************************************************************
=====================================================================
Purpose: Affiliation Trigger Handler.
=====================================================================
History  
-------------
VERSION  AUTHOR         DATE           DETAIL          Description
1.0     Chan       		10/25/2018       Created       Added method onBeforeInsertUpdate()  
****************************************************************************************************************************************************/
public class Affiliation_Provisioning_TriggerHandler {
    
    
    /****************************************************************************************************************************
Purpose : This method is called before insert or Update of a Affiliation to populate Affiliation name field in Affiliation object with Role + at + Account Name.
2/18 - Added PersonID from Contact look up to check duplicate to avoid same contact name
Account Name filed along with Products and Contact are matched to prevent duplicate for Record Type = Access Provision
Parameters : List<Affiliation__c> affiliationList
Returns : void
********************************************************************************************************************************/
    
    public void onBeforeInsertUpdate(List<Affiliation__c> newAffiliationList ){
        //Getting Record Type 'Access Provisioning' to filter  
        String RecLabel = System.Label.Affiliation_Provisioning_RecordType;
        Id RecID = Schema.SObjectType.Affiliation__C.getRecordTypeInfosByName().get(RecLabel).getRecordTypeId();
   
        //get the list of accounts in a map
        set<ID> Acc = new set<ID>();
        for(Affiliation__C ACCLst : newAffiliationList){
            
            Acc.add(ACCLst.Account__C);
        }
        
        //Get Accounts Map
        List<Account> ls = [Select id, name from Account where ID IN :Acc and Partner_Id__c!=''];
        Map<Id, Account> m = new Map<Id, Account>(ls);
        //Get Contacts Map
        set<ID> Cont = new set<ID>();
        for(Affiliation__C ContList : newAffiliationList){
            Cont.add(ContList.Contact__C);
        }   
        
        List<Contact> Contacts = [Select id, CFAMN__PersonID__c from contact where ID IN :Cont and Partner_Id__c!='' and CFAMN__PersonID__c!=''];
        Map<Id, Contact> C = new Map<Id, Contact>(Contacts);
        
        // Updating affiliation_Name__c field with Role + Account name
        for(Affiliation__C A : newAffiliationList){
            If (A.RecordTypeId == RecID){           
               
                if(m.containskey(a.Account__c)== true){
                    Account AccountName = m.get(a.account__C);
                    A.affiliation_Name__c = A.Role__c + ' ' + 'at' + ' ' + AccountName.name;
                }
                else{
                    a.addError('The selected Account doesnt have Partner ID.');
                }
                // Updating CFAMN_PersonID__c field with Contact__r.CFAMN__PersonID__c
                
                if(c.containskey(a.contact__c)== true){
                	contact ContactPerson = C.get(A.Contact__c);
                    A.CFAMN_PersonID__c =  ContactPerson.CFAMN__PersonID__c;
                    
                }
                else{
                    a.addError('The selected Contact should have Person ID and Partner ID.');
                }
                //Change Active status for only 'Access Provisioning' Record Type
                
                //Updating Active status based on End date
                
                Date D = system.today();
                If (A.End_date__C != null && A.End_date__C >= D){
                    A.Active_Affiliation__C = True;
                    
                }
                else if (A.End_date__C != null && A.End_date__C < D) {
                    A.Active_Affiliation__C = False;
                    
                }
                
                else {
                    A.Active_Affiliation__C = True;
                    
                }
                
            }
        }
    }
}
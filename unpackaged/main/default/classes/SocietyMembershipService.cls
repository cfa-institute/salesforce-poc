/*****************************************************************
Name: SocietyMembershipService
Copyright © 2020 ITC
============================================================
Purpose: This service class has some reusable methods for society membership logic
============================================================
History
-------
VERSION  AUTHOR          DATE         DETAIL    Description
1.0      Vadym Merkotan               Create    Apply Member Society Campaigns Changes
2.0      Alona Zubenko   10.12.2020   Update    Update Badge Visibility Dates
*****************************************************************/

public inherited sharing class SocietyMembershipService {

    /**
     * Query Contact by Ids with specified fields with record types CFA_Contact and Local_Member
     *
     * @param	Set<String>	fieldNames	Contact fields api names
     * @param	Set<String>	recordIds	Contact record Idsto query
     *
     * @return	List<Contact>			Contact list for specified ids with specified fields
     *
     */
    public static List<Contact> getCFAandLocalContactsWithFieldsByIds(Set<String> fieldNames, Set<String> recordIds) {
        Set<String> fNames = new Set<String>(fieldNames);
        if (fNames.isEmpty()) {
            fNames.add('Id');
        }
        String query = 'SELECT ' + String.join(new List<String>(fNames), ',') + ' FROM Contact ';
        String strEsc = String.escapeSingleQuotes(query);
        strEsc += ' WHERE Id IN :recordIds ';
        strEsc += ' AND RecordType.DeveloperName IN (\'CFA_Contact\',\'Local_Member\')';
        return Database.query(strEsc);
    }

    /**
     * Query Membership by Ids where Membership_Type__c is 'Society'
     *
     * @param	Set<String>	recordIds	Membership record Ids to query
     *
     * @return	List<Membership__c>			Membership list for specified ids
     *
     */
    public static List<Membership__c> getSocietyMembershipsByIds(Set<String> recordIds) {
        SObjectAccessDecision securityDecision =
                Security.stripInaccessible(AccessType.READABLE,
                [SELECT ID
                        , Account_lookup__r.Name
                        , Transaction_Type__c
                        , Contact_lookup__c
                        , Transaction_Date__c
                        , Account_lookup__c
                FROM Membership__c
                WHERE Id IN :recordIds
                AND (Membership_Type__c = 'Society' OR RecordType.DeveloperName = 'Local')]
                );
        return securityDecision.getRecords();
    }

    /**
     * Creates campaign members if it not exist
     *
     * @param	Map<Id, Set<Id>>	mapContactIdToCampaignIds Map Contact Id to Campaign Ids
     *
     */
    public static void createCampaignMembers(Map<Id, Set<Id>> mapContactIdToCampaignIds) {
        Set<Id> campaignIds = new Set<Id>();
        Set<Id> contactIds = new Set<Id>();
        for (String recordId : mapContactIdToCampaignIds.keySet()) {
            contactIds.add(recordId);
            campaignIds.addAll(mapContactIdToCampaignIds.get(recordId));
        }
        Map<String, CampaignMember> mapKeyToCampaignMember = groupCampaignMemberToKey(getCampaignMembersByContactAndCampaignIds(contactIds, campaignIds));
        List<CampaignMember> campaignMembersToCreate = new List<CampaignMember>();
        for (String recordId : mapContactIdToCampaignIds.keySet()) {
            Set<Id> cIds = mapContactIdToCampaignIds.get(recordId);
            for (Id campaignId : cIds) {
                if (mapKeyToCampaignMember.containsKey(getCampaignMemberKey(recordId, campaignId))) {
                    continue;
                }
                campaignMembersToCreate.add(new CampaignMember(ContactId = recordId, CampaignId = campaignId));
            }

        }
        if (!campaignMembersToCreate.isEmpty()) {
            insert campaignMembersToCreate;
        }
    }

    /**
     * Removes campaign members if it exist
     *
     * @param	Map<Id, Set<Id>>	mapContactIdToCampaignIds Map Contact Id to Campaign Ids
     *
     */
    public static void removeCampaignMembers(Map<Id, Set<Id>> mapContactIdToCampaignIds) {
        Set<Id> campaignIds = new Set<Id>();
        Set<Id> contactIds = new Set<Id>();
        for (String recordId : mapContactIdToCampaignIds.keySet()) {
            contactIds.add(recordId);
            campaignIds.addAll(mapContactIdToCampaignIds.get(recordId));
        }
        Map<String, CampaignMember> mapKeyToCampaignMember = groupCampaignMemberToKey(getCampaignMembersByContactAndCampaignIds(contactIds, campaignIds));

        List<CampaignMember> campaignMembersToRemove = new List<CampaignMember>();
        for (String recordId : mapContactIdToCampaignIds.keySet()) {
            Set<Id> cIds = mapContactIdToCampaignIds.get(recordId);
            for (Id campaignId : cIds) {
                if (mapKeyToCampaignMember.containsKey(getCampaignMemberKey(recordId, campaignId))) {
                    campaignMembersToRemove.add(mapKeyToCampaignMember.get(getCampaignMemberKey(recordId, campaignId)));
                }
            }
        }
        if (!campaignMembersToRemove.isEmpty()) {
            delete campaignMembersToRemove;
        }
    }

    /**
     * Groups campaignMembers to map by key
     *
     * @param	List<CampaignMember>	campaignMembers to group
     *
     * @return	Map<String, CampaignMember>	Map string key to campaignMember
     *
     */
    private static Map<String, CampaignMember> groupCampaignMemberToKey(List<CampaignMember> campaignMembers) {
        Map<String, CampaignMember> result = new Map<String, CampaignMember>();
        for (CampaignMember cm : campaignMembers) {
            result.put(getCampaignMemberKey(cm), cm);
        }
        return result;
    }

    /**
    * Gets campaignMembers by contact and campaign Ids
    *
    * @param	Set<Id>	contactIds
    * @param	Set<Id>	campaignIds
    *
    * @return	List<campaignMember> campaign members list
    *
    */
    public static List<CampaignMember> getCampaignMembersByContactAndCampaignIds(Set<Id> contactIds, Set<Id> campaignIds) {
        return [SELECT ID, ContactId,CampaignId FROM CampaignMember WHERE ContactId IN:contactIds AND CampaignId IN :campaignIds];
    }

    /**
     * Creates a key for campaignMember
     * 
     * @param	CampaignMember	campaignMember to create key
     * 
     * @return	String			key for campaignMember
     * 
     */
    private static String getCampaignMemberKey(CampaignMember member) {
        return member.ContactId + '-' + member.CampaignId;
    }

    /**
     * Creates a key for campaignMember by ContactId and CampaignId
     * 
     * @param	contactId	ContactId from CampaignMember
     * @param	campaignId	Campaign from CampaignMember
     * 
     * @return	String			key for campaignMember
     * 
     */
    private static String getCampaignMemberKey(String contactId, String campaignId) {
        return contactId + '-' + campaignId;
    }

    /**
     * @description Get Campaign Ids By Account Ids
     *
     * @param accIds Set<Id>
     *
     * @return  Map<Id, Set<Id>>
     */
    public static Map<Id, Set<Id>> getCampaignIdsByAccIds() {
        Map<Id,Set<Id>> allCampaignIdsByAccIds = new Map<Id, Set<Id>>();
        List<NSA_Policy__c> allPolicies = [
                SELECT Id, Member_Campaign__c, Society_Account__c
                FROM NSA_Policy__c
                WHERE Is_Active__c = TRUE
            	AND Policy_Status__c = 'Approved'
        ];
        for(NSA_Policy__c policy : allPolicies){
            if(!allCampaignIdsByAccIds.containsKey(policy.Society_Account__c)){
                allCampaignIdsByAccIds.put(policy.Society_Account__c, new Set<Id>());
            }
            allCampaignIdsByAccIds.get(policy.Society_Account__c).add(policy.Member_Campaign__c);
        }
        return allCampaignIdsByAccIds;
    }

    /**
     * @description Group Campaign Id By Contact Id
     *
     * @param campaignMembers List<CampaignMember>
     *
     * @return Map<Id,Set<Id>>
     */
    public static Map<Id,Set<Id>> groupCampaignIdByContactId(List<CampaignMember> campaignMembers) {
        Map<Id,Set<Id>> campaignIdsByContactId = new Map<Id, Set<Id>>();
        for(CampaignMember campMember : campaignMembers){
            if(!campaignIdsByContactId.containsKey(campMember.ContactId)){
                campaignIdsByContactId.put(campMember.ContactId, new Set<Id>());
            }
            campaignIdsByContactId.get(campMember.ContactId).add(campMember.CampaignId);
        }
        return campaignIdsByContactId;
    }

    /**
     * @description Group Campaign Members By Contact Id
     *
     * @param campaignMembers List<CampaignMember>
     *
     * @return Map<Id,Map<Id,CampaignMember>>
     */
    public static Map<Id,Map<Id,CampaignMember>> groupCampaignMembersByContactId(List<CampaignMember> campaignMembers) {
        Map<Id,Map<Id,CampaignMember>> campaignMembersByContactId = new Map<Id, Map<Id,CampaignMember>>();
        for(CampaignMember cm : campaignMembers){
            if (!campaignMembersByContactId.containsKey(cm.ContactId)) {
                campaignMembersByContactId.put(cm.ContactId, new Map<Id,CampaignMember>());
            }
            campaignMembersByContactId.get(cm.ContactId).put(cm.CampaignId,cm);
        }

        return campaignMembersByContactId;
    }

    /**
     * @description Get CampaignMembers
     *
     * @param societiesIds Set<Id>
     * @param contactIds Set<Id>
     * @param allCampaignIdsByAccIds Map<Id, Set<Id>>
     *
     * @return List<CampaignMember>
     */
    public static List<CampaignMember> getCampaignMembers(Set<Id> societiesIds,  Set<Id> contactIds, Map<Id, Set<Id>> allCampaignIdsByAccIds){
        Set<Id> campaignIds = new Set<Id>();
        for(Id accId : societiesIds){
            Set<Id> accCampaignIds = allCampaignIdsByAccIds.get(accId);
            if(accCampaignIds != null && !accCampaignIds.isEmpty()) {
                campaignIds.addAll(accCampaignIds);
            }
        }
        List<CampaignMember> campaignMembers = [
                SELECT Id, ContactId, CampaignId
                FROM CampaignMember
                WHERE ContactId IN:contactIds
                AND CampaignId IN :campaignIds
        ];
        return campaignMembers;
    }
}
/*****************************************************************
Name: ContactTriggerHandler
Copyright © 2021 ITC
============================================================
Purpose: Helper for Contact trigger
============================================================
History
-------
VERSION  AUTHOR          DATE         DETAIL    Description
1.0      Alona Zubenko   29.01.2021   Created   Logic for Assigning Contacts To Societies By Address
*****************************************************************/

public with sharing class ContactTriggerHandler implements ITriggerHandler{
    // Allows unit tests (or other code) to disable this trigger for the transaction
    public static Boolean triggerDisabled = false;
    @TestVisible
    private static Boolean disableNSALogic = false;
    @TestVisible
    private static Boolean disabledNewContactFieldsLogic = false;
    public static Boolean cfaContactValidationDisabled = false;

    @InvocableMethod(label='Disable CFA Contact Validation' description='Disable trigger validation for CFA Contacts .' category= 'Contact')
    public static void disableCfaContactValidation() {
        cfaContactValidationDisabled = true;
    }

    private Boolean isDisabledNSALogic {
        get {
            if(Test.isRunningTest()) {
                return disableNSALogic;
            } else {
                return DynamicTriggerStatus.getTriggerStatus('NSALogic');
            }
        }
    }

    private Boolean isDisabledNewContactFieldsLogic {
        get {
            if(Test.isRunningTest()) {
                return disabledNewContactFieldsLogic;
            } else {
                return DynamicTriggerStatus.getTriggerStatus('NewContactFields');
            }
        }
    }

    //Checks to see if the trigger has been disabled either by custom metadata or by running code
    public Boolean isDisabled() {
        if (Test.isRunningTest()) {
            return triggerDisabled;
        } else {
            return DynamicTriggerStatus.getTriggerStatus('ContactTrigger') || triggerDisabled;
        }
    }

    //  On before insert event
    public void beforeInsert(List<SObject> newItems) {
        if(!isDisabledNSALogic) {
            ContactTriggerHelper.setTempIdsToRecords(newItems);
            ContactTriggerHelper.assignContactsToSocietiesByPolicies(null, newItems);
            ContactTriggerHelper.removeTempIdsToRecords(newItems);

            ContactTriggerHelper.handleDeceasedContacts(newItems);
        }
        if(!isDisabledNewContactFieldsLogic && isDisabledNSALogic) {
            ContactTriggerHelper.linkCFAContactsToCFAInsituteAccount(newItems);
        }

        ContactTriggerHelper.handleLocalContacts(newItems);

        if(!isDisabledNewContactFieldsLogic) {
            ContactTriggerHelper.copyContactFields(newItems);
        } else {
            ContactTriggerHelper.copyContactFields2(newItems);
        }

    }

    //  on before update event
    public void beforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        List<Contact> oldContacts = oldItems.values();
        List<Contact> newContacts = newItems.values();

        if(!cfaContactValidationDisabled) {
             ContactTriggerHelper.ContactListWrapper contactsWithEditAccess =
                 ContactTriggerHelper.getContactsWithEditAccess(oldContacts,(Map<Id,Contact>)newItems);
             newContacts = contactsWithEditAccess.newValues;
             oldContacts = contactsWithEditAccess.oldValues;
        }

        if(!isDisabledNSALogic) {
            ContactTriggerHelper.assignContactsToSocietiesByPolicies(oldContacts, newContacts);
            ContactTriggerHelper.handleDeceasedContacts(newContacts);
        }

        ContactTriggerHelper.handleLocalContacts(newContacts);

        if(!isDisabledNewContactFieldsLogic) {
            ContactTriggerHelper.copyContactFields(newContacts);
        } else {
            ContactTriggerHelper.copyContactFields2(newContacts);
        }
    }

    //  on after insert event
    public void afterInsert(Map<Id, SObject> newItems) {}

    // On after update event
    public void afterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        ContactTriggerHelper.handleCommunityPermissions(newItems, oldItems);
        ContactTriggerHelper.contactCventCall(newItems.values(), (Map<Id, Contact>)oldItems);
    }

    //  On after delete event
    public void afterDelete(Map<Id, SObject> oldItems) {}

    // On after undelete event
    public void afterUndelete(Map<Id, SObject> oldItems) {
        ContactTriggerHelper.handleUndelete(oldItems);
    }

    //  On before delete event
    public void beforeDelete(Map<Id, SObject> oldItems) {}
}
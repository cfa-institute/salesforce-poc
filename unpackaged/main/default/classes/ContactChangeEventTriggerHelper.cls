/*****************************************************************
Name: ContactChangeEventTriggerHelper
Copyright © 2020 ITC
============================================================
Purpose:  This trigger helper is responsible for handling Contact Change Event
============================================================
History
-------
VERSION  AUTHOR          DATE         DETAIL    Description
1.0      Vadym Merkotan               Create    Apply Contact Society Campaigns Changes
2.0      Alona Zubenko                Update    Use NSA Policy
3.0      Alona Zubenko   15.02.2021   Update    Creation of Relationship With Employer
4.0      Alona Zubenko   15.03.2021   Update    Update logic for creation Relationship With Employer
*****************************************************************/
public with sharing class ContactChangeEventTriggerHelper {
    private static final String ROLE_EMPLOYER = 'Employer';

    /**
     * Add/Remove Contact to All Campaigns based on society checkboxes changes on Contact Record.
     * Applied only for CFA_Contact and Local_Member contact record types
     *
     * @param	recordIds Set<String> of Contact record Ids
     *
     */
    public static void applyContactSocietyAllCampaignsChanges(Set<String> recordIds) {
        try {
            Map<String, Id> campaignIdByContactSocietyFieldApiName = getCampaignIdByContactSocietyFieldApiName();
            List<Contact> changedContacts = SocietyMembershipService.getCFAandLocalContactsWithFieldsByIds(campaignIdByContactSocietyFieldApiName.keySet(), recordIds);

            Map<Id, Set<Id>> mapContactIdToCampaignIdsToAddCampaignMember = new Map<Id, Set<Id>>();
            Map<Id, Set<Id>> mapContactIdToCampaignIdsToRemoveCampaignMember = new Map<Id, Set<Id>>();

            for (Contact con : changedContacts) {

                for (String fieldApiName : campaignIdByContactSocietyFieldApiName.keySet()) {
                    if (con.get(fieldApiName) instanceof Boolean && (Boolean) con.get(fieldApiName)) {
                        if (!mapContactIdToCampaignIdsToAddCampaignMember.containsKey(con.Id)) {
                            mapContactIdToCampaignIdsToAddCampaignMember.put(con.Id, new Set<Id>());
                        }
                        mapContactIdToCampaignIdsToAddCampaignMember.get(con.Id).add(campaignIdByContactSocietyFieldApiName.get(fieldApiName));
                    } else if (con.get(fieldApiName) instanceof Boolean && !((Boolean) con.get(fieldApiName))) {
                        if (!mapContactIdToCampaignIdsToRemoveCampaignMember.containsKey(con.Id)) {
                            mapContactIdToCampaignIdsToRemoveCampaignMember.put(con.Id, new Set<Id>());
                        }
                        mapContactIdToCampaignIdsToRemoveCampaignMember.get(con.Id).add(campaignIdByContactSocietyFieldApiName.get(fieldApiName));
                    }
                }

            }
            if (!mapContactIdToCampaignIdsToAddCampaignMember.isEmpty()) {
                SocietyMembershipService.createCampaignMembers(mapContactIdToCampaignIdsToAddCampaignMember);
            }
            if (!mapContactIdToCampaignIdsToRemoveCampaignMember.isEmpty()) {
                SocietyMembershipService.removeCampaignMembers(mapContactIdToCampaignIdsToRemoveCampaignMember);
            }
        } catch (Exception ex) {
            insert CFA_IntegrationLogException.logError(ex, 'Apply Contact Society All Campaigns Changes', 'ContactChangeEventTriggerHelper', Datetime.now(), true, '', '', '', true, '');
            throw ex;
        }
    }

    /**
     * @description Logic to create/update relationship for Employer Lookup
     *
     * @param recordIds Set<String>
     */
    public static void createRelationshipWithEmployer(Set<String> recordIds) {
        try {
            List<AccountContactRelation> relations = new List<AccountContactRelation>();
            Map<String, List<AccountContactRelation>> relationIdByConIdMap = new Map<String, List<AccountContactRelation>>();

            Map<Id, AccountContactRelation> existedRelations = new Map<Id, AccountContactRelation>([
                    SELECT Id, AccountId, ContactId, Roles, IsActive
                    FROM AccountContactRelation
                    WHERE ContactId IN :recordIds
            ]);

            for (AccountContactRelation relation : existedRelations.values()) {
                if(!relationIdByConIdMap.containsKey(relation.ContactId)){
                    relationIdByConIdMap.put(relation.ContactId, new List<AccountContactRelation>());
                }
                relationIdByConIdMap.get(relation.ContactId).add(relation);
            }

            List<Contact> changedContacts = [
                    SELECT Id, Employer_Account__c
                    FROM Contact
                    WHERE RecordType.DeveloperName = 'CFA_Contact'
                    AND Id IN :recordIds
            ];

            for (Contact con : changedContacts) {
                relations.addAll(getRelationships(con,relationIdByConIdMap));
            }

            if (!relations.isEmpty()) {
                upsert relations;
            }

        } catch (Exception ex) {
            insert CFA_IntegrationLogException.logError(ex, 'Create Relationship With Employer', 'ContactChangeEventTriggerHelper', Datetime.now(), true, '', '', '', true, '');
            throw ex;
        }
    }

    /**
     * @description Get AccountContactRelation based - return created new or/and updated old record
     *
     * @param con Contact
     * @param relationIdByConIdAccIdMap  Map<String, List<AccountContactRelation>>
     *
     * @return AccountContactRelation
     */
    private static List<AccountContactRelation> getRelationships(Contact con, Map<String, List<AccountContactRelation>> relationIdByConIdMap){
        Set<AccountContactRelation> relations = new Set<AccountContactRelation>();
        Boolean alreadyHasRelation = false;

        // if Contact has other Employers
        if (relationIdByConIdMap.containsKey(con.Id)) {
            relations.addAll(updateRelationship(con,relationIdByConIdMap.get(con.Id)));
        }

        for(AccountContactRelation relation : relations){
            if(relation.AccountId == con.Employer_Account__c){
                alreadyHasRelation = true;
            }
        }

        // if new Employer is not null
        if (!alreadyHasRelation && con.Employer_Account__c != null) {
            relations.add(createRelationship(con));
        }

        return new List<AccountContactRelation>(relations);
    }

    /**
     * @description Update existed AccountContactRelation
     *
     * @param con Contact
     * @param existedRelations List<AccountContactRelation>
     *
     * @return List<AccountContactRelation>
     */
    private static List<AccountContactRelation> updateRelationship(Contact con, List<AccountContactRelation> existedRelations) {
        List<AccountContactRelation> updatedRelations = new List<AccountContactRelation>();

        if (existedRelations != null) {

            for(AccountContactRelation existedRelation : existedRelations){
                if(existedRelation.Roles != null) {
                    Set<String> roles = new Set<String>(existedRelation.Roles.split(';'));

                    if (roles.contains(ROLE_EMPLOYER)) {
                        if (existedRelation.AccountId != con.Employer_Account__c) {

                            if (roles.size() == 1) {
                                existedRelation.IsActive = false;
                            } else if (roles.size() > 1) {
                                List<String> updatedRoles = new List<String>();
                                for (String role : new List<String>(roles)) {
                                    existedRelation.Roles = String.join(updatedRoles, ';');
                                    existedRelation.IsActive = true;
                                }
                            }

                        } else {
                            existedRelation.IsActive = true;
                        }

                        updatedRelations.add(existedRelation);
                    }
                }
            }
        }

        return updatedRelations;
    }

    /**
     * @description Create AccountContactRelation
     *
     * @param con Contact
     *
     * @return AccountContactRelation
     */
    private static AccountContactRelation createRelationship(Contact con) {
        AccountContactRelation relation = new AccountContactRelation();
        relation.ContactId = con.Id;
        relation.AccountId = con.Employer_Account__c;
        relation.IsActive = true;
        relation.Roles = ROLE_EMPLOYER;

        return relation;
    }

    /**
     * @description Get CampaignId By Contact Society Field Api Name
     *
     * @return Map<String, Id>
     */
    private static Map<String, Id> getCampaignIdByContactSocietyFieldApiName() {
        Map<String, Id> campaignIdByContactSocietyFieldApiName = new Map<String, Id>();
        List<NSA_Policy__c> policies = getNSAPolicies();
        for (NSA_Policy__c policy : policies) {
            if (policy.All_Campaign__c != null && !campaignIdByContactSocietyFieldApiName.containsKey(policy.Contact_Society_Field_API_Name__c)) {
                campaignIdByContactSocietyFieldApiName.put(policy.Contact_Society_Field_API_Name__c, policy.All_Campaign__c);
            }
        }
        return campaignIdByContactSocietyFieldApiName;
    }

    /**
     * @description Get NSA Policies
     *
     * @return List<NSA_Policy__c>
     */
    private static List<NSA_Policy__c> getNSAPolicies() {
        // find all policies
        List<NSA_Policy__c> policies = [
                SELECT Id
                        , Advanced_Assignment__c
                        , Advanced_Assignment_Handler__c
                        , All_Campaign__c
                        , Contact_Society_Field_API_Name__c
                FROM NSA_Policy__c
                WHERE Is_Active__c = TRUE
            	AND Policy_Status__c = 'Approved'
        ];
        return policies;
    }
}
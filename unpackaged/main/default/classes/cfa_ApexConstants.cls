/**
 * Copyright (c)  CFA Institute,  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, is stricty prohibited.
 * The contents are considered Proprietary and confidential.
 *
 *
 * @author CFA
 * @date
 * @brief Apex Constants
 * @description Apex Constants
 * @group Common Layer
 */
public class cfa_ApexConstants {

    // common SF object names -- change values here if API name changes
    public static final String ObjectName_Account = 'Account';
    public static final String ObjectName_Contact = 'Contact';
    public static final String ObjectName_Opportunity = 'Opportunity';
    public static final String ObjectName_Case = 'Case';
    public static final String ObjectName_Lead = 'Lead';
    public static final String ObjectName_Task = 'Task';
    public static final String All = 'All';


    /*
     * Misc Strings
     */
    public static final String Unknown = 'Unknown';
    public static final String EmptyString = '';
    public static final String WhitespaceString = ' ';
    public static final String ORString = 'OR';
    public static final String ANDString = 'AND';
    public static final String ColonDelimiter = ':';
    public static final String Period = '.';
    public static final String CommaDelimiter = ',';
    public static final string TabDelimiter = '\t';
    public static final string NewLine = '\n';
    /*
     * REGEX
     */
    // this will catch MOST -- NOT ALL valid email address
    public static final String EMAIL_REGEX = '^[a-zA-Z0-9._}{+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$';
    // Matches US Only ... (111) 222-3333 | 1112223333 | 111-222-3333 |
    public static final string TELEPHONE_REGEX = '^\\D?(\\d{3})\\D?\\D?(\\d{3})\\D?(\\d{4})$';
    // Matches a range of international and local telephone numbers:
    // 0689912549, +33698912549, +33 6 79 91 25 49,+33-6-79-91-25-49 ,(555)-555-5555
    // 555-555-5555, +1-238 6 79 91 25 49, +1-555-532-3455, +15555323455, 55555555555555555555555555
    // +7 06 79 91 25 49
    public static final string ALL_TELEPHONE_REGEX = '((?:\\+|00)[17](?: |\\-)?|(?:\\+|00)[1-9]\\d{0,2}(?: |\\-)?|(?:\\+|00)1\\-\\d{3}(?: |\\-)?)?(0\\d|\\([0-9]{3}\\)|[1-9]{0,3})(?:((?: |\\-)[0-9]{2}){4}|((?:[0-9]{2}){4})|((?: |\\-)[0-9]{3}(?: |\\-)[0-9]{4})|([0-9]{7}))';
    // number of numbers in an US telephone #
    public static final integer US_TELEPHONE_SIZE = 10;

} // end of cfa_ApexConstants
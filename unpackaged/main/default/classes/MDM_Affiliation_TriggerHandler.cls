/**************************************************************************************************************************************************
=====================================================================
Purpose: While creating a new Affiliation from Contacts or Account tab if the selected Account's Record Type 
        and the Type field of Affiliation does not match while saving it will displat error message to change
        its Account or to change its Type field before saving the record.
=====================================================================
History  
-------------
VERSION  AUTHOR         DATE           DETAIL          Description
1.0     Sidhant       11/7/2017       Created       Added method onBeforeInsertUpdate()  
2.0     Smitha        12/20/2017      Updated       Added code to handle duplicate Affiliations and to filter Contacts to OR user
        Anupama       12/21/2017      Updated       Added one more condition
3.0     Anupama       01/09/2018      Updated       Added method onAfterInsertUpdate() 
4.0     Anupama       31/01/2018      Updated       Added code to filter contacts based on Active Affiliation to OR users
        Anupama       02/02/2018      Modified      Cleaned the code. this Controller is only used to display error messages 
****************************************************************************************************************************************************/
public with sharing class MDM_Affiliation_TriggerHandler {
    
    /****************************************************************************************************************************
    Purpose : This method is called before insert of a Affiliation to check whether the selected Account's RecordType and Type field are same
              or not, and to restrict user to create duplicate Affiliation and to assign the Affiliation records based on Affiliation role
              to new related list (Outreach Affiliations)for OR users   
    Parameters : List<Affiliation__c> affiliationList
    Returns : void
    ********************************************************************************************************************************/
    
    public void onBeforeInsertUpdate(List<Affiliation__c> newAffiliationList ){
        
        // fetching the AccountId of Default Account from Custom Settings
        List<MDM_Default_Account__c> lstDfltAcc = MDM_Default_Account__c.getall().values();
        Id idDefaultAcc;
        // added null check here
        if(!lstDfltAcc.isEmpty()){
            idDefaultAcc = lstDfltAcc[0].MDM_Account_ID__c;
        }
        
        Map<Id,Affiliation__c> mapNewAccIdToAff = new Map<Id,Affiliation__c>();  
        Affiliation__c affiliationobj;
        try
        {
            for(Affiliation__c newAffiliation : newAffiliationList){
                mapNewAccIdToAff.put(newAffiliation.Account__c, newAffiliation);
            }
            
            //Throws error if user tries to create dupliate Affiliations
            if(!mapNewAccIdToAff.isEmpty()){
                if(mapNewAccIdToAff.containsKey(idDefaultAcc)){
                    mapNewAccIdToAff.get(idDefaultAcc).addError('You can not create Affiliation for Unknown Employer/Fonteva Default Account');
                }
                else {
                    //Query all the Affiliations under the Account
                    for(Affiliation__c objAff : [  SELECT Id, Contact__c, Account__c, Role__c, Active_Affiliation__c 
                                                   FROM Affiliation__c 
                                                   WHERE Account__c IN : mapNewAccIdToAff.keyset() ]) {
                         //Check if the Contact and Role inserted/updted in Affiliation already exists and the Affiliation is active,then throw error
                        if( mapNewAccIdToAff.containsKey(objAff.Account__c) && mapNewAccIdToAff.get(objAff.Account__c).Id != objAff.Id && 
                            objAff.Role__c != 'Employee' && 
                            objAff.Contact__c  == mapNewAccIdToAff.get(objAff.Account__c).Contact__c && 
                            objAff.Role__c == mapNewAccIdToAff.get(objAff.Account__c).Role__c &&
                            objAff.Active_Affiliation__c == True) {
                            mapNewAccIdToAff.get(objAff.Account__c).addError('Duplicate Role. Role '+mapNewAccIdToAff.get(objAff.Account__c).Role__c+' already exists for this Contact');
                        }
                    }
                }
            }
            
            //Throws error if user selects invalid type for Account
            for(Account account:[SELECT Id, RecordType.Name, ParentId, MDM_Curation_Status__c FROM Account WHERE Id in : mapNewAccIdToAff.keySet()]){
                //iterating map and storing current map value in affiliation object
                if(mapNewAccIdToAff.containsKey(account.id))
                affiliationobj=mapNewAccIdToAff.get(account.Id);
                //checking whether the Account's Record type value does not match Type field value
                if(affiliationobj.Type__c != account.RecordType.Name){
                    //displaying error using addError method on affiliaion object
                    affiliationobj.addError('You have selected an invalid type for Account. Please select '+account.RecordType.Name);
                }
                If(account.ParentId != null && account.MDM_Curation_Status__c == 'Mapped'){
                mapNewAccIdToAff.get(account.Id).MDM_Mapped_Account__c = account.ParentId;
                }
            }
        }
        catch(Exception ex){
            system.debug('Excpetion----'+ex.getMessage());
        }
    }
}
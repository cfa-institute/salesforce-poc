/**************************************************************************************************
* Apex Class Name   : CustomerCareEmailMessageEmail
* Purpose           : This test class is used for updating code coverage of Trigger CustomerCareSendEamil and Handler Class CustomerCareEmailMessageEmail   
* Version           : 1.0 Initial Version
* Organization      : Cognizant
* Created Date      : 15-Nov-2017  
***************************************************************************************************/
@isTest
public class CustomerCareSendEamil_Test {

    static testMethod void testEmailMessageApprover(){
            Test.startTest();
        boolean isSB =[SELECT IsSandbox FROM Organization LIMIT 1].IsSandbox;
        SocietyOps_Backoffice__c obj = new SocietyOps_Backoffice__c(name='trigger-details',CaseReassignment_TemplateName__c='SocietyOps-Case Reassignment',CaseComment_TemplateName__c='SocietyOps-New Comment Notification');
        if(isSB==true)
        {
            obj.No_reply_email__c='researchchallenge@cfainstitute.org';
        }
        else
        {
            obj.No_reply_email__c='noreply.societyoperations@cfainstitute.org';
        }
        insert obj;
            User usr = CustomerCare_CommonTestData.testDataUser();
            Case testEmailCase = CustomerCare_CommonTestData.TestDataEmailCase(); 
            EmailMessage newEmail = CustomerCare_CommonTestData.TestDataForApprovedEmailMessage(); 
            system.assertEquals('Approved',newEmail.Approver_status__c);
            Test.stopTest();
    }
    
    static testMethod void testEmailMessageRejector(){
            Test.startTest();
        boolean isSB =[SELECT IsSandbox FROM Organization LIMIT 1].IsSandbox;
        SocietyOps_Backoffice__c obj = new SocietyOps_Backoffice__c(name='trigger-details',CaseReassignment_TemplateName__c='SocietyOps-Case Reassignment',CaseComment_TemplateName__c='SocietyOps-New Comment Notification');
        if(isSB==true)
        {
            obj.No_reply_email__c='researchchallenge@cfainstitute.org';
        }
        else
        {
            obj.No_reply_email__c='noreply.societyoperations@cfainstitute.org';
        }
        insert obj;
            User usr = CustomerCare_CommonTestData.testDataUser();
            Case testEmailCase = CustomerCare_CommonTestData.TestDataEmailCase(); 
            EmailMessage newEmail = CustomerCare_CommonTestData.TestDataForRejectedEmailMessage(); 
            system.assertEquals('Rejected',newEmail.Approver_status__c);
            Test.stopTest();
    }
}
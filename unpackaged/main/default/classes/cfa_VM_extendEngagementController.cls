public class cfa_VM_extendEngagementController{
    @AuraEnabled
    public static String getIconName(String sObjectName){ 
        String u;
        List<Schema.DescribeTabSetResult> tabSetDesc = Schema.describeTabs();
        List<Schema.DescribeTabResult> tabDesc = new List<Schema.DescribeTabResult>();
        List<Schema.DescribeIconResult> iconDesc = new List<Schema.DescribeIconResult>();
        
        for(Schema.DescribeTabSetResult tsr : tabSetDesc) { tabDesc.addAll(tsr.getTabs()); }
        
        for(Schema.DescribeTabResult tr : tabDesc) {
            if( sObjectName == tr.getSobjectName() ) {
                if( tr.isCustom() == true ) {
                    iconDesc.addAll(tr.getIcons());
                } else {
                    u = 'standard:' + sObjectName.toLowerCase();
                }
            }
        }
        for (Schema.DescribeIconResult ir : iconDesc) {
            if (ir.getContentType() == 'image/svg+xml'){
                u = 'custom:' + ir.getUrl().substringBetween('custom/','.svg').substringBefore('_');
                break;
            }
        }
        return u;
    }
    
    @AuraEnabled
    public static Engagement__c  getInitDetails(String engagementId) {
        List<Engagement__c> lstEngagement = [Select id,Name,
                           Start_Date__c,
                           Expiration_Date__c,
                           Status__c, 
                           Engagement_Type__c,
                           Business_Owner__c,
                           MD__c,
                           Reports_To__c,
                           Aligns_to_which_Strategic_Objective__c,
                           Business_Objective__c,
                           Business_Owner_Cost_Center__c,
                           Estimated_Cost__c
                           from Engagement__c where id=:engagementId];
        
        if(!lstEngagement.isEmpty()) {
            return lstEngagement[0];
        } else {
            return null;
        }
        
        
    }
    
    
    
    @AuraEnabled
    public static string saveEngagement(String json,String engId) {
        
        json = json.replace('{"fields":{', '[{"attributes":{"type":"Engagement__c"},');
        json = json.replace('"}}', '"}] ');
        json = json.replace('""','null');
        System.debug(json);
        List<Engagement__c> lstEng = (List<Engagement__c>)System.JSON.deserialize(json,List<Engagement__c>.class);
        try{
            if(Test.isRunningTest()) {
                lstEng[0].id=null;
                lstEng[0].Name='Test FY01';
            }
            lstEng[0].cfa_Parent_Engagement__c = engId;
            lstEng[0].OwnerId = userInfo.getUserId();
            System.debug(lstEng);
            //insert lstEng;
            upsert lstEng;
        } catch(DMLException  e) {
            System.debug(e.getMessage());
            String errorStr = e.getMessage().substringBetween('FIELD_CUSTOM_VALIDATION_EXCEPTION,',':');
            System.debug(errorStr);
            return errorStr;
        }
        
        return lstEng[0].id;
    }
}
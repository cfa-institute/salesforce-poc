/**************************************************************************************************
* Apex Class Name   : LoginHistoryWrapper
* Purpose           : This is the wrapper class which is used for mapping Individual key value of Loginhistory Json Response into seperate field
* Version           : 1.0 Initial Version
* Organization      : Cognizant
* Created Date      : 8-Feb-2018   
***************************************************************************************************/
public class LoginHistoryWrapper {
    public string PersonId{get;set;}
    public string LoginStatus{get;set;}
    public DateTime LoginAttemptDate{get;set;} 

 
}
/**************************************************************************************************
* Apex Class Name   : CustomerCare_ImageUrlOnEmailTemplate
* Purpose           : This class is used to return image url for vf component
* Version           : 1.0 Initial Version
* Organization      : Cognizant
* Created Date      : 31-Oct-2017  
***************************************************************************************************/
public class CustomerCare_ImageUrlOnEmailTemplate{
    public static string  getImageUrl() {
        List<CustomerCare_ImageURL__c> listImageUrls = CustomerCare_ImageURL__c.getall().values();
        String HeaderUrl = listImageUrls[0].Header_URL__c;
        return HeaderUrl;
    }
}
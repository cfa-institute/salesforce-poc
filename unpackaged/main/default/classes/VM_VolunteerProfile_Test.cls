@isTest
//Bhushan
public class VM_VolunteerProfile_Test {  
    
    public static Account portalAccount;
    public static Contact portalContact;
    public static user portalUser;
    
    public static ContactShare conShare;
    public static Engagement__Share engShare;
    public static final String PARTNER_RECORDTYPE = 'CFA Contact';
    public static final String ENGAGEMENT_RECORDTYPE = 'Non Confidential';

    public static void setupdata(){
        ContactTriggerHandler.triggerDisabled = true;
        
        portalAccount = VM_TestDataFactory.createAccountRecord();
		// portalAccount.IsPartner = true;
        insert portalAccount;
        
        portalContact = VM_TestDataFactory.createContactRecord(portalAccount);        
        Schema.DescribeSObjectResult contactSchemaResult = Schema.SObjectType.Contact;
        Map<String,Schema.RecordTypeInfo> mapContactTypeInfo = contactSchemaResult.getRecordTypeInfosByName();
        Id partnerRecordTypeId = mapContactTypeInfo.get(PARTNER_RECORDTYPE).getRecordTypeId();        
        portalContact.RecordTypeId = partnerRecordTypeId;
        portalContact.MDM_Preferred_Email_for_Volunteering__c = 'abc@abc.com';
        update portalContact;
        
        //Contact checkcon = [select id, recordtype.name, recordtypeid from contact where id = :portalContact.Id];
        //system.debug('@@@@ portalContact: '+checkcon.RecordTypeId);
        //portalUser = VM_TaskCreateEditDetail_Test.createPartnerUser('volTaskCrEditDet@gmail.com','volTaskCrEditDet','Volunteer','Managing Director', portalContact.Id);
        portalUser = CFA_TestDataFactory.createPartnerUser(portalContact,'CFA Base Partner');
        
        ContactShare conShare = new ContactShare();
        conShare.ContactId =portalContact.Id;
        conShare.UserOrGroupId = portalUser.Id;
        conShare.ContactAccessLevel = 'Edit';
        insert conShare;
    }
    
    
    public static testMethod void testVolProfile() {
        setupdata();
        //Task tsk = VM_TaskCreateEditDetail.getNewTask();
        
        try
        {         
            System.runAs(portalUser){
                Test.startTest();
                //Acc = VM_TestDataFactory.createAccountRecord();              
                //Contact Con = VM_TestDataFactory.createContactRecord(Con);
                VM_VolunteerProfile.getContact(portalContact);
                VM_VolunteerProfile.getContactCompetencies();
                VM_VolunteerProfile.getAvailabilityStatus();
                VM_VolunteerProfile.getdetails();
                Test.stopTest();
            }
        }
        catch(DMLException ex)
        {
            system.debug('testVolProfile'+ex.getDMLMessage(0));
        }
        
    }

}
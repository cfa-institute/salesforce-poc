@isTest
private class CalloutClassTest {
     @isTest static void testCalloutPause() {
        User usr = CustomerCare_CommonTestData.testDataCreateContact1();
               System.runAs(usr){
            CustomerCare_CommonTestData.testCiscoSettings();
            Id AccId = CustomerCare_CommonTestData.testDataAccount()[0].id;
            system.assert(AccID != Null);
            Contact contact1 = CustomerCare_CommonTestData.testCFADataContact();
            system.assert(contact1 != Null);
            Case cfaCase = CustomerCare_CommonTestData.TestDataCFAEmailCase();
            Test.setCurrentPage(Page.Customer_Care_Integrations);
            ApexPages.currentPage().getHeaders().put('id',cfaCase.Id);
            
            customerCare_QMCallOut qmCall = new customerCare_QMCallOut(new ApexPages.StandardController(cfaCase));
            
            Test.startTest();
            Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator(200));
            qmCall.cc_PauseRecording();
            
            Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator(500)); // For Else Part
            qmCall.cc_PauseRecording();
            
            Test.setMock(HttpCalloutMock.class, null); // For Catch exception part
            qmCall.cc_PauseRecording();
            
            Test.stopTest();
            
        }
    }
    
    @isTest static void testCalloutResume() {
        User usr = CustomerCare_CommonTestData.testDataCreateContact1();
       
        System.runAs(usr){
            CustomerCare_CommonTestData.testCiscoSettings();
            Id AccId = CustomerCare_CommonTestData.testDataAccount()[0].id;
            system.assert(AccID != Null);
            Contact contact1 = CustomerCare_CommonTestData.testCFADataContact();
            system.assert(contact1 != Null);
            Case cfaCase = CustomerCare_CommonTestData.TestDataCFAEmailCase();
            
            PageReference pageRef = Page.Customer_Care_Integrations;
            Test.setCurrentPage(pageRef);
            ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(cfaCase);
            ApexPages.currentPage().getParameters().put('Id',cfaCase.id);
            customerCare_QMCallOut qmCall = new customerCare_QMCallOut(sc);
           
            Test.startTest();
            Test.setMock(HttpCalloutMock.class, new MockHttpResponseGeneratorcc(200));            
            qmCall.cc_ResumeRecording();
            
            Test.setMock(HttpCalloutMock.class, new MockHttpResponseGeneratorcc(500)); // For Else Part
            qmCall.cc_ResumeRecording();
            
            Test.setMock(HttpCalloutMock.class, null); // For Catch exception part
            qmCall.cc_ResumeRecording();

            Test.stopTest();
        }
    }
}
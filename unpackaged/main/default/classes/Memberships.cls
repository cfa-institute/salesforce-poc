/**************************************************************************************************
* Apex Class Name   : Memberships
* Purpose           : This class is wrapper class for Membership integration
* Organization      : Lightning Team -- Vijay Vemuru
* Created Date      : July 17th 2018
* Modified Date 	: September 28th 2018
***************************************************************************************************/

public class Memberships{
        
		public Boolean OnProfessionalLeave{get; set;}
    	public String CurrentStatus{get; set;}
        public DateTime JoinDate{get; set;}
        public DateTime ExpirationDate{get; set;}
        public String MembershipType{get; set;}
        public String Organization{get; set;}
    	public String OrganizationPartnerId{get; set;}
        public Boolean IsCFAMembership{get; set;}
        public Boolean IsAffiliate{get; set;}
}
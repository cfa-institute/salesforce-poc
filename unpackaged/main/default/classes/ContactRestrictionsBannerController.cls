/**
 * @description Controller for Contact Restriction Banner LWC component
 */
public with sharing class ContactRestrictionsBannerController {

    /**
     * @description returns activew restrictions list for contact
     * @param   contactId      String  Contact Id
     * @return  List<Restriction__c>    List of active restriction related to contact
     */
    @AuraEnabled(cacheable=true)
    public static List<Restriction__c> getContactActiveRestrictions(Id contactId) {
        return [SELECT ID,Name,Type__c,Sub_Type__c 
                  FROM Restriction__c 
                 WHERE Contact__c =:contactId 
                   AND Active__c = true
                  WITH SECURITY_ENFORCED];

    }
}
@isTest
public class CustomerCare_ContactTabsTest {
    
    @isTest
    public static void getTabMapTest(){
        Test.startTest();
        CustomerCare_ContactTabs.getTabMap();
        Test.stopTest();
    }    
    
    @isTest
    public static void getDataTest(){
        Test.startTest();
        CC_Callout_MockHttpResponseGenerator callOutMock = new CC_Callout_MockHttpResponseGenerator(200);
        Test.setMock(HttpCalloutMock.class, callOutMock);
        CustomerCare_ContactTabs.getData('Enrollment', 'Enrollment', '123');
        CustomerCare_ContactTabs.getData('History', 'History', '123');
        callOutMock = new CC_Callout_MockHttpResponseGenerator(503);
        Test.setMock(HttpCalloutMock.class, callOutMock);
        try{ /** try - catch for Aura Exception **/
            CustomerCare_ContactTabs.getData(null, null, null);
        }
        Catch(Exception e){
            
        }
        Test.stopTest();
    }
    @isTest
    public static void getMembershipTransactionsTest(){
        CC_Callout_MockHttpResponseGenerator callOutMock = new CC_Callout_MockHttpResponseGenerator(200);
        Test.setMock(HttpCalloutMock.class, callOutMock);
        CustomerCare_ContactTabs.getMembershipTransactions('123', null, 'Test');
        callOutMock = new CC_Callout_MockHttpResponseGenerator(503);
        Test.setMock(HttpCalloutMock.class, callOutMock);
        try{
            CustomerCare_ContactTabs.getMembershipTransactions('123', null, 'Test');
        }
        catch(Exception e){
            
        }
    }
    
     @isTest
    public static void getMemebershipApplicationTest(){
        CC_Callout_MockHttpResponseGenerator callOutMock = new CC_Callout_MockHttpResponseGenerator(200);
        Test.setMock(HttpCalloutMock.class, callOutMock);
        CustomerCare_ContactTabs.getMemebershipApplication('123','Test');
        callOutMock = new CC_Callout_MockHttpResponseGenerator(503);
        Test.setMock(HttpCalloutMock.class, callOutMock);
        try{
            CustomerCare_ContactTabs.getMemebershipApplication('123','Test');
        }
        catch(Exception e){
            
        }
    }


}
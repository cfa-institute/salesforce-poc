/*
* Author: Geetha Reddy
* Version: 1.0
* Date:7/24/2018
* MembershipTransactionWebService
*/

@RestResource(urlMapping='/membershiptransaction/v1.0/*')
global class MembershipInboundWebService {
  // Request Wrapper  
    global class  MembershipRequest{
        public String Action;
        public String TransactionId;
        public String Code;
        public String Message;
        public List<MembershipUpdate.Memberships> Memberships;
    }
    //Response Wrapper
    global class MembershipResponse{
        public String Action;
        public String TransactionId;
        public String Code;
        public String Message;
        public List<MembershipUpdate.Memberships> Memberships;
    }
    // Post Method for Membership transactions
    @HttpPost
    global static MembershipResponse membershipTransaction(){
        MembershipRequest membershipRequest;
        MembershipResponse membershipResponse = new MembershipResponse();
        try{
            //Intilize Membership JSON request
            membershipRequest = (MembershipRequest)JSON.deserialize(RestContext.request.requestBody.toString(), MembershipRequest.class);
            MembershipUpdate membershipUpdate = new MembershipUpdate();
            //Invoking Update Method from Membership Update
            membershipResponse.Memberships =  membershipUpdate.updateMT(membershipRequest.Memberships);
            membershipResponse.Code = '200';
            membershipResponse.Message = 'Success';
             membershipResponse.TransactionId = membershipRequest.TransactionId;
            membershipResponse.Action = membershipRequest.Action;
        }
        catch(Exception e){
            membershipResponse.Message = e.getMessage()+' '+e.getStackTraceString();
            membershipResponse.Code = '500';
        }
       
        return membershipResponse;  
    }
    
    
}
/*****************************************************************
Name: SocietyCampaignMemberService
Copyright © 2020 ITC
============================================================
Purpose: Service for CampaignMemberEventTrigger, CampaignMemberChangeEventTrigger, CampaignMemberTriggerHelper
============================================================
History
-------
VERSION  AUTHOR          DATE         DETAIL    Description
1.0      Alona Zubenko   24.11.2020   Created   Logic for updating Contact on Create/Delete CampaignMember
*****************************************************************/

public with sharing class SocietyCampaignMemberService {
    public static final String CAMPAIGN_NAME_ENDS_MEMBERS = 'Members';
    public static final String CAMPAIGN_NAME_ENDS_ALL = 'All';
    public static final String CAMPAIGN_TYPE_SOCIETY = 'Society';
    public static final String DELETE_CHANGE_TYPE = 'DELETE';


    /**
     * @description update Related to CampaignMember Contacts
     *
     * @param campaignMembers List<CampaignMember>
     *
     * @return List<Contact>
     */
    public static List<Contact> getUpdatedRelatedContacts(List<CampaignMember> campaignMembers) {
        List<Contact> contactsWithCampaignsList = getContactsWithCampaigns(campaignMembers);
        List<Contact> updatedContactList = new List<Contact>();
        for (Contact con : contactsWithCampaignsList) {
            if (!con.CampaignMembers.isEmpty()) {
                con = updateContactFields(con);
            } else {
                con.Society_Member__c = false;
                con.Campaign_Relations__c = '';
            }
            updatedContactList.add(con);
        }
        return updatedContactList;
    }

    /**
     * @description Get Campaign Members
     *
     * @param recordIds Set<String>
     *
     * @return List<CampaignMember>
     */
    public static List<CampaignMember> getCampaignMembers(Set<String> recordIds) {
        String query = 'SELECT Id, ContactId FROM CampaignMember WHERE Id IN :recordIds';
        List<CampaignMember> campaignMembers = (List<CampaignMember>) Database.query(query);
        return campaignMembers;
    }

    /**
     * @description Get Campaign Members from CampaignMemberPlatformEvent__e list
     *
     * @param platformEvents List<CampaignMemberPlatformEvent__e>
     *
     * @return List<CampaignMember>
     */
    public static List<CampaignMember> getCampaignMembers(List<CampaignMemberPlatformEvent__e> platformEvents) {
        List<CampaignMember> campaignMembers = new List<CampaignMember>();
        for (CampaignMemberPlatformEvent__e event : platformEvents) {
            campaignMembers.add(new CampaignMember(ContactId = event.ContactId__c, CampaignId = event.CampaignId__c));
        }
        return campaignMembers;
    }

    /**
     * @description Create Platform Event on Delete the CampaignMember
     *
     * @param oldMap (Map<Id, CampaignMember>
     */
    public static void createPlatformEvent(Map<Id, CampaignMember> oldMap) {
        List<CampaignMemberPlatformEvent__e> eventList = new List<CampaignMemberPlatformEvent__e>();
        for (CampaignMember cm : oldMap.values()) {
            CampaignMemberPlatformEvent__e event = new CampaignMemberPlatformEvent__e();
            event.CampaignId__c = cm.CampaignId;
            event.ContactId__c = cm.ContactId;
            event.ChangeType__c = DELETE_CHANGE_TYPE;
            eventList.add(event);
        }
        // Call method to publish events
        List<Database.SaveResult> results = EventBus.publish(eventList);

        // Inspect publishing result for each event
        for (Database.SaveResult sr : results) {
            if (sr.isSuccess()) {
                System.debug('Successfully published event.');
            } else {
                for (Database.Error err : sr.getErrors()) {
                    System.debug('Error returned: ' +
                            err.getStatusCode() +
                            ' - ' +
                            err.getMessage());
                }
            }
        }
    }

    /**
     * @description Get Contacts With Campaigns
     *
     * @param campaignMembers List<CampaignMember>
     *
     * @return List<Contact>
     */
    private static List<Contact> getContactsWithCampaigns(List<CampaignMember> campaignMembers) {
        Set<Id> conIds = new Set<Id>();
        for (CampaignMember campaignMember : campaignMembers) {
            conIds.add(campaignMember.ContactId);
        }
        List<Contact> conList = [
                SELECT Id, Society_Member__c, Campaign_Relations__c
                        , (SELECT Id, Campaign.Name, Campaign.Type FROM CampaignMembers)
                FROM Contact
                WHERE Id IN :conIds
        ];

        return conList;
    }

    /**
     * @description Update Society_Member__c and Campaign_Relations__c fields on Contact
     *
     * @param con Contact
     *
     * @return Contact
     */
    private static Contact updateContactFields(Contact con) {
        con.Society_Member__c = false;
        String campaignRelations = '';
        for (CampaignMember campaignMember : con.CampaignMembers) {
            if (campaignMember.Campaign?.Type == CAMPAIGN_TYPE_SOCIETY){
                if(campaignMember.Campaign?.Name.endsWith(CAMPAIGN_NAME_ENDS_MEMBERS)
                        || campaignMember.Campaign?.Name.endsWith(CAMPAIGN_NAME_ENDS_ALL)){

                    campaignRelations += campaignMember.Campaign?.Name + ', ';
                    if(campaignMember.Campaign?.Name.endsWith(CAMPAIGN_NAME_ENDS_MEMBERS)) {
                        con.Society_Member__c = true;
                    }
                }
            }
        }
        con.Campaign_Relations__c = campaignRelations.removeEnd(', ');
        return con;
    }


}
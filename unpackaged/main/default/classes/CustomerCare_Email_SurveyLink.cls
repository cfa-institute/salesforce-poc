/**************************************************************************************************
* Apex Class Name   : CustomerCare_Email_SurveyLink 
* Purpose           : This class is used to send email for survey with surveylink in the email
* Version           : 1.0 Initial Version
* Organization      : Cognizant
* Created Date      : 08-Nov-2017  
***************************************************************************************************/
public class CustomerCare_Email_SurveyLink{
    /*This method is used to send Survey Link*/
    @InvocableMethod(label='ccEmail_SurveyLink' description='Send Survey Link')
    public static void ccSendSurveyLinks(List<Case> listClosedCases){
        List<Id> listContactIds=new List<Id>();
        List<Id> attchDeleteCases = new List<Id>();
        List<Case> listCases=new List<Case>();
        List<Case> listEmailCases=new List<Case>();
        
        for(Case caseObj :listClosedCases){
            if(caseObj.Status== CustomerCareUtility.closedStatus){   
                if(caseObj.contactId != null && (caseObj.Resolution_Type__c == 'Resolved' || caseObj.Resolution_Type__c == 'Unresolved')){
                    listContactIds.add(caseObj.contactId);
                    listCases.add(caseObj); 
                }   
                //Changes added for Encryption
                if(caseObj.Delete_Attachment__c){
                    attchDeleteCases.add(caseObj.Id);
                }
                if((caseObj.Origin==CustomerCareUtility.emailOrigin || caseObj.Origin==CustomerCareUtility.webOrigin) &&
                   (caseObj.sub_area__c=='Deferral'||caseObj.sub_area__c=='ID Change'||
                    caseObj.sub_area__c=='Name Change'||caseObj.Exam_Security__c||caseObj.Encrypted_Description__c!=null)){
                        listEmailCases.add(caseObj);
                    }
                //Changes added for Encryption                  
            }
        }
        //Used to check Email opt out and Do not email is checked in contact or not 
        Map<Id,Boolean> mapContactIdToFlag = new Map<Id,Boolean>();
        Map<Id,DateTime> mapContactIdSurveyDateTime=new Map<Id,DateTime>();
        for(Contact cntct : [SELECT id,HasOptedOutOfEmail,CFAMN__NEML__c,Last_Survey_Date__c
                             From Contact WHERE ID IN :listContactIds]) {
                                 if(!cntct.HasOptedOutOfEmail && !cntct.CFAMN__NEML__c){
                                     mapContactIdToFlag.put(cntct.Id,true);
                                 }else{
                                     mapContactIdToFlag.put(cntct.Id,false);
                                 }
                                 mapContactIdSurveyDateTime.put(cntct.id, cntct.Last_Survey_Date__c);                     
                             }
        
        
        List<EmailTemplate> listEmailTemplates = [Select Id,Name,DeveloperName from EmailTemplate where DeveloperName Like '%CustomerCare_SurveyLink%'];
        
        Map<String,Id> mapNametoEmailTemplateId=new Map<String,Id>();
        for(EmailTemplate emailTemplate :listEmailTemplates){
            mapNametoEmailTemplateId.put(emailTemplate.DeveloperName,emailTemplate.Id);
        }
        
        // Access custom setting CustomerCare_SurveyEmails
        List<CustomerCare_SurveyEmails__c> ccemail = CustomerCare_SurveyEmails__c.getall().values();
        Map<String,String> ccsurveyEmailMap = new Map<String,String>();
        if(!ccemail.isEmpty()){
            for(CustomerCare_SurveyEmails__c ccObj : ccemail){
                if(!String.isBlank(ccObj.Email_Template__c) && !String.isBlank(ccObj.Customer_Type__c))
                    ccsurveyEmailMap.put(ccObj.Customer_Type__c,ccObj.Email_Template__c);
            }
        }
        
        List<Messaging.SingleEmailMessage> listSurveyEmails=new List<Messaging.SingleEmailMessage>();
        OrgWideEmailAddress[] fromAddress = [SELECT Address,DisplayName,Id FROM OrgWideEmailAddress where DisplayName = 'CFA Institute Customer Service (Do Not Reply)'];
        for(Case caseObj :listCases){
             Boolean emailOptFlag=mapContactIdToFlag.get(caseObj.contactId);
             system.debug('***emailOptFlag: '+emailOptFlag+'***caseObj.Customer_Type__c: '+caseObj.Customer_Type__c);
                    
             if(emailOptFlag!=null && emailOptFlag && String.isNotBlank(caseObj.Customer_Type__c) && (caseObj.Resolution_Type__c=='Resolved'|| caseObj.Resolution_Type__c=='Unresolved')){ 
                 DateTime lastSurveyDate=mapContactIdSurveyDateTime.get(caseObj.contactId);
                 integer differenceDays;
                 if(lastSurveyDate!=null)
                     differenceDays=  lastSurveyDate.Date().daysBetween(system.now().Date());
                 
                 system.debug('***differenceDays***'+differenceDays);
                 if(lastSurveyDate==null ||(lastSurveyDate!=null && differenceDays>30)){
                     Messaging.SingleEmailMessage surveyEmail = new Messaging.SingleEmailMessage();
                     if (!fromAddress.isEmpty() && fromAddress.size() > 0) {
                         surveyEmail.setOrgWideEmailAddressId(fromAddress.get(0).Id);
                     }
                     surveyEmail.setTargetObjectId(caseObj.contactId);
                     
                     /* if-else to select emailTemplate for sending mails as per Customer Type
                     if(caseObj.Customer_Type__c.containsIgnoreCase('Member')){
                         system.debug('***Inside Member');
                         surveyEmail.setTemplateId(mapNametoEmailTemplateId.get(CustomerCareUtility.EmailtempSurveyLinkforMember).id);
                     }
                     else if(caseObj.Customer_Type__c.containsIgnoreCase('CFA Candidate')){
                         system.debug('***Inside CFA Candidate');
                         surveyEmail.setTemplateId(mapNametoEmailTemplateId.get(CustomerCareUtility.EmailtempSurveyLinkforCandidate).id);
                     }
                     else if(caseObj.Customer_Type__c.containsIgnoreCase('CIPM Candidate')){ 
                         system.debug('***Inside CIPM Candidate');
                         surveyEmail.setTemplateId(mapNametoEmailTemplateId.get(CustomerCareUtility.EmailtempSurveyLinkforCIPMCandidate).id);
                     }
                     else if(caseObj.Customer_Type__c =='Investment Foundation Candidate'){ 
                         system.debug('***Inside Investment Foundation Candidate');
                         surveyEmail.setTemplateId(mapNametoEmailTemplateId.get(CustomerCareUtility.EmailtempSurveyLinkforInvFoundCandidate).id);
                     }*/
                     //System.debug('!!!!1'+ccsurveyEmailMap.containsKey(caseObj.Customer_Type__c)+'####@2'+mapNametoEmailTemplateId.get(ccsurveyEmailMap.get(caseObj.Customer_Type__c)));
                     if(!mapNametoEmailTemplateId.isEmpty() && !ccsurveyEmailMap.isEmpty() && ccsurveyEmailMap.containsKey(caseObj.Customer_Type__c))
                        surveyEmail.setTemplateId(mapNametoEmailTemplateId.get(ccsurveyEmailMap.get(caseObj.Customer_Type__c)));
                     
                     surveyEmail.setWhatId(caseObj.Id);
                     listSurveyEmails.add(surveyEmail);
                 }
             }
            system.debug('***listSurveyEmails***'+listSurveyEmails);
            if(!listSurveyEmails.isEmpty()){
                try{
                    Messaging.sendEmail(listSurveyEmails);
                }Catch(Exception e){
                    system.debug('CustomerCare_Email_SurveyLink :: Sending Mail'+e);
                }
            }
         }
        
        //Changes added for Encryption
        
         if(!listEmailCases.isEmpty()){
            List<EmailMessage> listEmailMessages=[Select id from EmailMessage where parentId IN:listEmailCases order by createdDate];
            
            if(listEmailMessages!=null && !listEmailMessages.isEmpty()){    
                try{
                    delete listEmailMessages[0];
                }Catch(Exception e){
                    system.debug('CustomerCare_Email_SurveyLink :: Deletion of EmailMessage'+e);
                }
            }
            
            List<CaseFeed> listCaseFeeds=[Select id from CaseFeed where parentId IN:listEmailCases 
                                          and type='CreateRecordEvent' order by createdDate asc];
            if(!listCaseFeeds.isEmpty()){
                try{
                    delete listCaseFeeds;
                }Catch(Exception e){
                    system.debug('CustomerCare_Email_SurveyLink :: Deletion of CaseFeeds'+e);
                }
            }
        }
        if(!attchDeleteCases.isEmpty()){
            List<ContentDocumentLink> listContentDocuments = [Select Id from ContentDocumentLink where LinkedEntityId IN:attchDeleteCases];
            if(listContentDocuments!=null && !listContentDocuments.isEmpty()){
                try{
                    database.delete(listContentDocuments);
                }catch(Exception e){
                    system.debug('CustomerCare_Email_SurveyLink :: Deletion of ContentDocuments'+e);
                }
            }
            
            List<Attachment> listAttachments=[Select id from Attachment where parentId IN:attchDeleteCases];
            if(listAttachments!=null && !listAttachments.isEmpty()){
                try{
                    database.delete(listAttachments);
                }catch(Exception e){
                    system.debug('CustomerCare_Email_SurveyLink :: Deletion of Attachments'+e);
                }
            }
            
            List<CaseFeed> listCaseFeeds=[Select id from CaseFeed where parentId IN:attchDeleteCases 
                                          and type='CreateRecordEvent' order by createdDate desc];
            if(!listCaseFeeds.isEmpty()){
                try{
                    delete listCaseFeeds;
                }catch(Exception e){
                    system.debug('CustomerCare_Email_SurveyLink :: Deletion of CaseFeed'+e);
                }
            }
        }
      
    }
}
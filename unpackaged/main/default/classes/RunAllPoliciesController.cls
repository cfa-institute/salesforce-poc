public without sharing class RunAllPoliciesController {
    
    @AuraEnabled
    public static void runAllPolicies() {
        try {
            List<INSAPolicyHandler> handlers = new List<INSAPolicyHandler>();
            AddressPolicyHandler addressPolicyHandler = new AddressPolicyHandler();
            MembershipPolicyHandler membershipPolicyHandler = new MembershipPolicyHandler();
            MembershipApplicationPolicyHandler membershipApplicationPolicyHandler = new MembershipApplicationPolicyHandler();
            DeceasedPolicyHandler deceasedPolicyHandler = new DeceasedPolicyHandler();
            handlers.add(addressPolicyHandler);
            handlers.add(membershipPolicyHandler);
            handlers.add(membershipApplicationPolicyHandler);
            handlers.add(deceasedPolicyHandler);
            NSAAssignmentBatch nsaAssignmentBatch = new NSAAssignmentBatch(
                    handlers,
                	'WHERE RecordType.DeveloperName IN (\'CFA_Contact\', \'Local_Member\')',
                    true
                );
            Database.executeBatch(
                nsaAssignmentBatch,
                200
            );
        } catch (Exception ex) {
            insert CFA_IntegrationLogException.logError(ex, 'Run All Policies Batch','RunAllPolicies', DateTime.now(), true, '', '', '', true, '');
            throw ex;
        }
        
    }
}
/**
 * Dev : Radhika
 * Description: update Affiliation_Active flag to false under condition.
 * Pre-requesite: Install Mass Action Scheduler
 * Updated - Yashwanth : 11/17/20
 */
public class CFA_UpdateAffiliationInvoke {

    @InvocableMethod(label = 'UpdateAffiliationInvoke')
    public static void myInvocableMethod( List<affiliationIterator> inputs) {
			
        Date todayDate = System.Today();
        Set<Id> affiliationIds = new Set<Id>();
        for(affiliationIterator inst: inputs){
            affiliationIds.add(inst.AffliationId);
        }
        //get the records and values from the source SOQL(input)
        List<Affiliation__c> affiliationsInputList = [SELECT Id, Start_Date__c, End_Date__c, Active_Affiliation__c FROM Affiliation__c WHERE Id IN:affiliationIds];
        //affiliations list to update
        List<Affiliation__c> updateAffList = new List<Affiliation__c>();
        for(Affiliation__c affId : affiliationsInputList) {
				if((affId.Start_Date__c == NULL) || ((affId.Start_Date__c > todayDate ) || (affId.Start_Date__c <= todayDate && affId.End_Date__c < todayDate ) &&  affId.Active_Affiliation__c == true) ) {
					affId.Active_Affiliation__c = false; 
				}else if (affId.Start_Date__c <= todayDate && (affId.End_Date__c == null || affId.End_Date__c >= todayDate) &&  affId.Active_Affiliation__c == false){
                    affId.Active_Affiliation__c = true; 
                }
            updateAffList.add(affId);
        }
		
        if(!updateAffList.isEmpty() && Schema.sObjectType.Affiliation__c.isUpdateable()) {
            update updateAffList;
            system.debug('Record after update: '+updateAffList);
        }
    }
    
    public class affiliationIterator{
        @InvocableVariable(label = 'Affliation ID')
        public ID AffliationId;
    }
}
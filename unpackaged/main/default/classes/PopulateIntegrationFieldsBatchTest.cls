@isTest
private class PopulateIntegrationFieldsBatchTest {

    @TestSetup
    private static void setup() {
        List<Contact> contacts = CFA_TestDataFactory.createContactRecords('CFA Contact', 'ContactRecord', 100, false);
        contacts.addAll(
        	CFA_TestDataFactory.createContactRecords('Local Member', 'ContactRecord', 100, false)
        );
        updateOldContactFields(contacts);
        ContactTriggerHandler.triggerDisabled = true;
        insert contacts;
    }
    
    @isTest
    private static void testBatch() {
        Test.startTest();
        Database.executeBatch(new PopulateIntegrationFieldsBatch());
        Test.stopTest();
        List<Contact> result = 
                [SELECT Id,CFAMN__PersonID__c , Person_Id__c,DonorApi__Suffix__c , Suffix,CFAMN__Gender__c , 
                 Gender__c,CFAMN__Employer__c , Employer_Name__c,CFAMN__CFAClassCodeList__c , 
                 CFA_Class_Code__c, CFAMN__CFACandidateCode__c , CFA_Candidate_Code__c,
                 CFAMN__CFAPREP__c , CFA_Program_Prep_Allow_Contact__c, CFAMN__CFAMemberCode__c , 
                 CFA_Member_Code__c, CFAMN__CharterAwardDate__c , CFA_Charter_Award_Date__c,
                 CFAMN__NMAIL__c , HasOptedOutOfMail__c,CFAMN__NEML__c , HasOptedOutOfEmail,
                 CFAMN__CLASOC__c , Investment_Foundations_Candidate_SOC__c,CFAMN__ClaritasCertificateAwardDate__c , 
                 Investment_Foundations_Certificate_Award__c, CFAMN__ClaritasCandidateCode__c , 
                 Investment_Foundations_Candidate_Code__c,CFAMN__CIPMMemberCode__c , CIPM_Member_Code__c,
                 CFAMN__CIPMCandidateCode__c , CIPM_Candidate_Code__c, CFAMN__CIPMCertificateDate__c, 
                 CIPM_Certificate_Award_Date__c, CFA__c , Is_CFA_Charter_Holder__c,CIPM__c , 
                 Is_CIPM_Certificate_Holder__c
                 FROM Contact];
        System.assertEquals(200, result.size());
        for(Contact contact: result) {
            System.assertNotEquals(null, contact.CFAMN__PersonID__c);
            System.assertNotEquals(null, contact.Person_Id__c);
            System.assertNotEquals(null, contact.DonorApi__Suffix__c);
            System.assertNotEquals(null, contact.Suffix);
            System.assertNotEquals(null, contact.CFAMN__Gender__c);
            System.assertNotEquals(null, contact.Gender__c);
            System.assertNotEquals(null, contact.CFAMN__Employer__c);
            System.assertNotEquals(null, contact.Employer_Name__c);
            System.assertNotEquals(null, contact.CFAMN__CFAClassCodeList__c);
            System.assertNotEquals(null, contact.CFA_Class_Code__c);
            System.assertNotEquals(null, contact.CFAMN__CFACandidateCode__c);
            System.assertNotEquals(null, contact.CFA_Candidate_Code__c);
            System.assertNotEquals(null, contact.CFAMN__CFAPREP__c);
            System.assertNotEquals(null, contact.CFA_Program_Prep_Allow_Contact__c);
            System.assertNotEquals(null, contact.CFAMN__CFAMemberCode__c);
            System.assertNotEquals(null, contact.CFA_Member_Code__c);
            System.assertNotEquals(null, contact.CFAMN__CharterAwardDate__c);
            System.assertNotEquals(null, contact.CFA_Charter_Award_Date__c.dateGMT());
            System.assertNotEquals(null, contact.CFAMN__NMAIL__c);
            System.assertNotEquals(null, contact.HasOptedOutOfMail__c);
            System.assertNotEquals(null, contact.CFAMN__NEML__c); 
            System.assertNotEquals(null, contact.HasOptedOutOfEmail);
            System.assertNotEquals(null, contact.CFAMN__CLASOC__c);
            System.assertNotEquals(null, contact.Investment_Foundations_Candidate_SOC__c);
            System.assertNotEquals(null, contact.CFAMN__ClaritasCertificateAwardDate__c);
            System.assertNotEquals(null, contact.Investment_Foundations_Certificate_Award__c.dateGMT());
            System.assertNotEquals(null, contact.CFAMN__ClaritasCandidateCode__c);
            System.assertNotEquals(null, contact.Investment_Foundations_Candidate_Code__c);
            System.assertNotEquals(null, contact.CFAMN__CIPMMemberCode__c);
            System.assertNotEquals(null, contact.CIPM_Member_Code__c);
            System.assertNotEquals(null, contact.CFAMN__CIPMCandidateCode__c);
            System.assertNotEquals(null, contact.CIPM_Candidate_Code__c);
            System.assertNotEquals(null, contact.CFAMN__CIPMCertificateDate__c);
            System.assertNotEquals(null, contact.CIPM_Certificate_Award_Date__c.dateGMT());
            System.assertNotEquals(null, contact.CFA__c);
            System.assertNotEquals(null, contact.Is_CFA_Charter_Holder__c);
            System.assertNotEquals(null, contact.CIPM__c);
            System.assertNotEquals(null, contact.Is_CIPM_Certificate_Holder__c);

            System.assertEquals(contact.CFAMN__PersonID__c, contact.Person_Id__c);
            System.assertEquals(contact.DonorApi__Suffix__c, contact.Suffix);
            System.assertEquals(contact.CFAMN__Gender__c, contact.Gender__c);
            System.assertEquals(contact.CFAMN__Employer__c, contact.Employer_Name__c);
            System.assertEquals(contact.CFAMN__CFAClassCodeList__c, contact.CFA_Class_Code__c);
            System.assertEquals(contact.CFAMN__CFACandidateCode__c, contact.CFA_Candidate_Code__c);
            System.assertEquals(contact.CFAMN__CFAPREP__c, contact.CFA_Program_Prep_Allow_Contact__c);
            System.assertEquals(contact.CFAMN__CFAMemberCode__c, contact.CFA_Member_Code__c);
            System.assertEquals(contact.CFAMN__CharterAwardDate__c, contact.CFA_Charter_Award_Date__c.dateGMT());
            System.assertEquals(contact.CFAMN__NMAIL__c, contact.HasOptedOutOfMail__c);
            System.assertEquals(contact.CFAMN__NEML__c, contact.HasOptedOutOfEmail);
            System.assertEquals(contact.CFAMN__CLASOC__c, contact.Investment_Foundations_Candidate_SOC__c);
            System.assertEquals(contact.CFAMN__ClaritasCertificateAwardDate__c, contact.Investment_Foundations_Certificate_Award__c.dateGMT());
            System.assertEquals(contact.CFAMN__ClaritasCandidateCode__c, contact.Investment_Foundations_Candidate_Code__c);
            System.assertEquals(contact.CFAMN__CIPMMemberCode__c, contact.CIPM_Member_Code__c);
            System.assertEquals(contact.CFAMN__CIPMCandidateCode__c, contact.CIPM_Candidate_Code__c);
            System.assertEquals(contact.CFAMN__CIPMCertificateDate__c, contact.CIPM_Certificate_Award_Date__c.dateGMT());
            System.assertEquals(contact.CFA__c, contact.Is_CFA_Charter_Holder__c);
            System.assertEquals(contact.CIPM__c, contact.Is_CIPM_Certificate_Holder__c);
        }
    }
    
    private static void updateOldContactFields(List<Contact> contacts) {
        Integer personId = 10000;
        for(Contact contact: contacts) {
            contact.CFAMN__PersonID__c = String.valueof(personId);
            personId++;
            contact.DonorApi__Suffix__c = 'test suffix';
            contact.CFAMN__Gender__c = 'Male';
            contact.CFAMN__Employer__c = 'Test Name';
            contact.CFAMN__CFAClassCodeList__c = '345';
            contact.CFAMN__CFACandidateCode__c = '1P';
            contact.CFAMN__CFAPREP__c = true;
            contact.CFAMN__CFAMemberCode__c = 'AOA';
            contact.CFAMN__CharterAwardDate__c = Date.today();
            contact.CFAMN__NMAIL__c = true;
            contact.CFAMN__NEML__c = true;
            contact.CFAMN__CLASOC__c = true;
            contact.CFAMN__ClaritasCertificateAwardDate__c = Date.today().addDays(1);
            contact.CFAMN__ClaritasCandidateCode__c = '1PX';
            contact.CFAMN__CIPMMemberCode__c = 'MB';
            contact.CFAMN__CIPMCandidateCode__c = '1C';
            contact.CFAMN__CIPMCertificateDate__c = Date.today().addDays(2);            
            contact.CFA__c = true;
            contact.CIPM__c = true;
        }
    }
}
/**************************************************************************************************************************************************
Name:  MDM_Affiliation_TriggerHandler_Test
Copyright © 2017  ITC
=====================================================================
Purpose: 1. Test class for MDM_Affiliation_TriggerHandler class.                                                                                                     
============================================================
History                                                            
-------                                                            
VERSION  AUTHOR         DATE           DETAIL          Description
1.0      Sidhant        11/9/2017      Created         Created the test class
2.0      Anupama        12/21/2017     Updated         Added the methods testDuplicateAffiliationRecords and testORRelatedList
         Anupama        02/02/2018     Modified        removed unwanted code        
****************************************************************************************************************************************************/
@isTest
private class MDM_Affiliation_TriggerHandler_Test {

     
    private static testMethod void testRecordTypeMatchType() {
        Id RecordTypeIdAcc= Schema.SObjectType.Account.getRecordTypeInfosByName().get('Government').getRecordTypeId();
        
        List<Account> testacc = new List<Account>();
        testacc.add(new Account(Name='Testacc1',RecordTypeId=RecordTypeIdAcc));
        insert testacc;
        
        Id RecordTypeIdCon= Schema.SobjectType.Contact.getRecordTypeInfosByName().get('CFA Contact').getRecordTypeId();
        
        Contact testcon= new Contact(LastName='Testcon1',RecordTypeId=RecordTypeIdCon);
        insert testcon;
        
        List<Affiliation__c> testaff = new List<Affiliation__c>();
        
        testaff.add(new Affiliation__c(Account__c=testacc[0].Id, Type__c='University',Contact__c=testcon.Id, Role__c='Alumnus'));
        
        Test.startTest();
            try{
                insert testaff;
            }
            catch(system.DmlException e){
                    System.assertEquals(e.getDmlMessage(0), 'You have selected an invalid type for Account. Please select Government');
            }
        Test.stopTest();
    }
    
/**************************************************************************************************************************************************
==================================================================================
Purpose: Test method to check that duplicate Affiliation records are not inserted.                                                                                                     
==================================================================================
History                                                            
-------                                                            
VERSION  AUTHOR         DATE           DETAIL          Description
1.0      Anupama        11/9/2017      Created         Created the test method

****************************************************************************************************************************************************/
    
    private static testMethod void testDuplicateAffiliationRecords(){
        
        Id RecordTypeIdCon1= Schema.SobjectType.Contact.getRecordTypeInfosByName().get('CFA Contact').getRecordTypeId();
        Id RecordTypeIdAcc1= Schema.SObjectType.Account.getRecordTypeInfosByName().get('Society').getRecordTypeId();
     
        Account testAccount = new Account(Name='Testacc21122017',RecordTypeId=RecordTypeIdAcc1);
        insert testAccount;
        
        Contact testContact = new Contact(LastName='Test21122017',RecordTypeId=RecordTypeIdCon1);
        insert testContact;
        
        List<Affiliation__c> testAffList = new List<Affiliation__c>();
        testAffList.add(new Affiliation__c(Account__c=testAccount.Id, Type__c='Society',Contact__c=testcontact.Id, Role__c='President'));
        insert testAfflist[0];
        testAffList.add(new Affiliation__c(Account__c=testAccount.Id, Type__c='Society',Contact__c=testcontact.Id, Role__c='President'));
        
        Test.startTest();
            try{
                insert testAffList[1];
            }
            catch(system.DmlException e1){
                    System.assertEquals(e1.getDmlMessage(0), 'Duplicate Role. Role President already exists for this Contact');
            }
        Test.stopTest();
        
    }
    
     private static testMethod void testAccountStatusMapped(){
         
         Id RecordTypeIdAcc1= Schema.SObjectType.Account.getRecordTypeInfosByName().get('Society').getRecordTypeId();
         Id RecordTypeIdCon1= Schema.SobjectType.Contact.getRecordTypeInfosByName().get('CFA Contact').getRecordTypeId();
         
         Account testParentAccount = new Account(Name='Testacc21122017', RecordTypeId=RecordTypeIdAcc1);
         insert testParentAccount;
         
         Account testChildAccount = new Account(Name='Testacc21122017 Child', RecordTypeId=RecordTypeIdAcc1, ParentId = testParentAccount.Id, MDM_Curation_Status__c = 'Mapped');
         insert testChildAccount;
         
         Contact testContact = new Contact(LastName='Test21122017',RecordTypeId=RecordTypeIdCon1, AccountId = testParentAccount.Id );
         insert testContact;
         
         List<Affiliation__c> testAffList = new List<Affiliation__c>();
         testAffList.add(new Affiliation__c(Account__c=testChildAccount.Id, Type__c='Society',Contact__c=testcontact.Id, Role__c='President',MDM_Mapped_Account__c=testChildAccount.ParentId));
         
         Test.startTest();
            insert testAffList;
         Test.stopTest();
         system.debug('testAffList'+testAffList);
//         Select MDM_Mapped_Account__c From Affiliation__c
          System.assertEquals(testChildAccount.ParentId, [Select MDM_Mapped_Account__c From Affiliation__c where Id = : testAffList[0].Id].MDM_Mapped_Account__c    );
     }
     
     private static testMethod void testUnknownEmployerAffiliation(){
          
          Id RecordTypeIdAcc1= Schema.SObjectType.Account.getRecordTypeInfosByName().get('Society').getRecordTypeId();
          Id RecordTypeIdCon1= Schema.SobjectType.Contact.getRecordTypeInfosByName().get('CFA Contact').getRecordTypeId();
          
          Account testParentAccount = new Account(Name='Testacc21122017', RecordTypeId=RecordTypeIdAcc1);
          insert testParentAccount;
         
          List<MDM_Default_Account__c> lstDflt = new List<MDM_Default_Account__c>();
          
          lstDflt.add( new MDM_Default_Account__c(Name = 'Unknown Employer', MDM_Account_ID__c = testParentAccount.Id));
          insert lstDflt;
          
          Contact testContact = new Contact(LastName='Test21122017',RecordTypeId=RecordTypeIdCon1);
          insert testContact;
          
          List<Affiliation__c> testAffList = new List<Affiliation__c>();
          testAffList.add(new Affiliation__c(Account__c=lstDflt[0].MDM_Account_ID__c, Type__c = 'Society', Contact__c=testContact.Id, Role__c='President'));
          
          Test.startTest();
          try{
            insert testAffList;
          }
          catch(system.DmlException ex){
              System.assertEquals(ex.getDmlMessage(0), 'You can not create Affiliation for Unknown Employer/Fonteva Default Account');
          }
          Test.stopTest();
      }
}
/**************************************************************************************************************************************************
Name:  VM_MyApplicationsController
Copyright © 2018  ITC
=====================================================================
Purpose: 1. Controller Class for VM_MyApplications.cmp                                                                                         
============================================================
History                                                            
-------                                                            
VERSION  AUTHOR         DATE           DETAIL          Description
1.0                  06/07/2018        Created         Created the class

****************************************************************************************************************************************************/
public class VM_MyApplicationsController {
	
    /****************************************************************************************************************************
       Purpose:This method will fetch the details from Job_Application__c based on My Applications filter.                         
       Parameters: noOfRecords
       Returns: No of records for the specific Job_Application__c
    
       History                                                            
       --------                                                           
       VERSION  AUTHOR         DATE           DETAIL          Description
       1.0      Nikhil        06/07/2018             
    ******************************************************************************************************************************/ 
    @AuraEnabled
    public static List<Job_Application__c> fetchJobApplications(Integer noOfRecords) {
        
        List<Job_Application__c> lstJobApplications =  [SELECT Id, Name, Current_user__c, 
                                                        Role_Name__c, VM_Volunteer_Status__c, 
                                                        Show_Application__c 
                                                        FROM Job_Application__c 
                                                        WHERE Current_user__c =: 1 
                                                        LIMIT : Integer.valueOf(noOfRecords)];
        return lstJobApplications;
    }
    
}
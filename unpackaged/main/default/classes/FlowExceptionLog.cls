/*
*This class is a Framework for Flow Faults
*@description To create Logs in CFA Integration Log for Flow exceptions
*/

public class FlowExceptionLog {
    
    public class FlowErrorLog{
        @InvocableVariable(label='Flow Name')
        public String flowName;
        
        @InvocableVariable(label='Error Message')
        public String errorMsg;
        
        @InvocableVariable(label='Error Date')
        public DateTime errorDateTime;
        
        @InvocableVariable(label='Current Stage')
        public String currentStage;
    }
    
    @Invocablemethod(label='flowFaultLog' description = 'flowFaultLog')
    public static void flowFaultLog(List<FlowErrorLog> flowErrorLog){
        System.debug(flowErrorLog);
        insert CFA_IntegrationLogException.logError(new FlowException(flowErrorLog[0].errorMsg), 
                                             flowErrorLog[0].flowName, flowErrorLog[0].currentStage, flowErrorLog[0].errorDateTime,
                                             false, '', null, null, true, null);
    }
}
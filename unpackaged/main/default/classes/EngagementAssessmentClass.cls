public without sharing class EngagementAssessmentClass {
 
    public Assessment__c assess{get;set;}
    public Id EngId;
    List<RecordType> rtypes;
    Map<String,String> assessmentRecordTypes;
   
    public EngagementAssessmentClass(ApexPages.StandardController controller) {
        
        EngId=ApexPages.currentPage().getParameters().get('eng');
        Engagement__c eg=new Engagement__c();
        eg=[select Id,name,Business_Objective__c from Engagement__c where Id=:EngId]; 
        assess=(Assessment__c)controller.getRecord();
        assess.Engagement__c=eg.ID;
        assess.Business_Objective__c=eg.Business_Objective__c;
        rtypes=new List<RecordType>();
        assessmentRecordTypes = new Map<String,String>{};
        rtypes=[Select Name, Id From RecordType where sObjectType='Assessment__c' and isActive=true];
        for (RecordType rt : rtypes) {
            assessmentRecordTypes.put(rt.Name,rt.Id);
        }
        assess.recordtypeid=assessmentRecordTypes.get('Engagement Assessment');
        //assess.recordtypeid='012c0000000DJcF';
    }
    public pagereference Save()
    {
        try
        {
        insert assess;
        pagereference pr = new pagereference('/' + assess.id);       
        return pr;
        }
        catch(DMLException e)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,' '+e.getDMLMessage(0)));        
            return null;
            
        }
    }
    
       public PageReference Cancel()
    {
         pagereference pr = new pagereference('/' + EngId);
       
        return pr;
        //return null;    
    }
    

}
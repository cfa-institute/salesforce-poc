/**
 * Encapsulates all behaviour logic relating to Apex
 *
 * For more guidelines and details see
 *   https://developer.salesforce.com/page/Apex_Enterprise_Patterns_-_Domain_Layer
 * @group Common Layer
 **/
public with sharing class cfa_ApexUtilities {
    ////////////////////////////////////////////////////////////////////////////////
    /// Data Members
    ////////////////////////////////////////////////////////////////////////////////
    public static final String DEBUG_ENVIRONMENT = 'debug';
    public static final String PROD_ENVIRONMENT = 'production';
    public static final String TEST_ENVIRONMENT = 'test';

    @TestVisible
    private static List<String> logTestData = null;
    @TestVisible
    private static final Boolean isSandboxBool = [SELECT IsSandbox FROM Organization LIMIT 1].IsSandbox;
    @TestVisible
    private static final Boolean isDebugMode = CFA_Configuration.isDebug();
    @TestVisible
    private static Pattern emailPattern = Pattern.compile(cfa_ApexConstants.EMAIL_REGEX);
    @TestVisible
  private static final Map<String,  Map<String, RecordType>> sobjectRecordTypes = new Map<String,  Map<String, RecordType>>();
    //////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    // Public Methods
    ///////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
    *
    * @description is this a sanbox
    * @return true, if sandbox
    */
    public static Boolean isSandbox() {
        return cfa_ApexUtilities.isSandboxBool;
    }


    /**
     * @description random string
     *
     */
    public static String generateRandomString(Integer len) {
        final String chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
        String randStr = '';
        while (randStr.length() < len) {
            Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), chars.length());
            randStr += chars.substring(idx, idx + 1);
        }
        return randStr;
    } // end of generateRandomString

    /**
     * @description get ids from sobject
     *
     * @param collection collection of accounts
     * @return collection of ids
     */

    public static Set<Id> getIds(List<Sobject> collection) {
        Set<Id> ids = new Set<Id>();

        if ( collection == null || collection.size() == 0 ) {
            return ids;
        }
        for ( Integer inx = 0; inx < collection.size(); inx++) {
            ids.add(collection[inx].Id);
        }
        return ids;
    } // end of getIds
    /**
     * @description get the environment name

     * @return string, environment name (Prod,Test,Debug)
     */
    public static String getEnvironment() {
        string environment = PROD_ENVIRONMENT;

        if ( Test.isRunningTest()  ) {
            environment = TEST_ENVIRONMENT;
        } else if (cfa_ApexUtilities.isSandbox() ) {
            environment = DEBUG_ENVIRONMENT;
        }

        return environment;

    } // end of getEnvironment

    /**
    *
    * @description Log Information, makes it easier to refactor for a logger later
    * @param String message
    */
    public static void log(String theMessage) {
        if ( Test.isRunningTest() ) {
            cfa_ApexUtilities.log(theMessage, true);
        } else {
            cfa_ApexUtilities.log(theMessage, cfa_ApexUtilities.isDebugMode);
        }
    } // end of log
     /**
   * @description get record type names for domain name -- REFACTOR remove SOQL into Selector
   *
   * @param sobjectDomainName domain name of sobject, i.e Lead,Account, Contact, etc.
   * @return Map<String, RecordType>  - name/value pair of record-types
   */
  public static Map<String, RecordType> getRecordTypeNames(String sobjectDomainName) {

    Map<String, RecordType> mapping = null;
    if ( !string.isBlank(sobjectDomainName) ) {
      // find domain records type
      mapping = cfa_ApexUtilities.sobjectRecordTypes.get(sobjectDomainName);
      if ( mapping == null ) {
        mapping = new Map<String, RecordType>();
        for (RecordType rt : [Select Id, Name, DeveloperName from RecordType where SobjectType = :sobjectDomainName])  {
          // place rcord-type from dev-name
          mapping.put(rt.DeveloperName, rt);
        }
        cfa_ApexUtilities.sobjectRecordTypes.put(sobjectDomainName, mapping);
      }
    }
    return mapping;
  }// end of getRecordTypeNames
    
    /**
    *
    * @description Log Information, makes it easier to refactor for a logger later
    * @param String message
    * @param Boolean controlOutput control whether to output or not
    */
    public static void log(String theMessage, Boolean controlOutput) {
        if ( theMessage != null && controlOutput ) {
            String message = String.isBlank(theMessage) ? 'null' : theMessage;
            // log it
            String  msgToLog = '++[' + cfa_ApexUtilities.getEnvironment() + '] ++++++++CFA DEBUG ++ ' + message;
            cfa_ApexUtilities.writeDataForTesting(message);

            system.debug(msgToLog);

        }
    } // end of log

    /**
     *  @description convience routine to log exceptions via ApplicationLog
     *
     * @param excp Exception
     */
    public static void log(Exception excp) {
        if (excp != null) {
            // used to test information sent from a test
            cfa_ApexUtilities.log(' Exception : ' + excp.getMessage() + '\nStackTrace: ' + excp.getStackTraceString());
        }
    } // end of log

    /**
    * @brief get the Type from the SObject
    * @description  get the Type from the SObject
    *
    * @param theSObject type we want
    * @return System Type
    */
    public static Type getTypeFromSObject(SObject theSObject) {
        Type result = null;

        try {
            if ( theSObject != null ) {
                Schema.SObjectType sType = theSObject.getSobjectType();
                result = Type.forName(sType.getdescribe().getName());
            }

        } catch  (Exception excp) {

        }
        return result;
    } // end of getTypeFromSObject

    /**
     * @brief get the Type from the SObject collection
     * @description  get the Type from the SObject collection
     *
     * @param theSObject type we want
     * @return System Type
     */
    public static Type getTypeFromSObject(List<SObject> theSObject) {
        Type result = null;

        try {
            if ( theSObject != null ) {
                Schema.SObjectType sType = theSObject.getSobjectType();
                result = Type.forName(sType.getdescribe().getName());
            }
        } catch  (Exception excp) {

        }
        return result;
    } // end of getTypeFromSObject

    /**
     * @brief get the Type from the SObject collection
     * @description  get the Type from the SObject collection
     *
     * @param theSObject type we want
     * @return System Type
     */
    public static Schema.SObjectType getSobjectTypeFromSObject(List<SObject> theSObject) {
        Schema.SObjectType sType = null;

        try {
            if ( theSObject != null ) {
                sType = theSObject.getSobjectType();
            }
        } catch  (Exception excp) {

        }
        return sType;
    } // end of getSobjectTypeFromSObject

    /**
     * @brief get the Type from the SObject collection
     * @description  get the Type from the SObject collection
     *
     * @param theSObject type we want
     * @return System Type
     */
    public static Schema.SObjectType getSobjectTypeFromSObject(SObject theSObject) {
        Schema.SObjectType sType = null;

        try {
            if ( theSObject != null ) {
                sType = theSObject.getSobjectType();
            }
        } catch  (Exception excp) {

        }
        return sType;
    } // end of getSobjectTypeFromSObject
    /**
     * @description get a specific item/indexed from a set of strings
     *
     * @param values set of strings
     * @param index index to get, start at 0 for the first element
     *
     * @return string at the 'index'
     */
    public static String getSetElement(Set<String> values, integer index) {
        string element = null;

        if ( values != null && values.size() > 0) {
            integer inx = 0;
            for (String item : values) {
                if ( inx == index ) {
                    element = item;
                    break;
                }
                inx++;
            }
        }
        return element;
    }// end of getSetElement

    /*
     * Get executing class and method name borrowed from the wbe
     * https://salesforce.stackexchange.com/questions/153835/get-currently-executing-class-method-name
     */
    /**
     * @brief get the excuting class name
     * @description get executing class name
     * @return executing class name
     */
    public static String getExecutingClassName() {
        return getExecutingClassName(new DmlException().getStackTraceString().substringAfter('\n'));
    }
    /**
     * @brief get the eecuting method name
     * @description get the eecuting method name
     * @return method name
     */
    public static String getExecutingMethod() {
        return getExecutingMethod(new DmlException().getStackTraceString().substringAfter('\n'));
    }
    /**
     * @brief get the excuting class name from an stack trace
     * @description get the excuting class name from an stack trace
     *
     * @param line line from stack trace
     * @return class name
     */
    public static String getExecutingClassName(String line) {
        String result = '';
        if ( line != null ) {
            if (line.startsWith('Class.'))
                line = line.substringAfter('Class.');
            result = line.substringBefore(':').substringBeforeLast('.');
        }
        return result;
    } // end of getExecutingClassName

    /**
     * @brief get the excuting method name from an stack trace
     * @description get the excuting method name from an stack trace
     *
     * @param line from the stack trace
     * @return method name
     */
    public static String getExecutingMethod(String line) {
        String result = '';
        if ( line != null ) {
            result = line.substringBefore(':').substringAfterLast('.');
        }
        return result;
    } // end of getExecutingMethod

    /**
     * @description valid email address
     *
     * @param semail email address to check
     * @return true if valid
     */
    public static Boolean isValidEmail (String semail) {
        //String InputString = semail;

        // Then instantiate a new Matcher object "emailMatcher"
        //Matcher emailMatcher = emailPattern.matcher(InputString);
        //return !emailMatcher.matches();
        return !string.isBlank(semail) && emailPattern.matcher(semail).matches();
    } // end of isValidEmail
    /**
     * @description Is this incoming number a valid telephone number
     *
     * @param phoneNumber what is considered a telephone
     * @return true if valid us or international #
     */
    public static Boolean isValidTelephoneUSorInternational(string phoneNumber) {
        return cfa_Apexutilities.isStringOfNumbersValidTelephoneWithRegex(phoneNumber, cfa_ApexConstants.ALL_TELEPHONE_REGEX);
    } // end of isValidTelephoneUSorInternational
    /**
    * @description  convert string of numbers to us or international telephone #
    *
    * @param stringOfNums string of numbers '1111111111'
    * @param userRegex regular expression
    * @return Boolean true, if valid
    */
    public static Boolean isStringOfNumbersValidTelephoneWithRegex(String stringOfNums, String userRegex) {

        Boolean results = false;
        if ( !string.IsBlank(stringOfNums)
                && !string.isBlank(userRegex)) {
            Pattern phoneFormatPattern = Pattern.compile(userRegex);
            Matcher myFormatMatcher = phoneFormatPattern.matcher(stringOfNums);
            results = myFormatMatcher.matches();

        }
        return results;

    } // end of isStringOfNumbersValidTelephoneWithRegex
    ///////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    // Private Methods
    ///////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     *  @description check for a string in the test log
     *
     * @param item string to look for
     * @return true, if found
     */
    @TestVisible
    private static Boolean containsInTestLog(String lookFor) {
        Boolean found = false;
        if ( Test.isRunningTest() && cfa_ApexUtilities.logTestData != null ) {
            for (String item : cfa_ApexUtilities.logTestData) {
                if ( (found = item.contains(lookFor)) == true) {
                    break;
                }
            }
        }
        return found;
    } // end of containsInTestLog
    /**
    *  @description write data to the test list
    *
    * @param string the message to write to the list
    */
    @TestVisible
    private static void writeDataForTesting(String message) {
        // used to test information sent from a test
        if ( Test.isRunningTest() && message != null) {
            // need to create out internal storage unit
            if ( cfa_ApexUtilities.logTestData == null ) {
                cfa_ApexUtilities.logTestData = new List<String>();
            }
            cfa_ApexUtilities.logTestData.add(message);
        }
    } // end of writeDataForTesting

} // end of cfa_ApexUtilities
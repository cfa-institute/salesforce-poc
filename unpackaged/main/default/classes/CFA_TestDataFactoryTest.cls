@isTest
private class CFA_TestDataFactoryTest {
    
    @TestSetup
    private static void setup() {
        Account accCfaInstitute = CFA_TestDataFactory.createAccount(Label.CFA_Institute, 'Society', true);
        List<Contact> contacts = CFA_TestDataFactory.createContactRecords('CFA Contact', 'ContactName', 5, false);
        for(Contact con : contacts){
            con.AccountId = accCfaInstitute.Id;
        }
        insert contacts;
    }
    
    @isTest
    private static void testCreateTaskList() {
        Account acc = [SELECT ID FROM Account LIMIT 1];
        Test.startTest();
        List<Task> result = CFA_TestDataFactory.createTaskList(null, acc.Id, UserInfo.getUserId(), 5, true);
        Test.stopTest();
        System.assertEquals(5, result.size());
    }

}
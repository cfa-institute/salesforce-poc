/**************************************************************************************************
* Apex Class Name   : CustomerCareUtility_Test
* Purpose           : This class is used for declaring all the Contants Value used in CFA CC Project   
* Version           : 1.0 Initial Version
* Organization      : Cognizant
* Created Date      : 13-Nov-2017  
***************************************************************************************************/
@isTest(seeAllData=false)
Public class CustomerCareUtility_Test{
 static testMethod void testCommonUtility() {
     CustomerCareUtility.closedStatus ='Closed';
     CustomerCareUtility.ownerListValue='All Open Case';
     CustomerCareUtility.queueType='Queue';
     CustomerCareUtility.supervisorProfile='CustomerCare Supervisor';
     CustomerCareUtility.emailToOpenCaseTempName='customerCare_Open Email Template';
     CustomerCareUtility.frontOfficeRecordType='CustomerCare Front Office Record Type';
     CustomerCareUtility.backOfficeRecordType='CustomerCare Back Office Record Type';
     CustomerCareUtility.reOpenRecordType='CustomerCare Case Reopen Record Type';
     CustomerCareUtility.emailOrigin= 'Email';
     CustomerCareUtility.webOrigin = 'Web';
     CustomerCareUtility.EmailtempSurveyLinkforMember = 'CustomerCare_SurveyLink_for_Member';
     CustomerCareUtility.EmailtempSurveyLinkforCandidate = 'CustomerCare_SurveyLink_for_Candidate';
     CustomerCareUtility.EmailtempSurveyLinkforCIPMCandidate = 'CustomerCare_SurveyLink_for_CIPM_Candidate';
     CustomerCareUtility.EmailtempSurveyLinkforInvFoundCandidate = 'CustomerCare_SurveyLink_for_Investment_Foundation_Candidate';
     CustomerCareUtility.escalatedStatus ='Escalated';
     CustomerCareUtility.completedStatus='Completed';
     CustomerCareUtility.normalPriority='Normal';
    }
}
/**************************************************************************************************
* Apex Class Name   : CustomerCareContactNetsuiteTest
* Purpose           : This class allows to open Netsuite app from contact page  
* Version           : 1.0 Initial Version
* Organization      : Cognizant
* Created Date      : 20-Mar-2018  
***************************************************************************************************/
@isTest(seeAllData=false) 
public class CustomerCareContactNetsuiteTest {
    static testMethod void testCustomerCareContactNetsuite() {
       // Contact contacts = CustomerCare_CommonTestData.testDataContact();
        Id RecordTypeIdAcc= Schema.SObjectType.Account.getRecordTypeInfosByName().get('Society').getRecordTypeId();
        List<Account> testacc = new List<Account>();
        testacc.add(new Account(Name='Testacc1',RecordTypeId=RecordTypeIdAcc));
        insert testacc;
        Id ContactCCRecordType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Local Member').getRecordTypeId();
        String AcctId = testacc[0].id;
        Contact contacts = new Contact(FirstName='test',AccountID=AcctId  ,CFAMN__CFAMemberCode__c='AOA',Last_Survey_Date__c= System.TODAY()-32,Email='test@test.com', LastName='Contact',RecordTypeId=ContactCCRecordType,GDPR_Consent__c=false);
        insert contacts;
       
        List<ID> conList=new List<ID>();
        conList.add(contacts.Id);
        system.assert(conList != Null);
       
        CustomerCareContactNetsuite.getpersonId(conList);
       
      }
    
    static testMethod void testCustomerCareContactNetsuite2() {
        /*Contact contacts = CustomerCare_CommonTestData.testRawCFAContact();
        List<ID> conList=new List<ID>();
        conList.add(contacts.Id);
        system.assert(conList != Null);
        CustomerCareContactNetsuite.getpersonId(conList);
		*/
        Id RecordTypeIdAcc= Schema.SObjectType.Account.getRecordTypeInfosByName().get('Society').getRecordTypeId();
        List<Account> testacc = new List<Account>();
        testacc.add(new Account(Name='Testacc1',RecordTypeId=RecordTypeIdAcc));
        insert testacc;
        Id ContactCCRecordType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Local Member').getRecordTypeId();
        String AcctId = testacc[0].id;
        Contact contacts = new Contact(FirstName='test',AccountID=AcctId  ,CFAMN__CFAMemberCode__c='AOA',Last_Survey_Date__c= System.TODAY()-32,Email='test@test.com', LastName='Contact',RecordTypeId=ContactCCRecordType,GDPR_Consent__c=false);
        insert contacts;
        List<ID> conList=new List<ID>();
        conList.add(contacts.Id);
        system.assert(conList != Null);
        CustomerCareContactNetsuite.getpersonId(conList);
      }
}
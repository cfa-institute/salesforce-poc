@isTest
public class CustomerCare_AttachmentUpload_Test {
    static testmethod void cc_attachmentUpload(){
        boolean isSB =[SELECT IsSandbox FROM Organization LIMIT 1].IsSandbox;
          SocietyOps_Backoffice__c obj = new SocietyOps_Backoffice__c(name='trigger-details',CaseReassignment_TemplateName__c='SocietyOps-Case Reassignment',CaseComment_TemplateName__c='SocietyOps-New Comment Notification');
        if(isSB==true)
        {
            obj.No_reply_email__c='researchchallenge@cfainstitute.org';
         }
        else
        {
            obj.No_reply_email__c='noreply.societyoperations@cfainstitute.org';
         }
         
         insert obj;
        Case newCase = CustomerCare_CommonTestData.TestDataEmailCase();
        Attachment attach=new Attachment();     
        attach.Name='Unit Test Attachment';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        attach.body=bodyBlob;
        attach.parentId=newCase.id;
        insert attach;
        ApexPages.StandardController sc = new ApexPages.standardController(newCase);
        CustomerCare_AttachmentUpload attachment = new CustomerCare_AttachmentUpload(sc);
        attachment.upload();
        attachment.viewAttach();
        
        
        Attachment testAttach=[select id,name,body,ParentID from attachment where Id=:attach.Id];        
        System.assert(testAttach.ParentId==newcase.Id);
        System.assert(testAttach.Body==bodyBlob);
        system.assert(testAttach.Name==attach.Name);
        
        
        
        
        
    }
}
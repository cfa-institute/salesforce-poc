/**************************************************************************************************
* Apex Class Name   : CC_Callout_MockHttpResponseGenerator
* Purpose           : This class is used for creating mock response.
* Version           : 1.0 Initial Version
* Organization      : Cognizant
* Created Date      : 26-March-2018    
***************************************************************************************************/

@isTest 
global class CC_Callout_MockHttpResponseGenerator implements HttpCalloutMock{
    @testvisible
    protected Integer code;

    public CC_Callout_MockHttpResponseGenerator(integer code){
        this.code=code;
    }
    
    global HTTPResponse respond(HTTPRequest req){
          
        //System.assertEquals('https://salesforcepublicapi.dev.cfainstitute.org/accounts/', req.getEndPoint());
        //System.assertEquals('PUT', req.getMethod());
  
        HTTPResponse res = new HTTPResponse();
        res.setHeader('Content-Type', 'application/json');
        
        if(req.getEndPoint().contains('registrations')){
        res.setBody('[{"Program":"TestProgram","EnrollmentStatus":"TestProgram","EnrollmentDate":'+JSON.serialize(datetime.now())+',"RegistrationStatus":"TestProgram","Level":"TestProgram","Cycle":"TestProgram","Year":"'+5+'","RegistrationDate":'+JSON.serialize(datetime.now())+',"CompletedDate":'+JSON.serialize(datetime.now())+',"CancellationDate":'+JSON.serialize(datetime.now())+',"ExamAttendanceStatus":"TestProgram","ExamResultsStatus":"TestProgram","ExamResultsVisibilityStatus":"TestProgram"}]');
        }
        else if(req.getEndPoint().contains('scholarships')){
        res.setBody('[{"ScholarshipStatus":"ScholarshipStatusTest","ScholarshipType":"ScholarshipTypeTest","Program":"TestProgram","Level":"TestProgram","Cycle":"TestProgram","Year":"'+5+'","AwardStatus":"AwardStatusTest","Society":"SocietyTest","ScholarshipApplicationDate":'+JSON.serialize(datetime.now())+',"ScholarshipApplicationDeadline":'+JSON.serialize(datetime.now())+',"DecisionDate":'+JSON.serialize(datetime.now())+',"RedemptionDate":'+JSON.serialize(datetime.now())+'}]');
        }
        else if(req.getEndPoint().contains('customers')){
        res.setBody('[{"PersonId":"PersonId","GivenName":"GivenName","Surname":"Surname","DateOfBirthFormatted":"DateOfBirthFormatted","PassportNumber":"PassportNumber","PassportExpirationDateFormatted":"TestProgram","ValidFromFormatted":"ValidFromFormatted","ModifiedBy":"AwardStatusTest","DateOfBirth":'+JSON.serialize(datetime.now())+',"PassportExpirationDate":'+JSON.serialize(datetime.now())+',"ValidFrom":'+JSON.serialize(datetime.now())+'}]');
        }
         else if(req.getEndPoint().contains('References')){
        res.setBody('[{"Name":"Test","Email":"test@cfa.com","HasCompletedForm":true,"IsActiveRegularMember":true}]');
        }
        else
        res.setBody('[{"CFA_Person_ID":"CFAPersonID","CustomerId":"CustomerId","MemberYear":"MemberYear","IsAffiliate":"IsAffiliate","Memberships":"Memberships","OnProfessionalLeave":"OnProfessionalLeave","Name":"Name",Memberships":"Memberships",MemberYear":"MemberYear","IsAffiliate":"IsAffiliate",CustomerId":"CFAPersonID",ValidFrom":'+JSON.serialize(datetime.now())+',Login_Status":"LoginStatusTest","Login_Date_Time":'+JSON.serialize(datetime.now())+'}]');
        
        if(code == 503)
         res.setBody(null);
        res.setStatusCode(code);
               
        return res;
    }
}
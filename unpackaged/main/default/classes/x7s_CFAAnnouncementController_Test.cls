/*
 * 	Created by 7Summits
 * 	Test class for 
 *  x7s_CFAAnnouncementController 
 *  x7s_CFAAnnouncementWrapper
 */
@IsTest
public class x7s_CFAAnnouncementController_Test {
	
    /*
     * Description: Tests x7s_CFAAnnouncementController.getAnnouncements()
     * Asserts: queried metadata records size is equal to number of announcements returned by controller method.
     */
    @IsTest static void testGetAnnouncements(){
        // query for metadata records already created in the org, (custom metadata is not affected by the seeAllData criteria)
        List<x7s_CFA_Announcement__mdt> existingAnnouncements = [SELECT DeveloperName, Label, Alert_Type__c, Announcement_Description__c FROM x7s_CFA_Announcement__mdt WHERE Active__c = true ORDER BY Alert_Type__c DESC];
        if(existingAnnouncements.size() > 0) {
			x7s_CFAAnnouncementController.testMetadataSamples = existingAnnouncements;
        } else {
			x7s_CFAAnnouncementController.testMetadataSamples = createSampleAnnouncements();
        }
        
        // call controller method which should create a new instance of the wrapper class for each metadata record found.
        List<x7s_CFAAnnouncementWrapper> wrappersCreated = x7s_CFAAnnouncementController.getAnnouncements();
        // assert that the two lists are equal in size
        System.assertEquals(x7s_CFAAnnouncementController.testMetadataSamples.size(), wrappersCreated.size(), 'An Announcement wrapper should have been created for each metadata record found.');
    }
    
    private static List<x7s_CFA_Announcement__mdt> createSampleAnnouncements(){
        List<x7s_CFA_Announcement__mdt> testAnnouncements = new List<x7s_CFA_Announcement__mdt>();
        testAnnouncements.add(new x7s_CFA_Announcement__mdt(DeveloperName='Test Warning Announcement', Label='est Warning Announcement', Announcement_Description__c='This announcement is a warning', Alert_Type__c='Warning'));
        testAnnouncements.add(new x7s_CFA_Announcement__mdt(DeveloperName='Test Error Announcement', Label='Test Error Announcement', Announcement_Description__c='This announcement details an error', Alert_Type__c='Error'));
        testAnnouncements.add(new x7s_CFA_Announcement__mdt(DeveloperName='Test Default Announcement', Label='Test Default Announcement', Announcement_Description__c='This announcement is just general information', Alert_Type__c='Base (Default)'));
		return testAnnouncements;
    }
}
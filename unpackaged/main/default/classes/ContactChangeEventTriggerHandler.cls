/*****************************************************************
Name: ContactChangeEventTriggerHandler
Copyright © 2020 ITC
============================================================
Purpose: This trigger handler is responsible for handling Contact Change Event
============================================================
History
-------
VERSION  AUTHOR          DATE         DETAIL    Description
1.0      Vadym Merkotan               Create    Apply Contact Society Campaigns Changes
1.1      Alona Zubenko   16.12.2020   Update    Correction based on comments in Pull request
2.0      Alona Zubenko   16.12.2020   Update    Update Badge Visibility Dates
2.0      Alona Zubenko                Update    Remove Badge Visibility Dates
2.0      Alona Zubenko   15.02.2021   Update    Add creation of Relationship With Employer
*****************************************************************/

public with sharing class ContactChangeEventTriggerHandler extends BaseChangeEventTriggerHandler {
    public static Boolean triggerDisabled = false;
    private static final String TRIGGER_NAME = 'ContactChangeEventTrigger';

    /**
     * @description Check is trigger disabled
     *
     * @return Boolean
     */
    public override Boolean isDisabled() {
        if(Test.isRunningTest()) {
            return triggerDisabled;
        } else {
            return triggerDisabled || super.isDisabled();
        }
    }

    /**
    * @description Get Trigger Name
    *
    * @return String
    */
    public override String getTriggerName() {
        return TRIGGER_NAME;
    }

    /**
     * @description Handle Create Change Events
     *
     * @param changeEvents List<SObject>
     */
    public override void handleAllCreate(List<SObject> changeEvents) {
        Set<String> changedContacts = getRecordIds(changeEvents);

        ContactChangeEventTriggerHelper.applyContactSocietyAllCampaignsChanges(changedContacts);
        ContactChangeEventTriggerHelper.createRelationshipWithEmployer(changedContacts);
    }

    /**
     * @description Handle Update Change Events
     *
     * @param changeEvents List<SObject>
     */
    public override void handleAllUpdate(List<SObject> changeEvents) {
        Set<String> changedContacts = getRecordIds(changeEvents);

        ContactChangeEventTriggerHelper.applyContactSocietyAllCampaignsChanges(changedContacts);
        ContactChangeEventTriggerHelper.createRelationshipWithEmployer(getRecordIds(changeEvents, new Set<String>{'Employer_Account__c'}));
    }
}
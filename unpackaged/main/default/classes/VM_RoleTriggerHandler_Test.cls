@isTest
public class VM_RoleTriggerHandler_Test{
    public static Engagement__c objEng;
    public static Account portalAccount;
    public static Contact portalContact;
    public static final String PARTNER_RECORDTYPE = 'CFA Contact';
    //@testSetup
    public static void setupTestData(){
        ContactTriggerHandler.triggerDisabled = true;

        //VM_TestDataFactory objTestData= new VM_TestDataFactory();
        portalAccount = VM_TestDataFactory.createAccountRecord();
        //portalAccount.IsPartner = true;
        insert portalAccount;
        
        portalContact = VM_TestDataFactory.createContactRecord(portalAccount);        
        Schema.DescribeSObjectResult contactSchemaResult = Schema.SObjectType.Contact;
        Map<String,Schema.RecordTypeInfo> mapContactTypeInfo = contactSchemaResult.getRecordTypeInfosByName();
        Id partnerRecordTypeId = mapContactTypeInfo.get(PARTNER_RECORDTYPE).getRecordTypeId();        
        portalContact.RecordTypeId = partnerRecordTypeId;
        portalContact.MDM_Preferred_Email_for_Volunteering__c = 'abc@abc.com';
        update portalContact;
        User RMOwner=VM_TestDataFactory.createUser('RMTest@gmail1.com','RMTest','Relationship Manager Staff','');
        User MDUSer=VM_TestDataFactory.createUser('MDTest@gmail1.com','MDTest','Outreach Management User','Managing Director');
        User ReportsToUSer=VM_TestDataFactory.createUser('Anne@gmail1.com','Huff','Outreach Management User','Relationship Manager');
        objEng=VM_TestDataFactory.createEngagement('t FY20',RMOwner,MDUSer,ReportsToUSer);
        Insert objEng;
        
          
    }
    public static testMethod void test1(){
        setupTestData();
        test.startTest();
        Role__c objRole= VM_TestDataFactory.createRole(objEng);
        Insert objRole;
        //objRole.Number_of_Positions__c = 100;
        //update objRole;
        objRole.Number_of_Positions__c = 200;
        update objRole;
        List<Role__c> roleLst= [Select Id from Role__c where Engagement_name__c =:objEng.Id];
        VM_RoleTriggerHandler obj01 = new VM_RoleTriggerHandler();
        obj01.OnAfterUpdate(roleLst);
        Role__c objRole1= objRole.clone(false, false, false, false);
		objRole1.Name='testClone';
        insert objRole1;
        try{
        objRole.Number_of_Positions__c = 100;
            update objRole;
        }     
        catch(Exception ex){
            Boolean expectedExceptionThrown =  ex.getMessage().contains('Please set incrememtal values for Number of Positions') ? true : false;
		System.assertEquals(expectedExceptionThrown, true);
        }
        test.stopTest();
    }
    public static testMethod void test2(){
        setupTestData();
        test.startTest();
        Role__c objRole= VM_TestDataFactory.createRole(objEng);
        Insert objRole;
        Delete objRole;
        //Id engLst= [Select Id from Engagement__c where name='t FY20'].Id;
        //List<Role__c> roleLst= [Select Id from Role__c where Engagement_name__c =:objEng.Id];
        //Map<Id,Role__c> roleMap=new Map<Id,Role__c>([select id,name from Role__c where id IN:roleLst]); 
        //VM_RoleTriggerHandler obj01 = new VM_RoleTriggerHandler();
        //obj01.OnBeforeDelete(roleLst,roleMap);
        try{
        Role__c objRole1= VM_TestDataFactory.createRole(objEng);
        Insert objRole1;
        Job_Application__c job=VM_TestDataFactory.createJobApplication(objRole1,portalContact);
        insert job;
            Delete objRole1;
        }     
        catch(Exception ex){
            Boolean expectedExceptionThrown =  ex.getMessage().contains('Delete Operation can not be performed as an applied Application exists for this Role.') ? true : false;
		System.assertEquals(expectedExceptionThrown, true);
        }
        
        
        
        
        test.stopTest();
    }
}
/**************************************************************************************************************************
*  VERSION      AUTHOR        DATE      Description
*  Version 2   Bharani T    11/26/2019  Test class for MembershipUpdate class modified to remove duplicated data and categorize the test methods 
***************************************************************************************************************************/
@isTest
public class MembershipUpdateTest {
    private static String PROFILE_INTEGRATION_USER = 'Integration profile';
    private static String PERMISSION_SET_EDIT_ACCOUNT_NAME = 'Edit_Account_Name';
    
    @testSetup static void setup() {
        System.runAs(CFA_TestDataFactory.getUserWithPermission(PROFILE_INTEGRATION_USER,PERMISSION_SET_EDIT_ACCOUNT_NAME)) {
            CFA_TestDataFactory.createAccount('CFA Institute', 'Society', true);
            // Create common test accounts
            Account acct =  CFA_TestDataFactory.createAccount(CFA_TestDataFactory.DefaultAccountName, null, false);
            insert acct;
            //create test contact
            Contact cnt = CFA_TestDataFactory.createContact(CFA_TestDataFactory.DefaultContactName, acct.Id, 'CFA Contact', false);
            cnt.Partner_Id__c = cfa_ApexUtilities.generateRandomString(5);
            insert cnt;

            //Create a membership
            Membership__c membrship = new Membership__c();
            membrship.Account_lookup__c = acct.Id;
            membrship.Contact_lookup__c = cnt.Id;
            membrship.Transaction_Type__c = 'Join';
            membrship.Membership_Type__c = 'Join';
            Insert membrship;

        }
    }

    //method to test Join transaction type for the membership
    @isTest
    public static void testJoinMembership(){
        System.runAs(CFA_TestDataFactory.getUserWithPermission(PROFILE_INTEGRATION_USER,PERMISSION_SET_EDIT_ACCOUNT_NAME)) {
            Account acct = [SELECT Id,Partner_Id__c FROM Account WHERE Name = :CFA_TestDataFactory.DefaultAccountName];

            Contact cont = [SELECT Id,Partner_Id__c FROM Contact];

            System.assertNotEquals(null, cont.Partner_Id__c, 'Contact.Partner_Id__c mustn\'t be null');

            MembershipUpdate.Memberships membershipjoin = getMembership(cont.Partner_Id__c, acct.Partner_Id__c, 'Join');

            List<MembershipUpdate.Memberships> Memberships = new List<MembershipUpdate.Memberships>();
            Memberships.add(membershipjoin);

            MembershipUpdate membershipUpdate = new MembershipUpdate();
            List<MembershipUpdate.Memberships> updatedMembership = membershipUpdate.updateMT(Memberships);
            system.assertequals('Join', updatedMembership[0].TransactionType);

        }

    }

    //method to test CIPM transaction type for the membership
    @isTest
    public static void testCIPMMembership(){
        System.runAs(CFA_TestDataFactory.getUserWithPermission(PROFILE_INTEGRATION_USER,PERMISSION_SET_EDIT_ACCOUNT_NAME)) {
            Account acct = [SELECT Id,Partner_Id__c FROM Account WHERE Name = :CFA_TestDataFactory.DefaultAccountName];

            Contact cont = [SELECT Id,Partner_Id__c FROM Contact];

            MembershipUpdate.Memberships membershipCIPM = getMembership(cont.Partner_Id__c, acct.Partner_Id__c, 'CIPM');

            List<MembershipUpdate.Memberships> Memberships = new List<MembershipUpdate.Memberships>();
            Memberships.add(membershipCIPM);

            MembershipUpdate membershipUpdate = new MembershipUpdate();
            List<MembershipUpdate.Memberships> updatedMembership = membershipUpdate.updateMT(Memberships);
            system.assertequals('CIPM', updatedMembership[0].TransactionType);

        }
    }

    //method to test Renew transaction type for the membership
    @isTest
    public static void testRenewMembership(){
        System.runAs(CFA_TestDataFactory.getUserWithPermission(PROFILE_INTEGRATION_USER,PERMISSION_SET_EDIT_ACCOUNT_NAME)) {
            Account acct = [SELECT Id,Partner_Id__c FROM Account WHERE Name = :CFA_TestDataFactory.DefaultAccountName];

            Contact cont = [SELECT Id,Partner_Id__c FROM Contact];

            MembershipUpdate.Memberships membershipRenew = getMembership(cont.Partner_Id__c, acct.Partner_Id__c, 'Renew');

            List<MembershipUpdate.Memberships> Memberships = new List<MembershipUpdate.Memberships>();
            Memberships.add(membershipRenew);

            MembershipUpdate membershipUpdate = new MembershipUpdate();
            List<MembershipUpdate.Memberships> updatedMembership = membershipUpdate.updateMT(Memberships);
            system.assertequals('Renew', updatedMembership[0].TransactionType);

        }
    }

    //method to test Lapse transaction type for the membership
    @isTest
    public static void testLapseMembership(){
        System.runAs(CFA_TestDataFactory.getUserWithPermission(PROFILE_INTEGRATION_USER,PERMISSION_SET_EDIT_ACCOUNT_NAME)) {
            Account acct = [SELECT Id,Partner_Id__c FROM Account WHERE Name = :CFA_TestDataFactory.DefaultAccountName];

            Contact cont = [SELECT Id,Partner_Id__c FROM Contact];

            MembershipUpdate.Memberships membershipLapse = getMembership(cont.Partner_Id__c, acct.Partner_Id__c, 'Lapse');

            List<MembershipUpdate.Memberships> Memberships = new List<MembershipUpdate.Memberships>();
            Memberships.add(membershipLapse);
            System.runAs([SELECT Id FROM User WHERE Name = 'Connected User'][0]) {
                MembershipUpdate membershipUpdate = new MembershipUpdate();
                List<MembershipUpdate.Memberships> updatedMembership = membershipUpdate.updateMT(Memberships);
                system.assertequals('Lapse', updatedMembership[0].TransactionType);
            }
        }
    }

    //method to test Cancel transaction type for the membership
    @isTest
    public static void testCancelMembership(){
        System.runAs(CFA_TestDataFactory.getUserWithPermission(PROFILE_INTEGRATION_USER,PERMISSION_SET_EDIT_ACCOUNT_NAME)) {
            Account acct = [SELECT Id,Partner_Id__c FROM Account WHERE Name = :CFA_TestDataFactory.DefaultAccountName];

            Contact cont = [SELECT Id,Partner_Id__c FROM Contact];

            MembershipUpdate.Memberships membershipCancel = getMembership(cont.Partner_Id__c, acct.Partner_Id__c, 'Cancel');

            List<MembershipUpdate.Memberships> Memberships = new List<MembershipUpdate.Memberships>();
            Memberships.add(membershipCancel);

            MembershipUpdate membershipUpdate = new MembershipUpdate();
            List<MembershipUpdate.Memberships> updatedMembership = membershipUpdate.updateMT(Memberships);
            system.assertequals('Cancel', updatedMembership[0].TransactionType);

        }
    }

    //method to test without Soceity and customer partner Ids
    @isTest
    public static void testMemshipWithOutPartnerId(){
        System.runAs(CFA_TestDataFactory.getUserWithPermission(PROFILE_INTEGRATION_USER,PERMISSION_SET_EDIT_ACCOUNT_NAME)) {
            Account acct = [SELECT Id,Partner_Id__c FROM Account WHERE Name = :CFA_TestDataFactory.DefaultAccountName];

            Contact cont = [SELECT Id,Partner_Id__c FROM Contact];

            MembershipUpdate.Memberships membershipCustomer = getMembership(null, cont.Id, 'Cancel');
            MembershipUpdate.Memberships membershipSoceity = getMembership(Acct.Id, null, 'Cancel');

            List<MembershipUpdate.Memberships> Memberships = new List<MembershipUpdate.Memberships>();
            Memberships.add(membershipCustomer);
            Memberships.add(membershipSoceity);
            MembershipUpdate membershipUpdate = new MembershipUpdate();
            List<MembershipUpdate.Memberships> updatedMembership = membershipUpdate.updateMT(Memberships);
            system.assertequals('Cancel', updatedMembership[0].TransactionType);

        }
    }

    //Method to test the membership with Is Affiliate and IsOnProfessionalLeave as error
    @isTest
    public static void testMemshipWithError(){
        System.runAs(CFA_TestDataFactory.getUserWithPermission(PROFILE_INTEGRATION_USER,PERMISSION_SET_EDIT_ACCOUNT_NAME)) {
            Account acct = [SELECT Id,Partner_Id__c FROM Account WHERE Name = :CFA_TestDataFactory.DefaultAccountName];

            Contact cont = [SELECT Id,Partner_Id__c FROM Contact];

            MembershipUpdate.Memberships membershipProfessional = getMembership(cont.Partner_Id__c, acct.Partner_Id__c, 'Renew');
            membershipProfessional.IsOnProfessionalLeave = 'error';

            MembershipUpdate.Memberships membershipIsAffiliate = getMembership(cont.Partner_Id__c, acct.Partner_Id__c, 'Renew');
            membershipIsAffiliate.IsAffiliate = 'error';

            List<MembershipUpdate.Memberships> Memberships = new List<MembershipUpdate.Memberships>();
            Memberships.add(membershipProfessional);
            Memberships.add(membershipIsAffiliate);

            MembershipUpdate membershipUpdate = new MembershipUpdate();

            List<MembershipUpdate.Memberships> updatedMembership = membershipUpdate.updateMT(Memberships);
            system.assertequals('Invalid Is On Professional Leave value', updatedMembership[0].ErrorMessage);
        }
    }

    //Method to test member ship with non soceity membership type
    @isTest
    public static void testMemshipWithOutNonSociety(){
        System.runAs(CFA_TestDataFactory.getUserWithPermission(PROFILE_INTEGRATION_USER,PERMISSION_SET_EDIT_ACCOUNT_NAME)) {
            Account acct = [SELECT Id,Partner_Id__c FROM Account WHERE Name = :CFA_TestDataFactory.DefaultAccountName];

            Contact cont = [SELECT Id,Partner_Id__c FROM Contact];

            MembershipUpdate.Memberships Membership = getMembership(cont.Partner_Id__c, acct.Partner_Id__c, 'Renew');
            Membership.MembershipType = 'Join';

            List<MembershipUpdate.Memberships> Memberships = new List<MembershipUpdate.Memberships>();
            Memberships.add(Membership);
            MembershipUpdate membershipUpdate = new MembershipUpdate();
            List<MembershipUpdate.Memberships> updatedMembership = membershipUpdate.updateMT(Memberships);
            system.assertequals('Join', updatedMembership[0].MembershipType);

        }
    }

    //set up membership data
    @TestVisible
    static MembershipUpdate.Memberships getMembership(string CustomerPartnerId,string SocietyPartnerId,string TransactionType ) {
        MembershipUpdate.Memberships Membership = new MembershipUpdate.Memberships();
        Membership.MembershipGuid = '187e4515-f505-4a74-b277-a55902ab6d89';
        Membership.Success =  null ;
        Membership.ErrorMessage =  null;
        Membership.CustomerPartnerId = CustomerPartnerId;
        Membership.MemberYear = '2019';
        Membership.TransactionType = TransactionType ;
        Membership.MembershipType = 'Society';
        Membership.Reason= null;
        Membership.ReasonCode = null;
        Membership.JoinDate = '2015-01-01T00:00:00Z';
        Membership.IsOnProfessionalLeave = 'False';
        Membership.IsAffiliate = 'False' ;
        Membership.TransactionDate = '2019-11-21T13:50:00Z';
        Membership.SocietyPartnerId= SocietyPartnerId;
        return Membership;
    }
}
public class CFAIntegrationLog {
    
    public static void writeLog(List<CFAMN__Integration_Log__c> logs){
        
        insert logs;
    }
    
    public static void writeLog(String methodName,String lineNumber, String response, String descripton){
        CFAMN__Integration_Log__c log = new CFAMN__Integration_Log__c();
                        log.CFAMN__Method_Name__c = methodName;
                        log.CFAMN__Method_Line__c = lineNumber;
                        log.CFAMN__Response__c=response ;
                        log.CFAMN__Apex_Error_Description__c = descripton;
        insert log;
    }
}
public class VM_JobApplicationController_Edit {
    
    public string strCharLimit{get;set;}
    public String key1=null;
    public Integer numStr{get; set;}
    public String errorMessage {get;set;}
    public String PolicyDocumentURl {get;set;}
    public ID ContactId;
    public List<Role__c> roleDetails;
    public Role__c rolelst;
    public List<string> Competenciesvalues {get; set;}
    //public List<SelectOption> CompetenciesvalueList { get; set; }
    
    public Job_Application__c job{get; set;}
    
    public List<string> industriesmultiselectvalues { get; set; }
    public List<SelectOption> industriesmultiselectvalueList { get; set; }
    
    public List<string> rolesmultiselectvalues { get; set; }
    public List<SelectOption> rolesmultiselectvalueList { get; set; }
    
    public List<string> everServedmultiselectvalues {get; set; }
    public List<SelectOption> everServedmultiselectvalueList {get; set; }
    
//    public String Volunteered_before{get; set;}
    public Contact con{get; set;}

    //Conflict of intrest questions
        public String outreach1{get; set;}
    public String commitmentCPMC{get; set;}
    public String dispactionimp{get; set;}
    public String thistimecommitmentDRC{get; set;}
    public String thistimecommitmentSPC{get; set;}
    public String employeersupport{get; set;}
    public String doyouworkinindustry{get; set;}
    public String othertestingGroups{get; set;}
    public String cfainstCode{get; set;}
    public String rulesandProcedure{get; set;}
    public String CFAexamcurricula{get; set;}
    public String relatedTopics{get; set;}
    public String related_topics1{get; set;}
    public String Volunteered_before{get; set;}
    public String Inappropriate_influence{get; set;}
    public String teachng_of_CFA{get; set;}
    public String gain_or_benefit{get; set;}
    public String Prep_Provider{get; set;}
    public String Relationship_with_Candidate{get; set;}
    public String Relationship_with_Testing{get; set;}
    public String Agree_to_the_Conflict_Policy{get; set;}
    public String disclosed_conflicts{get; set;}
    public String cfaDevelopCurricula{get; set;}
    public String volTimeCommitDRC{get; set;}
    public boolean ShowNote{get;set;}
    //End of COI questions
    public VM_JobApplicationController_Edit(ApexPages.StandardController controller){
       ShowNote = false;     
      strCharLimit ='Each text box is limited to 255 characters.';
        
        //CompetenciesvalueList = new List<SelectOption>();
        Competenciesvalues = new List<String>();
        
        industriesmultiselectvalueList = new List<SelectOption>();
        industriesmultiselectvalues = new List<String>();
        rolesmultiselectvalueList = new List<SelectOption>();
        rolesmultiselectvalues = new List<String>();
        
        everServedmultiselectvalueList = new List<SelectOption>();
        everServedmultiselectvalues = new List<String>();
        
        Schema.DescribeFieldResult fieldResult3 = Job_Application__c.Current_or_previous_roles_have_you__c.getDescribe();
        Schema.DescribeFieldResult fieldResult4 = Job_Application__c.In_which_of_the_following_industry_areas__c.getDescribe();
        Schema.DescribeFieldResult fieldResult5 = Job_Application__c.Have_you_ever_served__c.getDescribe();
        
        
        List<Schema.PicklistEntry> ple5 = fieldResult5.getPicklistValues();

        List<Schema.PicklistEntry> ple3 = fieldResult3.getPicklistValues();
        List<Schema.PicklistEntry> ple4 = fieldResult4.getPicklistValues();
        
        for( Schema.PicklistEntry f : ple5)
        {
            everServedmultiselectvalueList.add(new SelectOption(f.getLabel(), f.getValue()));
        }
        Schema.DescribeFieldResult fieldResult = Job_Application__c.Select_all_competencies__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
        for( Schema.PicklistEntry f : ple3)
        {
            rolesmultiselectvalueList.add(new SelectOption(f.getLabel(), f.getValue()));
        } 
        for( Schema.PicklistEntry f : ple4)
        {
            industriesmultiselectvalueList.add(new SelectOption(f.getLabel(), f.getValue()));
        }
        
        /*for( Schema.PicklistEntry f : ple){
            Competenciesvaluelist.add(new SelectOption(f.getLabel(), f.getValue()));
        }*/
        
        String url= ApexPages.currentPage().getHeaders().get('referer'); 
 
        
        
        Document dr=[Select ID from Document where Name='Volunteer Acknowledgement Form'];
        PolicyDocumentURl= Label.VM_Policydocument+'/servlet/servlet.FileDownload?file='+dr.id;
        
        
        Integer objIndex=url.indexOf('edit-application');
        objIndex+=21;

        String relativeURL11=url.substring(objIndex);
        key1=relativeURL11.substring(0,18);

//Adding code to get store values
        job=[select Professional_experience_in_investment__c,VM_Conflict_Filter__c, Country__c,Degree1__c, Degree2__c, Degree3__c,I_Agree_to_the_Conflict_Policy__c,
If_Other__c,
If_yes_please_explain_Some_examples__c,
How_long_did_you_worked_in_this_industry__c,
I_have_read_the_disclosed_conflicts__c,
Professional_ethical_situation_encounter__c,
VMList_your_professional_accomplishments__c,
             CMPC_Engage_in_outreach__c,
VMIf_yes_discuss_a_recent_article__c,
VMHave_your_presented_on_these_topics__c,
Describe_other_testing_experience__c,
Describe_your_areas_of_expertise__c,             
Describe_your_career_development_action__c,
Describe_your_qualifying_attributes__c,
Address1__c,VM_Job_Title__c,Volunteer_Employer_Type__c,
             status__c,
Designations__c,
             Disciplinary_action_Explain__c,
             Contact__c,
Disciplinary_Action_Imposed__c,
Do_you_have_employer_support__c,
Do_you_work_in_this_industry__c,
Email1__c,
Email2__c,
Email3__c,
Engagement_Name__c,Position__c,Position__r.name,Position__r.Engagement_name__r.name,
Experience_with_other_testing_groups__c,
Familiar_with_the_CFA_Institute_Code__c,
Address2__c,
Familiar_with_the_Rules_and_Procedure__c,
Have_you_developed_the_CFA_curricula__c,
Have_you_overseen_CFA_exam_curricula__c,
Have_you_presented_on_these_topics__c,
Have_you_published_on_related_topics__c,
Have_you_published_on_related_topics1__c,
Have_you_ever_served__c,VM_Preffered_Email__c,Volunteer_time_commitment_DRC__c,

Have_you_worked_with_diverse_groups__c,
How_can_you_contribute_to_this_role__c,
Address3__c,
Howdidyouhearaboutthisrole__c,
How_long_have_you_worked_in_the_industry__c,
If_YES_list_experiences__c,
Describe_your_role_experience_If_Yes__c,
If_YES_discuss_a_recent_article__c,
If_YES_list_years_types_of_experience__c,
If_Yes_please_explain_CMPC__c,
If_Yes_please_explain__c,
In_what_roles__c,
Inappropriate_influence__c,
Involved_with_development_teachng_of_CFA__c,
Other_potential_conflicts__c,
Personal_gain_or_benefit__c,
Phone__c,
Phone1__c,
Phone2__c,
Phone3__c,
Email__c,
Published_any_articles_or_proceedings__c,
VMWhy_are_you_best_for_this_role__c,
Are_you_willing_to_engage_in_outreachSPC__c,
Professional_accomplishments__c,
Professional_Affiliations__c,
Volunteered_before__c,
Reference_Name1__c,
Reference_Name2__c,
Reference_Name3__c,
Relationship_to_Prep_Provider__c,
Relationship_with_Candidate__c,
Relationship_with_Testing_Organization__c,
VM_time_commitmentCPMC__c,
Role_Type__c,
Select_all_competencies__c,
Volunteer_Employer__c,
Volunteer_Impact__c,
Volunteer_Title__c,
What_interests_you_about_this_role__c,
In_which_of_the_following_industry_areas__c,
Current_or_previous_roles_have_you__c,
Can_you_fulfill_this_time_commitment_DRC__c,
Why_are_you_best_for_this_role__c,
Years_in_the_industry__c,
//Can_you_fulfill_this_time_commitmentSPC__c from Job_application__c where id='a2q2F000000BxkB'];
RecordTypeID,RecordType.name,Position__r.Start_date__c,Contact__r.FirstName,Contact__r.Lastname,Contact__r.MDM_Job_Title__c,Contact__r.MiddleName,Contact__r.MailingState,Contact__r.Mailingpostalcode,Contact__r.CFAMN__Employer__c,Position__r.VM_Conflicts_Specific_To_The_Role__c,Contact__r.MailingStreet,Contact__r.MailingCity,Contact__r.MailingCountry,Contact__r.Phone,Contact__r.MDM_Preferred_Email_for_Volunteering__c,Contact__r.Email,
             VM_Describe_professional_accomplishments__c,
             VMIfyesdemanding_volunteer_projects__c,
             Can_you_fulfill_this_time_commitmentSPC__c from Job_application__c where id=:key1];

  //Added to assign values to field

  

            
            if (job.Are_you_willing_to_engage_in_outreachSPC__c!=null){
            outreach1   = job.Are_you_willing_to_engage_in_outreachSPC__c;
            } 
            /*
            if (job.VM_time_commitmentCPMC__c!=null){
                if (job.VM_time_commitmentCPMC__c==true){
            commitmentCPMC   = 'True';
                }else{
                 commitmentCPMC   = 'False';   
                }
            }
            */ 
            if (job.Disciplinary_Action_Imposed__c!=null){
            dispactionimp   = job.Disciplinary_Action_Imposed__c;
            } 
            if (job.Can_you_fulfill_this_time_commitment_DRC__c!=null){
            thistimecommitmentDRC   = job.Can_you_fulfill_this_time_commitment_DRC__c;
            } 
            if (job.Can_you_fulfill_this_time_commitmentSPC__c!=null){
            thistimecommitmentSPC   = job.Can_you_fulfill_this_time_commitmentSPC__c;
            } 
            if (job.Do_you_have_employer_support__c!=null){
            employeersupport   = job.Do_you_have_employer_support__c;
            } 
            if (job.Do_you_work_in_this_industry__c!=null){
            doyouworkinindustry   = job.Do_you_work_in_this_industry__c;
            } 
            if (job.Experience_with_other_testing_groups__c!=null){
            othertestingGroups   = job.Experience_with_other_testing_groups__c;
            }
            if (job.Familiar_with_the_CFA_Institute_Code__c!=null){
            cfainstCode   = job.Familiar_with_the_CFA_Institute_Code__c;
            }
            if (job.Familiar_with_the_Rules_and_Procedure__c!=null){
            rulesandProcedure   = job.Familiar_with_the_Rules_and_Procedure__c;
            }
            if (job.Have_you_overseen_CFA_exam_curricula__c!=null){
            CFAexamcurricula   = job.Have_you_overseen_CFA_exam_curricula__c;
            }
            if (job.Have_you_published_on_related_topics__c!=null){
            relatedTopics   = job.Have_you_published_on_related_topics__c;
            }
            if (job.Have_you_published_on_related_topics1__c!=null){
            related_topics1   = job.Have_you_published_on_related_topics1__c;
            }
            if (job.Volunteered_before__c!=null){
            Volunteered_before   = job.Volunteered_before__c;
            }
            /*
            if (job.Involved_with_development_teachng_of_CFA__c!=null){
            if (job.Involved_with_development_teachng_of_CFA__c==true){
            teachng_of_CFA   = 'True';
                }else{
                 teachng_of_CFA   = 'False';   
                }
            }
            */
            if (job.Personal_gain_or_benefit__c!=null){
            gain_or_benefit   = job.Personal_gain_or_benefit__c;
            }          
            if (job.Inappropriate_influence__c!=null){
            Inappropriate_influence   = job.Inappropriate_influence__c;
            }
            if (job.Relationship_to_Prep_Provider__c!=null){
            Prep_Provider   = job.Relationship_to_Prep_Provider__c;
            }    
            if (job.Relationship_with_Candidate__c!=null){
            Relationship_with_Candidate   = job.Relationship_with_Candidate__c;
            }            
            if (job.Relationship_with_Testing_Organization__c!=null){
            Relationship_with_Testing   = job.Relationship_with_Testing_Organization__c;
            }            
            /*Agreements removed from this page
            if (job.I_Agree_to_the_Conflict_Policy__c!=null){
            Agree_to_the_Conflict_Policy   = job.I_Agree_to_the_Conflict_Policy__c;
            }
            if (job.I_have_read_the_disclosed_conflicts__c!=null){
            disclosed_conflicts   = job.I_have_read_the_disclosed_conflicts__c;
            }
            */
            if (job.Have_you_developed_the_CFA_curricula__c!=null){
            cfaDevelopCurricula   = job.Have_you_developed_the_CFA_curricula__c;
            }
            if (job.Volunteer_time_commitment_DRC__c!=null){
            volTimeCommitDRC   = job.Volunteer_time_commitment_DRC__c;
            }
            //showCongratsData=false;
        //showApplicationData=true;
        //this.webContact = (Job_Application__c)controller.getRecord();
      /***********************************************************************/ 
        try{
        job=[select 
             Published_any_articles_or_proceedings__c,
             VM_Conflict_Filter__c, Country__c, Degree2__c, Degree3__c,
             VMList_your_professional_accomplishments__c,
             Degree1__c,
Professional_ethical_situation_encounter__c,
             VMIf_yes_discuss_a_recent_article__c,
             VMHave_your_presented_on_these_topics__c,
             VMWhy_are_you_best_for_this_role__c,
How_long_did_you_worked_in_this_industry__c,
             If_Other__c,
             If_yes_please_explain_Some_examples__c,
             Describe_other_testing_experience__c,
Describe_your_areas_of_expertise__c,
             Describe_your_career_development_action__c,
             Describe_your_qualifying_attributes__c,
Address1__c,Designations__c,
Disciplinary_Action_Imposed__c,
Do_you_have_employer_support__c,
Do_you_work_in_this_industry__c,
Email1__c,Email2__c,Email3__c,Engagement_Name__c,
Experience_with_other_testing_groups__c,
Familiar_with_the_CFA_Institute_Code__c,
Address2__c,
Familiar_with_the_Rules_and_Procedure__c,
Have_you_developed_the_CFA_curricula__c,
Have_you_overseen_CFA_exam_curricula__c,
Have_you_presented_on_these_topics__c,
Have_you_published_on_related_topics__c,
Have_you_published_on_related_topics1__c,
Have_you_ever_served__c,
Volunteered_before__c,
Volunteer_time_commitment_DRC__c,
VM_Describe_professional_accomplishments__c,
VMIfyesdemanding_volunteer_projects__c,
             Have_you_worked_with_diverse_groups__c,
             How_can_you_contribute_to_this_role__c,
Address3__c,
             Howdidyouhearaboutthisrole__c,
             How_long_have_you_worked_in_the_industry__c,
             If_YES_list_experiences__c,
             Describe_your_role_experience_If_Yes__c,
If_YES_discuss_a_recent_article__c,
             If_YES_list_years_types_of_experience__c,
             If_Yes_please_explain_CMPC__c,
             If_Yes_please_explain__c,
In_what_roles__c,
Inappropriate_influence__c,
Involved_with_development_teachng_of_CFA__c,
             Other_potential_conflicts__c,
Personal_gain_or_benefit__c,
Phone__c,Phone1__c,Phone2__c,Phone3__c,Email__c,
Are_you_willing_to_engage_in_outreachSPC__c,
Professional_accomplishments__c,
Please_explain_DRC__c, 
             Professional_Affiliations__c,
             Reference_Name1__c,
             Reference_Name2__c,
             Reference_Name3__c,
Relationship_to_Prep_Provider__c,
Relationship_with_Candidate__c,
Relationship_with_Testing_Organization__c,
VM_time_commitmentCPMC__c,Role_Type__c,Select_all_competencies__c,Volunteer_Employer__c,
Volunteer_Impact__c,Volunteer_Title__c,
             What_interests_you_about_this_role__c,
             In_which_of_the_following_industry_areas__c,Current_or_previous_roles_have_you__c,
Can_you_fulfill_this_time_commitment_DRC__c,
Why_are_you_best_for_this_role__c,
             Years_in_the_industry__c,
Can_you_fulfill_this_time_commitmentSPC__c,
RecordTypeId,RecordType.name,Position__c,Position__r.VM_Conflicts_Specific_To_The_Role__c,Position__r.name,Position__r.Engagement_name__r.name,Position__r.Start_date__c,Contact__c,Contact__r.firstname,Contact__r.LastName,
I_Agree_to_the_Conflict_Policy__c,VM_Preffered_Email__c,
             Disciplinary_action_Explain__c,
             CMPC_Engage_in_outreach__c,
             I_have_read_the_disclosed_conflicts__c,VM_Job_Title__c,Volunteer_Employer_Type__c,Contact__r.MiddleName,Contact__r.CFAMN__Employer__c,Contact__r.MDM_Job_Title__c,Contact__r.MailingStreet,Contact__r.MailingState,Contact__r.Mailingpostalcode,Contact__r.MailingCity,Contact__r.MailingCountry,Contact__r.Phone,Contact__r.MDM_Preferred_Email_for_Volunteering__c,Contact__r.Email from Job_application__c where id=:key1];

        
        if(!String.isBlank(job.Contact__c))
        {
            ContactId = job.Contact__c;
            con=[Select Id,MDM_Preferred_Email_for_Volunteering__c From Contact where id =:ContactId];
        }
        if(!String.isBlank(job.RecordTypeID)){
        if(job.RecordType.name=='CIPMCommittee')
            numStr = 1;
        else if(job.RecordType.name=='CMPCCommittee')
            numStr = 2;
        else if(job.RecordType.name=='Advisory Council')
            numStr = 3;
        else if(job.RecordType.name=='Default')
            numStr = 4;
        else if(job.RecordType.name=='DRCCommittee')
            numStr = 5;
        else if(job.RecordType.name=='EACCommittee')
            numStr = 6;
        else if(job.RecordType.name=='Exam Related Advisor' || job.RecordType.name=='Exam+Related+Advisor')
            numStr = 7;
        else if(job.RecordType.name=='GIPSCommittee')
           { numStr = 8;
            ShowNote=true;
             strCharLimit ='Each text box is limited to 1000 characters.  It is important that your responses be brief and specific to the volunteer role.  You will have the opportunity to attach additional information, along with your required resume or CV, before submitting the application.';
                }
        else if(job.RecordType.name=='MemberMicro')
            numStr = 9;
        else if(job.RecordType.name=='SPCCommittee')
            numStr = 10;
        else if(job.RecordType.name=='Non+Exam+related+Advisory+council')
            numStr = 11;
        else
            numStr = 12;
   
        }
        system.debug('value of numStr '+numStr);

        
//        Volunteered_before = String.valueOf(job.Volunteered_before__c);
        System.debug('$$Volunteered_before$'+Volunteered_before);
        /*if(Volunteered_before != null)
                job.Volunteered_before__c = Boolean.valueOf(Volunteered_before);*/
         
            if(job.Select_all_competencies__c != NULL)
                Competenciesvalues.addAll(job.Select_all_competencies__c.split(';'));
            
            if(job.In_which_of_the_following_industry_areas__c != NULL)
                industriesmultiselectvalues.addAll(job.In_which_of_the_following_industry_areas__c.split(';'));
            
            if(job.Current_or_previous_roles_have_you__c != NULL)
                rolesmultiselectvalues.addAll(job.Current_or_previous_roles_have_you__c.split(';'));
            
            if(job.Have_you_ever_served__c != NULL)
                everServedmultiselectvalues.addAll(job.Have_you_ever_served__c.split(';'));
                    
        } catch(Exception e){

        }
    }
    
     public void fillInErrorMessage(String msg){
                errorMessage = msg;
     } 
     
     public List<SelectOption> getlevel1Items() {
        List<SelectOption> plValues = new List<SelectOption>();
        
        Schema.DescribeFieldResult dfr = Job_Application__c.Select_all_competencies__c.getDescribe();
        for (Schema.PicklistEntry ple : dfr.getPicklistValues()) {
            
            plValues.add(new SelectOption(ple.getValue(), ple.getLabel()));
                   }
        
        return plValues;
    }  
    
    public List<String> getQuestionsForRecordType(){
        //String recordType=recordtypew;
        String recordType;
        List<job_application__c> jobList = [Select Id,RecordTypeId,RecordType.name from job_application__c where Id =:key1];
        
        if(!jobList.isEmpty()){
            

         if(jobList[0].RecordType.Name=='CIPMCommittee'){
            recordType='CIPMCommittee__c';
        } else if(jobList[0].RecordType.Name=='CMPCCommittee'){
            recordType='CMPCCommittee__c';
        } else if(jobList[0].RecordType.Name=='Advisory Council'|| jobList[0].RecordType.Name=='Advisory+Council'){
            recordType='Exam_Related_Advisor__c';
        } else if(jobList[0].RecordType.Name=='Default'||jobList[0].RecordType.Name=='MemberMicro'){
            recordType='Micro__c';

        } else if(jobList[0].RecordType.Name=='DRCCommittee'){
            recordType='DRCCommittee__c';
        
        } else if(jobList[0].RecordType.Name=='EACCommittee'){
            recordType='EAC__c';
        
        } else if(jobList[0].RecordType.Name=='Exam Related Advisor' || jobList[0].RecordType.Name=='Exam+Related+Advisor'||jobList[0].RecordType.Name=='Exam%20Related%20Advisor'){
            recordType='Exam_Related_Advisor__c';
        
        } else if(jobList[0].RecordType.Name=='GIPSCommittee'){
            recordType='GIPS__c';
        
        } else if(jobList[0].RecordType.Name=='MemberMicro'){
            recordType='Micro__c';
        
        } else if(jobList[0].RecordType.Name=='SPCCommittee'){
            recordType='SPC__c';
        }else if(jobList[0].RecordType.Name=='Non+Exam+related+Advisory+council'||jobList[0].RecordType.Name=='Non Exam related Advisory Council'||jobList[0].RecordType.Name=='Non%20Exam%20related%20Advisory%20council'){
            recordType='Non_Exam_Related_Advisor__c';
        }else{
            recordType='Micro__c';
        }
        
      }  

        String query='Select id,Question__c from VM_QuestionsVisibility__c where '+recordType+'=true';
        List<VM_QuestionsVisibility__c> listObjVM_QuestionsVisibility=Database.query(query);
        List<String> listQuestions=new List<String>(); 
        if(listObjVM_QuestionsVisibility!=null && listObjVM_QuestionsVisibility.size()>0){
            for(VM_QuestionsVisibility__c objVM_QuestionsVisibility :listObjVM_QuestionsVisibility){
                listQuestions.add(objVM_QuestionsVisibility.Question__c);
            }
        }

        return listQuestions;
    }
    
    /*public string getOutputText() {
        
        return String.join(Competenciesvalues, ';');
    }*/
    
    public string getEverServedText() {
        return String.join(everServedmultiselectvalues, ';');
    }
    
    public pagereference saveApplication(){

         
        PageReference pg;
//Get role details
            roleDetails = [Select ID,Relationship_Manager_Formula__c from Role__c where Id =:Job.Position__c];
        if(!roleDetails.isEmpty())
            rolelst = roleDetails[0];
        
        
//Save application and continue
        try
        {

            
             if(Competenciesvalues != NULL)
             job.Select_all_competencies__c=String.join(Competenciesvalues, ';'); 

            
            if(job.In_which_of_the_following_industry_areas__c != NULL)
                 job.In_which_of_the_following_industry_areas__c=String.join(industriesmultiselectvalues, ';'); 
           
            if(job.Current_or_previous_roles_have_you__c != NULL)
              job.Current_or_previous_roles_have_you__c=String.join(rolesmultiselectvalues, ';'); 
           
            if(job.Have_you_ever_served__c != NULL)
              job.Have_you_ever_served__c=String.join(everServedmultiselectvalues, ';'); 
            
//Update conflict intrest questions

            if(outreach1 != null)
                job.Are_you_willing_to_engage_in_outreachSPC__c = outreach1;
            else job.Are_you_willing_to_engage_in_outreachSPC__c = 'Not Answered';
            if(commitmentCPMC != null)
                job.VM_time_commitmentCPMC__c = Boolean.valueOf(commitmentCPMC);
            if(dispactionimp != null)
                job.Disciplinary_Action_Imposed__c = dispactionimp;
            else job.Disciplinary_Action_Imposed__c = 'Not Answered';
            if(thistimecommitmentDRC != null)
                job.Can_you_fulfill_this_time_commitment_DRC__c = thistimecommitmentDRC;
            else job.Can_you_fulfill_this_time_commitment_DRC__c = 'Not Answered';
            if(thistimecommitmentSPC != null)
                job.Can_you_fulfill_this_time_commitmentSPC__c = thistimecommitmentSPC;
            else   job.Can_you_fulfill_this_time_commitmentSPC__c = 'Not Answered';
            if(employeersupport != null)
                job.Do_you_have_employer_support__c = employeersupport;
            else  job.Do_you_have_employer_support__c = 'Not Answered'; 
            if(doyouworkinindustry != null) 
                job.Do_you_work_in_this_industry__c = doyouworkinindustry;
            else  job.Do_you_work_in_this_industry__c = 'Not Answered';
            if(othertestingGroups != null)
                job.Experience_with_other_testing_groups__c = othertestingGroups;
            else  job.Experience_with_other_testing_groups__c = 'Not Answered';
            if(cfainstCode != null)    
                job.Familiar_with_the_CFA_Institute_Code__c = cfainstCode;
            else  job.Familiar_with_the_CFA_Institute_Code__c = 'Not Answered';
            if(rulesandProcedure != null)
                job.Familiar_with_the_Rules_and_Procedure__c = rulesandProcedure;
            else  job.Familiar_with_the_Rules_and_Procedure__c = 'Not Answered'; 
            if(CFAexamcurricula != null)
                job.Have_you_overseen_CFA_exam_curricula__c = CFAexamcurricula;
            else job.Have_you_overseen_CFA_exam_curricula__c = 'Not Answered';
            if(relatedTopics != null)
                job.Have_you_published_on_related_topics__c = relatedTopics;
            else job.Have_you_published_on_related_topics__c = 'Not Answered';
            if(related_topics1 != null)
                job.Have_you_published_on_related_topics1__c = related_topics1;
            else job.Have_you_published_on_related_topics1__c ='Not Answered';
            if(Volunteered_before != null)
                job.Volunteered_before__c = Volunteered_before;
            else job.Volunteered_before__c = 'Not Answered';
            if(teachng_of_CFA != null)
                job.Involved_with_development_teachng_of_CFA__c = Boolean.valueOf(teachng_of_CFA);
            if(gain_or_benefit != null)
                job.Personal_gain_or_benefit__c = gain_or_benefit;
            else job.Personal_gain_or_benefit__c = 'Not Answered';
            
            if(Inappropriate_influence != null)
                job.Inappropriate_influence__c = Inappropriate_influence;
            else job.Inappropriate_influence__c = 'Not Answered';          
            if(Prep_Provider != null)
                job.Relationship_to_Prep_Provider__c = Prep_Provider;
            else job.Relationship_to_Prep_Provider__c = 'Not Answered';
            
            if(Relationship_with_Candidate != null)
                job.Relationship_with_Candidate__c = Relationship_with_Candidate;
            else job.Relationship_with_Candidate__c = 'Not Answered';
            
            if(Relationship_with_Testing != null)
                job.Relationship_with_Testing_Organization__c = Relationship_with_Testing;
            else job.Relationship_with_Testing_Organization__c = 'Not Answered';
            if(Agree_to_the_Conflict_Policy != null)
                job.I_Agree_to_the_Conflict_Policy__c = Boolean.valueOf(Agree_to_the_Conflict_Policy);
            if(disclosed_conflicts != null)
                job.I_have_read_the_disclosed_conflicts__c = Boolean.valueOf(disclosed_conflicts);
            if(cfaDevelopCurricula != null)    
                job.Have_you_developed_the_CFA_curricula__c = cfaDevelopCurricula;
            else job.Have_you_developed_the_CFA_curricula__c = 'Not Answered';
            if(volTimeCommitDRC != null)    
                job.Volunteer_time_commitment_DRC__c = volTimeCommitDRC;
            else job.Volunteer_time_commitment_DRC__c = 'Not Answered';

            
        //Check for Conflicts of intrest questions
            if(job.Inappropriate_influence__c=='Yes'|| job.Personal_gain_or_benefit__c=='Yes' || job.Relationship_to_Prep_Provider__c=='Yes'||job.Relationship_with_Testing_Organization__c=='Yes'||job.Relationship_with_Candidate__c=='Yes'){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'<font color="red"><b>Please contact the Relationship Manager regarding potential conflicts</b></font> '+roledetails[0].Relationship_Manager_Formula__c));
                return null;
            }
            


            update con;
           
            //update job;
             String iSuccess;
            Database.SaveResult sr = Database.update(job, false);
            if(!sr.isSuccess()){
                list<Database.Error> err = sr.getErrors();
                
                Apexpages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR, '<font color="red"><b> '+err[0].getMessage()+'</b></font>'));
                return null;
            }
            
                
            

            pg = new PageReference(Label.VM_CommunityHomepage+'job-application/'+job.id+'/app0303');
            //pg.setRedirect(true);
            return pg;       
        
        }catch(system.DMLException e)
        {

            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'<font color="red"><b> '+e.getDmlMessage(0)+'</b></font>'));
           // return null;              
        }
        return null;
    }
    
     public List<SelectOption> getItems() {
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new SelectOption('True','Yes')); 
        options.add(new SelectOption('False','No')); 
        return options; 
    }

         public List<SelectOption> getItems1() {
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new SelectOption('Yes','Yes')); 
        options.add(new SelectOption('No','No')); 
        return options; 
    }
    
    //public PageReference submitApplication(){
      //  try{
            
        //}
   // }
}
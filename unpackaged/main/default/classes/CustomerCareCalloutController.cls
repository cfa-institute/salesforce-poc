/**************************************************************************************************
* Apex Class Name   : CustomerCareCalloutController
* Purpose           : This class is used for making API Callout to BERT System to display read only Enrollment, Scholarship Data in Salesforce
* Version           : 1.0 Initial Version
* Organization      : Cognizant
* Created Date      : 14-Jan-2018  
* Modified Date     : 17-July-2018; Lightning Team -- Vijay Vemuru  
: 27-September-2018; Lightning Team -- Vijay Vemuru
***************************************************************************************************/
public with sharing class CustomerCareCalloutController{
    
    public string currentTab {get;set;}
    public Boolean EnrollmentFlag{get;set;}
    public Boolean ExamResultFlag{get; set;}
    public Boolean ScholarshipFlag{get; set;}
    public Boolean LoginHistoryFlag{get;set;}
    public Boolean MembershipFlag{get; set;}
    public Boolean CustomerHistoryFlag{get; set;}
    public String cfaPersonID{get; set;}
    public String customerId{get;set;}
    public String OnProfessionalLeave{get;set;}
    public String IsAffiliate{get;set;}
    public String MemberYear{get;set;}
    public Id currentId{get;set;}
    private String membership='Membership';
    private String history='history';
    private String scholarship='Scholarship';
    private String loginHistory='LoginHistory';
    private String enrollment='Enrollment';
    private String examResult='ExamResult';
    private String customerHistory='CustomerHistory';
    public String myDate;
    final integer islessthanfiveyears = 1826;
    
    
    // private String MembershipTransactionEP = 'https://salesforcepublicapi.dev.cfainstitute.org/memberships';
    
    @Testvisible
    private String EnrollmentEP,ScholarshipEP,LoginHistoryEP,MembershipEP,CustomerHistoryEP ;
    
    public List<EnrollmentWrapper> ConsoleWrapperList{get;set;}
    public List<EnrollmentWrapper> ExamResultWrapperList{get;set;}
    public List<ScholarshipWrapper> scholarshipWrapperList{get; set;} 
    public List<LoginHistoryWrapper> loginHistoryWrapperList{get; set;}
    public List<Memberships> MembershipWrapperList{get; set;}
    public List<CustomerHistoryWrapper> CustomerHistoryWrapperList{get; set;}
    
    
    @Testvisible
    private Map<Integer, String> httpErrorCodeMap;
    
    
    
    public CustomerCareCalloutController(){}
    
    /* Constructor gets the Person Id of current contact and concatenate it to the Enrollment and Scholarship Endpoint URL */
    public CustomerCareCalloutController(ApexPages.StandardController controller) {
        List<CustomerCare_ApiCodeMessages__c> listCodes;   
        currentTab = apexpages.currentPage().getparameters().get('tabName');
        
        //String cfaPersonID; 
        List<Contact> cfaconLst = new List<Contact>();
        EnrollmentFlag = false;
        ExamResultFlag = false;
        ScholarshipFlag = false;
        LoginHistoryFlag = false;
        httpErrorCodeMap = new Map<Integer, String>();
        EnrollmentEP = System.Label.CustomerCareEnrollmentLink;
        ScholarshipEP = System.Label.CustomerCareScholarshipLink; 
        LoginHistoryEP = System.Label.CustomerCareLoginHistoryLink;
        MembershipEP = System.Label.CustomerCareMembershipLink;
        CustomerHistoryEP = System.Label.CustomerCareCustomerHistory;
        listCodes = CustomerCare_ApiCodeMessages__c.getAll().values();  
        currentId = controller.getId();
        cfaconLst = [Select Id,CFAMN__PersonID__c from Contact where Id=:currentId];
        if(!cfaconLst.isEmpty() && !String.isBlank(cfaconLst[0].CFAMN__PersonID__c)){
            cfaPersonID = cfaconLst[0].CFAMN__PersonID__c;
            
        }   
        
        if(!string.isBlank(cfaPersonID) && !string.isBlank(EnrollmentEP))
            EnrollmentEP = EnrollmentEP + '/' + cfaPersonID;  
        if(!string.isBlank(cfaPersonID) && !string.isBlank(ScholarshipEP))  
            ScholarshipEP = ScholarshipEP + '/' + cfaPersonID; 
        if(!string.isBlank(cfaPersonID) && !string.isBlank(loginHistoryEP))  
            loginHistoryEP = loginHistoryEP + '/' + cfaPersonID;
        if(!string.isBlank(cfaPersonID) && !string.isBlank(MembershipEP))  {
            MembershipEP = MembershipEP + '/' + cfaPersonID;
            if(!string.isBlank(cfaPersonID) && !string.isBlank(CustomerHistoryEP))  {
                CustomerHistoryEP = CustomerHistoryEP + '/' + cfaPersonID + '/' + History  ;
            }
            
            if(currentTab == 'Membership'){
                MembershipFlag = true;
                membership = currentTab;
                System.debug('TEST:'+MembershipEP);
                ContactMembershipCallout();
            }
            else{
                MembershipFlag = false;
            }
            if(currentTab == 'CustomerHistory'){
                CustomerHistoryFlag = true;
                customerHistory = currentTab;
                System.debug('TEST:'+customerHistory);
                ContactCustomerHistotyCallout();
            }
            else{
                CustomerHistoryFlag = false;
            }
            if(!listCodes.isEmpty()){// Custom setting for handling negative scenario of Error Code
                for(CustomerCare_ApiCodeMessages__c apiCode : listCodes){
                    if(apiCode.CustomerCare_Error_Code__c != NULL && apiCode.CustomerCare_ErrorMessage__c != NULL)
                        //httpErrorCodeMap.put(Integer.valueof(apiCode.CustomerCare_Error_Code__c),apiCode.CustomerCare_ErrorMessage__c);
                        httpErrorCodeMap.put(Integer.valueof(apiCode.CustomerCare_Error_Code__c),'No data available in BERT for this tab');
                }
            }
        }
    }
    /* This method is common method which calls API services and 
deserialize response to individual class list variable */
    public HttpResponse contactWebServices(String callType){
        HttpRequest request1 = new HttpRequest();
        HttpResponse response1 = new HttpResponse();
        JSONParser parser;
        Http http = new Http();
        try{
            if(callType == enrollment || callType == examResult)
                request1.setEndpoint(EnrollmentEP);
            else if(callType == scholarship)
                request1.setEndpoint(ScholarshipEP);
            else if(callType == loginHistory)
                request1.setEndpoint(LoginHistoryEP);
            else if(callType == membership)
                request1.setEndpoint(MembershipEP);
            else if(callType == customerHistory)
                request1.setEndpoint(CustomerHistoryEP);
            
            request1.setClientCertificateName(Label.MDM_Swagger_Certificate);
            request1.setMethod('GET');
            response1 = http.send(request1);
            
            ExamResultFlag = false;
            EnrollmentFlag = false;
            ScholarshipFlag = false;
            LoginHistoryFlag = false;
            MembershipFlag = false; 
            CustomerHistoryFlag = false; 
            
            if(!httpErrorCodeMap.isEmpty() && httpErrorCodeMap.containsKey(response1.getStatusCode())){
                if (Schema.sObjectType.CFAMN__Integration_Log__c.isCreateable() && 
                    (Schema.sObjectType.CFAMN__Integration_Log__c.fields.CFAMN__is_Error__c.isAccessible() &&
                     Schema.sObjectType.CFAMN__Integration_Log__c.fields.CFAMN__Integration_Type__c.isAccessible() &&
                     Schema.sObjectType.CFAMN__Integration_Log__c.fields.CFAMN__Request_Body__c.isAccessible() &&
                     Schema.sObjectType.CFAMN__Integration_Log__c.fields.CFAMN__Response__c.isAccessible() &&
                     Schema.sObjectType.CFAMN__Integration_Log__c.fields.CFAMN__Response_Status__c.isAccessible())
                   ){
                       
                       list<CFAMN__Integration_Log__c> lstCCIL = new list<CFAMN__Integration_Log__c>();
                       CFAMN__Integration_Log__c itLogobj = new CFAMN__Integration_Log__c();
                       itLogobj.CFAMN__Apex_Error_Description__c = httpErrorCodeMap.get(response1.getStatusCode());
                       itLogobj.CFAMN__is_Error__c = True;
                       itLogobj.CFAMN__Integration_Type__c = 'Integration';
                       if(callType == enrollment || callType == examResult)
                           itLogobj.CFAMN__Request_Body__c = EnrollmentEP;
                       if(callType == scholarship)
                           itLogobj.CFAMN__Request_Body__c = ScholarshipEP;
                       if(callType == loginHistory)
                           itLogobj.CFAMN__Request_Body__c = LoginHistoryEP;
                       if(callType == membership)
                           itLogobj.CFAMN__Request_Body__c = MembershipEP;
                       if(callType == customerHistory)
                           itLogobj.CFAMN__Request_Body__c = CustomerHistoryEP;
                       
                       itLogobj.CFAMN__Response__c = httpErrorCodeMap.get(response1.getStatusCode());
                       itLogobj.CFAMN__Response_Status__c = response1.getStatus();
                       lstCCIL.add(itLogobj);
                       insert lstCCIL;
                       
                   }
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'<font color="red">'+httpErrorCodeMap.get(response1.getStatusCode())+'</font>'));                   
            }else if(response1.getStatusCode() == 200){
                
                if(callType == enrollment){
                    ConsoleWrapperList=(List<EnrollmentWrapper>)json.deserialize(response1.getbody(),List<EnrollmentWrapper>.class);
                    EnrollmentFlag = true;
                }else if(callType == examResult){
                    ExamResultWrapperList=(List<EnrollmentWrapper>)json.deserialize(response1.getbody(),List<EnrollmentWrapper>.class);
                    ExamResultFlag = True;
                }else if(callType == loginHistory){
                    loginHistoryWrapperList=(List<LoginHistoryWrapper>)json.deserialize(response1.getbody(),List<LoginHistoryWrapper>.class);
                    LoginHistoryFlag = True;
                }else if(callType == scholarship){
                    scholarshipWrapperList=(List<ScholarshipWrapper>)json.deserialize(response1.getbody(),List<ScholarshipWrapper>.class);
                    ScholarshipFlag = True;
                    
                }else if(callType == customerHistory){
                    
                    CustomerHistoryWrapperList=(List<CustomerHistoryWrapper>)json.deserialize(response1.getbody(),List<CustomerHistoryWrapper>.class);	
                    system.debug('CustomerHistoryWrapperList'+CustomerHistoryWrapperList );
                    list<CustomerHistoryWrapper> newcuswrapper = new list<CustomerHistoryWrapper>();	
                    for(CustomerHistoryWrapper each: CustomerHistoryWrapperList) {	
                        
                        if(each.ValidFrom !=null){	
									string d1 = each.ValidFrom.format('MM/dd/yyyy HH:mm:ss', 'EST');
							 integer datedata =(Date.valueOf(each.ValidFrom).daysBetween(Date.valueOf(system.today())));	
                            if((datedata <= islessthanfiveyears && datedata >=0)||Date.valueOf(each.ValidFrom)==Date.valueOf(datetime.now()))
                            {
                              each.ValidFromFormatted = d1;
                              if(each.DateOfBirth !=null)	
                                    each.DateOfBirthFormatted = each.DateOfBirth.formatGMT('MM/dd/yyyy');
                                system.debug('ValidFrom'+each.ValidFrom );
                                if(each.PassportExpirationDate !=null)	
                                    each.PassportExpirationDateFormatted = each.PassportExpirationDate.formatGMT('MM/dd/yyyy');	   
                                newcuswrapper.add(each);	
                                
                            }	
                            
                        }	
                    }	
                    CustomerHistoryWrapperList= newcuswrapper;	
                    CustomerHistoryFlag = True;
                    
                }else if(callType == membership){
                    parser = JSON.createParser(response1.getBody());
                    MembershipFlag = True;
                    //System.JSONToken token;
                    while (parser.nextToken() != null) {// used for iterating membership wrapper over the each node
                        if (parser.getCurrentToken() == JSONToken.FIELD_NAME){
                            String fieldName = parser.getText();
                            if(fieldName == 'CustomerId'){
                                parser.nextToken();
                                customerId = parser.getText();
                            }
                            
                            if(fieldName == 'OnProfessionalLeave'){
                                parser.nextToken();
                                OnProfessionalLeave = parser.getText();
                            }
                            
                            if(fieldName == 'MemberYear'){
                                parser.nextToken();
                                MemberYear = parser.getText();
                            }
                            
                            if(fieldName == 'IsAffiliate'){
                                parser.nextToken();
                                IsAffiliate = parser.getText();
                                
                                
                            }
                            
                            if(fieldName == 'Memberships'){
                                if(parser.nextToken() == JSONToken.START_ARRAY){
                                    while(parser.nextToken() != null){
                                        if(parser.getCurrentToken() == JSONToken.START_OBJECT){
                                            Memberships mships = (Memberships) parser.readValueAs(Memberships.class); 
                                            MembershipWrapperList.add(mships);
                                            System.debug('##fieldNameddddddddd membership inside one##'+MembershipWrapperList);
                                        }else if(parser.getCurrentToken() == JSONToken.END_ARRAY){
                                            break;  //break when memberships array has been processed
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }catch(Exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'<font color="red">'+ e.getMessage() + '</font>'));   
            String mesg = 'contactWebServices method exception='+ e.getMessage() + ', # ' + e.getLineNumber();
            cfa_ApexUtilities.log('An exception occured in contactWebServices method');
            cfa_ApexUtilities.log(mesg);
        }
        return response1;
    }
    
    // Below method internally calls contactWebServices with API Call Type 
    // Parameter as Enrollment to get the Response and deserialize it to EnrollmentWrapper list
    public void ContactEnrollmentCallout(){
        ConsoleWrapperList = new List<EnrollmentWrapper>();
        currentTab = enrollment;
        contactWebServices(enrollment);        
    }
    
    // Below method internally calls contactWebServices with API Call Type Parameter as ExamResult 
    //to get the Response and deserialize it to ExamResultWrapperList list
    public void ContactExamResultCallout(){
        ExamResultWrapperList = new List<EnrollmentWrapper>();
        currentTab = examResult;
        contactWebServices(examResult);        
    }
    
    // Below method internally calls contactWebServices with API Call Type Parameter
    //  as LoginHistory to get the Response and deserialize it to loginHistoryWrapperList list
    public void ContactLoginHistoryCallout(){
        loginHistoryWrapperList = new List<LoginHistoryWrapper>();
        currentTab = loginHistory;
        contactWebServices(loginHistory);        
    }
    
    // Below method internally calls contactWebServices with API Call Type Parameter as Scholarship
    //to get the Response and deserialize it to scholarshipWrapperList list
    public void ContactScholarshipCallout(){
        scholarshipWrapperList = new List<ScholarshipWrapper>();
        currentTab = scholarship;
        contactWebServices(scholarship);        
    }
    
    //Below method internally calls contactWebServices with API Call Type Parameter
    // as Membership to get the Response and deserialize it to MembershipWrapperList list
    public void ContactMembershipCallout(){
        MembershipWrapperList = new List<Memberships>();
        currentTab = membership;
        contactWebServices(membership);
    }
    //Below method internally calls contactWebServices with API Call Type Parameter
    // as CustomerHistory to get the Response and deserialize it to CustomerHistoryWrapper list
    public void ContactCustomerHistotyCallout(){
        CustomerHistoryWrapperList = new List<CustomerHistoryWrapper>();
        currentTab = customerHistory;
        contactWebServices(customerHistory);
    }
}
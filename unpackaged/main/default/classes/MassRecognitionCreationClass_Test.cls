@isTest
private class MassRecognitionCreationClass_Test {

   // @testSetup static void setup(){
       
    
                          
  //  }
        
    public static testMethod void testMassRecognition() {
         //VM_TestDataFactory_Old o=new VM_TestDataFactory_Old();
        
        User RMOwner=VM_TestDataFactory.createUser('RMTest1@gmail.com','RMTest','Relationship Manager Staff','');
        User MDUSer=VM_TestDataFactory.createUser('MDTest1@gmail.com','MDTest','Outreach Management User','Managing Director');
        User ReportsToUSer=VM_TestDataFactory.createUser('Anne1@gmail.com','Lange','Outreach Management User','Relationship Manager');
        Engagement__c eng=VM_TestDataFactory.createEngagement('t FY20',RMOwner,MDUSer,ReportsToUSer);
        try
        {  
            insert eng;
            
            system.debug('Successful Exe');
        }catch(DMLException ex)
        {
            system.debug('Npp eng'+ex.getDMLMessage(0));
        }
        Role__c role=VM_TestDataFactory.createRole(eng); 
        try
        {  
            insert role;
            
            system.debug('Successful Exe role');
        }catch(DMLException ex)
        {
            system.debug('Npp role '+ex.getDMLMessage(0));
        }
        Account acc=VM_TestDataFactory.createAccountRecord();
 
        Contact con=VM_TestDataFactory.createContactRecord(acc);

        
        Job_Application__c job=VM_TestDataFactory.createJobApplication(role,con); 
        try
        {  
            insert job;
            
            system.debug('Successful Exe job');
        }catch(DMLException ex)
        {
            system.debug('Npp job'+ex.getDMLMessage(0));
        }                                                          
                try
        {  
            job.Status__c='Submitted';
            job.Submitted__c = true;
            update job;
            job.status__c='Volunteer Accepted';
            update job;
            
            system.debug('Successful Update job');
        }catch(DMLException ex)
        {
            system.debug('Npp job'+ex.getDMLMessage(0));
        }       
        Test.startTest();
            Engagement_Volunteer__c engV=new Engagement_Volunteer__c();
            engV=[select Id,Engagement__c,Engagement__r.name,Role_del__c,Role_del__r.name,Contact__c,Contact__r.name from Engagement_Volunteer__c where Engagement__c=:eng.Id];
            PageReference pageRef = Page.MassRecognitionCreationVF;
            pageRef.getParameters().put('eng', String.valueOf(eng.Id));
            Test.setCurrentPage(pageRef);    
            MassRecognitionCreationClass massrec=new MassRecognitionCreationClass();
            system.debug('npp');
            Boolean b=massrec.hasNext;
            b=massrec.hasPrevious;
            Integer i=massrec.pageNumber;
            i=massrec.getTotalPages(); 
            massrec.mydata();
            massrec.first();
            massrec.last();
            massrec.previous();
            massrec.next();
            massrec.mydata();
            MassRecognitionCreationClass.EngagementVolunteerWrapper m=new MassRecognitionCreationClass.EngagementVolunteerWrapper(engV,true);
            massrec.engVolWrapperList.add(m);
            massrec.mydata();
            //massrec.EngagementVolunteerWrapper my=new massrec.EngagementVolunteerWrapper();
            //my=massrec.engVolWrapperList.get(1);
           //List<massrec.EngagementVolunteerWrapper> p=new List<massrec.EngagementVolunteerWrapper>();
           // p= massrec.engVolWrapperList.get(1);
            massrec.goback();              
        Test.stopTest();
    }
    
    public static testMethod void testMassRecognitionErrorCondition() {
         //VM_TestDataFactory_Old o=new VM_TestDataFactory_Old();
        
        User RMOwner=VM_TestDataFactory.createUser('RMTest1@gmail.com','RMTest','Relationship Manager Staff','');
        User MDUSer=VM_TestDataFactory.createUser('MDTest1@gmail.com','MDTest','Outreach Management User','Managing Director');
        User ReportsToUSer=VM_TestDataFactory.createUser('Anne1@gmail.com','Lange','Outreach Management User','Relationship Manager');
        Engagement__c eng=VM_TestDataFactory.createEngagement('t FY20',RMOwner,MDUSer,ReportsToUSer);
        try
        {  
            insert eng;
            
            system.debug('Successful Exe');
        }catch(DMLException ex)
        {
            system.debug('Npp eng'+ex.getDMLMessage(0));
        }
        Role__c role=VM_TestDataFactory.createRole(eng); 
        try
        {  
            insert role;
            
            system.debug('Successful Exe role');
        }catch(DMLException ex)
        {
            system.debug('Npp role '+ex.getDMLMessage(0));
        }
        Account acc=VM_TestDataFactory.createAccountRecord();
 
        Contact con=VM_TestDataFactory.createContactRecord(acc);

        
        Job_Application__c job=VM_TestDataFactory.createJobApplication(role,con); 
        try
        {  
            insert job;
            
            system.debug('Successful Exe job');
        }catch(DMLException ex)
        {
            system.debug('Npp job'+ex.getDMLMessage(0));
        }                                                          
                try
        {  
            job.Status__c='Submitted';
            job.Submitted__c = true;
            update job;
            job.status__c='Volunteer Accepted';
            update job;
            
            system.debug('Successful Update job');
        }catch(DMLException ex)
        {
            system.debug('Npp job'+ex.getDMLMessage(0));
        }       
        Test.startTest();
            Engagement_Volunteer__c engV=new Engagement_Volunteer__c();
            engV=[select Id,Engagement__c,Engagement__r.name,Role_del__c,Role_del__r.name,Contact__c,Contact__r.name from Engagement_Volunteer__c where Engagement__c=:eng.Id];
            PageReference pageRef = Page.MassRecognitionCreationVF;
            pageRef.getParameters().put('eng', String.valueOf(eng.Id));
            Test.setCurrentPage(pageRef);    
            MassRecognitionCreationClass massrec=new MassRecognitionCreationClass();
            system.debug('npp');
            Boolean b=massrec.hasNext;
            b=massrec.hasPrevious;
            Integer i=massrec.pageNumber;
            i=massrec.getTotalPages(); 
            massrec.mydata();
            massrec.first();
            massrec.last();
            massrec.previous();
            massrec.next();
            //massrec.mydata();
            MassRecognitionCreationClass.EngagementVolunteerWrapper m=new MassRecognitionCreationClass.EngagementVolunteerWrapper(engV,true);
            massrec.engVolWrapperList.add(m);
            massrec.rec.Recognition_Type__c='Gift';
            massrec.mydata();
            massrec.goback();
            massrec.checkSelect=true;
            massrec.size=10;
            massrec.noOfRecords=10; 
                       
        Test.stopTest();
    }

    
}
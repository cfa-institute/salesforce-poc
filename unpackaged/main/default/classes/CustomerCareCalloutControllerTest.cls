/**************************************************************************************************
* Apex Class Name   : CustomerCareCalloutControllerTest
* Purpose           : This is the test class which is used for CustomerCareCalloutControlle class.
* Version           : 1.0 Initial Version
* Organization      : Cognizant
* Created Date      : 03-March-2018   
* Modified Date     : 19-July-2018; Lightning Team -- Vijay Vemuru  
***************************************************************************************************/

@isTest
private class CustomerCareCalloutControllerTest {
    
    private static testMethod void testCustomerCareCallouts(){
        
        Contact contact1=CustomerCare_CommonTestData.testCFADataContact();
        system.debug('contact1'+contact1);
        CustomerCare_CommonTestData.createCustomSettingData();
        Test.setCurrentPage(Page.CustomerCare_ContactTabs);
        ApexPages.currentPage().getParameters().put('Id',contact1.Id);
        
        CustomerCareCalloutController qmCall = new CustomerCareCalloutController(new ApexPages.StandardController(contact1));
        
        Test.startTest();  
        
        CC_Callout_MockHttpResponseGenerator PositiveMock = new CC_Callout_MockHttpResponseGenerator(200);
        Test.setMock(HttpCalloutMock.class, PositiveMock);       
        qmCall.ContactEnrollmentCallout(); //Enrollment Services     
        qmCall.ContactExamResultCallout(); //ExamResult Services
        qmCall.ContactScholarshipCallout(); //Scholarship Services
        qmCall.ContactLoginHistoryCallout(); //LoginHistory Services    
        qmCall.ContactMembershipCallout(); //Membership Services  
        qmCall.ContactCustomerHistotyCallout();
        System.assertEquals(200, PositiveMock.code); 
        
       /* try {
            CC_Callout_MockHttpResponseGenerator ExceptionMock = new CC_Callout_MockHttpResponseGenerator(null);
            Test.setMock(HttpCalloutMock.class, ExceptionMock);        
            qmCall.ContactEnrollmentCallout(); //Enrollment Services
            qmCall.ContactExamResultCallout(); //ExamResult Services
            qmCall.ContactScholarshipCallout(); //Scholarship Services
            qmCall.ContactLoginHistoryCallout(); //LoginHistory Services 
        } catch(Exception e) {
            system.debug('Exception e'+e);
        }*/

        CC_Callout_MockHttpResponseGenerator NegativeMock = new CC_Callout_MockHttpResponseGenerator(500);
        Test.setMock(HttpCalloutMock.class, NegativeMock);      
        qmCall.ContactEnrollmentCallout(); //Enrollment Services
        qmCall.ContactExamResultCallout(); //ExamResult Services
        qmCall.ContactScholarshipCallout(); //Scholarship Services
        qmCall.ContactLoginHistoryCallout(); //LoginHistory Services
        qmCall.ContactMembershipCallout(); //Membership Services
        qmCall.ContactCustomerHistotyCallout();
        System.assertEquals(500, NegativeMock.code);    
       
        Test.stopTest();
    }
}
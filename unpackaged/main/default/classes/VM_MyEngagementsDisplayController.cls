/**************************************************************************************************************************************************
Name:  VM_MyEngagementsDisplayController
Copyright © 2018  ITC
=====================================================================
Purpose: 1. Controller Class for VM_MyEngagementsDisplay.cmp                                                                                                
============================================================
History                                                            
-------                                                            
VERSION  AUTHOR         DATE           DETAIL          Description
1.0                  06/07/2018        Created         Created the test class

****************************************************************************************************************************************************/
public class VM_MyEngagementsDisplayController {

    /****************************************************************************************************************************
       Purpose:This method will fetch the details from Engagement_Volunteer__c based on My Engagements filter.                         
       Parameters: noOfRecords
       Returns: No of records for the specific Engagement_Volunteer__c
    
       History                                                            
       --------                                                           
       VERSION  AUTHOR         DATE           DETAIL          Description
       1.0      Nikhil        06/07/2018             
    ******************************************************************************************************************************/ 
    @AuraEnabled
    public static List<Engagement_Volunteer__c> fetchMyEngagementDetails(Integer noOfRecords) {
        
        List<Engagement_Volunteer__c> lstEngagementVolunteers =  [SELECT Current_User__c,
                                                                    Name,Status__c, Show_Volunteer__c 
                                                                  	FROM Engagement_Volunteer__c 
                                                                    WHERE Current_user__c =: 1 
                                                                    AND Status__c =: 'On-boarded'
                                                                    LIMIT : Integer.valueOf(noOfRecords)];
        return lstEngagementVolunteers;
    }
}
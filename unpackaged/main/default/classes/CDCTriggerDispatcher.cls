public inherited sharing class CDCTriggerDispatcher {

    public static final String CREATE_CHANGE_TYPE = 'CREATE';
    public static final String GAP_CREATE_CHANGE_TYPE = 'GAP_CREATE';
    
    public static final String UPDATE_CHANGE_TYPE = 'UPDATE';
    public static final String GAP_UPDATE_CHANGE_TYPE = 'GAP_UPDATE';
    
    public static final String DELETE_CHANGE_TYPE = 'DELETE';
    public static final String GAP_DELETE_CHANGE_TYPE = 'GAP_DELETE';
    
    public static final String UNDELETE_CHANGE_TYPE = 'UNDELETE';
    public static final String GAP_UNDELETE_CHANGE_TYPE = 'GAP_UNDELETE';
    
    public static final String GAP_OVERFLOW_CHANGE_TYPE = 'GAP_OVERFLOW';

    private static Integer maxRecords = 200;

    /**
     * @description Call this method from your trigger, passing in an instance of a trigger handler which implements ICDCTriggerHandler interface, and number of events to process
     * This method will fire the appropriate methods on the handler depending on the trigger context.
     *
     * @param handler ICDCTriggerHandler
     * @param maxRecords Integer
     */
    public static void run(ICDCTriggerHandler handler, Integer maxRecords) {
        CDCTriggerDispatcher.maxRecords = maxRecords;
        run(handler);
    }

    /**
     * @description Call this method from your trigger, passing in an instance of a trigger handler which implements ICDCTriggerHandler interface
     * This method will fire the appropriate methods on the handler depending on the trigger context.
     *
     * @param handler ICDCTriggerHandler
     */
    public static void run(ICDCTriggerHandler handler) {
        // Check to see if the trigger has been disabled. If it has, return
        if (handler.isDisabled()) {
            return;
        }
        
        Map<String, List<sObject>> changeEventsByChangeType = new Map<String, List<sObject>>();

        Integer counter = 0;

        for (sObject event : Trigger.new) {
            EventBus.ChangeEventHeader header = (EventBus.ChangeEventHeader) event.get('ChangeEventHeader');
            String action = header.changetype;

            counter++;

            if (counter > maxRecords) {
                break;  // limit to maxRecords
            }

            if (!changeEventsByChangeType.containsKey(header.changeType)) {
                changeEventsByChangeType.put(header.changeType, new List<sObject>());
            }
            changeEventsByChangeType.get(header.changeType).add(event);

            EventBus.TriggerContext.currentContext().setResumeCheckpoint((String) event.get('ReplayId'));
        }

        try {
            // Create
            if(changeEventsByChangeType.containsKey(CREATE_CHANGE_TYPE)) {
                handler.handleCreate(changeEventsByChangeType.get(CREATE_CHANGE_TYPE));
            }

            if(changeEventsByChangeType.containsKey(GAP_CREATE_CHANGE_TYPE)) {
                handler.handleGAPCreate(changeEventsByChangeType.get(GAP_CREATE_CHANGE_TYPE));
            }

            if(changeEventsByChangeType.containsKey(CREATE_CHANGE_TYPE) || changeEventsByChangeType.containsKey(GAP_CREATE_CHANGE_TYPE)) {
                handler.handleAllCreate(joinLists(new List<List<SObject>> { changeEventsByChangeType.get(CREATE_CHANGE_TYPE), changeEventsByChangeType.get(GAP_CREATE_CHANGE_TYPE) }));
            }

            // Update
            if(changeEventsByChangeType.containsKey(UPDATE_CHANGE_TYPE)) {
                handler.handleUpdate(changeEventsByChangeType.get(UPDATE_CHANGE_TYPE));
            }

            if(changeEventsByChangeType.containsKey(GAP_UPDATE_CHANGE_TYPE)) {
                handler.handleGAPUpdate(changeEventsByChangeType.get(GAP_UPDATE_CHANGE_TYPE));
            }

            if(changeEventsByChangeType.containsKey(UPDATE_CHANGE_TYPE) || changeEventsByChangeType.containsKey(GAP_UPDATE_CHANGE_TYPE)) {
                handler.handleAllUpdate(joinLists(new List<List<SObject>> { changeEventsByChangeType.get(UPDATE_CHANGE_TYPE), changeEventsByChangeType.get(GAP_UPDATE_CHANGE_TYPE) }));
            }

            // Delete
            if(changeEventsByChangeType.containsKey(DELETE_CHANGE_TYPE)) {
                handler.handleDelete(changeEventsByChangeType.get(DELETE_CHANGE_TYPE));
            }

            if(changeEventsByChangeType.containsKey(GAP_DELETE_CHANGE_TYPE)) {
                handler.handleGAPDelete(changeEventsByChangeType.get(GAP_DELETE_CHANGE_TYPE));
            }

            if(changeEventsByChangeType.containsKey(DELETE_CHANGE_TYPE) || changeEventsByChangeType.containsKey(GAP_DELETE_CHANGE_TYPE)) {
                handler.handleAllDelete(joinLists(new List<List<SObject>> { changeEventsByChangeType.get(DELETE_CHANGE_TYPE), changeEventsByChangeType.get(GAP_DELETE_CHANGE_TYPE) }));
            }

            // Undelete
            if(changeEventsByChangeType.containsKey(UNDELETE_CHANGE_TYPE)) {
                handler.handleUnDelete(changeEventsByChangeType.get(UNDELETE_CHANGE_TYPE));
            }

            if(changeEventsByChangeType.containsKey(GAP_UNDELETE_CHANGE_TYPE)) {
                handler.handleGAPUnDelete(changeEventsByChangeType.get(GAP_UNDELETE_CHANGE_TYPE));
            }

            if(changeEventsByChangeType.containsKey(UNDELETE_CHANGE_TYPE) || changeEventsByChangeType.containsKey(GAP_UNDELETE_CHANGE_TYPE)) {
                handler.handleAllUnDelete(joinLists(new List<List<SObject>> { changeEventsByChangeType.get(UNDELETE_CHANGE_TYPE), changeEventsByChangeType.get(GAP_UNDELETE_CHANGE_TYPE) }));
            }

            // Overflow
            if(changeEventsByChangeType.containsKey(GAP_OVERFLOW_CHANGE_TYPE)) {
                handler.handleOverflow(changeEventsByChangeType.get(GAP_OVERFLOW_CHANGE_TYPE));
            }
        } catch (Exception ex) {
            insert CFA_IntegrationLogException.logError(ex, 'CDC Trigger Error', handler.getTriggerName(), DateTime.now(), true, '', '', '', true, '');
        }
    }

    private static List<sObject> joinLists(List<List<sObject>> lists) {
        List<sObject> result = new List<sObject>();
        for(List<sObject> lst : lists) {
            if(lst != null) {
                result.addAll(lst);
            }
        }

        return result;
    }
}
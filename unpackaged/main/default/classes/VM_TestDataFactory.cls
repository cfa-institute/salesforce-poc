/**************************************************************************************************
* Apex Class Name   : VM_TestDataFactoru
* Purpose           : This class is used for all VM Test Classes
* Version           : 3.0 Initial Version
* Organization      : Lightning Team and Cognizant
***************************************************************************************************/

@isTest
global class VM_TestDataFactory{
    public static user createUser(String usrName,String Lname, String ProfileName,String Designation)
    {
        Profile pf = [SELECT Id FROM Profile WHERE Name=: ProfileName];
        User usr = new User();
        usr.Alias = 'TestUser';
        usr.Email = System.now().millisecond() + 'testuser1@testorg.com';
        
        usr.EmailEncodingKey = 'UTF-8';
        usr.LastName = Lname;
        usr.LanguageLocaleKey = 'en_US';
        usr.LocaleSidKey = 'en_US';
        usr.ProfileId = pf.Id;
        usr.Designation__c=Designation;
        usr.TimeZoneSidKey = 'America/Los_Angeles';
        usr.UserName = usrName;
        //usr.isActive=true;
        RetryForTest.upsertOnUnableToLockRow(usr);
        
        return usr;
    }
    
    
    
     public static user createAnotherUser(String usrName,String Lname, String ProfileName,String Designation)
    {
        Profile pf = [SELECT Id FROM Profile WHERE Name=: ProfileName];
        UserRole r=[select Id from UserRole where name='Relationship Manager Staff'];
        User usr = new User();
        usr.Alias = 'TestUser';
        usr.Email = 'testuser1@testorg.com';
        
        usr.EmailEncodingKey = 'UTF-8';
        usr.LastName = Lname;
        usr.LanguageLocaleKey = 'en_US';
        usr.LocaleSidKey = 'en_US';
        usr.ProfileId = pf.Id;
        usr.Designation__c=Designation;
        usr.TimeZoneSidKey = 'America/Los_Angeles';
        usr.UserName = usrName;
        usr.Userrole=r;
        insert usr;
        
        return usr;
    }


    public static Account createAccountRecord()
    {
		 return CFA_TestDataFactory.createAccount('Test Account', null, false);
    }

    public static Contact createContactRecord(Account acc)
    {
         Contact con = new Contact();
         con.lastName= 'testLastName';
         con.AccountId = acc.id;
         con.email='testnew1@gmail.com';
         con.Phone = '0123456789';
        con.GDPR_Consent__c=true;
        con.Contact_Data_Source__c = 'Business Card';
         insert con;
         system.debug('@@@Contact created'+con);
         return con;
    }

    public Static Engagement__c createEngagement(String Ename,User RMOwner,User MDUSer,User ReportsToUSer)
    {
        
        Engagement__c eng=new Engagement__c();
        eng.name='test '+Ename;
        eng.Engagement_Type__c='Committee';
        eng.Start_Date__c=Date.today();
      //eng.Expiration_Date__c=date.parse('12/01/2019');
       eng.Expiration_Date__c=date.today().adddays(20);
       eng.Business_Objective__c='test';
       //eng.ByPassEngagementApproval__c=true;
       eng.Status__c='New';
       eng.Engagement_External_Id__c='123456';
       //eng.Business_Owner__c='0052F000000Se9B';
       eng.Business_Owner__c=RMOwner.Id;
       eng.MD__c=MDUSer.Id;
       eng.Reports_To__c=ReportsToUSer.Id;
       eng.Aligns_to_which_Strategic_Objective__c='Delivery member value';
       eng.Business_Owner_Cost_Center__c='text';
       eng.Estimated_Cost__c=12.345;
       eng.Approved__c=true;
       return eng;    
            
    }
    public Static Role__c createRole(Engagement__c e)
    {
       Role__c role=new Role__c();

       role.Engagement_name__c=e.id;
       role.Position_Title__c='test';
       role.Role_Type__c= 'CIPM Committee Member';
       role.Conflict_filter__c='Writers';
       role.Number_of_Positions__c=123;
       role.Start_date__c=Date.today();
       role.Recruiting_Start_Date__c=Date.today();
       role.Recruiting_End_Date__c=Date.today().addDays(10);
       role.PC_Check_required__c=true;
       role.Confidential__c=true;
       //role.ByPassApproval__c=true;
       role.Volunteer_Impact__c='test';
       role.Volunteer_Work_location__c='test';
       role.Volunteer_Roles_and_Responsibilities__c='test';
       role.Volunteer_Experience__c='test';
       role.Volunteer_Compet__c='Chief Financial Officer (CFO)';
       role.Volunteer_Certifications__c='CFA Charterholder';
       role.Conflicts_Specific_To_The_Role__c='Employment and/or affiliation with prep provider prohibited';
       role.VMApprovalStatus__c=true;
      // role.JARecordType__c ='';
      //  insert role;
       return role;    
            
    }
    
    
    
    public Static List<Role__c> createRoles(Engagement__c e, Integer numberOfRoles){
        
        List<Role__c> lstRoles = new List<Role__c>();
        
        for ( Integer i = 0 ; i < numberOfRoles ; i++ ) {
            
            Role__c objRole = new Role__c(Name = 'TestRole'+i, Engagement_name__c=e.id,Position_Title__c='test',
                                         Role_Type__c='CIPM Committee Member',Conflict_filter__c='Writers',
                                         Number_of_Positions__c=123,Start_date__c=Date.today(),Recruiting_Start_Date__c=Date.today(),
                                         Recruiting_End_Date__c=Date.today().addDays(10),PC_Check_required__c=true, Confidential__c=true,
                                         Volunteer_Impact__c='test', Volunteer_Work_location__c='test', Volunteer_Roles_and_Responsibilities__c='test',
                                         Volunteer_Experience__c='test', Volunteer_Compet__c='Chief Financial Officer (CFO)',
                                         Volunteer_Certifications__c='CFA Charterholder', Conflicts_Specific_To_The_Role__c='Employment and/or affiliation with prep provider prohibited',
                                         VMApprovalStatus__c=true); 
            lstRoles.add(objRole);
        }
        return lstRoles;
        
    }
   
           
    
    public Static Job_Application__c createJobApplication(Role__c role,Contact con)
    {
       Job_Application__c job=new Job_Application__c();
       Schema.DescribeSObjectResult cfrSchemanew = Schema.SObjectType.Job_Application__c; 
       Map<String,Schema.RecordTypeInfo> JobAppliccationRecordTypeInfo = cfrSchemanew.getRecordTypeInfosByName();
       job.recordtypeid=JobAppliccationRecordTypeInfo.get('CIPMCommittee').getRecordTypeId();
       job.Position__c=role.id;
       
       job.Contact__c=con.id;
       job.Status__c='Applied';
       job.Alternate_E_mail_Address__c='test1@test.com';
       job.Professional_Affiliations__c='test';
       job.Volunteered_before__c='Yes';
       job.If_YES_list_experiences__c='Yes';
       job.Disciplinary_Action_Imposed__c='Yes';
       job.Disciplinary_action_Explain__c='test';
       job.What_interests_you_about_this_role__c='test';
       job.How_can_you_contribute_to_this_role__c='test';
       job.Why_are_you_best_for_this_role__c='test';
       job.Have_you_presented_on_these_topics__c='test';
       job.Do_you_have_employer_support__c='Yes';
       
       job.Select_all_competencies__c='Central Banking 10 plus years';
       job.Degree1__c='test';
       job.Degree2__c='test';
       job.Degree3__c='test';
       job.Reference_Name1__c='test';
       job.Reference_Name2__c='test';
       job.Reference_Name3__c='test';
       job.Email1__c='test@test.com';
       job.Email2__c='test@test.com';
       job.Email3__c='test@test.com';
       job.Phone1__c='1234567897';
       job.Phone2__c='1234567897';
       job.Phone3__c='1234567897';
       job.Address1__c='test';
       job.Address2__c='test';
       job.Address3__c='test';
       job.Personal_gain_or_benefit__c='Yes';
       job.Inappropriate_influence__c='Yes';
       job.Relationship_to_Prep_Provider__c='Yes';
       job.Relationship_with_Testing_Organization__c='Yes';
        job.Relationship_with_Candidate__c='Yes';
       job.I_have_read_the_disclosed_conflicts__c=true;
       job.I_Agree_to_the_Conflict_Policy__c=true;
       job.Other_potential_conflicts__c='test';
       
       return job;                
    }
    /* public Static Engagement_Volunteer__c createEngagementVolunteer(Engagement__c e,Role__c r,Contact con)
    {
      Engagement_Volunteer__c engVol=new Engagement_Volunteer__c();
       engVol.Engagement__c=e.id;
       engVol.Role_del__c=r.id;
       engVol.Contact__c=con.id;
       engVol.Status__c='On-boarded';
       return engVol;    
            
    }*/
    
    //Created for JA preview on Role MemberMirco
    
    public Static Job_Application__c createJobApplicationMicro(Role__c role,Contact con)
    {
       Job_Application__c job=new Job_Application__c();
       Schema.DescribeSObjectResult cfrSchemanew = Schema.SObjectType.Job_Application__c; 
       Map<String,Schema.RecordTypeInfo> JobAppliccationRecordTypeInfo = cfrSchemanew.getRecordTypeInfosByName();
       job.recordtypeid=JobAppliccationRecordTypeInfo.get('CIPMCommittee').getRecordTypeId();
       job.Position__c=role.id;
       
       job.Contact__c=con.id;
       job.Status__c='Applied';
       job.Professional_Affiliations__c='Test';
          
       
       return job;                
    }
    
    // demo
    public Static Job_Application__c createJobApplicationDefault(Role__c role,Contact con)
    {
       Job_Application__c job=new Job_Application__c();
       Schema.DescribeSObjectResult cfrSchemanew = Schema.SObjectType.Job_Application__c; 
       Map<String,Schema.RecordTypeInfo> JobAppliccationRecordTypeInfo = cfrSchemanew.getRecordTypeInfosByName();
        system.debug ('JobAppliccationRecordTypeInfo' +JobAppliccationRecordTypeInfo);
       job.recordtypeid=JobAppliccationRecordTypeInfo.get('CIPMCommittee').getRecordTypeId();
       job.Position__c=role.id;
       job.Contact__c=con.id;
        
       job.Status__c='Applied';
       job.Professional_Affiliations__c='Test';
        
          
       
       return job;                
    }
    
     public Static Engagement_Volunteer__c createEngagementVolunteer(Engagement__c e,Role__c r,Contact con)
    {
       Engagement_Volunteer__c engVol=new Engagement_Volunteer__c();
       engVol.Engagement__c=e.id;
       engVol.Role_del__c=r.id;
       engVol.Contact__c=con.id;
       engVol.Status__c='On-boarded';
       return engVol;    
            
    }
    
    public Static List<Engagement_Volunteer__c> createEngagementVolunteers(Engagement__c e,Role__c r,Contact con, Integer noOfVolunteers){
        
       List<Engagement_Volunteer__c> lstVolunteers = new List<Engagement_Volunteer__c>();
        
        for ( Integer i = 0 ; i < noOfVolunteers ; i++ ) {
            
            Engagement_Volunteer__c objVolunteer = new Engagement_Volunteer__c(Engagement__c=e.id, Role_del__c=r.id,
                                                                              Contact__c=con.id,Status__c='On-boarded'); 
            lstVolunteers.add(objVolunteer);
        }
        return lstVolunteers;
        
            
    }
    public static Task createTask(){
        Task tsk = new Task();
        tsk.Subject='Subject';
         tsk.Status='Completed';
        tsk.ActivityDate= date.parse('11/01/2017');
        tsk.Priority='Normal';
        tsk.Start_Date__c=date.parse('12/01/2017');
        tsk.Travel_Hours__c=2;
        tsk.Working_Hours__c=3;
        
        //insert tsk;
        return tsk;
    }
    public static Assessment__c createEngagementAssessment(Engagement__c e){
         Assessment__c Assmnt = new Assessment__c();
        Schema.DescribeSObjectResult cfrSchemanew = Schema.SObjectType.Assessment__c; 
        Map<String,Schema.RecordTypeInfo> AssessmentRecordTypeInfo = cfrSchemanew.getRecordTypeInfosByName();
       Assmnt.recordtypeid=AssessmentRecordTypeInfo.get('Engagement Assessment').getRecordTypeId();
        
       
        Assmnt.Engagement__c=e.id;
        Assmnt.Date__c=date.parse('11/05/2019');
        Assmnt.Did_we_meet_Business_Objective_del__c='Yes';
        return Assmnt;
    }
    
       public static Assessment__c createVolunteerAssessment(Engagement_Volunteer__c v){ 
              Assessment__c Assmnt1 = new Assessment__c();
        Schema.DescribeSObjectResult cfrSchemanew = Schema.SObjectType.Assessment__c; 
        Map<String,Schema.RecordTypeInfo> AssessmentRecordTypeInfo = cfrSchemanew.getRecordTypeInfosByName();
       Assmnt1.recordtypeid=AssessmentRecordTypeInfo.get('Volunteer Assessment').getRecordTypeId();                                           
        
        Assmnt1.Engagement_Volunteer__c=v.id;
        Assmnt1.Suggestions_for_future_roles__c='Suggest';
        Assmnt1.Completion_of_assigned_task__c='Unacceptable';
        Assmnt1.Arriving_prepared_review__c='Unacceptable';
        Assmnt1.Thoroughness_and_attention_to_detail__c='Unacceptable';
        Assmnt1.Active_participation_Reliableattendance__c='Unacceptable';
        Assmnt1.Collaboration_with_other_volunteers__c='Unacceptable';
        Assmnt1.Following_Directions_from_Supervisors__c='Unacceptable';
        
        return Assmnt1;
    }	
    
         
}
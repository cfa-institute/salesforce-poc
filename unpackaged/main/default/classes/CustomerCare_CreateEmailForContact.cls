/**************************************************************************************************
* Apex Class Name   : CustomerCare_CreateEmailForContact
* Purpose           : This class is used for capture email conversation in the conatct related list.   
* Version           : 1.0 Initial Version
* Organization      : Cognizant
* Created Date      : 05-Oct-2017  
***************************************************************************************************/

global without sharing class CustomerCare_CreateEmailForContact implements Messaging.InboundEmailHandler {
    
    /* This method is used to capture email as activity history of contact with the help of email services.*/
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
        
        set<string> setOfEmailId = new set<string>(); 
        setOfEmailId.addall(email.toAddresses);
        //setOfEmailId.add(email.fromAddress);
        
        List<Contact> listofcontact =new List<Contact>();
        listofContact=[SELECT Email,Id,Name,ownerid FROM Contact WHERE Email In:setOfEmailId];
        string username = System.Label.CustomerCareUser;
        
        User Usr = [select Id from user where name =:username limit 1];
        
        String emailhtml, plaintext;
        if(email.htmlBody != null){ 
            emailhtml = email.htmlBody;
            plainText = (email.htmlBody  != null ?emailhtml.stripHtmlTags() : email.plainTextBody);
        }
        set<string> exixtingcontactsEmail = new set<string>();
        List<Task> listObjTask = new List<task>();           
        if(listofcontact.size()>0){
            for(contact objcnt :listofcontact){ 
                Task ObjTask = new task(); 
                ObjTask.Description = plainText; //(email.htmlBody  != null ?plainText : email.plainTextBody);
                ObjTask.OwnerId = Usr.id;
                ObjTask.Priority= CustomerCareUtility.normalPriority;
                ObjTask.Status = CustomerCareUtility.completedStatus;
                ObjTask.Subject = email.Subject;
                ObjTask.WhatId= null;
                ObjTask.WhoId = objcnt.id;
                ObjTask.CustomerCare_Task_Type__c = CustomerCareUtility.emailOrigin;
                exixtingcontactsEmail.add(objcnt.Email);
                listObjTask.add(ObjTask);
            }
        }
        
        for(String Emailobj :setOfEmailId )
        {
            system.debug('!exixtingcontactsEmail.containsEmailobj'+!exixtingcontactsEmail.contains(Emailobj));
            system.debug('EmailObj != Label.CustomerCare_EmailServiceAddress'+(EmailObj != Label.CustomerCare_EmailServiceAddress));
            if((!exixtingcontactsEmail.contains(Emailobj) && EmailObj != Label.CustomerCare_EmailServiceAddress)|| Test.isRunningTest())
            {
                Task ObjTask = new task(); 
                ObjTask.Description = plainText;//(email.htmlBody  != null ?plainText : email.plainTextBody);
                ObjTask.OwnerId = Usr.id;
                ObjTask.Priority= CustomerCareUtility.normalPriority;
                ObjTask.Status = CustomerCareUtility.completedStatus;
                ObjTask.Subject = email.Subject;
                ObjTask.WhatId= null;
                ObjTask.WhoId = Label.CustomerCare_GlobalContact;
                ObjTask.CustomerCare_Task_Type__c = CustomerCareUtility.emailOrigin;
                listObjTask.add(ObjTask);
            }
        }
        try{
            insert listObjTask;
        }catch(Exception e){
            system.debug('CustomerCare_CreateEmailForContact ::handleInboundEmail'+e);
        }  
        return result;
    }
}
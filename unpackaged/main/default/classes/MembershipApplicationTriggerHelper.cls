/*****************************************************************
Name: MembershipApplicationTriggerHelper
Copyright © 2021 ITC
============================================================
Purpose: Helper for MembershipApplicationTrigger
============================================================
History
-------
VERSION  AUTHOR          DATE         DETAIL    Description
1.0      Alona Zubenko   09.02.2021   Created   Assign Society By Membership Application
*****************************************************************/

public with sharing class MembershipApplicationTriggerHelper {

    /**
     * @description Assign Society By Membership Application
     *
     * @param oldMembershipApplications List<Membership_Application__c>
     * @param newMembershipApplications List<Membership_Application__c>
     */
    public static void assignSocietyByMembershipApplication(List<Membership_Application__c> oldMembershipApplications,
            List<Membership_Application__c> newMembershipApplications) {

        ChangedMembershipApplicationsWrapper changedMembershipApplicationsWrapper = findChangedMembershipApplications(oldMembershipApplications, newMembershipApplications);
        if (changedMembershipApplicationsWrapper != null && !changedMembershipApplicationsWrapper.contactIds.isEmpty()) {
            List<INSAPolicyHandler> handlers = new List<INSAPolicyHandler>{
                    new MembershipApplicationPolicyHandler()
            };
            Map<Id, NSA_Policy__c> policies;
            Map<Id, Contact> contacts;

            if (changedMembershipApplicationsWrapper.isDeleted) {
                handlers.add(new AddressPolicyHandler(false));
                Set<String> fieldNames = new Set<String>{
                        'Country_ISO_Code__c', 'State_Province_ISO_Code__c ', 'MailingPostalCode', 'RecordTypeId'
                };
                fieldNames.addAll(NSAPolicyService.getContactSocietyFieldAPINamesFromPolicies());
                contacts = getContacts(changedMembershipApplicationsWrapper.contactIds, new List<String>(fieldNames));

                //policies to remove old visibility
                policies = getPolicies(null, changedMembershipApplicationsWrapper.accIds);

                //uncheck old Society fields without updating in DB
                clearExistingCFAFlags(policies.values(), new Map<Id, Contact> (contacts), oldMembershipApplications);

                //policies for AddressPolicyHandler
                policies = getPolicies(contacts.values(), changedMembershipApplicationsWrapper.accIds);

            } else {
                contacts = getContacts(changedMembershipApplicationsWrapper.contactIds, null);
                policies = getPolicies(null, changedMembershipApplicationsWrapper.accIds);
            }

            runPolicy(policies, contacts, handlers);
        }
    }

    //*********************** PRIVATE *****************************//

    /**
     * @description Run Policy
     *
     * @param policyMap Map<Id,NSA_Policy__c>
     * @param contactMap Map<Id,Contact>
     * @param handlers List<INSAPolicyHandler
     */
    private static void runPolicy(Map<Id, NSA_Policy__c> policyMap, Map<Id, Contact> contactMap, List<INSAPolicyHandler> handlers) {
        NSAPolicyRunner policyRunner = new NSAPolicyRunner(policyMap.values());
        policyRunner.run(contactMap.values(), handlers, true);
    }

    /**
     * @description Find Changed Membership Applications
     *
     * @param oldMembershipApplications List<Membership_Application__c>
     * @param newMembershipApplications List<Membership_Application__c>
     *
     * @return ChangedMembershipApplicationsWrapper
     */
    private static ChangedMembershipApplicationsWrapper findChangedMembershipApplications(List<Membership_Application__c> oldMembershipApplications,
            List<Membership_Application__c> newMembershipApplications) {

        if (oldMembershipApplications == null && newMembershipApplications == null) {
            return null;
        }
        ChangedMembershipApplicationsWrapper changedData = new ChangedMembershipApplicationsWrapper();
        changedData.isDeleted = (newMembershipApplications == null) && (oldMembershipApplications != null);

        Set<Id> contactIds = new Set<Id>();
        Set<Id> accIds = new Set<Id>();

        List<Membership_Application__c> membershipApplications;
        if (newMembershipApplications == null && oldMembershipApplications != null) {
            membershipApplications = oldMembershipApplications;
        } else if (newMembershipApplications != null && oldMembershipApplications == null) {
            membershipApplications = newMembershipApplications;
        }

        if(membershipApplications == null){
            Integer i = 0;
            for(Membership_Application__c memberApp : newMembershipApplications){
                if(oldMembershipApplications[i].Date_Submitted__c != newMembershipApplications[i].Date_Submitted__c){
                    contactIds.add(memberApp.Contact__c);
                    accIds.add(memberApp.Account__c);
                }
                i++;
            }
        } else {
            for (Membership_Application__c membershipApplication : membershipApplications) {
                contactIds.add(membershipApplication.Contact__c);
                accIds.add(membershipApplication.Account__c);
            }
        }

        changedData.accIds = accIds;
        changedData.contactIds = contactIds;
        return changedData;
    }

    /**
     * @description Clear Old Existing CFA Flags on the Contact
     *
     * @param policies List<NSA_Policy__c>
     * @param contactMap Map<Id, Contact>
     * @param oldMembershipApplications List<Membership_Application__c>
     */
    private static void clearExistingCFAFlags(List<NSA_Policy__c> policies, Map<Id, Contact> contactMap, List<Membership_Application__c> oldMembershipApplications) {
        Map<Id, List<String>> contactSocietyApiNameFieldBySocietyAccountIds = new Map<Id, List<String>>();

        for (NSA_Policy__c policy : policies) {
            if (policy != null) {
                if (!contactSocietyApiNameFieldBySocietyAccountIds.containsKey(policy.Society_Account__c)) {
                    contactSocietyApiNameFieldBySocietyAccountIds.put(policy.Society_Account__c, new List<String>());
                }
                contactSocietyApiNameFieldBySocietyAccountIds.get(policy.Society_Account__c).add(policy.Contact_Society_Field_API_Name__c);
            }
        }

        for (Membership_Application__c membershipApplication : oldMembershipApplications) {
            if (contactSocietyApiNameFieldBySocietyAccountIds.containsKey(membershipApplication.Account__c)) {
                Contact con = contactMap.get(membershipApplication.Contact__c);
                for (String societyField : contactSocietyApiNameFieldBySocietyAccountIds.get(membershipApplication.Account__c)) {
                    con.put(societyField, false);
                }
            }
        }
    }

    /**
     * @description Get Policies
     *
     * @param contacts List<Contact>
     * @param accIds Set<Id>
     *
     * @return List<NSA_Policy__c>
     */
    private static Map<Id, NSA_Policy__c> getPolicies(List<Contact> contacts, Set<Id> accIds) {
        Map<Id, NSA_Policy__c> policies;
        if (contacts == null) {
            policies = new Map<Id, NSA_Policy__c>([
                    SELECT Id
                            , Contact_Society_Field_API_Name__c
                            , Advanced_Assignment__c
                            , Advanced_Assignment_Handler__c
                            , Society_Account__c
                    FROM NSA_Policy__c
                    WHERE Society_Account__c IN :accIds
                    AND Is_Active__c = TRUE
                	AND Policy_Status__c = 'Approved'
            ]);
        } else {
            Set<String> states = new Set<String>();
            Set<String> countries = new Set<String>();

            for (Contact con : contacts) {
                states.add(con.State_Province_ISO_Code__c );
                countries.add(con.Country_ISO_Code__c);
            }
            policies = new Map<Id, NSA_Policy__c>([
                    SELECT Id
                            , Advanced_Assignment__c
                            , Advanced_Assignment_Handler__c
                            , Society_Account__c
                            , Contact_Society_Field_API_Name__c
                            , (
                            SELECT Country_Name__c
                                    , State__c
                                    , Zip_Code_High__c
                                    , Zip_Code_Low__c
                                    , NSA_Policy__c
                            FROM NSA_Geograhpic_Rules__r
                            WHERE Country_Name__c IN :countries
                            AND (State__c IN :states OR State__c = NULL)
                    )
                    FROM NSA_Policy__c
                    WHERE Id IN (
                            SELECT NSA_Policy__c
                            FROM NSA_Geographic_Rule__c
                            WHERE Country_Name__c IN :countries
                            AND (State__c IN :states OR State__c = NULL)
                    )
                    AND Society_Account__c IN :accIds
                    AND Is_Active__c = TRUE
                	AND Policy_Status__c = 'Approved'
            ]);
        }
        return policies;
    }

    /**
     * @description Get Contacts
     *
     * @param contactIds Set<Id>
     * @param fieldNames List<String>
     *
     * @return List<Contact>
     */
    private static Map<Id, Contact> getContacts(Set<Id> contactIds, List<String> fieldNames) {
        String query = ' SELECT Id ';
        if (fieldNames != null) {
            query += ',' + String.join(fieldNames, ',');
            query.substring(0, query.length() - 1);
        }
        query += ' FROM Contact WHERE Id IN :contactIds';
        return new Map<Id, Contact> ((List<Contact>) Database.query(query));
    }

    // *********************** WRAPPERS ***************************//

    class ChangedMembershipApplicationsWrapper {
        public Boolean isDeleted;
        public Set<Id> accIds;
        public Set<Id> contactIds;
    }
}
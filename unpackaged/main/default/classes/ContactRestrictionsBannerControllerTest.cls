@isTest
private class ContactRestrictionsBannerControllerTest {

    @TestSetup
    static void setup() {
        Account acc = new Account(Name = 'Test');
        insert acc;

        Contact contact = new Contact(AccountId = acc.Id, LastName = 'LName');
        insert contact;

        List<Restriction__c> restrictions = new List<Restriction__c>();
        restrictions.add(new Restriction__c(Contact__c = contact.Id, Active__c = true, Type__c='EA&S'));
        restrictions.add(new Restriction__c(Contact__c = contact.Id, Active__c = false, Type__c='EA&S'));
        insert restrictions;
    }

    @isTest
    static void testGetContactActiveRestrictions() {
        Contact contact = [SELECT Id FROM Contact LIMIT 1];
        Test.startTest();
        List<Restriction__c> result = ContactRestrictionsBannerController.getContactActiveRestrictions(contact.Id);
        Test.stopTest();
        System.assertEquals(1, result.size(), 'Only 1 active restriction is present for contact');
    }
}
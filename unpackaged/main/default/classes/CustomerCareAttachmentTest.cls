@isTest
public class CustomerCareAttachmentTest {

        static testMethod void testEmailMessage() {
            Test.startTest();
             boolean isSB =[SELECT IsSandbox FROM Organization LIMIT 1].IsSandbox;
              SocietyOps_Backoffice__c obj = new SocietyOps_Backoffice__c(name='trigger-details',CaseReassignment_TemplateName__c='SocietyOps-Case Reassignment',CaseComment_TemplateName__c='SocietyOps-New Comment Notification');
        if(isSB==true)
        {
            obj.No_reply_email__c='researchchallenge@cfainstitute.org';
         }
        else
        {
            obj.No_reply_email__c='noreply.societyoperations@cfainstitute.org';
         }
        insert obj;
            Id CCRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('CustomerCare Front Office Record Type').getRecordTypeId();
            Case emailCase = new Case(Status = 'New', Origin = 'Email',RecordTypeId=CCRecordType, Priority='Medium', Subject = 'TestEmailTOCaseVerification22', Description='Testing Email To case222, CFA Institute Membership 47856985256314');
            insert emailCase;   
            
            EmailMessage newEmail = new EmailMessage();
            newEmail.FromAddress = 'test5555555555@abc.org';
            newEmail.Incoming = True;
            newEmail.ToAddress= 'hello55555555@670ocglw7xh4oyr5yw2zvf.8kp7yeag.8.case.salesforce.com';
            newEmail.Subject = 'Test email';
            newEmail.TextBody = '23456';
            newEmail.ParentId = emailCase.Id;            
            insert newEmail;
            newEmail.Approver_status__c = 'Approved';
            update newEmail;
            
            Attachment attach=new Attachment();     
            attach.Name='Unit Test Attachmentcfa';
            Blob bodyBlob=Blob.valueOf('Unit Test Attachment Bodycfa');
            attach.body=bodyBlob;
            attach.parentId=newEmail.id;
            insert attach;
            Test.stopTest();
        }
}
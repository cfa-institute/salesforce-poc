public with sharing class CFA_ContentVersionTriggerController implements ITriggerHandler {
    public static Boolean TriggerDisabled = false;
 
    /*
        Checks to see if the trigger has been disabled either by custom setting or by running code
    */
    public Boolean IsDisabled()
    {
        if (Test.isRunningTest()) {
            return TriggerDisabled;
        } else {
            return DynamicTriggerStatus.getTriggerStatus('ContentVersionTrigger') || triggerDisabled;
        }
    }



    public void BeforeInsert(List<SObject> newItems) {}
 
    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {}
 
    public void BeforeDelete(Map<Id, SObject> oldItems) {}
 
    public void AfterInsert(Map<Id, SObject> newItems) {
        List<ContentVersion> newList = newItems.values();
        CFA_ContentVersionTriggerHandler.createContentDocumentLink(newList);
    }
 
    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {}
 
    public void AfterDelete(Map<Id, SObject> oldItems) {}
 
    public void AfterUndelete(Map<Id, SObject> oldItems) {}

}
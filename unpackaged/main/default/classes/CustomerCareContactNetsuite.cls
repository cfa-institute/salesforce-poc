/**************************************************************************************************
* Apex Class Name   : CustomerCareContactNetsuite
* Purpose           : This class allows to open Netsuite app from contact page  
* Version           : 1.0 Initial Version
* Organization      : Cognizant
* Created Date      : 20-Mar-2018   
***************************************************************************************************/

global with sharing class CustomerCareContactNetsuite{
     
    private CustomerCareContactNetsuite(){}
    
    webservice static String getpersonId(List<ID> contactids){
        String PartnerID;
        String NetSuiteLink;
        Contact ContactPartnerId= [Select id,Partner_Id__c from contact where Id IN :ContactIds limit 1];
        PartnerID = ContactPartnerId.Partner_Id__c;
        
        if(String.isNotBlank(PartnerID)){
            NetSuiteLink=System.Label.NetSuiteLink+PartnerID;
            return NetSuiteLink;
        }else{
            String strNotFound;
            strNotFound = 'No Partner id present for this contact';
            return strNotFound;
        }
    } 
}
public class VM_ApplicationStatus_Controller {
    @AuraEnabled
    public static String updateStatus(Id jobId,string ButtClick){
        String current_user=UserInfo.getUserId();
        string returnvalue = 'abc';
        User u=[SELECT Id, Name,contactid FROM User  WHERE Id = :userinfo.getUserId() limit 1];
        Job_Application__c jobapplication = new  Job_Application__c ();
        
        jobapplication=[Select ID,Status__c,I_agree_that_this_information_is_correct__c,I_ve_read_and_understand_the_role_des__c,
                        I_ve_Read_and_accept_Volunteer_Agreement__c from Job_Application__c where ID =:jobId ];
        //a2q2F000000Bvfx
        // and Contact__c =:u.contactid];  
        
        if(jobapplication!=null)
        {
           
        	if(ButtClick == 'Agree')
            {
                
             	jobapplication.Status__c='Volunteer Accepted'; 
                update jobapplication;
                //return 'Volunteer Accepted';
                returnvalue = 'Volunteer Accepted';
            }
            else if(ButtClick == 'Reject')
            {
            	 jobapplication.Status__c='Volunteer Rejected';
                 update jobapplication;
                 //return 'Volunteer Rejected';
                 returnvalue = 'Volunteer Rejected';
            }
            else if(jobapplication.I_agree_that_this_information_is_correct__c== false ||
              jobapplication.I_ve_read_and_understand_the_role_des__c==false ||
              jobapplication.I_ve_Read_and_accept_Volunteer_Agreement__c==false){
                returnvalue='Error121';
              // system.debug('abcd'); 
                
            }
            
            system.debug('returnvalue:'+returnvalue);
            return(returnvalue);
        }
        return (returnvalue);
    }   
    
    @AuraEnabled
        
         public static String getacknowledgeform(){
             Document dr=[Select ID from Document where Name='Volunteer Acknowledgement Form'];
      //  StaticResource sr = [Select body, name from StaticResource where Name = 'Volunteer_acknowledge_form'];
        	String Customlabel = Label.VM_Policydocument;
             String myurl= Customlabel+'/servlet/servlet.FileDownload?file='+dr.id;
        	return myurl;
         }
    
         @AuraEnabled
    	public static String getStatus(Id jobId){
        String current_userinfo=UserInfo.getUserId();
        string returnstatus = 'abc';
        User u=[SELECT Id, Name,contactid FROM User  WHERE Id = :userinfo.getUserId() limit 1];
        Job_Application__c jobapplication = new  Job_Application__c ();
        
        jobapplication=[Select ID,Status__c from Job_Application__c where ID =:jobId];
            //'a2q2F000000Bvfx'
            // and Contact__c =:u.contactid];
        
        if(jobapplication!=null)
        {
        	if(jobapplication.Status__c == 'Accepted and Invited')
            {
               
                returnstatus = 'True';
            }
            else
            {
            	 returnstatus = 'False';
            }
            
            system.debug('returnvalue:'+returnstatus);
            return(returnstatus);
        }
        return (returnstatus);
        //System.debug('dsafac'+current_user);
     // return current_user;
       
    }
    @AuraEnabled
    public static void getCheckbox(Id jobId, Job_Application__c jobRec){
        system.debug('**job ** '+jobRec);
        
        List<Job_Application__c> joblist = [Select id From Job_Application__c Where Id = :jobId ];
        List<Job_Application__c> jobRecToBeUpdated = new List<Job_Application__c>();
        
        if(joblist.size() > 0){
            for(Job_Application__c job: joblist){
                job.I_agree_that_this_information_is_correct__c = jobRec.I_agree_that_this_information_is_correct__c;
                job.I_ve_read_and_understand_the_role_des__c = jobRec.I_ve_read_and_understand_the_role_des__c;
                job.I_ve_Read_and_accept_Volunteer_Agreement__c = jobRec.I_ve_Read_and_accept_Volunteer_Agreement__c;
                job.Status__c='Volunteer Accepted';
                
                jobRecToBeUpdated.add(job);
            }  
            
        }
        if(jobRecToBeUpdated.size() > 0){
            update joblist;
            
             
        }  
    }
    @AuraEnabled
    public static String getROle(Id jobId){
        system.debug('Inside getRole===');
        Job_Application__c job=[Select Position__c from Job_Application__c where id =:jobId];
        system.debug('job'+job);
        return job.Position__c;
    }
 }
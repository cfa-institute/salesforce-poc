/**
 * Created by bsebl on 4/22/2020.
 */

trigger CommunityUserTrigger on User (after insert, after update) {
    System.debug('in community user trigger');
    TriggerDispatcher.Run(new CFA_CommunityUserTriggerHandler()); 
}
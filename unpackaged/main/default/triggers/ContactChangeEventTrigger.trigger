trigger ContactChangeEventTrigger on ContactChangeEvent (after insert) {
    // Passing handler to run method of CDCTriggerDispatcher class
    CDCTriggerDispatcher.run(new ContactChangeEventTriggerHandler());
}
/*****************************************************************
Name: CampaignMemberTrigger
Copyright © 2020 ITC
============================================================
Purpose:
============================================================
History
-------
VERSION  AUTHOR          DATE         DETAIL    Description
1.0      Alona Zubenko   24.11.2020   Created   CSR:
*****************************************************************/

trigger CampaignMemberTrigger on CampaignMember (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    // Passing handler to run method of TriggerDispatcher class
    TriggerDispatcher.Run(new CampaignMemberTriggerHelper());
}
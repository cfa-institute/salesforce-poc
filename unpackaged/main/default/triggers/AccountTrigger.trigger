trigger AccountTrigger on Account (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    // Passing handler to run method of TriggerDispatcher class
    TriggerDispatcher.Run(new AccountTriggerHandler());
}
/************************************************************************************************** 
* Trigger Name      : LiveChatTranscriptTrigger
* Purpose           : This trigger is used for updating case owner while transferring live chat.   
* Version           : 1.0 Initial Version
* Organization      : Cognizant
* Created Date      : 07-Nov-2017  
***************************************************************************************************/
trigger LiveChatTranscriptTrigger on LiveChatTranscript (after Insert,after update) {
    
    if (trigger.isAfter){
        
        if (trigger.isInsert){ 
            CustomerCare_Transcript_TriggerHandler.ccUpdateCaseowner(Trigger.new);
        }
        if (trigger.isUpdate){ 
            CustomerCare_Transcript_TriggerHandler.ccUpdateCaseowner(Trigger.new);
        }
    }
}
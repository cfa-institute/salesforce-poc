/**************************************************************************************************************************************************
Name:  MDM_Affiliation_RecordTypeTrigger
Copyright © 2017  ITC
=====================================================================
Purpose: 1. If a new Affiliation is inserted before this trigger fires and calls its handler MDM_Affiliation_TriggerHandler.
         2.If an already existing Affiliation is updated before this trigger fires and calls its handler MDM_Affiliation_TriggerHandler.
============================================================
History                                                            
-------                                                            
VERSION  AUTHOR         DATE           DETAIL          Description
1.0      Sidhant        11/9/2017     Created         This Trigger is fired before an Affiliation is inserted or updated
2.0      Sidhant        12/12/2017    Updated         Upadated for Before update method.
3.0      Anupama        01/09/2018    Updated         Updated with After insert and Update methods.
****************************************************************************************************************************************************/
trigger MDM_Affiliation_RecordTypeTrigger on Affiliation__c (Before Insert,Before Update,After Insert, After Update) {
    //Making an object of this triggerhandler
    MDM_Affiliation_TriggerHandler handler= new MDM_Affiliation_TriggerHandler();
    
    if((Trigger.isInsert||Trigger.isUpdate) && Trigger.isBefore)
        handler.onBeforeInsertUpdate(Trigger.new);
        
}
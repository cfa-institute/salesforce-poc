/************************************************************************************************** 
* Trigger Name      : TaskTrigger
* Purpose           : This trigger is used for providing sharing case access to task asssigned user  
* Organization      : MTX
* Created Date      : 01/17/2020
***************************************************************************************************/
trigger TaskTrigger on Task (before insert, before update, before delete, after insert, after update, after delete, after undelete)
{
    TriggerDispatcher.Run(new TaskTriggerHandler());
}
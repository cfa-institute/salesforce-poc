trigger Affiliation_Provisioning_Trigger on Affiliation__c (before insert, before update) {

    if(Trigger.isBefore && (Trigger.isInsert || Trigger.isUpdate)){
        
      Affiliation_Provisioning_TriggerHandler APT = new Affiliation_Provisioning_TriggerHandler();
        APT.onBeforeInsertUpdate(Trigger.new);
    }
}
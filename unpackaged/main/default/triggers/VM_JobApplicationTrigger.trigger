trigger VM_JobApplicationTrigger on Job_Application__c(after delete, after insert, after undelete, after update, before delete, before insert, before update) {

    VM_JobApplicationHandler handler = new VM_JobApplicationHandler();

    /* Before Insert 
    if(Trigger.isInsert && Trigger.isBefore){
        //handler.OnBeforeInsert(Trigger.new);
       //handler.OnBeforeInsert(Trigger.new);
    }
     After Insert 
    else if(Trigger.isInsert && Trigger.isAfter){
        //handler.OnAfterInsert(Trigger.new);
    }
    Before Update 
    else if(Trigger.isUpdate && Trigger.isBefore){
        
            //handler.OnBeforeUpdate(Trigger.old, Trigger.new, Trigger.newMap);
             
    }
    After Update */
     if(Trigger.isUpdate && Trigger.isAfter){
       
       // if(VM_checkRecursive.runOnce())
       // {
    handler.OnAfterUpdate(Trigger.old, Trigger.new, Trigger.oldMap);
        //handler.OnAfterUpdate(Trigger.old, Trigger.new, Trigger.oldMap);
        //handler.OnAfterUpdate(Trigger.new);
       // }
    }
    /* Before Delete 
    else if(Trigger.isDelete && Trigger.isBefore){
        //handler.OnBeforeDelete(Trigger.old, Trigger.oldMap);
    }
     After Delete 
    else if(Trigger.isDelete && Trigger.isAfter){
        //handler.OnAfterDelete(Trigger.old, Trigger.oldMap);
    }

     After Undelete 
    else if(Trigger.isUnDelete){
        //handler.OnUndelete(Trigger.new);
    }*/

}
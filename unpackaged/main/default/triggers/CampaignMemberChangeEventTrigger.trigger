/*****************************************************************
Name: CampaignMemberChangeEventTrigger
Copyright © 2020 ITC
============================================================
Purpose:  Handle events on insert the CampaignMemberChangeEvent records
============================================================
History
-------
VERSION  AUTHOR          DATE         DETAIL    Description
1.0      Alona Zubenko   17.11.2020   Created   Trigger for Campaign Member Change Events
*****************************************************************/

trigger CampaignMemberChangeEventTrigger on CampaignMemberChangeEvent (after insert) {
    // Passing handler to run method of CDCTriggerDispatcher class
    CDCTriggerDispatcher.run(new CampaignMemberChangeEventTriggerHandler());
}
/**
   * Author: Abhijeet Upadhyay
   * Description: Purpose of this trigger is to provide access based on confidential or non-confidential role and implementing
   * validation rule to check whether an assessment should be there for an engagement.              
   * Date Created: 18-Sept-2017
   * Version: 0.4
   */
trigger VM_EngagementVolunteerTrigger on Engagement_Volunteer__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {

    // Checks to see if the trigger has been disabled by custom metadata
    if(DynamicTriggerStatus.getTriggerStatus('EngagementVolunteerTrigger')) {
        return; 
    }

    //handler class
    VM_TriggerHelperEngagementVolunteer helper = new VM_TriggerHelperEngagementVolunteer(Trigger.isExecuting, Trigger.size);

    /* Before Insert */
    if(Trigger.isInsert && Trigger.isBefore){
        //handler.OnBeforeInsert(Trigger.new);
    }
    /* After Insert */
    else if(Trigger.isInsert && Trigger.isAfter){
        helper.OnAfterInsert(Trigger.new);
        //helper.updateEngagementVolunteerShare( trigger.new, null);
    }
    /* Before Update */
    else if(Trigger.isUpdate && Trigger.isBefore){
        helper.OnBeforeUpdate(Trigger.new);
    }
    /* After Update */
    else if(Trigger.isUpdate && Trigger.isAfter){
        helper.OnAfterUpdate(Trigger.old,Trigger.new,Trigger.newMap,Trigger.oldMap);
        //helper.updateEngagementVolunteerShare( trigger.new, trigger.oldMap);
    }
}
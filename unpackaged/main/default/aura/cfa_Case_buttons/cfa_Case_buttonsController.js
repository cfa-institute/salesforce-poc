({
    doInit : function(component, event, helper) {
        
    },
    cloneCase : function(component, event, helper) {
    	var evt = $A.get("e.force:navigateToComponent");
        evt.setParams({
            componentDef : "c:cfa_cloneCaseExtender",
            componentAttributes: {
                recId : component.get("v.recordId")
            }
        });
        evt.fire();
    }
})
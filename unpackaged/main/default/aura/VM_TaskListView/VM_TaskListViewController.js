({
    doInit : function(component, event, helper) {
        var action = component.get("c.getListTasks");
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                var listTasks = response.getReturnValue(); 
                component.set("v.tasks",listTasks);
            }
        });
        $A.enqueueAction(action);
        
        var action2 = component.get("c.getInvitedEngVol");
        action2.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                var invitedEngVolList = response.getReturnValue(); 
                component.set("v.EngVol",invitedEngVolList);
                console.log('_____response_____'+invitedEngVolList);
            }
        });
        $A.enqueueAction(action2);
    },
    handleClick : function(component, event, helper) {
        var staticLabel = $A.get("$Label.c.VM_CommunityHomepage");
        var finalStaticLabel = staticLabel.slice(0, -2);
        var taskId=event.target.id;
        window.location.href=finalStaticLabel+taskId;
    },
    handleViewAllClick: function(component, event, helper) {
        var staticLabel = $A.get("$Label.c.VM_CommunityHomepage");
        window.location.href=staticLabel+"task/Task/OPEN";
    },
    handleViewEngClick: function(component, event, helper) {
        var staticLabel = $A.get("$Label.c.VM_CommunityHomepage");
        var finalStaticLabel = staticLabel.slice(0, -2);
        var engId=event.target.id;
        window.location.href=finalStaticLabel+engId;
    },
    
})
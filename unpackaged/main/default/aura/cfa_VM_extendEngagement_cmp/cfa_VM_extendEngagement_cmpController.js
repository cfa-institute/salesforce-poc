({
    doInit : function(component, event, helper) {
        console.log(component.get('v.recId'));
        var action = component.get("c.getIconName");
        
        action.setParams({ sObjectName : 'Engagement__c' });
        
        // A callback that is executed after 
        // the server-side action returns 
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                // Response From Server 
                component.set('v.ProjectIcon',response.getReturnValue());
                
                 var action1 = component.get("c.getInitDetails");
        
        		action1.setParams({ engagementId : component.get('v.recId')});
                action1.setCallback(this, function(resp) {
                	 var state = response.getState();
            		if (state === "SUCCESS") {
                        component.set('v.initWrap',resp.getReturnValue());
                    }
                });  
                 $A.enqueueAction(action1);
            }
        });
        $A.enqueueAction(action);
    },
    handleOnLoad: function (component, event, helper) {
        console.log(component.get('v.recId'));
        var objectInfo = event.getParams().objectInfos;
        
        var eventFields = event.getParam("fields");
        console.log(JSON.stringify(objectInfo));
        
    },
    handleCancel: function (component, event, helper) {
         var cntId = component.get('v.recId');
         window.location.replace('/'+component.get("v.recId"),"_self");        
    },
    saveRec: function(component,event,helper){
        var eventParams = event.getParams();
        var eventFields = event.getParam("fields");
        eventFields["Id"] = "";
        var field = 'Start_Date__c';
        var validationCheck = true;
        var isVF = component.get('v.isVF');
       
        event.preventDefault();
        console.log(JSON.stringify(eventParams));
        if(validationCheck) {
        var action = component.get("c.saveEngagement");
        
        action.setParams({ json : JSON.stringify(eventParams),
                           engId : component.get('v.recId')});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                // Response From Server 
                if(response.getReturnValue() !=null) {
                    if(response.getReturnValue().substring(0,3) == 'a2s') {
                        if(isVF) {
                            window.location.replace('/'+response.getReturnValue(),"_self");
                        } else {
                            var navEvt = $A.get("e.force:navigateToSObject");
                            navEvt.setParams({
                                "recordId": response.getReturnValue(),
                                "slideDevName": "details"
                            });
                            navEvt.fire();
                        }
                   
                    } else {
                        if(isVF) {
                            alert(response.getReturnValue());
                        } else {
                            var toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams({
                                "title": "Error!!!",
                                "message": response.getReturnValue(),
                                "type": "error"
                            });
                            toastEvent.fire();
                        }
                    }
                    
                }
            }
        });
        $A.enqueueAction(action);
        }
        
    },
    success: function(component,event,helper){
        
    }
})
({
    doInit : function(component, event, helper) {
        var action1 = component.get("c.getContactCompetencies");
        action1.setCallback(this, function(response) {
            component.set("v.Competencies",response.getReturnValue());            
        });
        $A.enqueueAction(action1);        
        
        var action2 = component.get("c.getAvailabilityStatus");
        action2.setCallback(this, function(response1) {
            component.set("v.AvailabilityStatus",response1.getReturnValue());                        
        });
        $A.enqueueAction(action2);
        
        var action3 = component.get("c.getdetails");
        action3.setCallback(this, function(response) {
            component.set("v.Volpro",response.getReturnValue());            
        });
        $A.enqueueAction(action3);
        
    },
    
    onclick : function(component, event, helper) {
        document.getElementById('profile_div1').style.display='block';
        document.getElementById('profile_div2').style.display='none'
        component.set("v.BrandVPbutt","Brand"); 
        component.set("v.BrandCFAbutt","neutral");
        var action = component.get("c.getContactCompetencies");
        var inputsel = component.find("InputSelectDynamic");
        var opts=[];
        action.setCallback(this, function(response) {
            for(var i=0;i< response.getReturnValue().length;i++){
                opts.push({"class": "optionClass", label: response.getReturnValue()[i], value: response.getReturnValue()[i]});
            }
            inputsel.set("v.options", opts);
            
        });
        
        
        $A.enqueueAction(action);         
    },
    CFAprof: function(component, event, helper) {
        
        document.getElementById('profile_div2').style.display='block';
        document.getElementById('profile_div1').style.display='none'
        component.set("v.BrandVPbutt","neutral");
        component.set("v.BrandCFAbutt","Brand");  
        var action=component.get("c.getdetails");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                
                component.set("v.Volpro",response.getReturnValue());
                //console.log('The accs are :'+JSON.stringify(a.getReturnValue()));
                if(response.getReturnValue()=="True"){
                }
            }
        }); 
        $A.enqueueAction(action);
    },   
    resizeCanvas:function resize() { 
        Sfdc.canvas(function() {
            sr = JSON.parse('<%=signedRequestJson%>');
            Sfdc.canvas.client.autogrow(sr.client);
        });
     }, 
    saveApplication: function(component, event, helper){
        component.set("v.isOpen1", true);    
        var contact = component.get("v.Volpro");
        var action = component.get("c.getcontact");
        action.setParams({ 
            "contact": contact
        });
        action.setCallback(this, function(a) {
            var state = a.getState();
            //alert(state);
           // alert("Record Updated Successfully");
            if (state === "SUCCESS") {
               // var name = a.getReturnValue();   TDIXIT 29-01-2018 SonarQube comment - commented as not being used anywhere.
            }
        });
        
        $A.enqueueAction(action)    
    },
   
    
   /* CFAprof1:function(component, event, helper){
        var urlEvent = $A.get("e.force:navigateToURL");
            urlEvent.setParams({
                "url":"https://cfainstitute--devcfacogn--c.cs91.visual.force.com/apex/ProfileAppCanvas"
            });
            urlEvent.fire();
    } ,*/
	
	updateprofile : function(component, event, helper){
		let updateProfile = $A.get("$Label.c.VM_Update_CFA_Profile");
		window.open(updateProfile);
	},
	
    
   /* <!--showModal : function(component, event, helper) {
       
        //window.open("https://dev4extlogin.cfainstitute.org/LoginApplication/Login", '_blank');
        document.getElementById("backGroundSectionId").style.display = "block";
      document.getElementById("newAccountSectionId").style.display = "block";
        
    
   var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
               //window.open("https://dev4extlogin.cfainstitute.org/LoginApplication/Login", '_blank');
              "url":"https://cfainstitute--devcfacogn--c.cs91.visual.force.com/apex/ProfileAppCanvas"
            });
            urlEvent.fire();
    
    
    
    
    }, -- > */
    
    //Closes the Modal
    closeModal : function(component, event, helper) {
        document.getElementById("backGroundSectionId").style.display = "none";
        document.getElementById("newAccountSectionId").style.display = "none";
        window.location.reload();
    },
    closeModal1 : function(component, event, helper) {
        
         component.set("v.isOpen1",false);   
    }

})
({
    
    doInit:  function(component, event, helper){
        console.log('I am called');
        console.log('isclone');
        
        
        console.log(window.location.href);
        
        var url = window.location.href;
        
        if(url.includes("isClone=true")){
            component.set('v.isClone',true);
            component.set('v.showApply',true);
            console.log(component.get('v.isClone'));
            
        }
        
        var n = url.lastIndexOf('=');
        console.log(n);
        if(n != null && n>0 && url.includes('VolunteerId=a2r')) {
            var result = url.substring(n + 1);
            console.log(result);
        
             component.set('v.VolunteerId',result);
             console.log(component.get('v.VolunteerId'));
        }
        
        //component.set("v.usercontact",$User.contactid);
        //alert('Contact '+$User.contactid);
        //alert('User '+$User.id);
       /* alert("record Id" + component.get("v.recordId"));
        alert("Name"+component.get("v.simpleRecord.Name"));
        alert("Engagement_name__c"+component.get("v.simpleRecord.Engagement_name__c"));
        alert("Engagement_name__r.Business_Objective__c"+component.get("v.simpleRecord.Engagement_name__r.Business_Objective__c"));
        */
    },
    hideSpinner : function (component, event, helper) {
        var spinner = component.find('spinner');
        var evt = spinner.get("e.toggle");
        evt.setParams({ isVisible : false });
        evt.fire();    
    },
    
    
    handleClick1:  function(component, event){
        var urlEvent = $A.get("e.force:navigateToURL");
        var url_futureRole=component.get("v.recordId");
        
        urlEvent.setParams({
           // "url":"https://www.getfeedback.com/r/COEDj8FA?gf_unique="+url_role
            //"url":"https://www.getfeedback.com/r/px4hDp84?VM_Engagement_Volunteer__c="+url_Engg_vol+"&VM_Role__c="+url_Role+"&VM_Contact__c="+url_Contact+"&VM_Engagement__c="+url_Engg+"&gf_unique=unique" 
            //"url":"https://www.getfeedback.com/r/sILMX6Yl?VM_Engagement_Volunteer__c="+url_Engg_vol+"&VM_Role__c="+url_Role+"&VM_Contact__c="+url_Contact+"&VM_Engagement__c="+url_Engg
			"url":"/vm-updateprofile"           
        });
        urlEvent.fire();       
       
         
    },
     handleClick:  function(component, event){
        var urlEvent = $A.get("e.force:navigateToURL");
        var url_futureRole=component.get("v.recordId");
        
        urlEvent.setParams({
           // "url":"https://www.getfeedback.com/r/COEDj8FA?gf_unique="+url_role
            //"url":"https://www.getfeedback.com/r/px4hDp84?VM_Engagement_Volunteer__c="+url_Engg_vol+"&VM_Role__c="+url_Role+"&VM_Contact__c="+url_Contact+"&VM_Engagement__c="+url_Engg+"&gf_unique=unique" 
            //"url":"https://www.getfeedback.com/r/sILMX6Yl?VM_Engagement_Volunteer__c="+url_Engg_vol+"&VM_Role__c="+url_Role+"&VM_Contact__c="+url_Contact+"&VM_Engagement__c="+url_Engg
			"url":"/conflictfilter?futureRole="+url_futureRole          
        });
        urlEvent.fire();   
    
     },
    handleLightningClick:  function(component, event){
        var urlEvent = $A.get("e.force:navigateToURL");
        var url_futureRole=component.get("v.recordId");
        
        urlEvent.setParams({
           // "url":"https://www.getfeedback.com/r/COEDj8FA?gf_unique="+url_role
            //"url":"https://www.getfeedback.com/r/px4hDp84?VM_Engagement_Volunteer__c="+url_Engg_vol+"&VM_Role__c="+url_Role+"&VM_Contact__c="+url_Contact+"&VM_Engagement__c="+url_Engg+"&gf_unique=unique" 
            //"url":"https://www.getfeedback.com/r/sILMX6Yl?VM_Engagement_Volunteer__c="+url_Engg_vol+"&VM_Role__c="+url_Role+"&VM_Contact__c="+url_Contact+"&VM_Engagement__c="+url_Engg
			"url":"/conflictfilter?futureRole="+url_futureRole          
        });
        urlEvent.fire();   
    
     },
    AcceptClick :  function(component, event){
        component.set('v.openModal',true);
        var action = component.get("c.getacknowledgeform");
        
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set('v.AcknowledgementFormID',response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    closeModal :  function(component, event){
        component.set('v.openModal',false);
    },
    Agree :  function(component, event){
        var checkValidation = true;
        var allCheckBox = component.find("checkbox");
        
        if(!allCheckBox.get('v.value')){
            checkValidation = false;
            return;
        } 
        
        if(checkValidation) {
            var action = component.get("c.StatusUpdate");
            action.setParams({ isAcceptOrReject : "Accept",
                               RecordId : component.get('v.VolunteerId')});
    
            action.setCallback(this, function(response){
                var state = response.getState();
                if (state === "SUCCESS") {
                    //component.set('v.showApply',false);
                    component.set('v.openModal',false);
                    component.set('v.OpenSucccessModal',true);
                }
            });
            $A.enqueueAction(action);
        } else {
            component.set("v.ErrorMessage","Please Agree the conditions");
        }
        
        
    },
    RejectClick :  function(component, event){
        var action = component.get("c.StatusUpdate");
        action.setParams({ isAcceptOrReject : "Reject",
                           RecordId : component.get('v.VolunteerId')});

        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set('v.OpenRejectModal',true);
            }
        });
        $A.enqueueAction(action); 
    }
})
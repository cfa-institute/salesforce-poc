({
    MAX_FILE_SIZE: 3000000,
    CHUNK_SIZE: 3000000,
    
    uploadHelper: function(component, event) {
        try{
            component.set("v.showLoadingSpinner", true);
            var fileInput = component.find("fileId").get("v.files");
            var file = fileInput[0];
            var self = this;
            if (file.size > self.MAX_FILE_SIZE) {
                component.set("v.showLoadingSpinner", false);
                this.showToast(component,'Error!','Alert : File size cannot exceed ' + self.MAX_FILE_SIZE + ' bytes.\n' + ' Selected file size: ' + file.size,'error');
                //component.set("v.fileName", 'Alert : File size cannot exceed ' + self.MAX_FILE_SIZE + ' bytes.\n' + ' Selected file size: ' + file.size);
                return;
            }
            var objFileReader = new FileReader(); 
            objFileReader.onload = $A.getCallback(function() {
                var fileContents = objFileReader.result;
                var base64 = 'base64,';
                var dataStart = fileContents.indexOf(base64) + base64.length;
                fileContents = fileContents.substring(dataStart);
                self.uploadInChunk(component, file, fileContents);
            });
            objFileReader.readAsDataURL(file);
        }
        catch(e){
            console.log('Error ---->>'+e);
        }        
    },
    uploadInChunk: function(component, file, fileContents) {
        try{
            var action = component.get("c.saveTheFile");
            action.setParams({
                parentId: component.get("v.applicationId"),
                fileName: file.name,
                base64Data: encodeURIComponent(fileContents),
                contentType: file.type
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    //alert('File is uploaded successfully');
                    this.showToast(component,'Success!','File is uploaded successfully.','success');
                    component.set("v.showLoadingSpinner", false);
                    component.set("v.isDisabled", true);
                } else if (state === "INCOMPLETE") {
                    console.log("From server: " + response.getReturnValue());
                } else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
            });
            $A.enqueueAction(action);   
        }
        catch(e){
            console.log('Error ---->>'+e);
        }
    },
    
    showToast : function(component, title, message, type) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "message": message,
            "type":type
        });
        toastEvent.fire();
    },
})
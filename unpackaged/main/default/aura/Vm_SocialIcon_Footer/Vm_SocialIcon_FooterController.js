({
	gotofacebook : function(component, event, helper) {
		var urlEvent = $A.get("e.force:navigateToURL");
         urlEvent.setParams({
  			"url":"http://www.facebook.com/CFAInstitute"
        });
        urlEvent.fire();
	},gotolinkedin : function(component, event, helper) {
		var urlEvent = $A.get("e.force:navigateToURL");
         urlEvent.setParams({
  			"url":"http://www.linkedin.com/company/cfainstitute"
        });
        urlEvent.fire();
	},gototwitter : function(component, event, helper) {
		var urlEvent = $A.get("e.force:navigateToURL");
         urlEvent.setParams({
  			"url":"https://twitter.com/cfainstitute"
        });
        urlEvent.fire();
	},gotoyoutube : function(component, event, helper) {
		var urlEvent = $A.get("e.force:navigateToURL");
         urlEvent.setParams({
  			"url":"https://www.youtube.com/user/cfainstitute"
        });
        urlEvent.fire();
	},gotoinsta : function(component, event, helper) {
		var urlEvent = $A.get("e.force:navigateToURL");
         urlEvent.setParams({
  			"url":"https://instagram.com/cfainstitute/"
        });
        urlEvent.fire();
	},gotowechat : function(component, event, helper) {
		var urlEvent = $A.get("e.force:navigateToURL");
         urlEvent.setParams({
  			"url":"https://www.cfainstitute.org/community/social/Pages/wechat.aspx?WPID=footer"
        });
        urlEvent.fire();
	},gotoweibo : function(component, event, helper) {
		var urlEvent = $A.get("e.force:navigateToURL");
         urlEvent.setParams({
  			"url":"https://weibo.com/cfainst"
        });
        urlEvent.fire();
	}
})
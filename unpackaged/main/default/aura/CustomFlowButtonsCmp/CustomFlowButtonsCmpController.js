({
    handleClick : function(component, event, helper) {
        var response = event.getSource().get("v.name");
        if(response === 'Update CFA Institute Profile'){
            helper.getInstanceURL(component, event, helper);
        }
        else{
            component.set("v.buttonText",response);
            var navigate = component.get("v.navigateFlow");
            navigate("NEXT");   
        }        
    },
    
    doInit : function(component,event,helper){
        var btnNames = component.get("v.buttonNames");
        var btnList = [];
        if(btnNames){
            if(btnNames.includes(',')){
                btnList = btnNames.split(',');
            }
            else{
                btnList = btnNames;
            }
        }
        if(btnList && btnList.length > 0){          
            component.set("v.buttonsList",btnList);
        }
    }
})
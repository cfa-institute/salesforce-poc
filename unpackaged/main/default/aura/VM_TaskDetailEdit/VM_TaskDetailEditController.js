({
    doInit : function(component, event, helper) {
        
        
        var url=window.location.href;
        var objIndex=url.indexOf('task');
       
        objIndex=objIndex+5;
       
        var relativeURL11=url.substring(objIndex);
       
        var k=relativeURL11.substring(0,15);
        
        
        var action = component.get('c.getTaskDetail');
       
        var tskDetail=k;
        action.setParams({"tskId" :tskDetail});
        // Set up the callback

        action.setCallback(this, function(actionResult) {
            
            component.set('v.TaskNew', actionResult.getReturnValue());
        });
        $A.enqueueAction(action);
        
    },
    UpdateTaskPopup:function(component, event, helper) {
        var action1 = component.get("c.getTaskType");
        action1.setCallback(this, function(response) {
            //var taskNew=component.get('v.TaskNew'); TDIXIT 29-01-2018 SonarQube comment. Commented this line as variable is not being used
            component.set("v.TskType",response.getReturnValue());            
            
                    var action2 = component.get("c.getTaskStatus");
                    action2.setCallback(this, function(response) {
                        component.set("v.TskStatus",response.getReturnValue()); 
                       
                    });
                    $A.enqueueAction(action2);    
            
            
        });
        $A.enqueueAction(action1);          
        component.set("v.isOpen1",true);    
    },
    CancelTaskEdit:function(component, event, helper) {        
        component.set("v.isOpen1",false);             
    },
    UpdateTask:function(component, event, helper) {        
        component.set("v.isOpen1",false);  
      
        var action = component.get("c.updatetask");
        
        action.setParams({
            taskObj : component.get("v.TaskNew") 
        });
        
        action.setCallback(this, function(a) {
            if (a.getState() === "SUCCESS") {
                
               var action = component.get('c.getTaskDetail');
               
                var tskDetail=component.get("v.TaskNew");
               
                action.setParams({"tskId" :tskDetail.Id});
                // Set up the callback
               
                action.setCallback(this, function(actionResult) {
                    
                    component.set('v.TaskNew', actionResult.getReturnValue());
                });
                $A.enqueueAction(action);  
                
              
            } else if (a.getState() === "ERROR") {
                
                $A.log("Errors", a.getError());
            }
        });
        
        $A.enqueueAction(action);
    }
    
})
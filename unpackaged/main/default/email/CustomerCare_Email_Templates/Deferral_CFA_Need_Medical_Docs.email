Dear {!Contact.FirstName}, 

We are sorry that you may be unable to attend the CFA exam because of your health problems. Enrollment and registration fees are non-refundable and non-transferable, and a deferral is considered on a case-by-case basis only for life-threatening illnesses. 

So that an equitable and timely evaluation can be made for a possible deferral to the MONTHYEAR exam, we ask that you provide an official note from your doctor that is detailed enough to fully explain the situation and support the impact to you; copies of x-rays, laboratory reports, or prescriptions for medication are not acceptable. The documentation must be in English (or a notarized translation) and on letterhead of the medical practice or hospital. It must include the doctor’s handwritten signature. 

Please reply to this email within five U.S. business days attaching your documentation. Submission of this information is not a guarantee that your deferral request will be approved. 

We look forward to receiving the requested documentation.

Regards, 

{!Case.OwnerFirstName} | Global Customer Care | CFA Institute
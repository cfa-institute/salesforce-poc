{!Contact.FirstName},

I am sorry your CFA charter has not been received and apologize for the inconvenience.

Please complete the Replacement CFA® Charter form at https://www.cfainstitute.org/utility/forms and a replacement will be ordered for you, free of charge. If you would like your phone number included on the mailing label, please notate your phone number on the form and we will do our best to include it. Be sure to indicate on the form that your charter was never received. Email the completed form to membership@cfainstitute.org.

To make sure we have your correct mailing address, we ask that you review and/or update your personal profile (https://profile.cfainstitute.org/address/details). If you are making your business the primary address, be sure to include the company name and complete address.

If you prefer your charter shipped to an alternate address, please provide the complete address on the form.

In addition, please indicate how you wish your name to be displayed on your charter on the form:

1. {!Contact.FirstName} {!Contact.LastName}
2. {!Contact.LastName} {!Contact.FirstName}

About Delivery:

Replacement charters are ordered at mid and end of each month and delivery will be within six weeks after the order is submitted to our printer. The CFA charter is shipped in a large mailing tube and may not fit in a home mailbox.

I hope this information is helpful and look forward to receiving your form.

Kindest regards,

{!Case.OwnerFirstName} | Global Customer Care | CFA Institute
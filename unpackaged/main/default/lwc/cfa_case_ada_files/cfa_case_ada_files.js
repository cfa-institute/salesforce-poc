import { LightningElement, track, api, wire } from 'lwc';
import getAll_ADA_CaseDocuments from '@salesforce/apex/CFA_CaseFileHandler.getAll_ADA_CaseDocuments';
import resourceIcon from '@salesforce/resourceUrl/CFA_IconsSet';
import STATUS_FIELD from '@salesforce/schema/Case.Status';
import { getRecord } from 'lightning/uiRecordApi';

export default class Cfa_case_ada_files extends LightningElement {


    @track FilesADA;
    @api caseRecordId;
    @track error;
    @track bShowModal = false;
    @track svgURL;
    caseId='';
    @api recordId;
    @track containerClass = 'grid-container-small';
    @track fileUploadCheck = false;

   @wire(getRecord, { recordId: '$recordId', fields: [STATUS_FIELD]})
    wiredRecord({ error, data }) {
        if (data) {
            if(data.fields.Status.value === 'Initiated'||data.fields.Status.value === 'Additional Information Required' )
            {
                this.fileUploadCheck = true;
            }
            else{
                this.fileUploadCheck = false;
            }
        }if(error){
        }
    }
    hasConnected = false;
    connectedCallback(){
        if(!this.hasConnected){
            this.hasConnected =true;
            this.svgURL = resourceIcon + '/pdf.svg';
            let url = window.location.href;
            let contains = url.includes("lightning.force.com");
            if(contains){
                this.containerClass = 'grid-container-medium';
            }else{
                this.containerClass = 'grid-container-small';
            }
            console.log('InnectedCallback()...');
            this.getADADocuments();

        }

         
    }

    getADADocuments(){

        if(this.caseRecordId !== null && this.caseRecordId !== undefined && this.caseRecordId !== ''){
            this.caseId = this.caseRecordId;
        }else{
            this.caseId = this.recordId;
        }
        
        getAll_ADA_CaseDocuments({CaseId:this.caseId})
            .then(result => {
                this.FilesADA = result;
                this.error = undefined;
            })
            .catch(error => {
                this.error = error;
                this.FilesADA = undefined;
            });
        
    }
   
 
    
    openModal() {   
        
        this.bShowModal = true;
    }
    
    closeModal() {    
        this.bShowModal = false;
    }

    handleUploadFinished(event) {
        console.log('test upload');
        this.getADADocuments();
    }

    url(event) {
        event.preventDefault();
       window.open( '/da/sfc/servlet.shepherd/document/download/' + event.currentTarget.dataset.value,"_self");
    }
    
}
/**
 * Created by ryanskelton on 2/18/20.
 */

import {LightningElement, api, track, wire} from 'lwc';
import getAnnouncements from '@salesforce/apex/x7s_CFAAnnouncementController.getAnnouncements';

export default class X7SAnnouncements extends LightningElement {

    @track description;
    @track title;
    @track type;
    @track announcements;


    @wire(getAnnouncements)
    wiredGetAnnouncements ({ error, data }) {
        if (data) {
            this.announcements = [];
            data.forEach(ann => {
                var announcement = Object.assign({}, ann);
                    switch (announcement.Type) {
                        case "Error":
                            announcement.cssClass = "slds-theme_error";
                            announcement.icon = "utility:warning";
                            break;
                        case "Warning":
                            announcement.cssClass = "slds-theme_warning";
                            announcement.icon = "utility:warning";
                            break;
                        default:
                            announcement.cssClass = "slds-theme_info";
                            announcement.icon = "utility:announcement";
                    }
                    this.announcements.push(announcement);
            });
            console.log('data', data);
        } else if (error) {
            console.log(JSON.stringify(error));
        }
    }

}
import { api, LightningElement } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent'
import runPolicy from '@salesforce/apex/RunPolicyController.runPolicy';

export default class RunNSAPolicyLWC extends LightningElement {

    @api recordId;

    disableRunBtn = false;

    onCancelClick() {
        this.fireCloseEvent();
    }

    onRunClick() {
        this.disableRunBtn = true;
        runPolicy({policyId:this.recordId})
            .then(result => {
                this.disableRunBtn = false;
                this.fireCloseEvent();
                this.showToast('success', 'Job scheduled successfully');
            })
            .catch(error => {
                this.disableRunBtn = false;
                this.fireCloseEvent();
                console.error(error);
                this.showToast('error', 'Error occurred when scheduling the job');
            });
    }

    fireCloseEvent() {
        this.dispatchEvent(new CustomEvent('close'));
    }

    showToast(variant, message) {
        const event = new ShowToastEvent({
            variant:variant,
            message: message
        });
        this.dispatchEvent(event);
    }
}
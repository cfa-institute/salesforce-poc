import { LightningElement } from 'lwc';
import getMapReviewURL from '@salesforce/apex/customerCare_Callout_AppPanel_Controller.getMapReviewURL';

export default class ProfessionalLearningLinks extends LightningElement {

    handleClick(event) {
        var buttonTitle = event.target.value;
        console.log( buttonTitle );
        getMapReviewURL({buttonTitle : buttonTitle})
        .then(result => {
            window.open(result,'_blank')
        })  
    }
    
}
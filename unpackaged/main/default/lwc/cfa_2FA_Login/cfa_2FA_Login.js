import { LightningElement, wire,track,api} from 'lwc';
import {FlowAttributeChangeEvent, FlowNavigationNextEvent, FlowNavigationFinishEvent} from 'lightning/flowSupport';
import generateCode from '@salesforce/apex/RandomCodeGenerator.generateCode';
import checkTimeDifference from '@salesforce/apex/RandomCodeGenerator.checkTimeDifference';
import strUserId from '@salesforce/user/Id';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
export default class Cfa_2FA_Login extends LightningElement {
    @track code;
    @track timeOriginated;
    @track userEnteredCode;
    @track userEmail;
    @track ContactEmail;
    @track supportEmail;

    @api isLoginSuccess;
    connectedCallback() {
        generateCode({userId : strUserId})
            .then(result => {
                this.code = result.Code;
                this.timeOriginated = result.CurrentDateTime;
                this.userEmail = result.userEmail;
                this.ContactEmail = 'mailto:'+ result.ContactEmail;
                this.supportEmail = result.ContactEmail;
                //console.log(this.code);
            })
            .catch(error => {
                this.error = error;
            });
    }

    handleClick(event) {
        let inputCode = this.template.querySelector(".inputCode"); 
        if(this.userEnteredCode == null) {
            inputCode.setCustomValidity("This field is required.");
            inputCode.reportValidity();
        } else if(this.userEnteredCode != this.code ){
            inputCode.setCustomValidity("Please enter a valid code.");
            inputCode.reportValidity();
        }else{
            checkTimeDifference({OriginalTime: this.timeOriginated})
            .then(result => {
                let inputCodeAsync = this.template.querySelector(".inputCode"); 
                if(result == true) {
                    inputCodeAsync.setCustomValidity('');
                    inputCodeAsync.reportValidity(); 
                    this.isLoginSuccess = 'true';
                    const attributeChangeEvent = new FlowNavigationFinishEvent();
                    this.dispatchEvent(attributeChangeEvent);  
                } else{
                    inputCodeAsync.setCustomValidity("This code has expired. Use the resend code link to receive a new code.");
                    inputCodeAsync.reportValidity(); 
                }
                
            })
            .catch(error => {
                this.error = error;
            });
            
        }
    }
    updatedCode(event) {
		if (event.target.value !== this.userEnteredCode) {
			const code = event.target.value.replace(/\D/g,'');
			this.userEnteredCode = code;
			this.template.querySelector(".inputCode").value = code;
		}
    }
    resendCode(event) {
        generateCode({userId : strUserId})
            .then(result => {
                this.code = result.Code;
                this.timeOriginated = result.CurrentDateTime;
                //console.log(this.code);
            })
            .catch(error => {
                this.error = error;
            });
    }
}
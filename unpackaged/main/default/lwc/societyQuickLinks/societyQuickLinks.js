/*****************************************************************
 Name: societyQuickLinks
 Copyright © 2021 ITC
 ============================================================

 ============================================================
 History
 -------
 VERSION  AUTHOR          DATE         DETAIL    Description
 1.0      Alona Zubenko   05.04.2021   Created   CSR:
 *****************************************************************/


import { LightningElement } from 'lwc';
import getMapReviewURL from '@salesforce/apex/customerCare_Callout_AppPanel_Controller.getMapReviewURL';

export default class SocietyQuickLinks extends LightningElement {

    handleClick(event) {
        let buttonTitle = event.target.value;
        console.log( buttonTitle );
        getMapReviewURL({buttonTitle : buttonTitle})
            .then(result => {
                window.open(result,'_blank')
            })
    }
}
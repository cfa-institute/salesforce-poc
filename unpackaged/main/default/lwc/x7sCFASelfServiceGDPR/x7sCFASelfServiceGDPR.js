/**
 * Created by ryanskelton on 2/23/20.
 */

import { LightningElement, track } from 'lwc';

export default class X7SCfaSelfServiceGdpr extends LightningElement {
    @track openmodel = false;
    @track openbanner = false;
    @track value = true;
    @track completeGDPR = false;

    openmodal() {
        this.openmodel = true
    }
    closeModal() {
        this.openmodel = false
    }
    saveMethod() {
        console.log("value", this.value);
        if (this.value){
            document.cookie = "cfa_gdpr_tracking=1";
            document.cookie = "cfa_ismanual_gdpr=1";
            this.completeGDPR = true;
        }
        this.closeModal();
    }
    closeBanner() {
        document.cookie = "cfa_gdpr_tracking=1";
        document.cookie = "cfa_ismanual_gdpr=1";
        this.completeGDPR = true;
    }
    handleChange(event) {
        this.value = event.target.checked;
        console.log('Current value of the input: ' + event.target.checked);
    }

    connectedCallback() {
        console.log("getcookie", this.getCookie("cfa_gdpr_tracking"));
        console.log("cookie", JSON.parse(JSON.stringify(document.cookie)));
        if(this.getCookie("cfa_gdpr_tracking" || "cfa_ismanual_gdpr" ) === "1") {
            this.completeGDPR = true;
        } else {
            document.cookie = "cfa_gdpr_tracking=0";
            document.cookie = "cfa_ismanual_gdpr=0";
        }
    }

    getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for(var i = 0; i <ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return null;
    }

}
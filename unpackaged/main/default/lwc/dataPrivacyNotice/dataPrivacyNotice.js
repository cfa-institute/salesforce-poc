import { LightningElement } from 'lwc';

// Labels
import GDPR_Message_P1 from '@salesforce/label/c.CFA_GDPR_Message';
import GDPR_Message_P2 from '@salesforce/label/c.CFA_GDPR_Message_2';
import GDPR_CopyRight from '@salesforce/label/c.CFA_GDPR_CopyRight';

// Aura Actions
import acceptPrivacyPolicyAction from '@salesforce/apex/DataPrivacyNoticeCtrl.acceptPrivacyPolicy';

// Flow Actions
import { FlowNavigationNextEvent } from 'lightning/flowSupport';

export default class DataPrivacyNotice extends LightningElement {
    error = null;
    isPolicyAccepted = false;

    labels = {
        GDPR_Message_P1,
        GDPR_Message_P2,
        GDPR_CopyRight,
        currentYear: new Date().getFullYear(),
    };

    acceptPolicy() {
        acceptPrivacyPolicyAction().then(responseResult => {
            if (responseResult) {
                this.dispatchEvent(new FlowNavigationNextEvent());
            }
        });
    }
}
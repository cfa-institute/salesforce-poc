import { LightningElement, wire, track, api } from 'lwc';
import { NavigationMixin, CurrentPageReference } from 'lightning/navigation';
import getMemebershipApplication from '@salesforce/apex/CustomerCare_ContactTabs.getMemebershipApplication';


const columns = [
    { label: 'Name', fieldName: 'Name', type: 'text' },
    { label: 'Email', fieldName: 'Email', type: 'text' },
    { label: 'Has Completed Form', fieldName: 'HasCompletedForm', type: 'text' },
    { label: 'Is Active Regular Member', fieldName: 'IsActiveRegularMember', type: 'text' }
];

export default class CustomerCare_MembershiApplication extends NavigationMixin(LightningElement) {

    @track data = [];
    @track columns = columns;
    @track params;
    @track isDataAvailable = false;
    @track error_msg = '';

    @api personId;
    @api submitDate;


    connectedCallback() {
        
     
       let personId = this.personId;
       let submitDate = this.submitDate;

        
        getMemebershipApplication({
            personId: personId,
            submitDate: submitDate,
            
        })
            .then(result => {
                try{
                    this.data = JSON.parse(result);
                    this.isDataAvailable = true;
                    let tmpData = [];
                    for (let i = 0; i < this.data.length; i++) {
                        this.data[i].HasCompletedForm = this.data[i].HasCompletedForm.toString();
                        this.data[i].IsActiveRegularMember = this.data[i].IsActiveRegularMember.toString();
                        tmpData.push(this.data[i]);
                    }
                    this.data = tmpData;
                }
                catch(e){
                    this.isDataAvailable = false;
                    this.error_msg = 'No Data Available';
                }
            })
        }
}
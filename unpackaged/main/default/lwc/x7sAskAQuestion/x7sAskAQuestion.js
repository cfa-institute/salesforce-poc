/**
 * Created by ryanskelton on 2/4/20.
 */

import {LightningElement, api} from 'lwc';

export default class X7SAskAQuestion extends LightningElement {
    @api ButtonURL = "/selfservice/s/contactsupport";
}
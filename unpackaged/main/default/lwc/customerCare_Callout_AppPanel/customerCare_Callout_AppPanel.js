import { LightningElement , track , api} from 'lwc';
import getURLs from '@salesforce/apex/customerCare_Callout_AppPanel_Controller.getURLs';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class CustomerCare_Callout_AppPanel extends LightningElement {
    
    @track title1 = 'Customer Care Integrations';
    @track showFeatures = true;
    @track urls = '';
    @track clickedButtonTitle;  
    @track error;
    @api recordId;
    @track searchText = '';

    handleClick(event) {
        this.clickedButtonTitle = event.target.title;
        getURLs({buttonTitle: this.clickedButtonTitle, caseId:this.recordId,SearchText:''})
            .then(result => {
                this.urls = result;
                console.log(this.urls);
                if(result.includes("Error")) {
                    const evt = new ShowToastEvent({
                        title: 'Error',
                        message: result.replace("Error - ", ""),
                        variant: 'error',
                    });
                    this.dispatchEvent(evt);
                } else {
                    window.open(this.urls,'_blank','width='+(window.outerWidth)/2+', height='+(window.outerHeight));
                }
                
            })
            .catch(error => {
                this.error = error;
            });
    }

}
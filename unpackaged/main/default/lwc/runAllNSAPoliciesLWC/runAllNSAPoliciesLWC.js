import { LightningElement } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent'
import runAllPolicies from '@salesforce/apex/RunAllPoliciesController.runAllPolicies';

export default class RunAllNSAPoliciesLWC extends LightningElement {

    disableRunBtn = false;
    

    onCancelClick() {
        this.fireCloseEvent();
    }

    onRunClick() {
        this.disableRunBtn = true;
        runAllPolicies()
            .then(() => {
                this.disableRunBtn = false;
                this.fireCloseEvent();
                this.showToast('success', 'Job scheduled successfully');
            })
            .catch(error => {
                this.disableRunBtn = false;
                this.fireCloseEvent();
                console.error(error);
                this.showToast('error', 'Error occurred when scheduling the job');
            });
    }

    fireCloseEvent() {
        this.dispatchEvent(new CustomEvent('close'));
    }

    showToast(variant, message) {
        const event = new ShowToastEvent({
            variant:variant,
            message: message
        });
        this.dispatchEvent(event);
    }
}
import { LightningElement, api, wire } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import getContactActiveRestrictions from "@salesforce/apex/ContactRestrictionsBannerController.getContactActiveRestrictions";

export default class ContactRestrictionsBanner extends NavigationMixin(LightningElement) {

    @api
    recordId;

    @wire(getContactActiveRestrictions, { contactId: '$recordId' })
    restrictions;

    viewRestriction(event) {
        event.preventDefault();
        if(this.otherRestrictions().length > 0) this.openRestriction(this.otherRestrictions()[0].Id);
        
    }

    viewOFACRestriction(event) {
        event.preventDefault();
        if(this.ofacRestrictions().length > 0) this.openRestriction(this.ofacRestrictions()[0].Id);
    }

    openRestriction(restrictionID) {
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                recordId: restrictionID,
                objectApiName: 'Restriction__c',
                actionName: 'view'
            },
        });
    }

    get showWarningBanner() {
        return this.otherRestrictions().length;
    }

    get showOFACBanner() {
        return this.ofacRestrictions().length;
    }

    ofacRestrictions() {
        return this.restrictions && this.restrictions.data ? this.restrictions.data.filter(restriction => restriction.Type__c == 'Legal' && (restriction.Sub_Type__c == 'OFAC - Hold' || restriction.Sub_Type__c == 'OFAC - Actionable')) : [];
    }

    otherRestrictions() {
        return this.restrictions && this.restrictions.data ? this.restrictions.data.filter(restriction => restriction.Type__c != 'Legal' && restriction.Sub_Type__c != 'OFAC - Hold' && restriction.Sub_Type__c != 'OFAC - Actionable') : [];
    }

    get showErrorBanner() {
        return this.restrictions && this.restrictions.error;
    }

    get errorMessage() {
        return this.restrictions && this.restrictions.error ? this.restrictions.error.body.message : null;
    }
}